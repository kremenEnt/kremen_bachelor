#Good code#
This code shows some interesting and good code using [recursive variadic templating](https://bitbucket.org/kremenEnt/kremen_bachelor/src/ddc3f787887fcbb67901a1f63359baab3cf52d83/GameProgrammingBachelor/GameProgrammingBachelor/DatabaseHandler.h?at=master&fileviewer=file-view-default#DatabaseHandler.h-40). The reason why it is good is that it reduces the amount of time spent for the programmer to write up code to retrieve data from a database. It recursively goes through the different parameters passed into the function and extracts it all to the different vectors sent it.

This is a [prime example](https://bitbucket.org/kremenEnt/kremen_bachelor/src/81ed48623bea2d3da3282d9dd4332022b7593b63/GameProgrammingBachelor/GameProgrammingBachelor/PhysicsTimedBlock.h?at=master&fileviewer=file-view-default#PhysicsTimedBlock.h-57) of hacking together something that despawns and then reappears after a set amount of time. 

Clean [Actor Creation](https://bitbucket.org/kremenEnt/kremen_bachelor/src/498925d97e78be65ee845441b509afa7a4e5cd5d/GameProgrammingBachelor/GameProgrammingBachelor/ActorFactory.cpp?at=master&fileviewer=file-view-default#ActorFactory.cpp-30) with self-explanatory and easy to read variable names.

Complex function which [applies terrain collision](https://bitbucket.org/kremenEnt/kremen_bachelor/src/498925d97e78be65ee845441b509afa7a4e5cd5d/GameProgrammingBachelor/GameProgrammingBachelor/PhysicsHandler.cpp?at=master&fileviewer=file-view-default#PhysicsHandler.cpp-45). We are using Marching Squares. The algorithm is not explained in detail in code.

Christer:

I believe this [custom cursor code](https://bitbucket.org/kremenEnt/kremen_bachelor/src/3fb7e0cd39e83b755a6c5d6c14556cb4e9cb7cf6/GameProgrammingBachelor/GameProgrammingBachelor/CustomCursor.cpp?at=master&fileviewer=file-view-default#CustomCursor.cpp-1) is quite good. I wrote it in the beginning of january. The concept of it isn't all too exciting, but the quality of the code is good.

Kristoffer:

I am quite happy with the [GUI system](https://bitbucket.org/kremenEnt/kremen_bachelor/src/9ac089f42eaab3d3daf963fabf5367d725304682/GameProgrammingBachelor/GameProgrammingBachelor/GuiFactory.cpp?at=master&fileviewer=file-view-default) including all the elements. There is however a few things I would change that I list in discussion.

Henning:

I'm very satisfied with how [component factories](https://bitbucket.org/kremenEnt/kremen_bachelor/src/44e0f9f8ca48763c19d431601d85be7f07708223/GameProgrammingBachelor/GameProgrammingBachelor/ComponentFactoryCreator.h?at=master&fileviewer=file-view-default#ComponentFactoryCreator.h-14) along with the new loading of actor data turned out after the refactoring. The flexibility and usability is way better than before, even if the code is very short and simple.

#Hacked/Bad code#
A custom [enemy](https://bitbucket.org/kremenEnt/kremen_bachelor/src/498925d97e78be65ee845441b509afa7a4e5cd5d/GameProgrammingBachelor/GameProgrammingBachelor/AISorceress.h?at=master&fileviewer=file-view-default#AISorceress.h-17) which moves back and forth uses no generic AI, and the movement is written specifically for this enemy.

An example of [background visualizing](https://bitbucket.org/kremenEnt/kremen_bachelor/src/498925d97e78be65ee845441b509afa7a4e5cd5d/GameProgrammingBachelor/GameProgrammingBachelor/SkyboxHandler.cpp?at=master&fileviewer=file-view-default#SkyboxHandler.cpp-65) that was hacked together to give us quick and easy support for day and night cycle but as it was hacked, the shader only support two types, which is day and night but the function can take in as many types as you want, but everything above two won’t do anything (or even crash the program).

We implemented perlin noise but the performance was too slow for our needs so we ended up scratching that implementation and get a proper noise library instead. To figure out what too slow was, we ran them up against each other and compared the time it took to finish generating a whole level. From our testing, we choose a large realistically sized level to generate and our own implementation could take up to 7 minutes to complete compared to 10 seconds using a noise library. The biggest bottleneck with our own implementation is the way rand() works in c++. We had to set the seed for every point in the level and setting seeds in the default rand function is slow.

Still a lot of [android code](https://bitbucket.org/kremenEnt/kremen_bachelor/src/3fb7e0cd39e83b755a6c5d6c14556cb4e9cb7cf6/GameProgrammingBachelor/GameProgrammingBachelor/EventHandler.cpp?at=master&fileviewer=file-view-default#EventHandler.cpp-138) left over from previous projects that was never cleaned up.

Kristoffer:
When the GUI was refactored so you could use other things than XML-Files the [GuiData](https://bitbucket.org/kremenEnt/kremen_bachelor/src/ba26ce18fdcb1b4eee5d10e83755314b136bfaf2/GameProgrammingBachelor/GameProgrammingBachelor/GuiData.cpp?at=master&fileviewer=file-view-default#GuiData.cpp-1) class was made to handle the original XML-files. This uses a lot of duplicated code because it was copy pasted out of old classes instead of rewritten from scratch. The GuiData file isn’t suppose to be a part of the engine but it ended up as this and should have been rewritten but it was not prioritized since it did what it was supposed to do and time were spent elsewhere. 

Henning:

The final [AI solution](https://bitbucket.org/kremenEnt/kremen_bachelor/src/94928422ab582c7c4f5871c3ee8381d78be15eca/GameProgrammingBachelor/GameProgrammingBachelor/AttemptX.cpp?at=master&fileviewer=file-view-default#AttemptX.cpp-78) code was somewhat rushed and unrefined. It is very loosely coupled (like most of the code), which makes it extremely easy to replace and rewrite if we needed.

#General coding guidelines:#
* The thesis is in a Visual Studio 2015 project, and we are using the C++14 standard. C++14 solutions are used whenever they are applicable.
* All conversions should be done using modern type conversions.
* Curly brackets on separate lines. Exceptions are allowed in extreme cases, where this would clutter the code more than anything. Do-While loops also have their condition at the same line as the bracket.

*For example:*
```
#!c++
If (something == true) true = false;

If (something == true)
{
true = false;
}
```

* Smart pointers are heavily preferred. Exceptions are allowed only in cases where smart pointers don’t work. This is mostly in combination with external libraries.
* Code is written to be as readable as possible, not as short as possible. Easy code should be self-explanatory
* Comment header files with Doxygen. Comment source files when necessary.
* Only header files which are part of the main engine require documentation.
* Documentation on files outside the engine(files in the BachelorProject filter in visual studio), are commented at will and Doxygen is not enforced. Commenting or refactoring is required if any of this code is too complex.
* Avoid duplicating code.
* Avoid heavy templating.
* Use suitable containers.
* Take expandability into consideration when writing code for the engine core (Engine.cpp/Actors/GUI/Graphics).
* Use C++ casts instead of C. Example: static_cast<int> instead of (int).
* Use matching variables. Example:
```
#!c++
for (std::size_t i; i < vector.size(); i++) 
```

instead of 
```
#!c++
for (int i; i < vector.size(); i++)
```


#Code review sessions:
We had planned code review sessions every four weeks, but were free to contact each other if we found bad or confusing code between sessions. The sessions took place after every fourth scrum meeting. We also reviewed some of the old code from the game programming course in early January. We have no external reviewers. All reviewers are working on the same bachelor project.

The component system was reviewed in January. It lacked expandability for future components, and custom components engineered for a specific game. It also lacked flexibility due to the fact that you could only load actors from XML-files. This was refactored during the bachelor. After refactoring you can now load Actors however you want. We have a default XML loader in the engine for ease of use. Loaders from other types have to be written separately. The new loading also support custom components, which can be written outside of the engine(like Unity).

The GUIsystem was also reviewed in January. The system had a lot of stuff set up to use inheritance but it did not. It also had the problem that you had to use XML-files to create GUI elements. This was refactored during the bachelor so the system became more flexible and dynamic. The GUI was also created and initiated in the engine.cpp file and this got changed to clutter the engine.cpp less.

It was mentioned that the conversions between different vector could become an inconvenience in the future and should probably be refactored. This was given an attempt, but quickly turned out to be way too much work for the scope of this project.

##22. Feburary##

The game states were reviewed and found lacking in terms of expandability and usability. The way to solve this was suggested to be to move the states out of engine.cpp and place them where the engine object was created. This way, you could more easily control what function were made as well as add in other modules to be run in the update that would be placed outside of the engine.cpp. This was never completed.

Engine.cpp was also discussed. We wanted to have the run function of the engine object to happen in a separate loop rather than running it inside of the engine object. That was it would be easier to add in other methods to be run that we did not want within the engine object itself. The way that actors and memory are managed got to be strange within the engine object. This was never completed.

We realized that animation was done by physics and graphics communicating heavily together in an unintuitive way. We agreed to do some research on this subject, and ended up replacing the old animation approach with an expandable actor state system with state machines.

The handling of the database statements was improved to be extremely dynamic to reduce the need for coders to write the same looking code every time they wanted to get some data out of the database. We wanted a way to do it so that you would only need to call one function to retrieve or write all the data to the database. This was implemented fairly quickly. The end result was that it turned out to be just like we wanted it to. Templating is a powerful tool when used correctly.

##21. Mars##
We knew for a while that our resource management started to become ugly and we needed a complete overhaul. The management was outdated and not very flexible, but we set this as a low priority. Some solutions were found but never implemented.

##18. April##
We were nearing the end of the project, and prioritized getting everything up and running instead of refactoring. We ended the meeting early and agreed on not doing more review sessions.

#Discussion of what we would have done differently:#
Many neat features could have been added by having 3D-physics. We had heard that implementing 3D-physics was difficult, but we have since learned that it is actually very easy.

Scripting would have been nice to have when doing console, GUI, generation of levels. Writing a script and then just being able to change the generation easily by just changing the script would have been nice to have. It would possibly make the workflow more smooth as one could do changes while the program was running. It would be extremely handy for GUI programming as you would be able to reduce the code base by a lot. It is currently all done in c++ rather than a script. This made the code base larger than necessary.

Christer:

I'm really disappointed that I could not get the installer to work after working so long on it. I simply could not figure out what was missing. I even added the C++ Visual studio 2015 redistributable which should have solved it, but it simply did not, which means that it is something completely unrelated to it that is messing up the running of the program. Sure, the installer works fine at installing everything, but the exe would not run. It would have also been fine for me to just package the files and send that, but that would not work either. It still ran on some computers, specifically on gamers' PC's, so it is something related to a dependency of some games being installed.
I noticed that when I ran the program on my laptop, it would default to using the integrated graphics card, so I would have to manually set that it should run on the dedicated graphics card. But, even with that, I still got messages in the console window that allowed me to see that it was running on the integrated graphics card.

I did some experiments during the project to see if I could store object without having to have a base class that they inherit from, turned out to be a slight waste of time. Although I learnt a lot from it, it still did not turn out to be anything that was useful. I tried capturing them in lambdas, that did not work to great, even though in my mind it should work. I can see the problems with storing objects generally, and that mostly revolves around how you solve the issue of where to actually store the memory. Although I am sure I am wrong in that respect, I really can not see where the problem in having that would be. You could just simply store them in a map, where one is the object and the other is the type of object.

Kristoffer:

I am quite happy with how the GUI ended up working after I rewrote the system in the beginning of the bachelor period. In the game programming course I had the GUI set up to use inheritance but it did not actually use it, but now it does. It is easy to add new GUI elements to the program by editing XML-files and the GUI elements does what they are supposed to. However some of the element have magic numbers and mainly the slider and container class is kind of messy right now. This is left over from the game programming course and I didn't rewrite because the elements are working as intented and GUI was not a big focus in the bachelor thesis so I left it like this for now. Also the receive message function in [GUIfactory](https://bitbucket.org/kremenEnt/kremen_bachelor/src/9ac089f42eaab3d3daf963fabf5367d725304682/GameProgrammingBachelor/GameProgrammingBachelor/GuiFactory.cpp?at=master&fileviewer=file-view-default) is hacked together. This function controls interaction with GUI elements and is suppose to be outside the engine so every game can have its own custom interactions instead of editing the engine every time you want new interactions. Rewriting this became to much of a problem because of the messaging system we have and I chose to spend my time working on more important things related to the bachelor.

Henning:

While many parts of the codebase were slightly modernised, most of the core bits of Engine.cpp remained the same. Replacing the state system in Engine.cpp with a simple state machine would help immensely, and one could add custom states outside of the main class. This would make Engine.cpp more "enginey", where it can just be included and you can make any 2D game you want. Totally unnecessary for the bachelor, but good to have.

Jonas:
The ease of use and the flexibility of the engine after all of the refactoring made me quite happy in the end. I was able to prototype new enemy types quickly with just a few lines of code and it is actually very simple to add them into the world. I am quite happy with the performance we were able to achieve using OpenGL and Box2D through optimizations and the visual effects were quite good even though not everything is actually displayed in the bachelor as some of them don't fit the theme we went for. 
