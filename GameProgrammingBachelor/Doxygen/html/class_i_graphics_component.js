var class_i_graphics_component =
[
    [ "IGraphicsComponent", "class_i_graphics_component.html#a0c56274c99fd73f07ccc79defd3a7437", null ],
    [ "~IGraphicsComponent", "class_i_graphics_component.html#a2a6cf1ba34d51f716b1ea1cedac6414b", null ],
    [ "anim", "class_i_graphics_component.html#adbbc4cf9bf0d419f141cf8ee2e8b3d93", null ],
    [ "componentData", "class_i_graphics_component.html#ad4a1dbcef8b6d3f3f71f4e0505a6bfcf", null ],
    [ "flipped", "class_i_graphics_component.html#a34141ba196730ba45581a2a13be91a62", null ],
    [ "tilePosition", "class_i_graphics_component.html#a23b3f0ae79e10a3938627f6c2b0d2965", null ],
    [ "xFrames", "class_i_graphics_component.html#ab758460ffafc2664f1825fbdc77554b0", null ],
    [ "yFrames", "class_i_graphics_component.html#a6c1504b75ff404b394401cb8e9def914", null ]
];