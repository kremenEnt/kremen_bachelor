var class_animation =
[
    [ "Animation", "class_animation.html#a83f0a16cef7117f187ad596de38dd9d6", null ],
    [ "~Animation", "class_animation.html#a401b68793d4fbf48d481c030ee4b2a16", null ],
    [ "config", "class_animation.html#a273fc65dfa8c1a564b25b98df2a086e7", null ],
    [ "getAmount", "class_animation.html#aab63e49990579ce53e920586c6fa8c4e", null ],
    [ "getFirst", "class_animation.html#a9efae4a757fe2a2228b976350161358a", null ],
    [ "getFrame", "class_animation.html#a0804ba4877dea37ac3ea21fdd43312ca", null ],
    [ "getSpeed", "class_animation.html#accafeec040e7919eda2ce173cc45ddcb", null ],
    [ "reset", "class_animation.html#a903eb4b61b8c7bc72bbedbdba4cbb501", null ],
    [ "update", "class_animation.html#a7c41f36321ddf12fb82f42f7f8d5663e", null ]
];