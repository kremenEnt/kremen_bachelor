var class_message =
[
    [ "Message", "class_message.html#aa2f90994fec67d59a0ebd6555e75c363", null ],
    [ "Message", "class_message.html#acb27ad11b748a00d6562734b9ef30744", null ],
    [ "Message", "class_message.html#ac6e6ef4047a6eb705e260032315d1324", null ],
    [ "~Message", "class_message.html#a3f7275462831f787a861271687bcad67", null ],
    [ "getSender", "class_message.html#ab5c16357ad79654713c1b100b02ed186", null ],
    [ "getSenderType", "class_message.html#adbb7a18a3fe9d073dee7fd277daac7d1", null ],
    [ "getVariable", "class_message.html#ab67937bb6527278d5428d25bef78f4ce", null ],
    [ "getVariableCount", "class_message.html#a69825d4bec9c01866f49d5aa5b820e1d", null ],
    [ "operator=", "class_message.html#a9d2757c25e96c0f88cf9ba9f352c7c35", null ]
];