var class_actor =
[
    [ "Actor", "class_actor.html#a950e55d5f558c3594038eaf672163120", null ],
    [ "Actor", "class_actor.html#a1ec149f5a1807d95f21d0c1e54415071", null ],
    [ "~Actor", "class_actor.html#ad807fe8f85e72ab263a0c05e3231cb39", null ],
    [ "addComponent", "class_actor.html#a9391098a8e26db08ebed1ca4cfd500be", null ],
    [ "getActorId", "class_actor.html#a7d93158138e5c3acdc16d53888565296", null ],
    [ "getCategory", "class_actor.html#a7546a4e2b1811219d688f1e7e9c6825c", null ],
    [ "getComponent", "class_actor.html#abca8377ab4f4ae63bca17cda655c520d", null ],
    [ "getPosition", "class_actor.html#a5113b814194b5ba37d73498dd4e90fe2", null ],
    [ "isActive", "class_actor.html#aef1a8e86e9355d9639e7222a4693dcc4", null ],
    [ "movePosition", "class_actor.html#af84775371d96df89581901b23fcad829", null ],
    [ "onDeath", "class_actor.html#a306fb5a5c11293ca304ab3cb1216c1ca", null ],
    [ "onHit", "class_actor.html#af4870e472108819cf6c3e772567839a9", null ],
    [ "onSpawn", "class_actor.html#ac56227583b251dd45dabfe539cdf4194", null ],
    [ "operator=", "class_actor.html#a9728b32060325e2b015ee844e8795da2", null ],
    [ "postInit", "class_actor.html#a78cc4300305ed632d155e7613d4d4e89", null ],
    [ "setActive", "class_actor.html#ae32c15556257c9501cec256eed7de821", null ],
    [ "setActorId", "class_actor.html#aecb7acd9de4cc99f0d5c2ef79cacd84a", null ],
    [ "setCategory", "class_actor.html#a0fae6f2210a7379d2005c1352f609eca", null ],
    [ "setPhysicsActive", "class_actor.html#a5b778bd23d1c73de0f52653aea71a9ce", null ],
    [ "setPosition", "class_actor.html#a348dbb232f60b16c86e1e098b17b40e4", null ],
    [ "update", "class_actor.html#a6a51fc033c1dc96bd98ef406e6a7e338", null ]
];