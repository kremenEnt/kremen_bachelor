var common_8h =
[
    [ "Vector2i", "structkrem_1_1_vector2i.html", "structkrem_1_1_vector2i" ],
    [ "Vector2f", "structkrem_1_1_vector2f.html", "structkrem_1_1_vector2f" ],
    [ "Vector3f", "structkrem_1_1_vector3f.html", "structkrem_1_1_vector3f" ],
    [ "Vector3i", "structkrem_1_1_vector3i.html", "structkrem_1_1_vector3i" ],
    [ "CollisionData", "struct_collision_data.html", "struct_collision_data" ],
    [ "actorID", "structactor_i_d.html", "structactor_i_d" ],
    [ "InputValues", "struct_input_values.html", "struct_input_values" ],
    [ "NetworkActorData", "struct_network_actor_data.html", "struct_network_actor_data" ],
    [ "PhysicsDefinition", "struct_physics_definition.html", "struct_physics_definition" ],
    [ "ActorCollisionDataType", "common_8h.html#ab9938439801467ef778e2a83d463fcb6", [
      [ "DATA_SNOWBALL", "common_8h.html#ab9938439801467ef778e2a83d463fcb6ae97bd9b9e38dda261c5020f02362c74a", null ],
      [ "DATA_RABBIT", "common_8h.html#ab9938439801467ef778e2a83d463fcb6a73e74d3bc8f3ccbaf419e4e935760837", null ],
      [ "DATA_MEGABLASTER", "common_8h.html#ab9938439801467ef778e2a83d463fcb6a3a2ef6557fb1d6328422291c120c11cd", null ],
      [ "NO_OF_COLLISION_DATA_TYPES", "common_8h.html#ab9938439801467ef778e2a83d463fcb6a862424df002fbaae7cd65b5d9f895a20", null ]
    ] ],
    [ "actorStates", "common_8h.html#ad9ccb60af78a039cac04c53b7a53d61e", [
      [ "AS_IDLE", "common_8h.html#ad9ccb60af78a039cac04c53b7a53d61ea786d9daf27a59acc4901e79813a7ad72", null ],
      [ "AS_WALKING", "common_8h.html#ad9ccb60af78a039cac04c53b7a53d61ea6de42af0fc62bd8b5145dfae8ff47f82", null ],
      [ "AS_RISING", "common_8h.html#ad9ccb60af78a039cac04c53b7a53d61ea1c4a39efb9e343c14231ddba797ddf76", null ],
      [ "AS_DROPPING", "common_8h.html#ad9ccb60af78a039cac04c53b7a53d61eaeb6037ab96cdc854a525ef279deaa037", null ],
      [ "AS_DEAD", "common_8h.html#ad9ccb60af78a039cac04c53b7a53d61eae42ebe91d2ce7c4ca514fedb12b89c5d", null ],
      [ "NO_OF_ACTORSTATES", "common_8h.html#ad9ccb60af78a039cac04c53b7a53d61eab24c9f864ea2678a29ca1df3cec9e59e", null ]
    ] ],
    [ "animations", "common_8h.html#adbf202963403ab9907e7ee1d5ec69ce2", [
      [ "IDLE", "common_8h.html#adbf202963403ab9907e7ee1d5ec69ce2afd6a0e4343048b10646dd2976cc5ad18", null ],
      [ "WALKING", "common_8h.html#adbf202963403ab9907e7ee1d5ec69ce2a7f92908f6f669259d971b608823d2ebb", null ],
      [ "DEATH", "common_8h.html#adbf202963403ab9907e7ee1d5ec69ce2aa65c1c1f61d319e66a6269441cdcbd71", null ],
      [ "JUMPING", "common_8h.html#adbf202963403ab9907e7ee1d5ec69ce2ab8cd32180a1d5897df8369b127256ad1", null ],
      [ "FALLING", "common_8h.html#adbf202963403ab9907e7ee1d5ec69ce2ad24712a6a30c1d431b927d1ba2f84b66", null ],
      [ "NO_OF_ANIMATIONS", "common_8h.html#adbf202963403ab9907e7ee1d5ec69ce2aeac1bc780ff4e9d430a90f41b1413627", null ]
    ] ],
    [ "collisionCategories", "common_8h.html#a6a369351edc5d583239dd4cb19810a70", [
      [ "CATEGORY_CLONE", "common_8h.html#a6a369351edc5d583239dd4cb19810a70af5cb84e9bc5737458cc35f9f2dfc2092", null ],
      [ "CATEGORY_GOAL", "common_8h.html#a6a369351edc5d583239dd4cb19810a70a1cee708a155b469266fd1219e4766797", null ],
      [ "CATEGORY_PICKUP", "common_8h.html#a6a369351edc5d583239dd4cb19810a70a09098ecf9356a83ec9ce405166458851", null ],
      [ "CATEGORY_SCENERY", "common_8h.html#a6a369351edc5d583239dd4cb19810a70a4db2560e659946c520410db2826ddc68", null ],
      [ "CATEGORY_OBJECT", "common_8h.html#a6a369351edc5d583239dd4cb19810a70aa6816a496224db20e20b3efcb61329ce", null ],
      [ "CATEGORY_SPECIAL", "common_8h.html#a6a369351edc5d583239dd4cb19810a70ad770ad3d139d261e118d4b6d30d11126", null ]
    ] ],
    [ "commandMessage", "common_8h.html#aa5fafc5f761df448f2eb9107fb99b3f0", [
      [ "COMMAND_LEFT", "common_8h.html#aa5fafc5f761df448f2eb9107fb99b3f0afb7cd346f2dbce73292e65a116f9af61", null ],
      [ "COMMAND_RIGHT", "common_8h.html#aa5fafc5f761df448f2eb9107fb99b3f0a9ac1800113e719d956ce58be8e506a50", null ],
      [ "COMMAND_UP", "common_8h.html#aa5fafc5f761df448f2eb9107fb99b3f0abbef7f28a01d5c2124c26424a64a8e4e", null ],
      [ "COMMAND_DOWN", "common_8h.html#aa5fafc5f761df448f2eb9107fb99b3f0adf39e0f29890b0b1e897e84481a9c55b", null ],
      [ "COMMAND_LEFTUP", "common_8h.html#aa5fafc5f761df448f2eb9107fb99b3f0ab0823600771b7f1f60a066d4f4b7104f", null ],
      [ "COMMAND_LEFTDOWN", "common_8h.html#aa5fafc5f761df448f2eb9107fb99b3f0aba82754b248e2ca8d9681975ef35808b", null ],
      [ "COMMAND_RIGHTUP", "common_8h.html#aa5fafc5f761df448f2eb9107fb99b3f0a0a38d34b0e61244ff13169bff53b3e18", null ],
      [ "COMMAND_RIGHTDOWN", "common_8h.html#aa5fafc5f761df448f2eb9107fb99b3f0a1a1d2ad27e910f257b24d0599e38a1b4", null ],
      [ "COMMAND_JUMP", "common_8h.html#aa5fafc5f761df448f2eb9107fb99b3f0a526b261c423561e483aaec249c5ab01f", null ],
      [ "COMMAND_THROW", "common_8h.html#aa5fafc5f761df448f2eb9107fb99b3f0a90ba1b427b818082a2e39b84ee87fd65", null ],
      [ "COMMAND_UPDATEPOSITION", "common_8h.html#aa5fafc5f761df448f2eb9107fb99b3f0aabfc6e7f35e6345db8c7d83c500d5ac9", null ],
      [ "COMMAND_UPDATEANGLE", "common_8h.html#aa5fafc5f761df448f2eb9107fb99b3f0a5b8b59b524517f2f0d1344673f392838", null ],
      [ "COMMAND_MOVE", "common_8h.html#aa5fafc5f761df448f2eb9107fb99b3f0a53d2fd4f2f1d0d0cb92fcbf4b5df4515", null ]
    ] ],
    [ "eventMessage", "common_8h.html#a538490584b55df1b9bd1d36ae1be8530", [
      [ "EVENT_LEFT", "common_8h.html#a538490584b55df1b9bd1d36ae1be8530a930ba21542af752335876dfece518a77", null ],
      [ "EVENT_RIGHT", "common_8h.html#a538490584b55df1b9bd1d36ae1be8530a65e1fa713c729b52d879fe7f9a739374", null ],
      [ "EVENT_UP", "common_8h.html#a538490584b55df1b9bd1d36ae1be8530aaccb3f99435b08da87a04591698f676b", null ],
      [ "EVENT_DOWN", "common_8h.html#a538490584b55df1b9bd1d36ae1be8530a8784f4938a237c5ced4ea0dfc1e89992", null ],
      [ "EVENT_LEFTUP", "common_8h.html#a538490584b55df1b9bd1d36ae1be8530a5c452e618c9546b7497eb8f872e3c664", null ],
      [ "EVENT_LEFTDOWN", "common_8h.html#a538490584b55df1b9bd1d36ae1be8530a9ce9d6a404d9b94f912fecc8ecfed4a8", null ],
      [ "EVENT_RIGHTUP", "common_8h.html#a538490584b55df1b9bd1d36ae1be8530ab27e8027fdc5fe8a203da6f083fd2b3b", null ],
      [ "EVENT_RIGHTDOWN", "common_8h.html#a538490584b55df1b9bd1d36ae1be8530a5da2ae2db4cef46296df5355f90fd048", null ],
      [ "EVENT_JUMP", "common_8h.html#a538490584b55df1b9bd1d36ae1be8530a89b5c9ac6d6ac054281c5063911d6e3a", null ]
    ] ],
    [ "gameState", "common_8h.html#aaf29bbe309504beb4cfec2481eacee62", [
      [ "STATE_MAINMENU", "common_8h.html#aaf29bbe309504beb4cfec2481eacee62ac88b835a0d023e7034d80f21170205db", null ],
      [ "STATE_RUNNING", "common_8h.html#aaf29bbe309504beb4cfec2481eacee62addfe11c6d06c4e27bd6efc18cc4862a6", null ],
      [ "STATE_LEVELSELECT", "common_8h.html#aaf29bbe309504beb4cfec2481eacee62af771530b4b3d8d99360a592f8723a91c", null ],
      [ "STATE_GAMEOVER", "common_8h.html#aaf29bbe309504beb4cfec2481eacee62a499befd644d29b7940f5d95ed90a83c3", null ],
      [ "STATE_JOINING", "common_8h.html#aaf29bbe309504beb4cfec2481eacee62a11e947a3ce9b710f98babd58fc803e36", null ],
      [ "STATE_JOINED", "common_8h.html#aaf29bbe309504beb4cfec2481eacee62a62eb112f8be7eefa3ed10e69c4aa5076", null ],
      [ "STATE_HOSTING", "common_8h.html#aaf29bbe309504beb4cfec2481eacee62a4baa8e5c44378cc80df17a59d9db39e1", null ],
      [ "STATE_SETTINGS", "common_8h.html#aaf29bbe309504beb4cfec2481eacee62a39762c02713f361cc4a2c3aee38bdd0f", null ],
      [ "STATE_QUITTING", "common_8h.html#aaf29bbe309504beb4cfec2481eacee62a63b43e6f050102d8c3e15e650f23e407", null ],
      [ "NO_OF_GAMESTATES", "common_8h.html#aaf29bbe309504beb4cfec2481eacee62ad1f7513ebac9c7a2ac25252d261682b0", null ]
    ] ],
    [ "networkSynchronizationLevel", "common_8h.html#a68f2e54b5c91e2082f32e077e9ba0c06", [
      [ "NETLEVEL_NOTHING", "common_8h.html#a68f2e54b5c91e2082f32e077e9ba0c06a280394362f153d33765a7d96f47d380d", null ],
      [ "NETLEVEL_POSITION", "common_8h.html#a68f2e54b5c91e2082f32e077e9ba0c06a6070897351e278dd11cbb2b6039b0d9b", null ],
      [ "NETLEVEL_ANIMATION", "common_8h.html#a68f2e54b5c91e2082f32e077e9ba0c06a65c345191b9b0c08b1c033dcd4d3d6b9", null ],
      [ "NETLEVEL_FULL", "common_8h.html#a68f2e54b5c91e2082f32e077e9ba0c06af40e7c4c607568fa1fd8e581d6717b85", null ],
      [ "NETLEVEL_COMPONENT_SPESIFIC", "common_8h.html#a68f2e54b5c91e2082f32e077e9ba0c06ad2fe192eb7d9f878e90481a6fea182e8", null ]
    ] ],
    [ "skyboxTextures", "common_8h.html#aa2875e26bb4eac207484b6613d2add32", [
      [ "SKYBOX_DAY", "common_8h.html#aa2875e26bb4eac207484b6613d2add32acb79a3adbe9f197c3ff1f8a501324745", null ],
      [ "SKYBOX_NIGHT", "common_8h.html#aa2875e26bb4eac207484b6613d2add32a6c3b85d53b62948bdf8fa238eaa28e3b", null ],
      [ "NO_OF_SKYBOX_TEXTURES", "common_8h.html#aa2875e26bb4eac207484b6613d2add32a8ac3a703a1623a5b9398a489db9aab53", null ]
    ] ],
    [ "uniforms", "common_8h.html#aa3a1b7477e4f1c4c5533b94bb1246b0d", [
      [ "U_TRANSFORM", "common_8h.html#aa3a1b7477e4f1c4c5533b94bb1246b0da9f0324a297f4eeb074c6917abdde253a", null ],
      [ "U_VIEW", "common_8h.html#aa3a1b7477e4f1c4c5533b94bb1246b0da4d8c759f781f1429af7df6affd6d3e3a", null ],
      [ "U_SIZE", "common_8h.html#aa3a1b7477e4f1c4c5533b94bb1246b0da6144e671cc4ca5a229bd8815adcb6060", null ],
      [ "U_SPRITE_NO", "common_8h.html#aa3a1b7477e4f1c4c5533b94bb1246b0dab5f342e1f28f4c660ca9d1dbb9d43a3a", null ],
      [ "U_LIGHT_POS", "common_8h.html#aa3a1b7477e4f1c4c5533b94bb1246b0dae826cf036594102cc7cbfe96c98c724c", null ],
      [ "U_TEXTURE0", "common_8h.html#aa3a1b7477e4f1c4c5533b94bb1246b0da78b89825b5a376246c884b11ad960a1c", null ],
      [ "U_TEXTURE1", "common_8h.html#aa3a1b7477e4f1c4c5533b94bb1246b0da46ab12eb06cd391d094bdd07011ff446", null ],
      [ "U_TEXTURE2", "common_8h.html#aa3a1b7477e4f1c4c5533b94bb1246b0dae7d9f54f0e0b9dc4d0b7b7aae1f19377", null ],
      [ "U_SCALE", "common_8h.html#aa3a1b7477e4f1c4c5533b94bb1246b0da0251b780d3bb9f1cbb0f57af156a2830", null ],
      [ "U_COLOR", "common_8h.html#aa3a1b7477e4f1c4c5533b94bb1246b0da3fd95c80beb410b64938b09bef00b41c", null ],
      [ "NUM_UNIFORMS", "common_8h.html#aa3a1b7477e4f1c4c5533b94bb1246b0dacb73fed4f63b1d4e8f4cc30c0773398e", null ]
    ] ],
    [ "vertexBuffers", "common_8h.html#a3c31304a5d3e806278463a5bc96f9d7b", [
      [ "POSITION_VB", "common_8h.html#a3c31304a5d3e806278463a5bc96f9d7ba5687f270ba0bbd4c9984df8f039d18e0", null ],
      [ "TEXCOORD_VB", "common_8h.html#a3c31304a5d3e806278463a5bc96f9d7ba7ba23128a0d0ddd719dfe6bd90a466d5", null ],
      [ "NORMAL_VB", "common_8h.html#a3c31304a5d3e806278463a5bc96f9d7baab4bd09c7c0ffc3a08b6fe2d65eaf729", null ],
      [ "INDEX_VB", "common_8h.html#a3c31304a5d3e806278463a5bc96f9d7ba2ba741cec4291c9f932a5091dd6c8227", null ],
      [ "NUM_BUFFERS", "common_8h.html#a3c31304a5d3e806278463a5bc96f9d7bab4880b8cfa821cc64b26727c7cecaf29", null ]
    ] ],
    [ "PI", "common_8h.html#a5d52d73621c3ddb8dee265925596513d", null ]
];