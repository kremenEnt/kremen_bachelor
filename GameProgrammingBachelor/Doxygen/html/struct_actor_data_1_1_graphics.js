var struct_actor_data_1_1_graphics =
[
    [ "Graphics", "struct_actor_data_1_1_graphics.html#a6f9ca47fc9c8022ca695c28658d993f0", null ],
    [ "animated", "struct_actor_data_1_1_graphics.html#afeb0d04862d5df16aba21437f044731d", null ],
    [ "meshPath", "struct_actor_data_1_1_graphics.html#a4c3b2d1faf0e3420e905c7fda47afa66", null ],
    [ "noSpriteFrames", "struct_actor_data_1_1_graphics.html#afd5c793916e089990f1faf8d024da9c5", null ],
    [ "shaderPath", "struct_actor_data_1_1_graphics.html#ac65af85bc761ce8253207d262ac0493d", null ],
    [ "size", "struct_actor_data_1_1_graphics.html#a4740a2ab68cadd5effab319f21a24a97", null ],
    [ "texturePath", "struct_actor_data_1_1_graphics.html#ad69caa557ce2dac93c4f21f9bb94297d", null ]
];