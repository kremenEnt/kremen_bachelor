var struct_gui_data_1_1_container =
[
    [ "activatable", "struct_gui_data_1_1_container.html#a3b78cdad70d70d2df6aeaeb12c6434af", null ],
    [ "atlasSizeX", "struct_gui_data_1_1_container.html#a63abe5b5538784244da5d113bd5384d1", null ],
    [ "atlasSizeY", "struct_gui_data_1_1_container.html#ab280d77b83b768667d4f7a6e0a7b9f1b", null ],
    [ "borderBottom", "struct_gui_data_1_1_container.html#a86d25d2c4b1c7d8fd91af30b5f47c67c", null ],
    [ "borderLeft", "struct_gui_data_1_1_container.html#a30a396bca726c9ab7ccf8a0a3a88a7f6", null ],
    [ "borderRight", "struct_gui_data_1_1_container.html#a2a05f709cd963035b3386ee3adbf9d20", null ],
    [ "borderTop", "struct_gui_data_1_1_container.html#a80c5696d1e3ba3dc557f49ad9f4f36ab", null ],
    [ "clickable", "struct_gui_data_1_1_container.html#a40cf21d8d4bd95e25b42c637ac51dbbe", null ],
    [ "destroyable", "struct_gui_data_1_1_container.html#ae5f0d47d4b7d481d1426d163d1cbc3c0", null ],
    [ "distanceBetweenBoxes", "struct_gui_data_1_1_container.html#a78a47b50af08cd9f547331188642955f", null ],
    [ "heightBoxes", "struct_gui_data_1_1_container.html#abe4bcfa1cecd65515d0386bf61a04d32", null ],
    [ "movable", "struct_gui_data_1_1_container.html#a53bcacbd5a6391b452ecb94167dc05b7", null ],
    [ "numberOfBoxesX", "struct_gui_data_1_1_container.html#a2937e8ce79d0051a7f019dce9008585f", null ],
    [ "numberOfBoxesY", "struct_gui_data_1_1_container.html#a5db80c9b20bc48428a5d3c94fa885894", null ],
    [ "presetItems", "struct_gui_data_1_1_container.html#a24ed6c2c882097f5d58c0c7414b13b1c", null ],
    [ "shaderBoxes", "struct_gui_data_1_1_container.html#ab0f816083da861c01925e15023aae8bc", null ],
    [ "textureBoxes", "struct_gui_data_1_1_container.html#ac74f2ddfce072b1b75c836e5faff1f85", null ],
    [ "visible", "struct_gui_data_1_1_container.html#af0a05ba696c744efb8c426768f96e22b", null ],
    [ "widthBoxes", "struct_gui_data_1_1_container.html#a854d37b1102428d90172559f96f3e127", null ]
];