var class_skybox_handler =
[
    [ "SkyboxHandler", "class_skybox_handler.html#ae736b8479208ba7edc98badd8d0ec331", null ],
    [ "~SkyboxHandler", "class_skybox_handler.html#a60eb95d8b4c6cb50e16e08921185cf05", null ],
    [ "draw", "class_skybox_handler.html#a4b53b18a2a7429ee07efad9dd42ff1fc", null ],
    [ "getRotation", "class_skybox_handler.html#a58abc3c5498b7fbebe374faa1a8f1dcb", null ],
    [ "init", "class_skybox_handler.html#a8791948c5091093778e12f66f1e19f6a", null ],
    [ "loadSTBTexture", "class_skybox_handler.html#aadb100081dfb532e83c44c892c996395", null ],
    [ "loadTexture", "class_skybox_handler.html#ae0ba7c27298d9b30c445feab786333a9", null ],
    [ "update", "class_skybox_handler.html#acfbaba2f7f5fb5f7e9cf4b3de57d679e", null ]
];