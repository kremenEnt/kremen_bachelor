var hierarchy =
[
    [ "ActorComponent", "class_actor_component.html", [
      [ "AIComponent", "class_a_i_component.html", [
        [ "AISorceress", "class_a_i_sorceress.html", null ],
        [ "AITotem", "class_a_i_totem.html", null ],
        [ "AIWizard", "class_a_i_wizard.html", null ]
      ] ],
      [ "FlashOnHitComponent", "class_flash_on_hit_component.html", null ],
      [ "IAnimationComponent", "class_i_animation_component.html", [
        [ "AnimationComponent", "class_animation_component.html", null ]
      ] ],
      [ "IAudioComponent", "class_i_audio_component.html", [
        [ "AudioComponent", "class_audio_component.html", null ]
      ] ],
      [ "ICombatComponent", "class_i_combat_component.html", [
        [ "CombatComponent", "class_combat_component.html", null ]
      ] ],
      [ "IGraphicsComponent", "class_i_graphics_component.html", [
        [ "GraphicsComponent", "class_graphics_component.html", null ]
      ] ],
      [ "InputComponent", "class_input_component.html", [
        [ "InputClone", "class_input_clone.html", null ]
      ] ],
      [ "IParticleComponent", "class_i_particle_component.html", [
        [ "ParticleComponent", "class_particle_component.html", null ]
      ] ],
      [ "IPhysicsComponent", "class_i_physics_component.html", [
        [ "PhysicsComponent", "class_physics_component.html", [
          [ "PhysicsClone", "class_physics_clone.html", null ],
          [ "PhysicsEnemy", "class_physics_enemy.html", null ],
          [ "PhysicsGoal", "class_physics_goal.html", null ],
          [ "PhysicsMelee", "class_physics_melee.html", null ],
          [ "PhysicsMovingPlatform", "class_physics_moving_platform.html", null ],
          [ "PhysicsPickup", "class_physics_pickup.html", null ],
          [ "PhysicsPlayer", "class_physics_player.html", null ],
          [ "PhysicsProjectile", "class_physics_projectile.html", null ],
          [ "PhysicsProjectileEnemy", "class_physics_projectile_enemy.html", null ],
          [ "PhysicsRetractable", "class_physics_retractable.html", null ],
          [ "PhysicsRock", "class_physics_rock.html", null ],
          [ "PhysicsSpinningBall", "class_physics_spinning_ball.html", null ],
          [ "PhysicsSpring", "class_physics_spring.html", null ],
          [ "PhysicsTimedBlock", "class_physics_timed_block.html", null ],
          [ "PhysicsTotemMissile", "class_physics_totem_missile.html", null ]
        ] ]
      ] ]
    ] ],
    [ "ActorData", "class_actor_data.html", [
      [ "CustomActorData", "class_custom_actor_data.html", null ],
      [ "PlatformData", "class_platform_data.html", null ]
    ] ],
    [ "ActorFactory", "class_actor_factory.html", null ],
    [ "actorID", "structactor_i_d.html", null ],
    [ "Animation", "class_animation.html", null ],
    [ "ActorData::Animation::AnimationSequence", "struct_actor_data_1_1_animation_1_1_animation_sequence.html", null ],
    [ "ASDefault", "class_a_s_default.html", [
      [ "ASDead", "class_a_s_dead.html", null ],
      [ "ASDropping", "class_a_s_dropping.html", null ],
      [ "ASIdle", "class_a_s_idle.html", null ],
      [ "ASRising", "class_a_s_rising.html", null ],
      [ "ASWalking", "class_a_s_walking.html", null ]
    ] ],
    [ "Astar", "class_astar.html", null ],
    [ "b2ContactListener", null, [
      [ "ContactListener", "class_contact_listener.html", null ]
    ] ],
    [ "Camera", "class_camera.html", null ],
    [ "chunk", "structchunk.html", null ],
    [ "Chunk", "struct_chunk.html", null ],
    [ "CollisionData", "struct_collision_data.html", null ],
    [ "ComponentCreator", "class_component_creator.html", [
      [ "ComponentCreatorImplementation< T >", "class_component_creator_implementation.html", null ]
    ] ],
    [ "ActorData::ComponentData", "struct_actor_data_1_1_component_data.html", [
      [ "ActorData::Animation", "struct_actor_data_1_1_animation.html", null ],
      [ "ActorData::ArtificialIntelligence", "struct_actor_data_1_1_artificial_intelligence.html", null ],
      [ "ActorData::Audio", "struct_actor_data_1_1_audio.html", null ],
      [ "ActorData::Combat", "struct_actor_data_1_1_combat.html", null ],
      [ "ActorData::Graphics", "struct_actor_data_1_1_graphics.html", null ],
      [ "ActorData::Input", "struct_actor_data_1_1_input.html", null ],
      [ "ActorData::Particle", "struct_actor_data_1_1_particle.html", null ],
      [ "ActorData::Physics", "struct_actor_data_1_1_physics.html", null ]
    ] ],
    [ "ComponentData", null, [
      [ "CustomActorData::FlashOnHit", "struct_custom_actor_data_1_1_flash_on_hit.html", null ]
    ] ],
    [ "ComponentFactoryCreator", "class_component_factory_creator.html", [
      [ "ComponentFactoryCreatorImplementation< T >", "class_component_factory_creator_implementation.html", null ],
      [ "ComponentFactoryCreatorImplementation< AIComponent >", "class_component_factory_creator_implementation.html", null ],
      [ "ComponentFactoryCreatorImplementation< AnimationComponent >", "class_component_factory_creator_implementation.html", null ],
      [ "ComponentFactoryCreatorImplementation< AudioComponent >", "class_component_factory_creator_implementation.html", null ],
      [ "ComponentFactoryCreatorImplementation< CombatComponent >", "class_component_factory_creator_implementation.html", null ],
      [ "ComponentFactoryCreatorImplementation< GraphicsComponent >", "class_component_factory_creator_implementation.html", null ],
      [ "ComponentFactoryCreatorImplementation< InputComponent >", "class_component_factory_creator_implementation.html", null ],
      [ "ComponentFactoryCreatorImplementation< ParticleComponent >", "class_component_factory_creator_implementation.html", null ],
      [ "ComponentFactoryCreatorImplementation< PhysicsComponent >", "class_component_factory_creator_implementation.html", null ]
    ] ],
    [ "ComponentFactoryFactory", "class_component_factory_factory.html", null ],
    [ "Console", "class_console.html", null ],
    [ "CustomCursor", "class_custom_cursor.html", null ],
    [ "ActorData::Physics::Definition", "struct_actor_data_1_1_physics_1_1_definition.html", null ],
    [ "GuiData::ElementData", "struct_gui_data_1_1_element_data.html", [
      [ "GuiData::Bar", "struct_gui_data_1_1_bar.html", null ],
      [ "GuiData::Button", "struct_gui_data_1_1_button.html", null ],
      [ "GuiData::CheckBox", "struct_gui_data_1_1_check_box.html", null ],
      [ "GuiData::Container", "struct_gui_data_1_1_container.html", null ],
      [ "GuiData::DropDown", "struct_gui_data_1_1_drop_down.html", null ],
      [ "GuiData::Panel", "struct_gui_data_1_1_panel.html", null ],
      [ "GuiData::Slider", "struct_gui_data_1_1_slider.html", null ],
      [ "GuiData::TextBox", "struct_gui_data_1_1_text_box.html", null ]
    ] ],
    [ "Engine", "class_engine.html", null ],
    [ "Font", "class_font.html", null ],
    [ "FunctionPointerStorage", "class_function_pointer_storage.html", null ],
    [ "Grid", "class_grid.html", null ],
    [ "GuiData", "class_gui_data.html", null ],
    [ "GuiElementCreator", "class_gui_element_creator.html", [
      [ "GuiElementCreatorImplementation< T >", "class_gui_element_creator_implementation.html", null ],
      [ "GuiElementCreatorImplementation< Bar >", "class_gui_element_creator_implementation.html", null ],
      [ "GuiElementCreatorImplementation< Button >", "class_gui_element_creator_implementation.html", null ],
      [ "GuiElementCreatorImplementation< CheckBox >", "class_gui_element_creator_implementation.html", null ],
      [ "GuiElementCreatorImplementation< Container >", "class_gui_element_creator_implementation.html", null ],
      [ "GuiElementCreatorImplementation< DropDown >", "class_gui_element_creator_implementation.html", null ],
      [ "GuiElementCreatorImplementation< Panel >", "class_gui_element_creator_implementation.html", null ],
      [ "GuiElementCreatorImplementation< Slider >", "class_gui_element_creator_implementation.html", null ],
      [ "GuiElementCreatorImplementation< TextBox >", "class_gui_element_creator_implementation.html", null ]
    ] ],
    [ "GuiElementFactory", "class_gui_element_factory.html", null ],
    [ "Handler", "class_handler.html", [
      [ "AttemptX", "class_attempt_x.html", null ],
      [ "AudioHandler", "class_audio_handler.html", null ],
      [ "DatabaseHandler", "class_database_handler.html", null ],
      [ "EventHandler", "class_event_handler.html", null ],
      [ "FontHandler", "class_font_handler.html", null ],
      [ "GuiFactory", "class_gui_factory.html", null ],
      [ "IGraphicsHandler", "class_i_graphics_handler.html", [
        [ "GraphicsHandler", "class_graphics_handler.html", null ]
      ] ],
      [ "INetworkHandler", "class_i_network_handler.html", [
        [ "NetworkClientHandler", "class_network_client_handler.html", null ],
        [ "NetworkServerHandler", "class_network_server_handler.html", null ]
      ] ],
      [ "IPhysicsHandler", "class_i_physics_handler.html", [
        [ "PhysicsHandler", "class_physics_handler.html", null ]
      ] ],
      [ "MeshHandler", "class_mesh_handler.html", null ],
      [ "ShaderHandler", "class_shader_handler.html", null ],
      [ "SkyboxHandler", "class_skybox_handler.html", null ],
      [ "TerrainHandler", "class_terrain_handler.html", null ],
      [ "TextureHandler", "class_texture_handler.html", null ],
      [ "UserFeedback", "class_user_feedback.html", null ],
      [ "WorldGenerator", "class_world_generator.html", null ],
      [ "XmlHandler", "class_xml_handler.html", null ]
    ] ],
    [ "huffman", "structhuffman.html", null ],
    [ "IActorState", "class_i_actor_state.html", [
      [ "ASDead", "class_a_s_dead.html", null ],
      [ "ASDropping", "class_a_s_dropping.html", null ],
      [ "ASIdle", "class_a_s_idle.html", null ],
      [ "ASRising", "class_a_s_rising.html", null ],
      [ "ASWalking", "class_a_s_walking.html", null ]
    ] ],
    [ "IAIComponent", null, [
      [ "AIChuck", "class_a_i_chuck.html", null ]
    ] ],
    [ "ILifeComponent", "class_i_life_component.html", null ],
    [ "IndexedModel", "class_indexed_model.html", null ],
    [ "InputValues", "struct_input_values.html", null ],
    [ "IObservable", "class_i_observable.html", [
      [ "EventHandler", "class_event_handler.html", null ],
      [ "GuiElement", "class_gui_element.html", [
        [ "Bar", "class_bar.html", null ],
        [ "Button", "class_button.html", null ],
        [ "CheckBox", "class_check_box.html", null ],
        [ "Container", "class_container.html", null ],
        [ "DropDown", "class_drop_down.html", null ],
        [ "Panel", "class_panel.html", null ],
        [ "Slider", "class_slider.html", null ],
        [ "TextBox", "class_text_box.html", null ]
      ] ],
      [ "NetworkServerHandler", "class_network_server_handler.html", null ]
    ] ],
    [ "IReceiver", "class_i_receiver.html", [
      [ "GuiFactory", "class_gui_factory.html", null ],
      [ "InputComponent", "class_input_component.html", null ]
    ] ],
    [ "jpeg", "structjpeg.html", null ],
    [ "LevelData", "struct_level_data.html", null ],
    [ "LevelGenerator", "class_level_generator.html", null ],
    [ "LuaScript", "class_lua_script.html", null ],
    [ "MapIdentifier", "struct_map_identifier.html", null ],
    [ "Mesh", "class_mesh.html", null ],
    [ "Message", "class_message.html", null ],
    [ "MousePicker", "class_mouse_picker.html", null ],
    [ "NetworkActorData", "struct_network_actor_data.html", null ],
    [ "NetworkIDObject", null, [
      [ "Actor", "class_actor.html", null ]
    ] ],
    [ "Node", "class_node.html", null ],
    [ "OBJIndex", "struct_o_b_j_index.html", null ],
    [ "OBJModel", "class_o_b_j_model.html", null ],
    [ "ParticleSystem::Particle", "struct_particle_system_1_1_particle.html", null ],
    [ "ParticleSystem", "class_particle_system.html", [
      [ "ExplosionParticles", "class_explosion_particles.html", null ],
      [ "ParticleComponent", "class_particle_component.html", null ],
      [ "WeatherParticles", "class_weather_particles.html", null ]
    ] ],
    [ "PhysicsDefinition", "struct_physics_definition.html", null ],
    [ "pic_packet_t", "structpic__packet__t.html", null ],
    [ "Player", "struct_player.html", null ],
    [ "png", "structpng.html", null ],
    [ "SavedFrames", "class_saved_frames.html", null ],
    [ "SensorContacts", "struct_sensor_contacts.html", null ],
    [ "Shader", "class_shader.html", null ],
    [ "SlotValues", "struct_slot_values.html", null ],
    [ "stbi", "structstbi.html", null ],
    [ "stbi_gif_lzw_struct", "structstbi__gif__lzw__struct.html", null ],
    [ "stbi_gif_struct", "structstbi__gif__struct.html", null ],
    [ "stbi_io_callbacks", "structstbi__io__callbacks.html", null ],
    [ "stbi_resample", "structstbi__resample.html", null ],
    [ "Texture", "class_texture.html", null ],
    [ "Grid::Tile", "struct_grid_1_1_tile.html", null ],
    [ "Timer", "class_timer.html", null ],
    [ "TimeRewinder", "class_time_rewinder.html", null ],
    [ "Transform", "class_transform.html", null ],
    [ "Variable", "class_variable.html", null ],
    [ "krem::Vector2f", "structkrem_1_1_vector2f.html", null ],
    [ "krem::Vector2i", "structkrem_1_1_vector2i.html", null ],
    [ "krem::Vector3f", "structkrem_1_1_vector3f.html", null ],
    [ "krem::Vector3i", "structkrem_1_1_vector3i.html", null ],
    [ "Vertex", "class_vertex.html", null ],
    [ "zbuf", "structzbuf.html", null ],
    [ "zhuffman", "structzhuffman.html", null ]
];