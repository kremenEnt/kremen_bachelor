var struct_actor_data_1_1_particle =
[
    [ "color", "struct_actor_data_1_1_particle.html#af5bf1401520df8e6bbb6129ab83775a4", null ],
    [ "gravity", "struct_actor_data_1_1_particle.html#a63f3e2c65139eb3a3342ad56682d1fc0", null ],
    [ "gravityMultiplier", "struct_actor_data_1_1_particle.html#a6776b38a2ae4f2b2f6a8c41ee90bec9b", null ],
    [ "maxLife", "struct_actor_data_1_1_particle.html#ad551752adee706e6a3e30ec065f8d522", null ],
    [ "maxVelocity", "struct_actor_data_1_1_particle.html#aedea4bda2258672627eee7ae4cdf2936", null ],
    [ "particlesPerSecond", "struct_actor_data_1_1_particle.html#a7b9ce57b67c55df99246b51623b6d9e9", null ],
    [ "size", "struct_actor_data_1_1_particle.html#a792ecec58814869da1d22a5bcad325a0", null ],
    [ "texturePath", "struct_actor_data_1_1_particle.html#ac5d69de4283ed32a4a2aa95cbbaf2a23", null ],
    [ "velocity", "struct_actor_data_1_1_particle.html#a4da615ed9ce4f8c07c01179a6912e26f", null ]
];