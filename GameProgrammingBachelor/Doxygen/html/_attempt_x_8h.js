var _attempt_x_8h =
[
    [ "SlotValues", "struct_slot_values.html", "struct_slot_values" ],
    [ "Chunk", "struct_chunk.html", "struct_chunk" ],
    [ "MapIdentifier", "struct_map_identifier.html", "struct_map_identifier" ],
    [ "LevelData", "struct_level_data.html", "struct_level_data" ],
    [ "AttemptX", "class_attempt_x.html", "class_attempt_x" ],
    [ "amountOfChunks", "_attempt_x_8h.html#afd7573f9b1d414351ab843bb546a1aa6", null ],
    [ "xChunkSize", "_attempt_x_8h.html#a2fc3414a8e44c56919c452aa393300fa", null ],
    [ "yChunkSize", "_attempt_x_8h.html#a3638e3259c99e2ba742d5d459bfb30dd", null ]
];