var class_database_handler =
[
    [ "DatabaseHandler", "class_database_handler.html#ac27357d3431faf4fd6810483a24a43fd", null ],
    [ "~DatabaseHandler", "class_database_handler.html#a54cc84549167c9d6a94ba686cf64a27f", null ],
    [ "connect", "class_database_handler.html#a780559485ab3cae90365d9c544b32cfc", null ],
    [ "disconnect", "class_database_handler.html#a66a28dd8e7e5515fe370c43a4c065980", null ],
    [ "getVariable", "class_database_handler.html#a885752e0e82dac800578ba7a8762e68a", null ],
    [ "getVariable", "class_database_handler.html#a0d329ba3fc1f75a3981545ab00aef329", null ],
    [ "getVariable", "class_database_handler.html#a91757a2ae3820f28f1edad51007ecc36", null ],
    [ "getVariable", "class_database_handler.html#afc93e2bacf4a14ae1bb9ac96b4c8ae1e", null ],
    [ "getVariables", "class_database_handler.html#a48f29e4f5fcd5000f2c792d7d7b189cc", null ],
    [ "getVariables", "class_database_handler.html#adc1e448b8ae21177b88702e0698c217e", null ],
    [ "init", "class_database_handler.html#a1d0dc3b39e0281b21eb7a6b4b40d2384", null ],
    [ "insertPreperedStatement", "class_database_handler.html#a452fef7dcf48773a93b125fb469a54f8", null ],
    [ "retrieveData", "class_database_handler.html#a2f388ccf359b01d95881fe7cb7258aa1", null ],
    [ "setVariable", "class_database_handler.html#aba71cdfee9f112cf97bc2665e305851a", null ],
    [ "setVariable", "class_database_handler.html#ab726fcc012f2e3eb4baf604dc6fbff76", null ],
    [ "setVariable", "class_database_handler.html#a2582e58782d5cd0c0039fd656b1f72f9", null ],
    [ "setVariable", "class_database_handler.html#ad4eb72f50bb22a1c44b8fce5f36dedbf", null ],
    [ "setVariables", "class_database_handler.html#a416b403c9b467d6ffb40099e30e7f228", null ],
    [ "setVariables", "class_database_handler.html#aef3686c3fd889043b63805bfb9d76268", null ]
];