var class_i_observable =
[
    [ "~IObservable", "class_i_observable.html#ad8c8c49c05773f66ea9b979882691eca", null ],
    [ "addSubscriber", "class_i_observable.html#ab3d0110a36b0549d3d841b1d82efe9be", null ],
    [ "postMessage", "class_i_observable.html#ae0e18badf298df0a8e4c8943692b34cc", null ],
    [ "removeSubscriber", "class_i_observable.html#a53bf69e2afc38dfc4da983837297bc93", null ]
];