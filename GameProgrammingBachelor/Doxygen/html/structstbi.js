var structstbi =
[
    [ "buffer_start", "structstbi.html#af99edda496281a6ca1b58271cabdbc69", null ],
    [ "buflen", "structstbi.html#a76d6f761529ecff7f02469b19371af0e", null ],
    [ "img_buffer", "structstbi.html#aace36d5487a596bea5faa0aef0398ac8", null ],
    [ "img_buffer_end", "structstbi.html#a55f78565e605f1784d47fc9acea475f3", null ],
    [ "img_buffer_original", "structstbi.html#a261be6edda817862e623972b21b4f965", null ],
    [ "img_n", "structstbi.html#ae22cfcc23f5ab67bede22942333ecbd7", null ],
    [ "img_out_n", "structstbi.html#a33f6519d8f99b84afbde795dc7a931f2", null ],
    [ "img_x", "structstbi.html#af3b42c257fb0d8896f29ca3921540a42", null ],
    [ "img_y", "structstbi.html#a60cb5a630e268b2d12306c6eca246dd1", null ],
    [ "io", "structstbi.html#a86596e1eb2b0f57a60a18777bd37ff53", null ],
    [ "io_user_data", "structstbi.html#a9838a0c89630f283c25a16f4e30f40aa", null ],
    [ "read_from_callbacks", "structstbi.html#acb201cc1b3eb134f342cee89f5d11e70", null ]
];