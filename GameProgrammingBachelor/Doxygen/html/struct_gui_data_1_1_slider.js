var struct_gui_data_1_1_slider =
[
    [ "atlasSize", "struct_gui_data_1_1_slider.html#ac50b3e754135cc619808d3e6345d555f", null ],
    [ "defaultValue", "struct_gui_data_1_1_slider.html#ad971c21288dc468e7d74a30e5afe4c9a", null ],
    [ "interval", "struct_gui_data_1_1_slider.html#a2602325484341390bbdd51f9a1de10e4", null ],
    [ "maxValue", "struct_gui_data_1_1_slider.html#a81c34168a1015a9200a7a6080712e610", null ],
    [ "minValue", "struct_gui_data_1_1_slider.html#a75828555e4bc03080e37eb8777dc7ac0", null ],
    [ "positionLeftX", "struct_gui_data_1_1_slider.html#aac47f102d7419d6558e18ecec3a16b86", null ],
    [ "positionRightX", "struct_gui_data_1_1_slider.html#a0b64c0b3fa60839bb442435a7b94d7c5", null ]
];