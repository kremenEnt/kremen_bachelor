var class_graphics_handler =
[
    [ "GraphicsHandler", "class_graphics_handler.html#ab9d2acbe4fbbb4e6a8b36e5d801a93f2", null ],
    [ "~GraphicsHandler", "class_graphics_handler.html#a5d90fcc6207b73302a239e4034b9fa30", null ],
    [ "calculatePixelsPerUnitAndUnitsOnScreen", "class_graphics_handler.html#aa3851aa312990e05ff6320cd0139662b", null ],
    [ "clearWindow", "class_graphics_handler.html#ab427ce53f2de81287252b0a7f12410cc", null ],
    [ "getCamera", "class_graphics_handler.html#abebdc5588745760517a7e4100336728e", null ],
    [ "getDesktopResolution", "class_graphics_handler.html#aa54f1f6bafd4f80cad6c403299ea1371", null ],
    [ "getUnitsOnScreen", "class_graphics_handler.html#ad8e124ff5eb4760e0bccd7c80086489f", null ],
    [ "getViewDistance", "class_graphics_handler.html#a795ba6bd22db6397f5d3e5993b95ade5", null ],
    [ "getWindowResolution", "class_graphics_handler.html#a2795710bee61f73d265458ed90dfbf74", null ],
    [ "init", "class_graphics_handler.html#a82f302c50122378c2412c189de3ebc64", null ],
    [ "setCameraDirection", "class_graphics_handler.html#a8be38477b00d8b4395a8116361416cd0", null ],
    [ "setCameraPosition", "class_graphics_handler.html#a01094f2dccda26e048dc657190f3c602", null ],
    [ "setCameraPosition", "class_graphics_handler.html#a6455185052210d6d9d5dfd4c4d06ad1d", null ],
    [ "setViewDistance", "class_graphics_handler.html#ab52f939e94d2a617caf1e54492921036", null ],
    [ "swapBuffers", "class_graphics_handler.html#a12cff826ad9ab85bd873ef31e53a0d26", null ],
    [ "updateWindow", "class_graphics_handler.html#acd5c6b6fb179bf0178093a31f7380cfc", null ]
];