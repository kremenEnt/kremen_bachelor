var searchData=
[
  ['name',['name',['../struct_gui_data_1_1_element_data.html#aebc0c1c525927eb8df75af76ea7562fb',1,'GuiData::ElementData::name()'],['../struct_player.html#af9c920fabaafdeb7961a645315b521ff',1,'Player::name()']]],
  ['newangle',['newAngle',['../class_i_physics_component.html#a76ba989bd908810099b907cd81b3622d',1,'IPhysicsComponent']]],
  ['newposition',['newPosition',['../class_i_physics_component.html#a937961b7cd54bc296bec050bb109d434',1,'IPhysicsComponent']]],
  ['noluaparameters',['noLuaParameters',['../class_a_i_component.html#ab0d19efcdad9a7a3396737ccc367f964',1,'AIComponent']]],
  ['noluareturnvalues',['noLuaReturnValues',['../class_a_i_component.html#a03e1764b7d95f0f6787ce33b4296e68a',1,'AIComponent']]],
  ['nomore',['nomore',['../structjpeg.html#a2d114f4d52f50d8d85f43b2a3f161cec',1,'jpeg']]],
  ['normalindex',['normalIndex',['../struct_o_b_j_index.html#a9af2bc243ac910fe1e6596aa85aa4ae6',1,'OBJIndex']]],
  ['normals',['normals',['../class_indexed_model.html#a43a9aa25e0461c1a729693fd7efaf45f',1,'IndexedModel::normals()'],['../class_o_b_j_model.html#af61e5eb97529d47fe8e050fcd0bc976a',1,'OBJModel::normals()']]],
  ['nospriteframes',['noSpriteFrames',['../struct_actor_data_1_1_graphics.html#afd5c793916e089990f1faf8d024da9c5',1,'ActorData::Graphics']]],
  ['num_5fbits',['num_bits',['../structzbuf.html#acd069cdb4100884a732ad2794edbbdff',1,'zbuf']]],
  ['numberofboxesx',['numberOfBoxesX',['../struct_gui_data_1_1_container.html#a2937e8ce79d0051a7f019dce9008585f',1,'GuiData::Container']]],
  ['numberofboxesy',['numberOfBoxesY',['../struct_gui_data_1_1_container.html#a5db80c9b20bc48428a5d3c94fa885894',1,'GuiData::Container']]]
];
