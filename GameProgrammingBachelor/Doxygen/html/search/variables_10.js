var searchData=
[
  ['ratio',['ratio',['../structstbi__gif__struct.html#ac082648ce87130679bcde2dd4651c530',1,'stbi_gif_struct']]],
  ['raw_5fdata',['raw_data',['../structjpeg.html#a4f02a910ab51fb53fa8226fecf68d4f4',1,'jpeg']]],
  ['read',['read',['../structstbi__io__callbacks.html#a623e46b3a2a019611601409926283a88',1,'stbi_io_callbacks']]],
  ['read_5ffrom_5fcallbacks',['read_from_callbacks',['../structstbi.html#acb201cc1b3eb134f342cee89f5d11e70',1,'stbi']]],
  ['resample',['resample',['../structstbi__resample.html#a94091463ebc5933cdaf7a813025b6e19',1,'stbi_resample']]],
  ['restart_5finterval',['restart_interval',['../structjpeg.html#ab13af34259b1f1c6cf8f35411a77e39e',1,'jpeg']]],
  ['restitution',['restitution',['../struct_actor_data_1_1_physics_1_1_definition.html#ac049665f4d47b0b5205a75f105a220df',1,'ActorData::Physics::Definition::restitution()'],['../struct_physics_definition.html#ac801ba7490074ecc04190cd91b1c6460',1,'PhysicsDefinition::restitution()']]]
];
