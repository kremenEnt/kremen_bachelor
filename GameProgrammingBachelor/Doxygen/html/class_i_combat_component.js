var class_i_combat_component =
[
    [ "ICombatComponent", "class_i_combat_component.html#a7b23b7abd8654ef9eae364b6ff20fa5a", null ],
    [ "~ICombatComponent", "class_i_combat_component.html#ae21fb10b644bacd9c10b8132783d010f", null ],
    [ "alive", "class_i_combat_component.html#a0c31c7f2a6d4a1b07d19171b37436189", null ],
    [ "componentData", "class_i_combat_component.html#a7fe3028e003488fb057b0322764fcd70", null ],
    [ "health", "class_i_combat_component.html#a800dcc8b1b601ee4a6381e706ec9ffd4", null ],
    [ "invincibilityTimer", "class_i_combat_component.html#a8d1f5a5706bfb9f606ca44fbffde961f", null ],
    [ "invincible", "class_i_combat_component.html#ae526794293327adb16665332daf28373", null ],
    [ "lifetimeTimer", "class_i_combat_component.html#aaf3cac29e6a87f36a76390c2ae5af719", null ]
];