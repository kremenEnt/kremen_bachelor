var class_audio_handler =
[
    [ "AudioHandler", "class_audio_handler.html#a605b5c91270eb7cad7a0ac60b354b56b", null ],
    [ "~AudioHandler", "class_audio_handler.html#a61a1e88ca69c4021792400389eec3f27", null ],
    [ "loadMusic", "class_audio_handler.html#a88116e533e398f584bf8324416eba5d6", null ],
    [ "playMusic", "class_audio_handler.html#ab85e194f3ff79c58ff4190528cf2f574", null ],
    [ "playSound", "class_audio_handler.html#a3beb4cc8a3aec3da9306b074fdbe12ec", null ],
    [ "setMasterVolume", "class_audio_handler.html#ad7ef6f7d208aede88c779f265dae7c1f", null ],
    [ "setMusicVolume", "class_audio_handler.html#a3b8d9b3fa5742720b7e905adfacf81f8", null ],
    [ "setSoundVolume", "class_audio_handler.html#a6017e083df7e62aac5cea6df9ec97082", null ],
    [ "stopMusic", "class_audio_handler.html#aeff0546d72fab5587f86c830efb6f40e", null ]
];