var class_timer =
[
    [ "Timer", "class_timer.html#a12512adf6f796c63b2186241511794de", null ],
    [ "~Timer", "class_timer.html#a14fa469c4c295c5fa6e66a4ad1092146", null ],
    [ "getTicks", "class_timer.html#a72306ed0d160095f6fa813c305d536aa", null ],
    [ "hasEnded", "class_timer.html#a886ceb6b9bdd66ec05471be296d56864", null ],
    [ "isPaused", "class_timer.html#adba0628ba7c2f61e72da83d9d8648773", null ],
    [ "isStarted", "class_timer.html#a2c03be883cf950d14e058b4205f1526e", null ],
    [ "pause", "class_timer.html#a0289effad7b573c508bc27e405900a23", null ],
    [ "restart", "class_timer.html#aa3f7871196bb56202af2bc982bfbfff6", null ],
    [ "setDuration", "class_timer.html#af56a22d2d0b164761d555d6c73d8e6d3", null ],
    [ "start", "class_timer.html#a3a8b5272198d029779dc9302a54305a8", null ],
    [ "stop", "class_timer.html#a63f0eb44b27402196590a03781515dba", null ],
    [ "unpause", "class_timer.html#aa4dd50d7ed48ac73efed2950749d35d6", null ]
];