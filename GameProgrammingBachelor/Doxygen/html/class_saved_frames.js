var class_saved_frames =
[
    [ "SavedFrames", "class_saved_frames.html#a598a501e544f5166e5326afd59a69e71", null ],
    [ "~SavedFrames", "class_saved_frames.html#a404aa19f012d6251ced10cf152f1ab81", null ],
    [ "getFrameSpawned", "class_saved_frames.html#ab6d59d4baa4290a025c11147013f34e3", null ],
    [ "getPosition", "class_saved_frames.html#a5db067907be470672dc9e6a1f9b1284d", null ],
    [ "getRotation", "class_saved_frames.html#a0a3845fe2421df24d10f6679ce7ba18c", null ],
    [ "getVelocity", "class_saved_frames.html#ae65a9514f0e938141545dfff46a5a7d0", null ],
    [ "MoveFrameSpawned", "class_saved_frames.html#a7c2db3256e9a052afc21de6e21346d9b", null ],
    [ "removeTimeFrame", "class_saved_frames.html#ac29098743a722f6324b843c172563a9e", null ],
    [ "setFrameSpawned", "class_saved_frames.html#a460855cbae6ebf7509ac2e944256b553", null ],
    [ "setPosition", "class_saved_frames.html#a462b4e8c56d6bde2234d97564cf096ae", null ],
    [ "setRotation", "class_saved_frames.html#a3f3a6a0028b43e421bf2c68fd6bc76d2", null ],
    [ "setVelocity", "class_saved_frames.html#a433887cee54a8a0e6241c4af5f7f7e0d", null ]
];