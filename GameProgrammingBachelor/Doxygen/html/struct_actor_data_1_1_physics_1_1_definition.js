var struct_actor_data_1_1_physics_1_1_definition =
[
    [ "BOX", "struct_actor_data_1_1_physics_1_1_definition.html#a668036c44352c31c592bc2088d25c482a3dc1dea0768a82df767896746f16d425", null ],
    [ "CIRCLE", "struct_actor_data_1_1_physics_1_1_definition.html#a668036c44352c31c592bc2088d25c482a4028671f04edc946425e476930ac7e98", null ],
    [ "POLYGON", "struct_actor_data_1_1_physics_1_1_definition.html#a668036c44352c31c592bc2088d25c482ac513c84594087e2c607fb50863e0588b", null ],
    [ "bodyType", "struct_actor_data_1_1_physics_1_1_definition.html#a2a2e2cab7abac22c8876979b2405b5cf", null ],
    [ "collisionCategory", "struct_actor_data_1_1_physics_1_1_definition.html#a359c6e554512b225d7d40191c1e57cb0", null ],
    [ "density", "struct_actor_data_1_1_physics_1_1_definition.html#a672b8474580edc64584ec045ce38956e", null ],
    [ "fixedRotation", "struct_actor_data_1_1_physics_1_1_definition.html#aea5387534c6214d351697fdbcbe58aff", null ],
    [ "friction", "struct_actor_data_1_1_physics_1_1_definition.html#afb6c664f48cecea9336733de13c8b790", null ],
    [ "gravity", "struct_actor_data_1_1_physics_1_1_definition.html#a53609e78a06bc20ceaabbc69c79d49f3", null ],
    [ "isSensor", "struct_actor_data_1_1_physics_1_1_definition.html#aef39a9eed6b7fc266e3519e1c185351f", null ],
    [ "jumpSensor", "struct_actor_data_1_1_physics_1_1_definition.html#a84cf60d51e8f0fe0a5ded001c7b52f00", null ],
    [ "polygonVertices", "struct_actor_data_1_1_physics_1_1_definition.html#a88af5c1472f891c23ff7c6cd551f4372", null ],
    [ "restitution", "struct_actor_data_1_1_physics_1_1_definition.html#ac049665f4d47b0b5205a75f105a220df", null ],
    [ "shape", "struct_actor_data_1_1_physics_1_1_definition.html#a61489e721c1e5dbe0eeba8ca74f1612c", null ],
    [ "size", "struct_actor_data_1_1_physics_1_1_definition.html#af7df07f405759d8a6082e37d4d1a3bfd", null ],
    [ "wallSensor", "struct_actor_data_1_1_physics_1_1_definition.html#a95a1a2b80b653521a9decb2578553933", null ]
];