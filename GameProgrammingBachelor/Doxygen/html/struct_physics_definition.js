var struct_physics_definition =
[
    [ "shapeType", "struct_physics_definition.html#a2a5b39f13ccce56781e1baa095142abf", [
      [ "BOX", "struct_physics_definition.html#a2a5b39f13ccce56781e1baa095142abfa556fd27c7da38b9142f79a61bd740c73", null ],
      [ "CIRCLE", "struct_physics_definition.html#a2a5b39f13ccce56781e1baa095142abfaaa88599bde9d9f9601abe4fa65e38e52", null ],
      [ "POLYGON", "struct_physics_definition.html#a2a5b39f13ccce56781e1baa095142abfaf4408db70cbace1609f3ec2be2781283", null ],
      [ "NO_OF_SHAPES", "struct_physics_definition.html#a2a5b39f13ccce56781e1baa095142abfad7743b4b65d218984328a2d53499eb55", null ]
    ] ],
    [ "bodyType", "struct_physics_definition.html#a71587a143c025ca3df9aaf3ba7119bdc", null ],
    [ "category", "struct_physics_definition.html#a42e57897a0867fadaf02600d77612b1a", null ],
    [ "density", "struct_physics_definition.html#a6d05eec17ef494ce8ebb0dd23dbc8e25", null ],
    [ "fixedRotation", "struct_physics_definition.html#aaba7053af0230f8fd719fcd536078acf", null ],
    [ "friction", "struct_physics_definition.html#a332763736b81b4c89ac39e99653132ba", null ],
    [ "gravity", "struct_physics_definition.html#a641a9f4155efef523d6b13d6efc1cada", null ],
    [ "isSensor", "struct_physics_definition.html#a42108af59e43a04379934e1d07631fb8", null ],
    [ "jumpSensor", "struct_physics_definition.html#a454ee8d44a79891d98a7b64e47067ac2", null ],
    [ "restitution", "struct_physics_definition.html#ac801ba7490074ecc04190cd91b1c6460", null ],
    [ "shape", "struct_physics_definition.html#ad38ddb2a7fff84ed4e4e949f9bb32cbc", null ],
    [ "size", "struct_physics_definition.html#a557fa57bb0a6970c436225706451cc30", null ],
    [ "wallSensor", "struct_physics_definition.html#a61b879af23f38307d16dafa8bcabb781", null ]
];