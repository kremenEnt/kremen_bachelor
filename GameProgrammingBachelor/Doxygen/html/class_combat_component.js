var class_combat_component =
[
    [ "CombatComponent", "class_combat_component.html#ad44fbf20269270c5f4eee1962d06172a", null ],
    [ "~CombatComponent", "class_combat_component.html#aab547d642fea58e0e7994965a46176f8", null ],
    [ "changeHealth", "class_combat_component.html#ab4aa7d0e6fa483fb134f293a02d48b5d", null ],
    [ "changeMana", "class_combat_component.html#a77bb70caca302b2aa13d7a43365f943b", null ],
    [ "getDamage", "class_combat_component.html#a26cc60228e89b471c5990915a2e0c96d", null ],
    [ "getHealth", "class_combat_component.html#ad482693ccd04bfe0a9d39642939014ec", null ],
    [ "kill", "class_combat_component.html#a92f04db6c52c7a3ee8d301442e659e41", null ],
    [ "update", "class_combat_component.html#a34a0a14ee948fc61e40a8f360e009612", null ]
];