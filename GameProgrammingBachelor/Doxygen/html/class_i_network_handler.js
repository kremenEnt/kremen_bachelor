var class_i_network_handler =
[
    [ "INetworkHandler", "class_i_network_handler.html#af921972ae86c1ebd16d240ea8722c16e", null ],
    [ "~INetworkHandler", "class_i_network_handler.html#ad0d082f090d99cea9b355fc660175d4f", null ],
    [ "receivePacket", "class_i_network_handler.html#a6e76fafddd4e62af1629e5fd92b32780", null ],
    [ "sendPacket", "class_i_network_handler.html#a6f9192c8aa20cb83f2f6531c61190885", null ],
    [ "oldActors", "class_i_network_handler.html#adc2d681e8c7b231974f5a91bc07f3d18", null ],
    [ "PORT", "class_i_network_handler.html#a2372c17a4c1cf9bfa40b88c4f0ff3f31", null ],
    [ "serverIP", "class_i_network_handler.html#a268de1e70addc0764b028f639fcb8edb", null ]
];