var structzbuf =
[
    [ "code_buffer", "structzbuf.html#a3bb8244d7be17801079c5a8587182edb", null ],
    [ "num_bits", "structzbuf.html#acd069cdb4100884a732ad2794edbbdff", null ],
    [ "z_distance", "structzbuf.html#ae7d9588b2548708e14f3c6ad89bf26b5", null ],
    [ "z_expandable", "structzbuf.html#ae662f24e0973ca19b543e64647a6bfb6", null ],
    [ "z_length", "structzbuf.html#a5906bdbe9dfb565339acac51af9efe89", null ],
    [ "zbuffer", "structzbuf.html#a7080eb91dcc67e1dfe818d08e6f22c4e", null ],
    [ "zbuffer_end", "structzbuf.html#af030baa17bebedd18272678da17a33f4", null ],
    [ "zout", "structzbuf.html#aaf137c25fa5b9fb14e92354da4203c38", null ],
    [ "zout_end", "structzbuf.html#af07c0b7b7227f670ee1413bc0dcab791", null ],
    [ "zout_start", "structzbuf.html#af31571e8d74c78c9bb18d92205150b28", null ]
];