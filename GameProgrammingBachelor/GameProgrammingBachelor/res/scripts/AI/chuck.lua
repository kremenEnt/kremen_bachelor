rockCooldown = 5 --constant

timeSinceLastRock = 5 --not constant
movingRight = true

function normalizeVector (playerX, playerY, aiX, aiY, deltaTime )
	
	throwRock = false
	timeSinceLastRock = timeSinceLastRock + deltaTime
	vectorX = playerX - aiX
	vectorY = playerY - aiY
    --math.pow() doesn't seem to work for whatever reason, so USE MANUAL EXPONENTIAL CALCULATIONS
	a = vectorX*vectorX
    b = vectorY*vectorY 
	
	x = 0
	
	if (movingRight == true) then
		x = -1
		if (vectorX > 1) then
			movingRight = false
			x = 1
		end
	end
	
	if (movingRight == false) then
		x = 1
		if (vectorX < -1) then
			movingRight = true
			x = -1
		end
	end
	
	
	
	magnitude = math.sqrt(a+b)
	--not sure what the equivalent of && is in lua. will code like this for now to make sure it actually does what I want it to do
	if (timeSinceLastRock >= rockCooldown and magnitude < 5) then
		throwRock = true
		timeSinceLastRock = 0
	end
	
	if throwRock then
		return 2*x, 5,"res/actors/EnemyProjectile/chucked.xml"
	else
		return 2*x, 5, ""
	end
	
end