#version 330 core

in vec4 vertexPosition;
in vec2 texCoordinate;
in vec4 vertexNormal;
in vec4 oLightPosition;

out vec4 color;

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;

void main()
{
	vec4 tex1 = texture2D(texture0, texCoordinate);
	vec4 tex2 = texture2D(texture1, texCoordinate);
	vec4 tex3 = texture2D(texture2, texCoordinate);
	vec4 lightColor = vec4(1.0, 1.0, 1.0, 1.0);
	vec4 n = normalize(vertexNormal);
	vec4 l = normalize(oLightPosition);
	vec4 matdiffuseColor;
	vec4 diffuseColor;
	
	float slope = 1.0f - n.y;
	float blendAmount = 0.f;
	float normalLightDotProduct = dot(n, l);
	
	if(slope < 0.2)
    {
        blendAmount = slope / 0.2f;
        matdiffuseColor = mix(tex1, tex2, blendAmount);
    }

     if((slope < 0.7) && (slope >= 0.2f)) 
    {
		//blendAmount = (slope - 0.2f) * (1.0f / (0.7f - 0.2f));
        matdiffuseColor = tex2;//mix(tex2, tex1, blendAmount);
    }
	
	 if(slope >= 0.7) 
    {
        matdiffuseColor = tex2;
	}
    

	normalLightDotProduct = clamp(normalLightDotProduct, 0.05, 1.0);
	diffuseColor = lightColor  * normalLightDotProduct;
	
	color = matdiffuseColor * diffuseColor;
}

