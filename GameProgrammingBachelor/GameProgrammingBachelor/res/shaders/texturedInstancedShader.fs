#version 330 core

in vec4 color;
in vec2 texCoord;

out vec4 colors;

uniform sampler2D texture0;

void main()
{
	vec4 texel = texture2D(texture0, texCoord);
	if (texel.a <= 0.5)
	{
		discard;
	}
	colors = color * texel;
}