#version 330 core

in vec2 texCoord;

out vec4 color;

uniform sampler2D texture0;
uniform vec2 size;
uniform int spriteNo;
uniform vec4 colors = vec4(1.0, 1.0, 1.0, 1.0);

void main()
{
	int columns = int(size.x);
	float x = spriteNo % columns;
	float y = spriteNo / columns;

	vec4 texel = texture2D(texture0, (vec2(x, y) + vec2(1.f, 1.f) * texCoord) / size);

	if (texel.a <= 0.9)
	{
		discard;
	}
	
	color = texel * colors;
}