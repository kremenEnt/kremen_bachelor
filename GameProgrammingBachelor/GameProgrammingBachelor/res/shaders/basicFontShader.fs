#version 330 core

in vec2 texCoord;

out vec4 color;

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform float scale = 1.0f;

float offset = (scale - 1.0) * 0.5;

void main()
{	
	vec4 button	= texture2D(texture0, texCoord);
	vec4 font = texture2D(texture1, (texCoord * scale) - offset);
	
	if (button.a <= 0.9)
	{
		discard;
	}
	
	color = mix(button, font, font.a);
}