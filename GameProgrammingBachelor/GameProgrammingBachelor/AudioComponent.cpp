#include "AudioComponent.h"
#include "Engine.h"
#include "AudioHandler.h"

AudioComponent::AudioComponent(ActorData::ComponentData* componentData)
	: IAudioComponent(componentData)
{
	this->componentData = *static_cast<ActorData::Audio*>(componentData);
}

AudioComponent::~AudioComponent()
{	
}


void AudioComponent::init()
{
	//seems pointless to let init be pure virtual 
}

void AudioComponent::onSpawn()
{
	
	auto it = componentData.soundMap.find("spawn");
	if (it != componentData.soundMap.end())
	{
		parentEngine->getAudioHandler()->playSound(it->second);
	}
}

void AudioComponent::onDeath()
{
	auto it = componentData.soundMap.find("death");
	if (it != componentData.soundMap.end())
	{
		parentEngine->getAudioHandler()->playSound(it->second);
	}
}

void AudioComponent::onHit()
{
	auto it = componentData.soundMap.find("hit");
	if (it != componentData.soundMap.end())
	{
		parentEngine->getAudioHandler()->playSound(it->second);
	}
}

