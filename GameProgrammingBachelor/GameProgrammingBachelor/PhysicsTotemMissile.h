#pragma once
#include "PhysicsComponent.h"
#include "Actor.h"
#include <glm\gtc\constants.hpp>
#include "CombatComponent.h"
#include <iostream>

class PhysicsTotemMissile : public PhysicsComponent
{
public:
	PhysicsTotemMissile(ActorData::ComponentData* componentData)
		: PhysicsComponent(componentData)
	{

	}
	void onCollision(int fixtureUserData, Actor* collidedActor) override
	{
		switch (fixtureUserData)
		{
		case CATEGORY_CLONE:
			dynamic_cast<CombatComponent*>(collidedActor->getComponent("CombatComponent"))->changeHealth(-damage);
			break;
		default:
			break;
		}
	}

private:
	int damage = 10;
};