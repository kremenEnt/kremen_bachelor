#pragma once
#include "Observable.h"
#include "Receiver.h"
#include "GuiElement.h"

class Engine;
class GuiElement;

/** A container. Used to display a panel with slots inside. Can be used for either a inventory like container where you can move stuff around or a kind of selection screen */
class Container : public GuiElement
{
public:
	Container(GuiData::ElementData* elementData);
	virtual ~Container();

	/** Updates this object. */
	void update() override;

	/**
	 * Determines if mouse click left is inside the container or a container slot.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickLeft() override;

	/**
	 * Determines if mouse click right is inside a container slot.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickRight() override;

	/**
	 * runs if the container has movable slots inside. I.E swapping items in an inventory.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool movableMouseClick();

	/**
	 * Determines if mouse release is inside the container or a slot.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseRelease() override;

	/**
	 * runs if the container has movable slots.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool movableMouseRelease();

	/**
	 * Sort items in the container
	 *
	 * @param	item1	The first item.
	 * @param	item2	The second item.
	 */
	void sortItems(int item1, int item2);

	/**
	 * Swap items in the container
	 *
	 * @param	item1	The first item.
	 * @param	item2	The second item.
	 */
	void swapItems(int item1, int item2);

	/**
	* Sets the current selected slot to a new one.
	*
	* @param	int		the number of the slot you want to change.
	*/
	void setSelectedBox(int box);
	
	/**
	 * Receives message. Used to receive messages from our messaging system.
	 *
	 * @param	message	The message.
	 */
	void receiveMessage(const Message &message);

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection) override;

	/** Flip visible. Shows or hides the container**/
	void flipVisible();

	/**
	* Sets the  texture of the spesific slot to a new one in runtime.
	*
	* @param	texture		the texture path that is used when changing.
	*/
	void setTexture(int slot, std::string texturePath);

	/**
	* Sets the  texture of the container background boxes to a new one in runtime.
	*
	* @param	texture		the texture path that is used when changing.
	*/
	void setBoxTexture(std::string texturePath);

	/**
	 * Gets the name.
	 *
	 * @return	The name.
	 */
	std::string getName();

	/**
	* Sets the container to visible or not, also deactivates interactions.
	*
	* @param	bool		true or false for visible or not.
	*/
	void setVisible(bool visible);
private:
	GuiData::Container elementData;

	std::string name;

	bool visible;
	bool clickable;
	bool movableItems;
	bool destroyable;
	bool activatable;

	int selectedBox;
	int activatedBox;
	int numberOfActors;
	int numberOfPresetItems;
	int firstFreeSlot;
	int textureAtlasSizeX;
	int textureAtlasSizeY;
	int numberOfBoxes;
	int numberOfBoxesX;
	int numberOfBoxesY;

	float distanceBetweenBoxes;

	float borderTop;
	float borderBottom;
	float borderLeft;
	float borderRight;

	Transform transform;
	std::vector<Transform> transformBoxes;
	std::vector<Transform> transformContainedItems;
	Mesh *mesh;

	Shader *shader;
	Shader *shaderBoxes;

	Texture *textureContainer;
	Texture *textureBoxes;

	std::map<int, Texture*> containedItems;

	virtual void init() override;
};

