#pragma once
#include <memory>
#include "GuiElement.h"
#include "GuiData.h"

class GuiElementFactory;

/**
 * @class	GuiElementCreator
 *
 * @brief	A graphical user interface element creator.
 */
class GuiElementCreator
{
public:

	/**
	 * @fn
	 * GuiElementCreator::GuiElementCreator(const std::string& elementType, GuiElementFactory* guiElementFactory);
	 *
	 * @brief	Constructor.
	 *
	 * @param	elementType				 	Type of the element.
	 * @param [in,out]	guiElementFactory	If non-null, the graphical user interface element factory.
	 */
	GuiElementCreator(const std::string& elementType, GuiElementFactory* guiElementFactory);

	/**
	 * @fn
	 * void GuiElementCreator::registerIt(const std::string& typeCategory, GuiElementCreator* guiElementCreator);
	 *
	 * @brief	Registers the iterator.
	 *
	 * @param	typeCategory			 	Category the type belongs to.
	 * @param [in,out]	guiElementCreator	If non-null, the graphical user interface element creator.
	 */
	void registerIt(const std::string& typeCategory, GuiElementCreator* guiElementCreator);

	/**
	 * @fn
	 * virtual std::unique_ptr<GuiElement> GuiElementCreator::create(GuiData::ElementData* elementData) = 0;
	 *
	 * @brief	Creates a new std::unique_ptr&lt;GuiElement&gt;
	 *
	 * @param [in,out]	elementData	If non-null, information describing the element.
	 *
	 * @return	A std::unique_ptr&lt;GuiElement&gt;
	 */
	virtual std::unique_ptr<GuiElement> create(GuiData::ElementData* elementData) = 0;
protected:
	std::map<std::string, GuiElementCreator*> table;
};

/**
 * @class	GuiElementCreatorImplementation
 *
 * @brief	A graphical user interface element creator implementation.
 *
 * @tparam	T	Generic type parameter.
 */
template <class T>
class GuiElementCreatorImplementation : public GuiElementCreator
{
public:

	/**
	 * @fn
	 * GuiElementCreatorImplementation::GuiElementCreatorImplementation(const std::string& elementType, GuiElementFactory* guiElementFactory)
	 *
	 * @brief	Constructor.
	 *
	 * @param	elementType				 	Type of the element.
	 * @param [in,out]	guiElementFactory	If non-null, the graphical user interface element factory.
	 */
	GuiElementCreatorImplementation(const std::string& elementType, GuiElementFactory* guiElementFactory) : GuiElementCreator(elementType, guiElementFactory) {};

	/**
	 * @fn
	 * virtual std::unique_ptr<GuiElement> GuiElementCreatorImplementation::create(GuiData::ElementData* elementData) override
	 *
	 * @brief	Creates a new std::unique_ptr&lt;GuiElement&gt;
	 *
	 * @param [in,out]	elementData	If non-null, information describing the element.
	 *
	 * @return	A std::unique_ptr&lt;GuiElement&gt;
	 */
	virtual std::unique_ptr<GuiElement> create(GuiData::ElementData* elementData) override
	{
		return std::make_unique<T>(elementData);
	}
};

