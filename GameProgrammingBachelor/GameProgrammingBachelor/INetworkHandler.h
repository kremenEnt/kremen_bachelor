#pragma once
#include "Handler.h"
#include "common.h"
#include <RakNetTypes.h>
#include <MessageIdentifiers.h>

/**
 * @enum	SpecialPackets
 *
 * @brief	Values that represent special packets.
 */
enum SpecialPackets
{
	//host sends start game when pressting start game
	START_GAME = ID_USER_PACKET_ENUM + NO_OF_GAMESTATES,
	//sent by host to make clients create new actors before updating actors (reliable, ordered, high prio)
	CREATE_ACTOR
};

/**
 * @class	INetworkHandler
 *
 * @brief	A network handler.
 */
class INetworkHandler : public Handler
{
public:
	INetworkHandler() {};
	~INetworkHandler() {};
	//look for clients
	virtual void sendPacket(int gameState) = 0;
	virtual void receivePacket(int gameState) = 0;
protected:
	RakNet::SystemAddress serverIP;
	const int PORT = 2000;
	std::vector<Actor*> oldActors;
};