#pragma once
#include <vector>
#include "common.h"

/**
 * @class	Grid
 *
 * @brief	A grid.
 */
class Grid
{
public:

	/**
	 * @enum	Type
	 *
	 * @brief	Values that represent types.
	 */
	enum Type
	{
		WALL,
		GROUND,
		NO_OF_TYPES
	};

	/**
	 * @struct	Tile
	 *
	 * @brief	A tile.
	 */
	struct Tile
	{
		krem::Vector2i pos{ 0,0 };
		unsigned int type = GROUND;
	};

	/**
	 * @fn	Grid::Grid(int width, int height);
	 *
	 * @brief	Constructor.
	 *
	 * @param	width 	The width.
	 * @param	height	The height.
	 */
	Grid(int width, int height);

	/**
	 * @fn	Grid::~Grid();
	 *
	 * @brief	Destructor.
	 */
	~Grid();

	/**
	 * @fn	void Grid::fillMap();
	 *
	 * @brief	Fill map.
	 */
	void fillMap();

	/**
	 * @fn	void Grid::setTile(const int type, const krem::Vector2i pos);
	 *
	 * @brief	Sets a tile.
	 *
	 * @param	type	The type.
	 * @param	pos 	The position.
	 */
	void setTile(const int type, const krem::Vector2i pos);

	/**
	 * @fn	Tile* Grid::getTile(const krem::Vector2i pos);
	 *
	 * @brief	Gets a tile.
	 *
	 * @param	pos	The position.
	 *
	 * @return	null if it fails, else the tile.
	 */
	Tile* getTile(const krem::Vector2i pos);
private:
	std::vector<std::vector<Tile>> grid;

	const int MAXWIDTH;
	const int MAXHEIGHT;
};