#include "Slider.h"
#include "Engine.h"
#include "GuiElement.h"
#include "EventHandler.h"
#include "MeshHandler.h"
#include "TextureHandler.h"
#include "ShaderHandler.h"
#include "FontHandler.h"

Slider::Slider(GuiData::ElementData* elementData) : GuiElement(elementData)
{
	this->elementData = *dynamic_cast<GuiData::Slider*>(elementData);
	//HARDCODED LIKE FUCK
	slider = false;
	left = false;
	right = false;
	fontDisplay = false;
	currentSliderTexture = 1;
	currentRightTexture = 0;
	currentLeftTexture = 2;
}

Slider::~Slider()
{
}

void Slider::update()
{ 
	transform[0].getPos().x = values[currentValue];
	//HARDCODED LIKE FUCK
	if (slider)
	{
		//checks if the slider is inbetween the left and right arrow while using the mouse
		if (parentEngine->getEventHandler()->getMouseOpenGLPos().y < transform[0].getPos().y + transform[0].getScale().y * 1.1
			&&parentEngine->getEventHandler()->getMouseOpenGLPos().y > transform[0].getPos().y - transform[0].getScale().y * 1.1)
		{		//decreases the value of the slider in the left direction
			if (parentEngine->getEventHandler()->getMouseOpenGLPos().x <= transform[0].getPos().x)
			{
				if (currentValue > minValue)
				{
					currentValue -= valueInterval;
					transform[0].getPos().x = values[currentValue];
				}
			}//increases the value of the slider in the right direction
			if (parentEngine->getEventHandler()->getMouseOpenGLPos().x >= transform[0].getPos().x)
			{
				if (currentValue < maxValue)
				{
					currentValue += valueInterval;
					transform[0].getPos().x = values[currentValue];
				}
			}
		}
		else
		{
			slider = false;
		}
	}

	if (left)
	{	//checks if the slider is higher than the left arrow when clicking the left arrow
		if (transform[0].getPos().x > (transform[1].getPos().x + transform[1].getScale().x))
		{ //decreases the value on the slider
			if (currentValue > minValue)
			{
				currentValue -= valueInterval;
				transform[0].getPos().x = values[currentValue];
			}
		}
		
	}

	if (right)
	{	//checks if the slider is lower than the right arrow when clicking the right arrow
		if (transform[0].getPos().x < (transform[2].getPos().x - transform[2].getScale().x))
		{ //increases the value of the slider
			if (currentValue < maxValue)
			{
				currentValue += valueInterval;
				transform[0].getPos().x = values[currentValue];
			}
			
		}
	}
	checkMouseOver();
	
	const char * tempName = name.c_str();
	//sends a message with the slider name and the current value displayed to be used in functions
	this->postMessage(Message(this, tempName, currentValue));
}

void Slider::checkMouseOver()
{
	//TODO: REMOVE MAGIC NUMBERS
	if (parentEngine->getEventHandler()->mouseInside(transform[0]) && !slider)
	{
		currentSliderTexture = 4;
	}
	else if (!slider)
	{
		currentSliderTexture = 1;
	}
	if (parentEngine->getEventHandler()->mouseInside(transform[1]) && !left)
	{
		currentLeftTexture = 3;
	}
	else if (!left)
	{
		currentLeftTexture = 0;
	}
	if (parentEngine->getEventHandler()->mouseInside(transform[2]) && !right)
	{
		currentRightTexture = 5;
	}
	else if (!right)
	{
		currentRightTexture = 2;
	}
}

bool Slider::checkMouseClickLeft()
{
	//TODO: REMOVE MAGIC NUMBERS
	{
		if (visible)
			if (parentEngine->getEventHandler()->mouseInside(transform[0]))
			{
				slider = true;
				currentSliderTexture = 7;
				return slider;
			}
		if (parentEngine->getEventHandler()->mouseInside(transform[1]))
		{
			left = true;
			currentLeftTexture = 6;

			return left;
		}

		if (parentEngine->getEventHandler()->mouseInside(transform[2]))
		{
			right = true;
			currentRightTexture = 8;

			return right;
		}
	}
	return false;
}

bool Slider::checkMouseClickRight()
{
	return false;
}

bool Slider::checkMouseRelease()
{
	slider = false;
	left = false;
	right = false;
	currentSliderTexture = 1;
	currentLeftTexture = 0;
	currentRightTexture = 2;

	return false;
}

void Slider::draw(glm::mat4 &viewProjection)
{
	float SCALE = 1.5f;
	//TODO: rewrite this shit aswell
	if (visible)
	{
		for (int i = 0; i < 3; i++)
		{
			//HARDCODED LIKE FUCK
			shader->bind();
			if (i == 0)
			{
				shader->loadTransform(transform[i], viewProjection);
				shader->loadVec2(U_SIZE, glm::vec2(textureAtlasSize, textureAtlasSize));
				shader->loadInt(U_TEXTURE0, 0);
				shader->loadInt(U_SPRITE_NO, currentSliderTexture);
				shader->loadFloat(U_SCALE, SCALE);
				if (fontDisplay)
				{
					font->update(std::to_string(currentValue), glm::vec3(0, 0, 0));
					shader->loadInt(U_TEXTURE1, 1);
					font->bind(1);
				}

			}
			else if (i == 1)
			{
				shader->loadTransform(transform[i], viewProjection);
				shader->loadVec2(U_SIZE, glm::vec2(textureAtlasSize, textureAtlasSize));
				shader->loadInt(U_TEXTURE0, 0);
				shader->loadInt(U_SPRITE_NO, currentLeftTexture);
				shader->loadFloat(U_SCALE, SCALE);
				if (fontDisplay)
				{
					font->update(" ", glm::vec3(0, 0, 0));
					shader->loadInt(U_TEXTURE1, -1);
					font->bind(1);
				}
			}
			else
			{
				shader->loadTransform(transform[i], viewProjection);
				shader->loadVec2(U_SIZE, glm::vec2(textureAtlasSize, textureAtlasSize));
				shader->loadInt(U_TEXTURE0, 0);
				shader->loadInt(U_SPRITE_NO, currentRightTexture);
				shader->loadFloat(U_SCALE, SCALE);
				if (fontDisplay)
				{
					font->update(" ", glm::vec3(0, 0, 0));
					shader->loadInt(U_TEXTURE1, -1);
					font->bind(1);
				}
			}
			texture->bind(0);
			mesh->draw();
		}
	}
}

void Slider::init()
{
	float tempPosX;
	float tempPosY;
	float tempWidth;
	float tempHeight;

	//TODO: rewrite
	name = elementData.name;
	tempPosX = elementData.positionX;
	tempPosY = elementData.positionY;
	tempWidth = elementData.width;
	tempHeight = elementData.height;
	tempHeight = (tempHeight / aspectRatio.y)*aspectRatio.x;
	visible = elementData.visible;
	mesh = parentEngine->getMeshHandler()->loadModel(elementData.meshPath);
	texture = parentEngine->getTextureHandler()->loadTexture(elementData.texturePath);
	shader = parentEngine->getShaderHandler()->loadShader(elementData.shaderPath);

	if (!elementData.fontPath.empty())
	{
		font = parentEngine->getFontHandler()->loadFont(elementData.fontPath);
		fontDisplay = true;
	}
	transform[0].setPos(glm::vec3(tempPosX, tempPosY, 0.999f));

	tempPosX = elementData.positionLeftX;
	transform[1].setPos(glm::vec3(tempPosX, tempPosY, 0.999f));

	tempPosX = elementData.positionRightX;
	transform[2].setPos(glm::vec3(tempPosX, tempPosY, 0.999f));

	minValue = elementData.minValue;
	maxValue = elementData.maxValue;
	currentValue = elementData.defaultValue;
	valueInterval = elementData.interval;

	textureAtlasSize = elementData.atlasSize;

	//rewrite this ugly thing aswell
	for (int i = 0; i < 3; i++)
		transform[i].setScale(glm::vec3(tempWidth, tempHeight, 1.0f));

	float tempDistance = transform[2].getPos().x - transform[1].getPos().x - transform[1].getScale().x;
	distance = (tempDistance - transform[1].getScale().x) / maxValue;

	float tempValuePos = transform[1].getPos().x + transform[1].getScale().x;
	int numberOfValue = maxValue - minValue;
	for (int i = 0; i <= numberOfValue; i++)
	{
		values.emplace_back(tempValuePos);
		tempValuePos += distance;
	}
}


std::string Slider::getName()
{
	return name;
}

void Slider::changeTexture(std::string newTexture)
{
	texture = parentEngine->getTextureHandler()->loadTexture(newTexture);
}

void Slider::setVisible(bool visible)
{
	this->visible = visible;
}
