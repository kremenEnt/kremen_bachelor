#pragma once
#include "InputComponent.h"
#include "Actor.h"
#include "Engine.h"
#include "Message.h"
#include "PhysicsComponent.h"

class InputClone final : public InputComponent
{
public:
	InputClone(ActorData::ComponentData* componentData)
		: InputComponent(componentData)
	{

	}

	void update(float deltatime) override
	{
		if (!parentEngine->getJoining())
		{
			glm::vec2 desiredSpeed = { 0,0 };
			float speed = 4.f;

			defaultInputValues.d ? desiredSpeed.x += speed : desiredSpeed.x += 0;
			defaultInputValues.a ? desiredSpeed.x -= speed : desiredSpeed.x += 0;
			defaultInputValues.w ? desiredSpeed.y += speed : desiredSpeed.y += 0;
			defaultInputValues.s ? desiredSpeed.y -= speed : desiredSpeed.y += 0;

			dynamic_cast<PhysicsComponent*>(actor->getComponent("PhysicsComponent"))->applySpeed(desiredSpeed);

			if (defaultInputValues.space)
			{
				dynamic_cast<PhysicsComponent*>(actor->getComponent("PhysicsComponent"))->applyLinearImpulse({ 0.f, 2.3f });
			}
		}
	}

	void receiveMessage(const Message & message) override
	{
		if (message.getSenderType() == typeid(EventHandler))
		{
			if (static_cast<const char*>(message.getVariable(0)) == "keyDown")
			{
				int number = message.getVariable(1);
				switch (number)
				{
				case SDLK_w:
					defaultInputValues.w = true;
					break;
				case SDLK_s:
					defaultInputValues.s = true;
					break;
				case SDLK_a:
					defaultInputValues.a = true;
					break;
				case SDLK_d:
					defaultInputValues.d = true;
					break;
				case SDLK_SPACE:
					defaultInputValues.space = true;
					break;
				default:
					break;
				}
				return;
			}
			if (static_cast<const char*>(message.getVariable(0)) == "keyUp")
			{
				int number = message.getVariable(1);
				switch (number)
				{
				case SDLK_w:
					defaultInputValues.w = false;
					break;
				case SDLK_s:
					defaultInputValues.s = false;
					break;
				case SDLK_a:
					defaultInputValues.a = false;
					break;
				case SDLK_d:
					defaultInputValues.d = false;
					break;
				case SDLK_SPACE:
					defaultInputValues.space = false;
					break;
				default:
					break;
				}
				return;
			}
		}
	}

private:
	bool init;
	bool wDown;
	bool sDown;
	bool aDown;
	bool dDown;
	bool spaceDown;
};
