#include "Grid.h"
#include <iostream>


Grid::Grid(int width, int height)
	: MAXWIDTH(width), MAXHEIGHT(height)
{
}


Grid::~Grid()
{
}

void Grid::fillMap()
{
	for (int i = 0; i < MAXWIDTH; i++)
	{
		std::vector<Grid::Tile> rows;
		for (int j = 0; j < MAXHEIGHT; j++)
		{
			Grid::Tile tile;
			tile.type = GROUND;
			if (rand() % 5 == 0)	//fill map with random walls
			{
				tile.type = WALL;	
			}
			tile.pos = { i, j };
			rows.push_back(tile);
		}
		grid.push_back(rows);
	}
}

void Grid::setTile(const int type, const krem::Vector2i pos)
{
	if (pos.x < 0 || pos.x >= MAXWIDTH || pos.y < 0 || pos.y >= MAXHEIGHT)	//out of bounds
	{
		std::cout << "Tile is out of bounds. Pos(" << pos.x << ", " << pos.y << ")\n";
		return;
	}
	this->grid[pos.x][pos.y].type = type;
}

Grid::Tile* Grid::getTile(const krem::Vector2i pos)
{
	if (pos.x < 0 || pos.x >= MAXWIDTH || pos.y < 0 || pos.y >= MAXHEIGHT)	//out of bounds
	{
		return nullptr;
	}
	return &this->grid[pos.x][pos.y];
}
