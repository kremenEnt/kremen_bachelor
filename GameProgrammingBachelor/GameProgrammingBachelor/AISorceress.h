#pragma once
#include "AIComponent.h"
#include "Actor.h"
#include "Engine.h"
#include "Message.h"
#include "PhysicsComponent.h"

class AISorceress final : public AIComponent
{
public:
	AISorceress(ActorData::ComponentData* componentData)
		: AIComponent(componentData),
		direction(-1)
	{

	}
	void update(float deltatime) override
	{

		if (!init)
		{
			physicsSibling = dynamic_cast<PhysicsComponent*>(actor->getComponent("PhysicsComponent"));
			xSpawnPosition = actor->getPosition().x;
			init = true;
		}

		glm::vec2 desiredSpeed = { 0,0 };
		if (actor->getPosition().x - xSpawnPosition > 5)
		{
			this->direction = -1;
		}

		if (actor->getPosition().x - xSpawnPosition < -5)
		{
			this->direction = 1;
		}

		if (physicsSibling->getSensorContacts().wallLeft > 0)
		{
			xSpawnPosition = actor->getPosition().x + 5.1f;
		}
		if (physicsSibling->getSensorContacts().wallRight > 0)
		{
			xSpawnPosition = actor->getPosition().x - 5.1f;
		}

		desiredSpeed.x = 5.f * direction;

		physicsSibling->applySpeed(desiredSpeed);
	}
private:
	float xSpawnPosition;
	glm::vec3* positionPointer;
	PhysicsComponent* physicsSibling;
	bool init;
	int direction;
};
