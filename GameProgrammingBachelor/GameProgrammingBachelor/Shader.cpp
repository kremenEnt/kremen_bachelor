#include "Shader.h"
#include <fstream>
#include <iostream>
#include "Transform.h"

#ifdef __ANDROID__
Shader::Shader(const std::string & fileName, krem::android::AssetManager * assetmgr)
#else
Shader::Shader(const std::string & fileName)
#endif
{
	program = glCreateProgram();
#ifdef __ANDROID__
	shaders[VERT] = createShader(getShaderString(fileName + ".vs", assetmgr), GL_VERTEX_SHADER);
	shaders[FRAG] = createShader(getShaderString(fileName + ".fs", assetmgr), GL_FRAGMENT_SHADER);
#else
	shaders[VERT] = createShader(loadShader(fileName + ".vs"), GL_VERTEX_SHADER);
	shaders[FRAG] = createShader(loadShader(fileName + ".fs"), GL_FRAGMENT_SHADER);
#endif
	for (unsigned int i = 0; i < NUM_SHADERS; i++)
	{
		glAttachShader(program, shaders[i]);
	}

	//glBindAttribLocation(program, 0, "position");
	//glBindAttribLocation(program, 1, "texCoords");
	//glBindAttribLocation(program, 2, "normal");

	glLinkProgram(program);
	checkShaderError(program, GL_LINK_STATUS, true, "Error: Program linking failed: ");

	glValidateProgram(program);
	checkShaderError(program, GL_VALIDATE_STATUS, true, "Error: Program is invalid: ");

	uniforms[U_TRANSFORM] = glGetUniformLocation(program, "transform");	//view * model matrix
	uniforms[U_VIEW] = glGetUniformLocation(program, "viewMatrix");
	uniforms[U_SIZE] = glGetUniformLocation(program, "size");
	uniforms[U_SPRITE_NO] = glGetUniformLocation(program, "spriteNo");
	uniforms[U_LIGHT_POS] = glGetUniformLocation(program, "lightPosition");
	uniforms[U_TEXTURE0] = glGetUniformLocation(program, "texture0");
	uniforms[U_TEXTURE1] = glGetUniformLocation(program, "texture1");
	uniforms[U_TEXTURE2] = glGetUniformLocation(program, "texture2");
	uniforms[U_SCALE] = glGetUniformLocation(program, "scale");
	uniforms[U_COLOR] = glGetUniformLocation(program, "colors");
}

Shader::~Shader()
{
	for (unsigned int i = 0; i < NUM_SHADERS; i++)
	{
		glDetachShader(program, shaders[i]);
		glDeleteShader(shaders[i]);
	}
	glDeleteProgram(program);
}

GLuint Shader::createShader(const std::string & text, GLenum shaderType)
{
	GLuint shader = glCreateShader(shaderType);

	if (shader == 0)
	{
		std::cerr << "Error: Shader creation failed!" << std::endl;
	}

	const GLchar* shaderSourceStrings[1];
	GLint shaderSourceStringsLengths[1];

	shaderSourceStrings[0] = text.c_str();
	shaderSourceStringsLengths[0] = text.length();

	glShaderSource(shader, 1, shaderSourceStrings, shaderSourceStringsLengths);
	glCompileShader(shader);
	checkShaderError(shader, GL_COMPILE_STATUS, false, "Error: Shader compilation failed: ");
	return shader;
}

void Shader::bind()
{
	glUseProgram(program);
}

void Shader::loadTransform(const Transform& transform, const glm::mat4 viewProjection)
{
	glm::mat4 model = viewProjection * transform.getModel();
	glUniformMatrix4fv(uniforms[U_TRANSFORM], 1, GL_FALSE, &model[0][0]);
}

void Shader::loadMat4(int location, glm::mat4 matrix)
{
	glUniformMatrix4fv(uniforms[location], 1, GL_FALSE, &matrix[0][0]);
}

void Shader::loadInt(int location, int integer)
{
	glUniform1i(uniforms[location], integer);
}

void Shader::loadFloat(int location, float floating)
{
	glUniform1f(uniforms[location], floating);
}

void Shader::loadVec4(int location, glm::vec4 vector)
{
	glUniform4f(uniforms[location], vector.x, vector.y, vector.z, vector.w);
}

void Shader::loadVec2(int location, glm::vec2 vector)
{
	glUniform2f(uniforms[location], vector.x, vector.y);
}


std::string Shader::loadShader(const std::string& filename)
{
	std::ifstream file;
	file.open(filename.c_str());
	std::string output = "";
	std::string line;
	if (file.is_open())
	{
		while (file.good())
		{
			getline(file, line);
			output.append(line + "\n");
		}
	}
	else
	{
		std::cerr << "Unable to load shader: " << filename << std::endl;
	}
	return output;
}

void Shader::checkShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string& errorMessage)
{
	GLint success = 0;
	GLchar error[1024] = { 0 };
	if (isProgram)
	{
		glGetProgramiv(shader, flag, &success);
	}
	else
	{
		glGetShaderiv(shader, flag, &success);
	}
	if (success == GL_FALSE)
	{
		if (isProgram)
		{
			glGetProgramInfoLog(shader, sizeof(error), NULL, error);
		}
		else
		{
			glGetShaderInfoLog(shader, sizeof(error), NULL, error);
		}
		std::cerr << errorMessage << ": '" << error << "'" << std::endl;
	}
}

#ifdef __ANDROID__
const std::string Shader::getShaderString(std::string path, krem::android::AssetManager * assetmgr)
{
	krem::android::AssetFile AF = assetmgr->open(path);


	std::vector<char> tempBuffer;

	tempBuffer = AF.readAll();

	std::string returnString = "";
	for (auto it : tempBuffer)
	{
		returnString += it;
	}
	return returnString;
}
#endif