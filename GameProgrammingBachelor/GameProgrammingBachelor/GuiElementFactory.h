#pragma once
#include <map>
#include <string>
#include "GuiElementCreator.h"

/**
 * @class	GuiElementFactory
 *
 * @brief	A graphical user interface element factory.
 */
class GuiElementFactory
{
public:

	/**
	 * @fn	GuiElementCreator* GuiElementFactory::getCreator(const std::string& elementType);
	 *
	 * @brief	Gets a creator.
	 *
	 * @param	elementType	Type of the element.
	 *
	 * @return	null if it fails, else the creator.
	 */
	GuiElementCreator* getCreator(const std::string& elementType);

	/**
	 * @fn
	 * void GuiElementFactory::registerIt(const std::string& elementType, GuiElementCreator* guiElementCreator);
	 *
	 * @brief	Registers the iterator.
	 *
	 * @param	elementType				 	Type of the element.
	 * @param [in,out]	guiElementCreator	If non-null, the graphical user interface element creator.
	 */
	void registerIt(const std::string& elementType, GuiElementCreator* guiElementCreator);

private:
	std::map<std::string, GuiElementCreator*> table;
};