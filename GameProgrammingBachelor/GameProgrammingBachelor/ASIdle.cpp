#include "ASIdle.h"
#include "Actor.h"
#include "PhysicsComponent.h"
#include "AnimationComponent.h"

int ASIdle::update()
{
	//printf("idle\n");

	if (physicsComponent->getVelocity().x != 0 && physicsComponent->getSensorContacts().jump > 0)
	{
		return AS_WALKING;
	}
	if (physicsComponent->getSensorContacts().jump > 0)
	{
		return AS_IDLE;
	}
	if (physicsComponent->getVelocity().y > 0)
	{
		return AS_RISING;
	}
	if (physicsComponent->getVelocity().y < 0)
	{
		return AS_DROPPING;
	}
	return AS_IDLE;
}

void ASIdle::enter()
{
	if (animationComponent != nullptr)
	{
		animationComponent->changeAnimation(IDLE);
	}
}

void ASIdle::leave()
{
}
