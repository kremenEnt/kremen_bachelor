#pragma once
#include "IActorState.h"
#include "ASDefault.h"


class ASRising : public IActorState, public ASDefault
{
public:
	ASRising() {};
	ASRising(Actor* actor) : ASDefault(actor) {};
	virtual int update() override;
	virtual void enter() override;
	virtual void leave() override;
};