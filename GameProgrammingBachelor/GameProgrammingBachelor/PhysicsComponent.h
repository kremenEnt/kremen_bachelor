#pragma once
#include "IPhysicsComponent.h"
#include <math.h>

class PhysicsHandler;
class GraphicsComponent;
class b2Fixture;
class  b2Body;

struct SensorContacts
{
	int jump;
	int wallLeft;
	int wallRight;
};

/**
 * @class	PhysicsComponent
 *
 * @brief	The physics component.
 * @details	
 */
class PhysicsComponent : public IPhysicsComponent
{
private:

	/**
	 * @fn	virtual void PhysicsComponent::init() override;
	 *
	 * @brief	S this object.
	 */
	virtual void init() override;
public:

	/**
	 * @fn	PhysicsComponent::PhysicsComponent(ActorData::ComponentData* componentData);
	 *
	 * @brief	Constructor.
	 *
	 * @param [in,out]	componentData	If non-null, information describing the component.
	 */
	PhysicsComponent(ActorData::ComponentData* componentData);

	/**
	 * @fn	PhysicsComponent::~PhysicsComponent() override;
	 *
	 * @brief	Destructor.
	 */
	~PhysicsComponent() override;

	/**
	 * @fn	virtual void PhysicsComponent::update(float deltaTime) override;
	 *
	 * @brief	Updates the given deltaTime.
	 *
	 * @param	deltaTime	The delta time.
	 */
	virtual void update(float deltaTime) override;

	/**
	 * @fn	void PhysicsComponent::setPosition(glm::vec3 position);
	 *
	 * @brief	Sets a position.
	 *
	 * @param	position	The position.
	 */
	void setPosition(glm::vec3 position);

	/**
	 * @fn	glm::vec3 PhysicsComponent::getPosition();
	 *
	 * @brief	Gets the position.
	 *
	 * @return	The position.
	 */
	glm::vec3 getPosition();

	/**
	 * @fn	void PhysicsComponent::setRotation(float angle);
	 *
	 * @brief	Sets a rotation.
	 *
	 * @param	angle	The angle.
	 */
	void setRotation(float angle);

	/**
	 * @fn	void PhysicsComponent::setVelocity(glm::vec3 velocity);
	 *
	 * @brief	Sets a velocity.
	 *
	 * @param	velocity	The velocity.
	 */
	void setVelocity(glm::vec3 velocity);

	/**
	 * @fn	float PhysicsComponent::getRotation();
	 *
	 * @brief	Gets the rotation.
	 *
	 * @return	The rotation.
	 */
	float getRotation();

	/**
	 * @fn	glm::vec2 PhysicsComponent::getVelocity();
	 *
	 * @brief	Gets the velocity.
	 *
	 * @return	The velocity.
	 */
	glm::vec2 getVelocity();

	/**
	 * @fn	void PhysicsComponent::applySpeed(glm::vec2 desiredSpeed);
	 *
	 * @brief	Applies the speed described by desiredSpeed.
	 *
	 * @param	desiredSpeed	The desired speed.
	 */
	void applySpeed(glm::vec2 desiredSpeed);

	/**
	 * @fn	void PhysicsComponent::applyLinearImpulse(glm::vec2 impulse);
	 *
	 * @brief	Applies the linear impulse described by impulse.
	 *
	 * @param	impulse	The impulse.
	 */
	void applyLinearImpulse(glm::vec2 impulse);

	/**
	* @fn	b2Fixture* applyFixture(krem::Vector2f size, krem::Vector2f relativePosition, void* fixturename, float angle = 0, bool sensor = false)
	*
	* @brief	Creates a new fixture which is applied to the body.
	* @return	Returns the created fixture
	*/
	b2Fixture* applyFixture(krem::Vector2f size, krem::Vector2f relativePosition, void* fixturename, float angle = 0, bool sensor = false);

	/**
	 * @fn	void PhysicsComponent::startContact(void* sensorType);
	 *
	 * @brief	Starts a contact.
	 *
	 * @param [in,out]	sensorType	If non-null, type of the sensor.
	 */
	void startContact(void* sensorType);

	/**
	 * @fn	void PhysicsComponent::endContact(void* sensorType);
	 *
	 * @brief	Ends a contact.
	 *
	 * @param [in,out]	sensorType	If non-null, type of the sensor.
	 */
	void endContact(void* sensorType);

	/**
	 * @fn	std::string PhysicsComponent::getActorName();
	 *
	 * @brief	Gets actor name.
	 *
	 * @return	The actor name.
	 */
	std::string getActorName();
	virtual void onCollision(int fixtureUserData, Actor* collidedActor) {}// = 0;
	virtual void endCollision(int fixtureUserData, Actor* collidedActor) {}// = 0;

	/**
	 * @fn	bool PhysicsComponent::wallCollision();
	 *
	 * @brief	Determines if we can wall collision.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool wallCollision();

	/**
	 * @fn	Actor* PhysicsComponent::getactor();
	 *
	 * @brief	Gets the getactor.
	 *
	 * @return	null if it fails, else a pointer to an Actor.
	 */
	Actor* getactor();

	/**
	 * @fn	b2Body* PhysicsComponent::getB2Body();
	 *
	 * @brief	Gets b 2 body.
	 *
	 * @return	null if it fails, else the b 2 body.
	 */
	b2Body* getB2Body();

	/**
	 * @fn	void PhysicsComponent::setB2Body(b2Body* body);
	 *
	 * @brief	Sets b 2 body.
	 *
	 * @param [in,out]	body	If non-null, the body.
	 */
	void setB2Body(b2Body* body);

	/**
	 * @fn	PhysicsDefinition PhysicsComponent::getPhysicsDefinition();
	 *
	 * @brief	Gets physics definition.
	 *
	 * @return	The physics definition.
	 */
	PhysicsDefinition getPhysicsDefinition();

	/**
	 * @fn	SensorContacts PhysicsComponent::getSensorContacts();
	 *
	 * @brief	Gets sensor contacts.
	 *
	 * @return	The sensor contacts.
	 */
	SensorContacts getSensorContacts();
protected:
	SensorContacts sensorContacts;
	GraphicsComponent* graphicsComponent;
	PhysicsDefinition physicsDefinition;
	PhysicsHandler *physicsHandler;
	b2Body* body;
	float defaultForce;
	float forceScale;
};