#pragma once
#include "ActorComponent.h"
#include "Receiver.h"

class Message;

/**
* @class	InputComponent
*
* @brief	The input component interface. Primarily used by players and items
*/
class InputComponent : public ActorComponent, public IReceiver
{
public:
	InputComponent(ActorData::ComponentData* componentData);
	virtual ~InputComponent();

	/**
	 * @fn	virtual void InputComponent::init() override;
	 *
	 * @brief	S this object.
	 */
	virtual void init() override;

	/**
	 * @fn	virtual void InputComponent::receiveMessage(const Message &message)
	 *
	 * @brief	Receive message.
	 *
	 * @param	message	The message.
	 */
	virtual void receiveMessage(const Message &message) {};

	/**
	 * @fn	virtual void InputComponent::receiveNetworkInput(InputValues receivedInput);
	 *
	 * @brief	Receive network input.
	 *
	 * @param	receivedInput	The received input.
	 */
	virtual void receiveNetworkInput(InputValues receivedInput);

	/**
	 * @fn	virtual InputValues InputComponent::getInputValues();
	 *
	 * @brief	Gets input values.
	 *
	 * @return	The input values.
	 */
	virtual InputValues getInputValues();
protected:
	//default input values used by the engine. only these values will be updated by network. needs improvement
	InputValues defaultInputValues;
	ActorData::Input componentData;
	glm::vec3 mousePos;
	bool inputChanged = false;
};