#pragma once

#include "GraphicsComponent.h"
#include "AnimationComponent.h"
#include "ParticleComponent.h"
#include "PhysicsComponent.h"
#include "AudioComponent.h"
#include "InputComponent.h"
#include "AIComponent.h"
#include "CombatComponent.h"