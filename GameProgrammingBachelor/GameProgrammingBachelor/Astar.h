#pragma once
#include "common.h"
#include "Node.h"
#include "Grid.h"
#include <iostream>
#include <algorithm>
#include <vector>
#include <memory>

/**
 * @class	Astar
 *
 * @brief	An astar.
 */
class Astar
{
public:

	/**
	 * @fn	Astar::Astar();
	 *
	 * @brief	Default constructor.
	 */
	Astar();

	/**
	 * @fn	Astar::~Astar();
	 *
	 * @brief	Destructor.
	 */
	~Astar();

	/**
	 * @fn	std::vector<Node*> Astar::findPath(krem::Vector2i start, krem::Vector2i goal, Grid& world);
	 *
	 * @brief	Searches for the first path.
	 *
	 * @param	start		 	The start.
	 * @param	goal		 	The goal.
	 * @param [in,out]	world	The world (grid).
	 *
	 * @return	null if it fails, else the found path.
	 */
	std::vector<Node*> findPath(krem::Vector2i start, krem::Vector2i goal, Grid& world);

	/**
	 * @fn	void Astar::test(Grid* grid, int length);
	 *
	 * @brief	Tests the path and visualize it.
	 *
	 * @param [in,out]	grid	If non-null, the grid.
	 * @param	length			The length.
	 */
	void test(Grid* grid, int length);
private:
	bool vectorInList(std::vector<Node*> list, krem::Vector2i vector);

	std::vector<std::unique_ptr<Node>>nodes;
};

