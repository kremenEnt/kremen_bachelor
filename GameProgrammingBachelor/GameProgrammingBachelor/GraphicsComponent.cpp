#include "GraphicsComponent.h"
#include "Engine.h"

#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "Camera.h"
#include "MeshHandler.h"
#include "TextureHandler.h"
#include "ShaderHandler.h"
#include "GraphicsHandler.h"
#include "Actor.h"

GraphicsComponent::GraphicsComponent(ActorData::ComponentData* componentData):
	IGraphicsComponent(componentData), color(1.0, 1.0, 1.0, 1.0)
{

}

GraphicsComponent::~GraphicsComponent()
{

}

void GraphicsComponent::init()
{
	setPosition(actor->getPosition());
	this->mesh = this->parentEngine->getMeshHandler()->loadModel(this->componentData.meshPath);
	this->texture = this->parentEngine->getTextureHandler()->loadTexture(this->componentData.texturePath);
	this->shader = this->parentEngine->getShaderHandler()->loadShader(componentData.shaderPath);
	
	this->transform.getScale().x = componentData.size.x;
	this->transform.getScale().y = componentData.size.y;
	this->transform.getScale().z = componentData.size.z;
	this->modelMatrix = transform.getModel();

}

void GraphicsComponent::draw(const glm::mat4& viewProjection)
{
	glm::vec3 camPos = parentEngine->getGraphicsHandler()->getCamera()->getPosition();
	int offset = 50;	//blocks to draw away from camera. fix this somewhat flexible
	bool inside = true;

	//AABB to reduce amount of draw calls
	if (transform.getPos().x > camPos.x + offset)
	{
		inside = false;
	}
	else if (transform.getPos().x < camPos.x - offset)
	{
		inside = false;
	}
	else if (transform.getPos().y > camPos.y + offset)
	{
		inside = false;
	}
	else if (transform.getPos().y < camPos.y - offset)
	{
		inside = false;
	}

	if (inside)
	{
		shader->bind();
		shader->loadVec2(U_SIZE, glm::vec2(componentData.noSpriteFrames.x, componentData.noSpriteFrames.y));

		if (componentData.animated)
		{
			shader->loadInt(U_SPRITE_NO, anim.getFrame());
		}
		else
		{
			shader->loadInt(U_SPRITE_NO, (static_cast<int>(tilePosition.x + (tilePosition.y*componentData.noSpriteFrames.x))));
		}
		shader->loadVec4(U_COLOR, this->color);
		shader->loadMat4(U_TRANSFORM, (viewProjection * modelMatrix));
		texture->bind(0);
		mesh->draw();
	}
}

void GraphicsComponent::update(float deltaTime)
{
	this->anim.update(deltaTime);
}

void GraphicsComponent::setPosition(glm::vec3 position)
{
	this->transform.setPos(position);
	this->modelMatrix = transform.getModel();
}

void GraphicsComponent::setScale(glm::vec3 scale)
{
	this->transform.setScale(scale);
	this->modelMatrix = transform.getModel();
}

void GraphicsComponent::setTilePosition(glm::vec2 tilePosition)
{
	this->tilePosition = tilePosition;
}

void GraphicsComponent::setTexture(Texture * texture)
{
	
	this->texture = texture;
}

void GraphicsComponent::setTexture(std::string texturePath)
{
	this->texture = this->parentEngine->getTextureHandler()->loadTexture(texturePath);
}

void GraphicsComponent::setRotation(float angle, char axis)
{
	if (axis == 'x')
	{
		transform.getRot().x = angle;
	}
	else if (axis == 'y')
	{
		transform.getRot().y = angle;
	}
	else if (axis == 'z')
	{
		transform.getRot().z = angle;
	}
	else
	{
		printf("Invalid axis: %c\n", axis);
	}
	this->modelMatrix = transform.getModel();
}

void GraphicsComponent::setFrames(glm::vec2 frames)
{
	this->xFrames = frames.x;
	this->yFrames = frames.y;
}

void GraphicsComponent::updateSpritePositions(ActorData::Animation::AnimationSequence animationSequence)
{
	anim.config(animationSequence.animationSpeed, animationSequence.amountOfFrames, animationSequence.firstFrame);
}

glm::vec3 GraphicsComponent::getPosition()
{
	return this->transform.getPos();
}

Texture* GraphicsComponent::getTexture()
{
	return texture;
}

void GraphicsComponent::setColor(glm::vec4 color)
{
	this->color = color;
}
