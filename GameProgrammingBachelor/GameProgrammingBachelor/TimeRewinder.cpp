#include "TimeRewinder.h"
#include "Engine.h"


TimeRewinder::TimeRewinder()
{

}


TimeRewinder::~TimeRewinder()
{
}

void TimeRewinder::init()
{
	isRewinding = false;
	indexValue = 0;
	numberOfActors = 0;
	vectorLimit = 500;
	//finds all the actors currently loaded in the actor class
	actorVector = parentEngine->getActorFactory()->getActors();
	for (std::size_t i = 0; i < actorVector.size(); i++)
	{
		savedFrames.emplace_back();
		savedFrames[i].setFrameSpawned(-1);
		numberOfActors++;
	}
}
//should probably be split up a bit
void TimeRewinder::update()
{
	PhysicsComponent* actorPhysicsComponent;

	//rewinds the actors until timer is up or rewind button is released
	if (isRewinding)
	{
		if (timer.getTicks() < static_cast<Uint32>(timeLimitInSeconds))
		{
			rewind();
		}
		else
		{
			timer.stop();
			isRewinding = false;
		}
	}
	
	//save actors properties every frame
	if (!isRewinding)
	{
		timer.start(); //resets the timerewind timer 
		for (std::size_t i = 0; i < actorVector.size(); i++)
		{
			actorPhysicsComponent = dynamic_cast<PhysicsComponent*>(actorVector[i]->getComponent("PhysicsComponent"));

			savedFrames[i].setPosition(actorPhysicsComponent->getPosition());
			savedFrames[i].setRotation(actorPhysicsComponent->getRotation());
			savedFrames[i].setVelocity(actorPhysicsComponent->getVelocity());
		}
	
		//check to see if there is any new actors spawned on runtime so they can have their frames saved 
		actorVector = parentEngine->getActorFactory()->getActors();
		if (static_cast<std::size_t>(numberOfActors) < actorVector.size())
		{
			int newActors = actorVector.size() - numberOfActors;
			for (int i = 0; i < newActors; i++)
			{
				savedFrames.emplace_back();
				for (int i = 0; i <= indexValue; i++)
				{
					actorPhysicsComponent = dynamic_cast<PhysicsComponent*>(actorVector[numberOfActors]->getComponent("PhysicsComponent"));

					savedFrames[numberOfActors].setPosition(actorPhysicsComponent->getPosition());
					savedFrames[numberOfActors].setRotation(actorPhysicsComponent->getRotation());
					savedFrames[numberOfActors].setVelocity(actorPhysicsComponent->getVelocity());
					
				}
				savedFrames.back().setFrameSpawned(indexValue);
				numberOfActors++;
			}
		}
	}

	//increase the index every frame
	if (indexValue < vectorLimit && !isRewinding)
	{
		indexValue++;
	}
	//removes the first frame currently saved because of max limit
	if (indexValue >= vectorLimit && !isRewinding)
	{
		for (std::size_t i = 0; i < actorVector.size(); i++)
		{
			savedFrames[i].removeTimeFrame(0);
			savedFrames[i].MoveFrameSpawned();
			
		}
		indexValue--;
	}
}

void TimeRewinder::rewind()
{
	PhysicsComponent* actorPhysicsComponent;
	if (indexValue > 0)
	{
		indexValue--;
		//set actors back to a saved frames property and removes the saved frame
		for (std::size_t i = 0; i < actorVector.size(); i++)
		{
			actorPhysicsComponent = dynamic_cast<PhysicsComponent*>(actorVector[i]->getComponent("PhysicsComponent"));
			actorPhysicsComponent->setPosition(savedFrames[i].getPosition(indexValue));
			actorPhysicsComponent->setRotation(savedFrames[i].getRotation(indexValue));
			//actorPhysicsComponent->setVelocity({ savedFrames[i].getVelocity(indexValue),0 }); //TODO: eidsv�ll fix this line when you fix timerewinder
			if (indexValue > savedFrames[i].getFrameSpawned())
			{
				savedFrames[i].removeTimeFrame(indexValue);
			}
			else
			{
				parentEngine->getActorFactory()->deleteActor(actorVector[i]);
				actorVector.erase(actorVector.begin() + i);
				savedFrames.erase(savedFrames.begin() + i);
				numberOfActors--;
			}
		}
		
	}
}

void TimeRewinder::setStatus(bool status)
{
	isRewinding = status;
}

void TimeRewinder::setParent(Engine& parentPtr)
{
	this->parentEngine = &parentPtr;
}

Engine* TimeRewinder::getParent()
{
	return parentEngine;
}

float TimeRewinder::getTimer()
{
	return timer.getTicks();
}
