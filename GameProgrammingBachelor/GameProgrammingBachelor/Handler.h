#pragma once
class Engine;
/**
 * @class	Handler
 *
 * @brief	The base class for some handlers(hopefully all in the future).
 * @details Has basic funtions and variables which all handlers need to operate.
 */
class Handler
{
public:

	/**
	 * @fn	Handler::Handler()
	 *
	 * @brief	Default constructor.
	 */
	Handler() {};

	/**
	 * @fn	Handler::~Handler()
	 *
	 * @brief	Destructor.
	 */
	~Handler() {};

	/**
	 * @fn	virtual void Handler::update(float deltaSeconds)
	 *
	 * @brief	Updates the given deltaSeconds.
	 *
	 * @param	deltaSeconds	The delta in seconds.
	 */
	virtual void update(float deltaSeconds) {};

	/**
	 * @fn	void Handler::setParent(Engine& parentPtr);
	 *
	 * @brief	Sets a parent.
	 *
	 * @param [in,out]	parentPtr	The parent pointer.
	 */
	void setParent(Engine& parentPtr);

	/**
	 * @fn	Engine* Handler::getParent()
	 *
	 * @brief	Gets the parent of this item.
	 *
	 * @return	null if it fails, else the parent.
	 */
	Engine* getParent() {};
protected:
	Engine* parentEngine;
private:
};
