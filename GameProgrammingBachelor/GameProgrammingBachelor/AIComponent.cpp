#include "AIComponent.h"
#include <string>
#include <iostream>


AIComponent::AIComponent(ActorData::ComponentData* componentData)
{
	this->componentData = *static_cast<ActorData::ArtificialIntelligence*>(componentData);
}

AIComponent::~AIComponent()
{
	if (luaState)
	{
		lua_close(luaState);
	}
}

void AIComponent::init()
{
	if (componentData.scriptPath != "")
	{
		// Create a new state
		luaState = luaL_newstate();
		// Register Standard Libraries for LUA use
		luaL_openlibs(luaState);

		// Load the program
		if (luaL_loadfile(luaState, componentData.scriptPath.c_str()))
		{
			printf("error: %s", lua_tostring(luaState, -1));
		}
		lua_pcall(luaState, 0, 0, 0);
	}
}

void AIComponent::update(float deltatime)
{
}