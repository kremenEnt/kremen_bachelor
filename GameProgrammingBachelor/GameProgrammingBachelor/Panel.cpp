#include "Panel.h"
#include "Engine.h"
#include "GuiElement.h"
#include "MeshHandler.h"
#include "ShaderHandler.h"
#include "TextureHandler.h"

Panel::Panel(GuiData::ElementData* elementData) : GuiElement(elementData)
{
	this->elementData = *dynamic_cast<GuiData::Panel*>(elementData);
}

Panel::~Panel()
{
}

void Panel::draw(glm::mat4 &viewProjection)
{
	shader->bind();
	shader->loadTransform(transform, viewProjection);
	shader->loadInt(U_TEXTURE0, 0);
	shader->loadFloat(U_SCALE, 1.5f);
	texture->bind(0);

	mesh->draw();
}

void Panel::init()
{
	float tempPosX;
	float tempPosY;
	float tempWidth;
	float tempHeight;

	name = elementData.name;
	tempPosX = elementData.positionX;
	tempPosY = elementData.positionY;
	tempWidth = elementData.width;
	tempHeight = elementData.height;
	tempHeight = (tempHeight / aspectRatio.y)*aspectRatio.x;
	visible = elementData.visible;
	mesh = parentEngine->getMeshHandler()->loadModel(elementData.meshPath);
	texture = parentEngine->getTextureHandler()->loadTexture(elementData.texturePath);
	shader = parentEngine->getShaderHandler()->loadShader(elementData.shaderPath);

	transform.setPos(glm::vec3(tempPosX, tempPosY, 0.997f));
	transform.setScale(glm::vec3(tempWidth, tempHeight, 1.0f));
}

void Panel::changeTexture(std::string texturePath)
{
	texture = parentEngine->getTextureHandler()->loadTexture(texturePath);
}

std::string Panel::getName()
{
	return name;
}

void Panel::setVisible(bool visible)
{
	this->visible = visible;
}
