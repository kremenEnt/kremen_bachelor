#include "ActorData.h"
#include "Actor.h"

ActorData::ActorData()
{
}

ActorData::Animation::AnimationSequence loadAnimation(tinyxml2::XMLElement* animationRoot);

ActorData::ActorData(std::string xmlPath):
netSyncLevel(NETLEVEL_POSITION),
category("")
{
	tinyxml2::XMLDocument* xmlDocument = actorXMLHandler->loadXml(xmlPath);
	tinyxml2::XMLElement* rootElement= xmlDocument->FirstChildElement();
	componentsElement = rootElement->FirstChildElement("Components");
	actorType = rootElement->FirstChildElement("Type")->GetText();		
	actorName = rootElement->FirstChildElement("ActorName")->GetText();
	actorSource = xmlPath;
	if (rootElement->FirstChildElement("Category"))
	{
		category = rootElement->FirstChildElement("Category")->GetText();
	}
	if (rootElement->FirstChildElement("NetworkLevel"))
	{
		netSyncLevel = rootElement->FirstChildElement("NetworkLevel")->QueryIntText(&netSyncLevel);
	}
	
	tinyxml2::XMLElement* componentElement;
	//Animation
	if (componentsElement->FirstChildElement("AnimationComponent"))
	{
		Animation animation;
		animation.type = "AnimationComponent";
		Animation::AnimationSequence newSequence;
		componentElement = componentsElement->FirstChildElement("AnimationComponent");
		if (componentElement->FirstChildElement("category"))
		{
			animation.identifier= componentElement->FirstChildElement("category")->GetText();
		}

		if (componentElement->FirstChildElement("idle"))
		{
			animation.animations[IDLE] = loadAnimation(componentElement->FirstChildElement("idle"));
		}
		
		if (componentElement->FirstChildElement("walking"))
		{
			animation.animations[WALKING] = loadAnimation(componentElement->FirstChildElement("walking"));
		}

		if (componentElement->FirstChildElement("death"))
		{
			animation.animations[DEATH] = loadAnimation(componentElement->FirstChildElement("death"));
		}
		if (componentElement->FirstChildElement("jumping"))
		{
			animation.animations[JUMPING] = loadAnimation(componentElement->FirstChildElement("jumping"));
		}
		if (componentElement->FirstChildElement("falling"))
		{
			animation.animations[FALLING] = loadAnimation(componentElement->FirstChildElement("falling"));
		}

		componentDataVector.push_back(std::make_unique<Animation>(animation));
	}
	//AI
	if (componentsElement->FirstChildElement("AIComponent"))
	{
		ArtificialIntelligence artificialIntelligence;
		artificialIntelligence.type = "AIComponent";
		componentElement = componentsElement->FirstChildElement("AIComponent");

		if (componentElement->FirstChildElement("category"))
		{
			artificialIntelligence.identifier = componentElement->FirstChildElement("category")->GetText();
		}

		if (componentElement->FirstChildElement("scriptPath"))
		{
			artificialIntelligence.scriptPath = componentElement->FirstChildElement("scriptPath")->GetText();
		}

		componentDataVector.push_back(std::make_unique<ArtificialIntelligence>(artificialIntelligence));
	}
	//Audio
	if (componentsElement->FirstChildElement("AudioComponent"))
	{
		Audio audio;
		audio.type = "AudioComponent";
		componentElement = componentsElement->FirstChildElement("AudioComponent");
		tinyxml2::XMLElement* itemElement;
		if (componentElement->FirstChildElement("sounds"))
		{
			itemElement = componentElement->FirstChildElement("sounds");
			if (itemElement->FirstChildElement("jumping"))
			{
				audio.soundMap.insert(std::make_pair("jumping", itemElement->FirstChildElement("jumping")->GetText()));
			}

			if (itemElement->FirstChildElement("spawn"))
			{
				audio.soundMap.insert(std::make_pair("spawn", itemElement->FirstChildElement("spawn")->GetText()));
			}

			if (itemElement->FirstChildElement("death"))
			{
				audio.soundMap.insert(std::make_pair("death", itemElement->FirstChildElement("death")->GetText()));
			}

			if (itemElement->FirstChildElement("hit"))
			{
				audio.soundMap.insert(std::make_pair("hit", itemElement->FirstChildElement("hit")->GetText()));
			}

		}

		if (componentElement->FirstChildElement("music"))
		{
			itemElement = componentElement->FirstChildElement("music");
		}

		componentDataVector.push_back(std::make_unique<Audio>(audio));
	}
	//Combat
	if (componentsElement->FirstChildElement("CombatComponent"))
	{
		Combat combat;
		combat.type = "CombatComponent";
		componentElement = componentsElement->FirstChildElement("CombatComponent");

		if (componentElement->FirstChildElement("category"))
		{
			combat.identifier = componentElement->FirstChildElement("category")->GetText();
		}
		if (componentElement->FirstChildElement("health"))
		{
			componentElement->FirstChildElement("health")->QueryIntText(&combat.maxHealth);
		}
		if (componentElement->FirstChildElement("lifeTime"))
		{
			componentElement->FirstChildElement("lifeTime")->QueryFloatText(&combat.lifetime);
		}
		if (componentElement->FirstChildElement("damage"))
		{
			componentElement->FirstChildElement("damage")->QueryIntText(&combat.damage);
		}
		componentDataVector.push_back(std::make_unique<Combat>(combat));

	}
	//Graphics
	if (componentsElement->FirstChildElement("GraphicsComponent"))
	{
		Graphics graphics;
		graphics.type = "GraphicsComponent";
		componentElement = componentsElement->FirstChildElement("GraphicsComponent");
		if (componentElement->FirstChildElement("category"))
		{
			graphics.identifier = componentElement->FirstChildElement("category")->GetText();
		}
		if (componentElement->FirstChildElement("sizeX"))
		{
			componentElement->FirstChildElement("sizeX")->QueryFloatText(&graphics.size.x);
		}
		if (componentElement->FirstChildElement("sizeY"))
		{
			componentElement->FirstChildElement("sizeY")->QueryFloatText(&graphics.size.y);
		}
		if (componentElement->FirstChildElement("sizeZ"))
		{
			componentElement->FirstChildElement("sizeZ")->QueryFloatText(&graphics.size.z);
		}

		if (componentElement->FirstChildElement("xFrames"))
		{
			componentElement->FirstChildElement("xFrames")->QueryIntText(&graphics.noSpriteFrames.x);
		}
		
		if (componentElement->FirstChildElement("yFrames"))
		{
			componentElement->FirstChildElement("yFrames")->QueryIntText(&graphics.noSpriteFrames.y);
		}
		if (componentElement->FirstChildElement("animated"))
		{
			componentElement->FirstChildElement("animated")->QueryBoolText(&graphics.animated);
		}

		if (componentElement->FirstChildElement("meshVerticesPath"))
		{
			graphics.meshPath = componentElement->FirstChildElement("meshVerticesPath")->GetText();
		}

		if (componentElement->FirstChildElement("texturePath"))
		{
			graphics.texturePath = componentElement->FirstChildElement("texturePath")->GetText();
		}

		if (componentElement->FirstChildElement("shader"))
		{
			graphics.shaderPath = componentElement->FirstChildElement("shader")->GetText();
		}

		componentDataVector.push_back(std::make_unique<Graphics>(graphics));
	}

	//Input
	if (componentsElement->FirstChildElement("InputComponent"))
	{
		Input input;
		input.type = "InputComponent";
		componentElement = componentsElement->FirstChildElement("InputComponent");
		if (componentElement->FirstChildElement("category"))
		{
			input.identifier = componentElement->FirstChildElement("category")->GetText();
		}
		componentDataVector.push_back(std::make_unique<Input>(input));
	}

	//Physics
	if (componentsElement->FirstChildElement("PhysicsComponent"))
	{
		Physics physics;
		physics.type = "PhysicsComponent";
		componentElement = componentsElement->FirstChildElement("PhysicsComponent");
		if (componentElement->FirstChildElement("category"))
		{
			physics.identifier = componentElement->FirstChildElement("category")->GetText();
		}
		if (componentElement->FirstChildElement("collisionCategory"))
		{
			physics.definition.collisionCategory = componentElement->FirstChildElement("collisionCategory")->GetText();
		}
		if (componentElement->FirstChildElement("bodyType"))
		{
			componentElement->FirstChildElement("bodyType")->QueryIntText(&physics.definition.bodyType);
		}
		
		if (componentElement->FirstChildElement("sensor"))
		{
			componentElement->FirstChildElement("sensor")->QueryBoolText(&physics.definition.isSensor);
		}
		
		if (componentElement->FirstChildElement("density"))
		{
			componentElement->FirstChildElement("density")->QueryFloatText(&physics.definition.density);
		}
		
		if (componentElement->FirstChildElement("xInPixels"))
		{
			componentElement->FirstChildElement("xInPixels")->QueryFloatText(&physics.definition.size.x);
		}
		
		if (componentElement->FirstChildElement("yInPixels"))
		{
			componentElement->FirstChildElement("yInPixels")->QueryFloatText(&physics.definition.size.y);
		}

		if (componentElement->FirstChildElement("jumpSensor"))
		{
			componentElement->FirstChildElement("jumpSensor")->QueryBoolText(&physics.definition.jumpSensor);
		}

		if (componentElement->FirstChildElement("wallSensor"))
		{
			componentElement->FirstChildElement("wallSensor")->QueryBoolText(&physics.definition.wallSensor);
		}

		if (componentElement->FirstChildElement("restitution"))
		{
			componentElement->FirstChildElement("restitution")->QueryFloatText(&physics.definition.restitution);
		}

		if (componentElement->FirstChildElement("friction"))
		{
			componentElement->FirstChildElement("friction")->QueryFloatText(&physics.definition.friction);
		}

		if (componentElement->FirstChildElement("fixedRotation"))
		{
			componentElement->FirstChildElement("fixedRotation")->QueryBoolText(&physics.definition.fixedRotation);
		}

		if (componentElement->FirstChildElement("gravity"))
		{
			componentElement->FirstChildElement("gravity")->QueryFloatText(&physics.definition.gravity);
		}

		if (componentElement->FirstChildElement("defaultForce"))
		{
			componentElement->FirstChildElement("defaultForce")->QueryFloatText(&physics.desiredSpeed);
		}

		if (componentElement->FirstChildElement("shape"))
		{
			std::string shape;
			shape = componentElement->FirstChildElement("shape")->GetText();

			if (shape == "box")
			{
				physics.definition.shape = physics.definition.BOX;
			}
			else if (shape == "circle")
			{
				physics.definition.shape = physics.definition.CIRCLE;
			}
			else if (shape == "polygon")
			{
				//do more fancy stuff here when needed. just defaults to box for now
				physics.definition.shape = physics.definition.POLYGON;
				int i = 0;
				for (tinyxml2::XMLElement* element = componentElement->FirstChildElement("pointX"); element != NULL; element = element->NextSiblingElement("pointX"), i++)
				{
					glm::vec2 newPhysicsPoint;
					element->QueryFloatText(&newPhysicsPoint.x);
					element->NextSiblingElement("pointY")->QueryFloatText(&newPhysicsPoint.y);
					physics.definition.polygonVertices.emplace_back(newPhysicsPoint);
				}
			}
		}
		componentDataVector.push_back(std::make_unique<Physics>(physics));
	}

	//Particle
	if (componentsElement->FirstChildElement("ParticleComponent"))
	{
		Particle particle;
		particle.type = "ParticleComponent";
		componentElement = componentsElement->FirstChildElement("ParticleComponent");
		
		if (componentElement->FirstChildElement("category"))
		{
			particle.identifier = componentElement->FirstChildElement("category")->GetText();
		}

		if (componentElement->FirstChildElement("amountPerSecond"))
		{
			componentElement->FirstChildElement("amountPerSecond")->QueryFloatText(&particle.particlesPerSecond);
		}

		if (componentElement->FirstChildElement("maxLife"))
		{
			componentElement->FirstChildElement("maxLife")->QueryFloatText(&particle.maxLife);
		}

		if (componentElement->FirstChildElement("velocityX"))
		{
			componentElement->FirstChildElement("velocityX")->QueryFloatText(&particle.velocity.x);
		}

		if (componentElement->FirstChildElement("velocityY"))
		{
			componentElement->FirstChildElement("velocityY")->QueryFloatText(&particle.velocity.y);
		}

		if (componentElement->FirstChildElement("velocityZ"))
		{
			componentElement->FirstChildElement("velocityZ")->QueryFloatText(&particle.velocity.z);
		}

		if (componentElement->FirstChildElement("maxVelocity"))
		{
			componentElement->FirstChildElement("maxVelocity")->QueryFloatText(&particle.maxVelocity);
		}

		if (componentElement->FirstChildElement("size"))
		{
			componentElement->FirstChildElement("size")->QueryFloatText(&particle.size);
		}

		if (componentElement->FirstChildElement("colorR"))
		{
			componentElement->FirstChildElement("colorR")->QueryFloatText(&particle.color.r);
		}

		if (componentElement->FirstChildElement("colorG"))
		{
			componentElement->FirstChildElement("colorG")->QueryFloatText(&particle.color.g);
		}

		if (componentElement->FirstChildElement("colorB"))
		{
			componentElement->FirstChildElement("colorB")->QueryFloatText(&particle.color.b);
		}

		if (componentElement->FirstChildElement("gravity"))
		{
			componentElement->FirstChildElement("gravity")->QueryBoolText(&particle.gravity);
		}

		if (componentElement->FirstChildElement("gravityMultiplier"))
		{
			componentElement->FirstChildElement("gravityMultiplier")->QueryFloatText(&particle.gravityMultiplier);
		}

		if (componentElement->FirstChildElement("texturePath"))
		{
			particle.texturePath = componentElement->FirstChildElement("texturePath")->GetText();
		}
		componentDataVector.push_back(std::make_unique<Particle>(particle));

	}

}

void ActorData::init()
{
	actorXMLHandler = std::make_unique<XmlHandler>();
}

ActorData::Animation::AnimationSequence loadAnimation(tinyxml2::XMLElement * animationRoot)
{
	ActorData::Animation::AnimationSequence returnSequence;
	if (animationRoot->FirstChildElement("firstFrame"))
	{
		animationRoot->FirstChildElement("firstFrame")->QueryIntText(&returnSequence.firstFrame);
	}
	
	if (animationRoot->FirstChildElement("amountOfFrames"))
	{
		animationRoot->FirstChildElement("amountOfFrames")->QueryIntText(&returnSequence.amountOfFrames);
	}
	
	if (animationRoot->FirstChildElement("animationSpeed"))
	{
		animationRoot->FirstChildElement("animationSpeed")->QueryFloatText(&returnSequence.animationSpeed);
	}
	return returnSequence;
}