#include "UserFeedback.h"
#include "Engine.h"
#include "DatabaseHandler.h"
#include "AttemptX.h"

UserFeedback::UserFeedback()
{
	numberOfQuestions = 0;
	maxNumberOfQuestions = 2;
	firstRaitingQuestion = 1;
}

UserFeedback::~UserFeedback()
{
}

void UserFeedback::update()
{
	numberOfQuestions++;
	
	dynamic_cast<Panel*>(parentEngine->getGuiFactory()->getElement("questions"))->changeTexture("res/gui/textures/question" + std::to_string(numberOfQuestions) + ".png");

	if (numberOfQuestions >= maxNumberOfQuestions)
	{
		dynamic_cast<Button*>(parentEngine->getGuiFactory()->getElement("yesbutton"))->setVisible(false);
		dynamic_cast<Button*>(parentEngine->getGuiFactory()->getElement("nobutton"))->setVisible(false);
		dynamic_cast<Button*>(parentEngine->getGuiFactory()->getElement("confirmbutton"))->setVisible(false);
		dynamic_cast<Container*>(parentEngine->getGuiFactory()->getElement("rating"))->setVisible(false);
		dynamic_cast<Panel*>(parentEngine->getGuiFactory()->getElement("questions"))->changeTexture("res/gui/textures/thanks.png");
		numberOfQuestions = 0;
		parentEngine->getAttemptX()->saveResultsToDB(question1);
		//sendToDb();
	}
	else if (numberOfQuestions >= firstRaitingQuestion)
	{
		dynamic_cast<Button*>(parentEngine->getGuiFactory()->getElement("yesbutton"))->setVisible(false);
		dynamic_cast<Button*>(parentEngine->getGuiFactory()->getElement("nobutton"))->setVisible(false);
		dynamic_cast<Button*>(parentEngine->getGuiFactory()->getElement("confirmbutton"))->setVisible(true);
		dynamic_cast<Container*>(parentEngine->getGuiFactory()->getElement("rating"))->setVisible(true);
	}
	else
	{
		dynamic_cast<Button*>(parentEngine->getGuiFactory()->getElement("yesbutton"))->setVisible(true);
		dynamic_cast<Button*>(parentEngine->getGuiFactory()->getElement("nobutton"))->setVisible(true);
		dynamic_cast<Button*>(parentEngine->getGuiFactory()->getElement("confirmbutton"))->setVisible(false);
		dynamic_cast<Container*>(parentEngine->getGuiFactory()->getElement("rating"))->setVisible(false);
	}
}

void UserFeedback::saveAnswer(bool answer)
{
	question1 = answer;
}

void UserFeedback::saveRating(int rating)
{
	this->rating = rating;
	switch (rating)
	{
	case 0:
		dynamic_cast<Container*>(parentEngine->getGuiFactory()->getElement("rating"))->setTexture(1, "res/gui/textures/unrated.png");
		dynamic_cast<Container*>(parentEngine->getGuiFactory()->getElement("rating"))->setTexture(2, "res/gui/textures/unrated.png");
		break;
	case 1:
		dynamic_cast<Container*>(parentEngine->getGuiFactory()->getElement("rating"))->setTexture(1, "res/gui/textures/rated.png");
		dynamic_cast<Container*>(parentEngine->getGuiFactory()->getElement("rating"))->setTexture(2, "res/gui/textures/unrated.png");
		break;
	case 2: 
		dynamic_cast<Container*>(parentEngine->getGuiFactory()->getElement("rating"))->setTexture(1, "res/gui/textures/rated.png");
		dynamic_cast<Container*>(parentEngine->getGuiFactory()->getElement("rating"))->setTexture(2, "res/gui/textures/rated.png");
		break;
	}
}

void UserFeedback::sendToDb()
{
	//parentEngine->getDatabaseHandler()->insertPreperedStatement("INSERT INTO Users(username, age, city) VALUES(? , ? , ? )", static_cast<std::string>("Quad"), 25, static_cast<std::string>("karasjok"));
	//parentEngine->getDatabaseHandler()->insertData("INSERT INTO Users(username, age, city) VALUES('FluFFey', 27 ,'Malvik')");
	//db.retrieveData("SELECT username, age, city FROM Users ORDER BY username ASC");
	//ALTER TABLE userfeedback AUTO_INCREMENT = 1

	TCHAR computerName[MAX_COMPUTERNAME_LENGTH + 1];
	DWORD size = sizeof(computerName) / sizeof(computerName[0]);
	GetComputerName(computerName, &size);

	//std::cout << computerName  << std::endl;
	
	GEOID myGEO = GetUserGeoID(GEOCLASS_NATION);
	int sizeOfBuffer = GetGeoInfo(myGEO, GEO_ISO2, NULL, 0, 0);
	char *countryBuffer = new char[sizeOfBuffer];
	int result = GetGeoInfo(myGEO, GEO_ISO2, countryBuffer, sizeOfBuffer, 0);
	//std::cout << countryBuffer << std::endl;

	std::string tempQuestion;
	if (question1)
	{
		tempQuestion = "Yes";
	}
	else
	{
		tempQuestion = "No";
	}
	rating += 1;
	parentEngine->getDatabaseHandler()->connect();
	parentEngine->getDatabaseHandler()->insertPreperedStatement
		("INSERT INTO userfeedback(country_code, identifier, question1, question2) VALUES(? , ? , ?, ?)",
		static_cast<std::string>(countryBuffer), static_cast<std::string>(computerName), static_cast<std::string>(tempQuestion), rating);
	parentEngine->getDatabaseHandler()->disconnect();
}

void UserFeedback::reset()
{
	numberOfQuestions = 0;
}
