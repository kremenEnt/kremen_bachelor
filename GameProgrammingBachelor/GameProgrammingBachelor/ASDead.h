#pragma once
#include "IActorState.h"
#include "ASDefault.h"

class ASDead : public IActorState, public ASDefault
{
public:
	ASDead() {};
	ASDead(Actor* actor) : ASDefault(actor) {};
	virtual int update() override;
	virtual void enter() override;
	virtual void leave() override;
};