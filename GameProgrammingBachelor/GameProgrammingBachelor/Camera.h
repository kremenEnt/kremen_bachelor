#pragma once

#ifdef __ANDROID__
#include <GLES3/gl31.h>
#else
#include <GL\glew.h>
#endif
#include <glm\glm.hpp>
#include <glm\gtx\transform.hpp>
/**
 * @class	Camera
 *
 * @brief	Camera class to know what to display. Only one, owned by GraphicsHandler
 */
class Camera
{
public:

	/**
	 * @fn	Camera::Camera(const glm::vec3& pos, float fov, float aspect, float zNear, float zFar);
	 *
	 * @brief	Constructor.
	 *
	 * @param	pos   	The position.
	 * @param	fov   	The field of view.
	 * @param	aspect	The aspect ratio.
	 * @param	zNear 	The near distance.
	 * @param	zFar  	The far distance.
	 */
	Camera(const glm::vec3 & pos, float fov, float aspectX, float aspectY, float zNear, float zFar);

	/**
	 * @fn	void Camera::updateCamera(float fov, float aspect, float zNear, float zFar);
	 *
	 * @brief	Updates the camera.
	 *
	 * @param	fov   	The field of view.
	 * @param	aspect	The aspect ratio.
	 * @param	zNear 	The near distance.
	 * @param	zFar  	The far distance.
	 */
	 
	 void updateCamera(float fov, float aspect, float zNear, float zFar);

	/**
	 * @fn	glm::mat4 Camera::getViewProjection() const;
	 *
	 * @brief	Gets view projection.
	 *
	 * @return	The view projection.
	 */

	glm::mat4 getViewProjection() const;

	/**
	 * @fn	glm::mat4 Camera::getViewMatrix() const;
	 *
	 * @brief	Gets view matrix.
	 *
	 * @return	The view matrix.
	 */
	glm::mat4 getViewMatrix() const;

	/**
	 * @fn	glm::mat4 Camera::getProjectionMatrix() const;
	 *
	 * @brief	Gets projection matrix.
	 *
	 * @return	The projection matrix.
	 */
	glm::mat4 getProjectionMatrix() const;

	/**
	 * @fn	void Camera::updateDirection(glm::vec2 rel, float deltaTime);
	 *
	 * @brief	Updates the direction.
	 *
	 * @param	rel		 	The relative distance moved.
	 * @param	deltaTime	The delta time.
	 */
	void updateDirection(glm::vec2 rel, float deltaTime);

	/**
	 * @fn	void Camera::updatePosition(bool forward, float direction);
	 *
	 * @brief	Updates the position.
	 *
	 * @param	forward  	true to forward.
	 * @param	direction	The direction.
	 */
	void updatePosition(bool forward, float direction);

	/**
	 * @fn	void Camera::updateViewMatrix();
	 *
	 * @brief	Updates the view matrix.
	 */
	void updateViewMatrix();

	/**
	 * @fn	void Camera::setPosition(glm::vec3 newPosition);
	 *
	 * @brief	Sets a position.
	 *
	 * @param	newPosition	The new position.
	 */
	void setPosition(glm::vec3 newPosition);

	/**
	 * @fn	glm::vec3 Camera::getPosition();
	 *
	 * @brief	Gets the position.
	 *
	 * @return	The position.
	 */
	glm::vec3 getPosition();

	/**
	 * @fn	Camera * Camera::getCamera();
	 *
	 * @brief	Gets the camera.
	 *
	 * @return	null if it fails, else the camera.
	 */
	Camera * getCamera();

	/**
	 * @fn	float Camera::getFov();
	 *
	 * @brief	Gets the fov.
	 *
	 * @return	The fov.
	 */
	float getFov();

	/**
	 * @fn	glm::vec2 Camera::getAspectRatio();
	 *
	 * @brief	Gets aspect ratio.
	 *
	 * @return	The aspect ratio.
	 */
	glm::vec2 getAspectRatio();

private:
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;

	float fov;
	glm::vec3 position;
	glm::vec3 forward;
	glm::vec3 up;
	glm::vec3 right;
	glm::vec2 aspectRatio;

	glm::vec2 angle = { glm::pi<float>(), 0.f };
};