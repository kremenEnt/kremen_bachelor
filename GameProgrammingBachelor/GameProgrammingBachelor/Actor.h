#pragma once
#include <unordered_map>
#include <NetworkIDObject.h>
#include "ActorComponent.h"
#include <stack>

class Engine;
class IActorState;
/**
 * @class	Actor
 *
 * @brief	The actor.
 * @details Each object in the game is an actor.
 * 			Actors have many different properties. Which properties they have is decided by the components defined in the xml file.
 * 			All components are independent.
 */
class Actor : public RakNet::NetworkIDObject
{
public:
	//replacement for xml constructor. dunno if it needs more atm.
	Actor(Engine* parentEngine);
	//copy constructor NEEDS TO BE UP TO DATE AT ALL TIMES! if you add a new member variable to actor.h, make sure it is copied. don't want to debug this stupid shit two months in the future
	//Doesn't do anything atm. don't use
	Actor(const Actor& newActor);
	//copy assignment operator NEEDS TO BE UP TO DATE (after it is implemented)
	Actor& operator=(const Actor& newActor);
	~Actor();

	/**
	 * @fn
	 * void Actor::addComponent(std::unique_ptr<ActorComponent>& actorComponent, const std::string identifier);
	 *
	 * @brief	Adds a component to 'identifier'.
	 *
	 * @param [in,out]	actorComponent	The actor component.
	 * @param	identifier			  	The identifier.
	 */
	void addComponent(std::unique_ptr<ActorComponent>& actorComponent, const std::string identifier);

	/**
	 * @fn	void Actor::postInit();
	 *
	 * @brief	Posts the init.
	 */
	void postInit();

	/**
	 * @fn	void Actor::update(float deltaTime);
	 *
	 * @brief	Updates the given deltaTime.
	 *
	 * @param	deltaTime	The delta time.
	 */
	void update(float deltaTime);

	/**
	 * @fn	void Actor::onDeath();
	 *
	 * @brief	Executes the death action.
	 */
	void onDeath();

	/**
	 * @fn	void Actor::onHit();
	 *
	 * @brief	Executes the hit action.
	 */
	void onHit();

	/**
	 * @fn	void Actor::onSpawn();
	 *
	 * @brief	Executes the spawn action.
	 */
	void onSpawn();

	/**
	 * @fn	ActorComponent* Actor::getComponent(std::string componentId);
	 *
	 * @brief	Gets a component.
	 *
	 * @param	componentId	Identifier for the component.
	 *
	 * @return	null if it fails, else the component.
	 */
	ActorComponent* getComponent(std::string componentId);

	/**
	 * @fn	actorID Actor::getActorId();
	 *
	 * @brief	Gets actor identifier.
	 *
	 * @return	The actor identifier.
	 */
	actorID getActorId();

	/**
	 * @fn	void Actor::setActorId(actorID actorId);
	 *
	 * @brief	Sets actor identifier.
	 *
	 * @param	actorId	Identifier for the actor.
	 */
	void setActorId(actorID actorId);

	/**
	 * @fn	std::string Actor::getCategory();
	 *
	 * @brief	Gets the category.
	 *
	 * @return	The category.
	 */
	std::string getCategory();

	/**
	 * @fn	void Actor::setCategory(std::string category);
	 *
	 * @brief	Sets a category.
	 *
	 * @param	category	The category.
	 */
	void setCategory(std::string category);

	/**
	 * @fn	void Actor::setPosition(glm::vec3 newPosition);
	 *
	 * @brief	Sets a position.
	 *
	 * @param	newPosition	The new position.
	 */
	void setPosition(glm::vec3 newPosition);

	/**
	 * @fn	glm::vec3 Actor::getPosition();
	 *
	 * @brief	Gets the position.
	 *
	 * @return	The position.
	 */
	glm::vec3 getPosition();

	/**
	 * @fn	void Actor::movePosition(glm::vec3 direction);
	 *
	 * @brief	Move position.
	 *
	 * @param	direction	The direction.
	 */
	void movePosition(glm::vec3 direction);

	/**
	 * @fn	void Actor::setActive(bool flag);
	 *
	 * @brief	Sets an active.
	 *
	 * @param	flag	true to flag.
	 */
	void setActive(bool flag);

	/**
	 * @fn	void Actor::setPhysicsActive(bool flag);
	 *
	 * @brief	Sets physics active.
	 *
	 * @param	flag	true to flag.
	 */
	void setPhysicsActive(bool flag);

	/**
	 * @fn	bool Actor::isActive();
	 *
	 * @brief	Query if this object is active.
	 *
	 * @return	true if active, false if not.
	 */
	bool isActive();

private:
	int currentState;
	//krem::Vector3f position;
	glm::vec3 position;
	ActorFactory* actorFactory;
	actorID actorId;
	std::string category;
	Engine* parentEngine;

	bool active;
	std::unordered_map<int, std::unique_ptr<IActorState>> actorStates;
	std::unordered_map<std::string, std::unique_ptr<ActorComponent>> actorComponentMap;
	std::stack<IActorState*> actorState;
	//std::stack<std::unique_ptr<IActorState>> actorState;

	/**
	 * @fn	void Actor::addChild(ActorComponent* actorComponent);
	 *
	 * @brief	Adds a child.
	 *
	 * @param [in,out]	actorComponent	If non-null, the actor component.
	 */
	void addChild(ActorComponent* actorComponent);
};