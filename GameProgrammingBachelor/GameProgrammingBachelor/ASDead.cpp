#include "ASDead.h"
#include "Actor.h"
#include "PhysicsComponent.h"
#include "AnimationComponent.h"

int ASDead::update()
{
	//printf("dead\n");
	return AS_DEAD;
}

void ASDead::enter()
{
	if (animationComponent != nullptr)
	{
		animationComponent->changeAnimation(DEATH);
	}
}

void ASDead::leave()
{
}
