#pragma once
#include "GuiFactory.h"
#include "Handler.h"
#include <chrono>
#include <ctime>

/**
 * @class	UserFeedback
 *
 * @brief	used to receive and update stuff that does any kind of userfeed back.
 */
class UserFeedback : public Handler
{
public:

	/**
	 * @fn	UserFeedback::UserFeedback();
	 *
	 * @brief	Default constructor.
	 */
	UserFeedback();

	/**
	 * @fn	UserFeedback::~UserFeedback();
	 *
	 * @brief	Destructor.
	 */
	~UserFeedback();

	/**
	 * @fn	void UserFeedback::update();
	 *
	 * @brief	Updates this object.
	 */
	void update();

	/**
	 * @fn	void UserFeedback::saveAnswer(bool answer);
	 *
	 * @brief	Saves an answer.
	 *
	 * @param	answer	true to answer.
	 */
	void saveAnswer(bool answer);

	/**
	 * @fn	void UserFeedback::saveRating(int rating);
	 *
	 * @brief	Saves the rating.
	 *
	 * @param	rating	The rating that is saved.
	 */
	void saveRating(int rating);

	/**
	 * @fn	void UserFeedback::sendToDb();
	 *
	 * @brief	Sends to database.
	 */
	void sendToDb();

	/**
	 * @fn	void UserFeedback::reset();
	 *
	 * @brief	Resets this object.
	 */
	void reset();
private:
	int numberOfQuestions;
	int maxNumberOfQuestions; 
	int firstRaitingQuestion;

	bool question1;
	int rating;
};
