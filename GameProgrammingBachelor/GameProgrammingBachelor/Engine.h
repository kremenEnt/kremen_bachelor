#pragma once
#include <vector>
#include <map>
#include <time.h>
#include <thread>

#include "Console.h"
#include "Timer.h"
#include "common.h"

#ifdef __ANDROID__
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <android/log.h>
#include "android/AssetFile.h"
#include "jni.h"
#include "android/SdlAndroidActivity.h"
#include "android\AssetManager.h"
#endif

class LevelGenerator;
class NetworkClientHandler;
class NetworkServerHandler;
class ParticleSystem;
class UserFeedback;
class WeatherParticles;
class WorldGenerator;

class ActorFactory;
class GuiFactory;

class GraphicsHandler;
class PhysicsHandler;
class AudioHandler;

class TextureHandler;
class MeshHandler;
class ShaderHandler;

class EventHandler;
class FontHandler;
class TerrainHandler;
class SkyboxHandler;
class DatabaseHandler;
class XmlHandler;

class AttemptX;

/**
 * @class	Engine
 *
 * @brief	The engine.
 * @details	Owns all handlers and factories
 * 			Runs the main loop and tracks the gamestate
 * @see		Engine::gameLoop()
 * @see		Handler
 * 			
 */

class Engine
{ 
public:

	/**
	 * @fn	void Engine::initRunning();
	 *
	 * @brief	Init running.
	 */
	void initRunning();

	/**
	 * @fn	void Engine::initMainmenu();
	 *
	 * @brief	Init mainmenu.
	 */
	void initMainmenu();

	/**
	 * @fn	void Engine::initLevelSelect();
	 *
	 * @brief	Init level select.
	 */
	void initLevelSelect();

	/**
	 * @fn	void Engine::initGameover();
	 *
	 * @brief	Init gameover.
	 */
	void initGameover();

	/**
	 * @fn	void Engine::initSettings();
	 *
	 * @brief	Init settings.
	 */
	void initSettings();

	/**
	 * @fn	void Engine::initJoining();
	 *
	 * @brief	Init joining.
	 */
	void initJoining();

	/**
	 * @fn	void Engine::initConnection();
	 *
	 * @brief	Init connection.
	 */
	void initConnection();

	/**
	 * @fn	void Engine::initHosting();
	 *
	 * @brief	Init hosting.
	 */
	void initHosting();

	/**
	 * @fn	void Engine::initQuitting();
	 *
	 * @brief	Init quitting.
	 */
	void initQuitting();

	/**
	 * @fn	void Engine::updateRunning();
	 *
	 * @brief	Updates the running.
	 */
	void updateRunning();

	/**
	 * @fn	void Engine::updateMainmenu();
	 *
	 * @brief	Updates the mainmenu.
	 */
	void updateMainmenu();

	/**
	 * @fn	void Engine::updateLevelSelect();
	 *
	 * @brief	Updates the level select.
	 */
	void updateLevelSelect();

	/**
	 * @fn	void Engine::updateGameover();
	 *
	 * @brief	Updates the gameover.
	 */
	void updateGameover();

	/**
	 * @fn	void Engine::updateSettings();
	 *
	 * @brief	Updates the settings.
	 */
	void updateSettings();

	/**
	 * @fn	void Engine::updateJoining();
	 *
	 * @brief	Updates the joining.
	 */
	void updateJoining();

	/**
	 * @fn	void Engine::updateJoined();
	 *
	 * @brief	Updates the joined.
	 */
	void updateJoined();

	/**
	 * @fn	void Engine::updateHosting();
	 *
	 * @brief	Updates the hosting.
	 */
	void updateHosting();

	/**
	 * @fn	void Engine::updateQuitting();
	 *
	 * @brief	Updates the quitting.
	 */
	void updateQuitting();
public:

	/**
	 * @fn	Engine::Engine();
	 *
	 * @brief	Default constructor.
	 */
	Engine();

	/**
	 * @fn	Engine::~Engine();
	 *
	 * @brief	Destructor.
	 */
	~Engine();

	/**
	 * @fn	void Engine::init();
	 *
	 * @brief	Initialize this object.
	 */
	void init();

	/**
	 * @fn	void Engine::run();
	 *
	 * @brief	Runs this object.
	 */
	void run();
	/**
	 * @fn	void Engine::genericUpdate();
	 *
	 * @brief	Generic update, handles miscellaneous updates
	 */
	void genericUpdate();
	/**
	 * @fn	void Engine::genericDraw();
	 *
	 * @brief	Generic draw.
	 */
	void genericDraw();

	/**
	 * @fn	ActorFactory* Engine::getActorFactory() const;
	 *
	 * @brief	Gets actor factory.
	 *
	 * @return	null if it fails, else the actor factory.
	 */
	ActorFactory* getActorFactory() const;

	/**
	 * @fn	PhysicsHandler* Engine::getPhysicsHandler() const;
	 *
	 * @brief	Get PhysicsHandler.
	 *
	 * @return	null if it fails, else the physics handler.
	 */
	PhysicsHandler* getPhysicsHandler() const;

	/**
	 * @fn	AudioHandler* Engine::getAudioHandler() const;
	 *
	 * @brief	Get AudioHandler.
	 *
	 * @return	null if it fails, else the audio handler.
	 */
	AudioHandler* getAudioHandler() const;

	/**
	 * @fn	GraphicsHandler* Engine::getGraphicsHandler() const;
	 *
	 * @brief	Get GraphicsHandler.
	 *
	 * @return	null if it fails, else the graphics handler.
	 */
	GraphicsHandler* getGraphicsHandler() const;

	/**
	 * @fn	TextureHandler* Engine::getTextureHandler() const;
	 *
	 * @brief	Get TextureHandler.
	 *
	 * @return	null if it fails, else the texture handler.
	 */
	TextureHandler* getTextureHandler() const;

	/**
	 * @fn	MeshHandler* Engine::getMeshHandler() const;
	 *
	 * @brief	Get MeshHandler.
	 *
	 * @return	null if it fails, else the mesh handler.
	 */
	MeshHandler* getMeshHandler() const;

	/**
	 * @fn	GuiFactory* Engine::getGuiFactory() const;
	 *
	 * @brief	Get GuiFactory.
	 *
	 * @return	null if it fails, else the graphical user interface factory.
	 */
	GuiFactory* getGuiFactory() const;

	/**
	 * @fn	ShaderHandler* Engine::getShaderHandler() const;
	 *
	 * @brief	Get ShaderHandler.
	 *
	 * @return	null if it fails, else the shader handler.
	 */
	ShaderHandler* getShaderHandler() const;

	/**
	 * @fn	EventHandler* Engine::getEventHandler() const;
	 *
	 * @brief	Get EventHandler.
	 *
	 * @return	null if it fails, else the event handler.
	 */
	EventHandler* getEventHandler() const;

	/**
	 * @fn	FontHandler* Engine::getFontHandler() const;
	 *
	 * @brief	Get FontHandler.
	 *
	 * @return	null if it fails, else the font handler.
	 */
	FontHandler* getFontHandler() const;

	/**
	 * @fn	TerrainHandler* Engine::getTerrainHandler() const;
	 *
	 * @brief	Get TerrainHandler.
	 *
	 * @return	null if it fails, else the terrain handler.
	 */
	TerrainHandler* getTerrainHandler() const;

	/**
	 * @fn	SkyboxHandler* Engine::getSkyboxHandler() const;
	 *
	 * @brief	Get SkyboxHandler.
	 *
	 * @return	null if it fails, else the skybox handler.
	 */
	SkyboxHandler* getSkyboxHandler() const;

	/**
	 * @fn	ParticleSystem* Engine::getParticleSystem() const;
	 *
	 * @brief	Gets particle system.
	 *
	 * @return	null if it fails, else the particle system.
	 */
	ParticleSystem* getParticleSystem() const;

	/**
	 * @fn	WorldGenerator* Engine::getWorldGenerator() const;
	 *
	 * @brief	Gets world generator.
	 *
	 * @return	null if it fails, else the world generator.
	 */
	WorldGenerator* getWorldGenerator() const;

	/**
	 * @fn	DatabaseHandler* Engine::getDatabaseHandler() const;
	 *
	 * @brief	Get DatabaseHandler.
	 *
	 * @return	null if it fails, else the database handler.
	 */
	DatabaseHandler* getDatabaseHandler() const;

	/**
	 * @fn	AttemptX* Engine::getAttemptX() const;
	 *
	 * @brief	Get AttemptX.
	 *
	 * @return	null if it fails, else the attempt x coordinate.
	 */
	AttemptX* getAttemptX() const;

	/**
	 * @fn	UserFeedback* Engine::getUserFeedback() const;
	 *
	 * @brief	Gets user feedback.
	 *
	 * @return	null if it fails, else the user feedback.
	 */	
	UserFeedback* getUserFeedback() const;
	
#ifdef __ANDROID__
	krem::android::AssetManager* getAssetMgr() const;
#else

	/**
	 * @fn	NetworkClientHandler* Engine::getClientHandler() const;
	 *
	 * @brief	Handler, called when the get client.
	 *
	 * @return	getClientHandler.
	 */
	NetworkClientHandler* getClientHandler() const;

	/**
	 * @fn	NetworkServerHandler* Engine::getServerHandler() const;
	 *
	 * @brief	Get ServerHandler.
	 *
	 * @return	null if it fails, else the server handler.
	 */
	NetworkServerHandler* getServerHandler() const;

#endif

	/**
	 * @fn	LevelGenerator* Engine::getLevelLoader() const;
	 *
	 * @brief	Gets level loader.
	 *
	 * @return	null if it fails, else the level loader.
	 */
	LevelGenerator* getLevelLoader() const;

	/**
	 * @fn	XmlHandler* Engine::getXmlHandler() const;
	 *
	 * @brief	Get XML Handler.
	 *
	 * @return	null if it fails, else the XML handler.
	 */
	XmlHandler* getXmlHandler() const;


	SDL_Event SDLEvent; //primary eventcontainer

	/**
	 * @fn	float Engine::getDeltaTime();
	 *
	 * @brief	Gets delta time.
	 *
	 * @return	The delta time.
	 */
	float getDeltaTime();

	/**
	 * @fn	int Engine::getSelectedTool() const;
	 *
	 * @brief	Gets selected tool.
	 *
	 * @return	The selected tool.
	 */
	int getSelectedTool() const;

	/**
	 * @fn	int Engine::getSelectedLevel() const;
	 *
	 * @brief	Gets selected level.
	 *
	 * @return	The selected level.
	 */
	int getSelectedLevel() const;

	/**
	 * @fn	void Engine::setGameState(int gameState);
	 *
	 * @brief	Sets game state.
	 *
	 * @param	gameState	State of the game.
	 */
	void setGameState(int gameState);

	/**
	 * @fn	int Engine::getGameState();
	 *
	 * @brief	Gets game state.
	 *
	 * @return	The game state.
	 */
	int getGameState();

	/**
	 * @fn	void Engine::setSeed(unsigned int seed);
	 *
	 * @brief	Sets a seed.
	 *
	 * @param	seed	The seed.
	 */
	void setSeed(unsigned int seed);

	/**
	 * @fn	unsigned int Engine::getSeed();
	 *
	 * @brief	Gets the seed.
	 *
	 * @return	The seed.
	 */
	unsigned int getSeed();

	/**
	 * @fn	bool Engine::getJoining();
	 *
	 * @brief	Gets joining.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool getJoining();
private:

	/**
	 * @fn	template<typename T> void Engine::addChild(T t);
	 *
	 * @brief	Adds a child to Engine.
	 *
	 * @tparam	T	Generic type parameter.
	 * @param	t	The T to process.
	 */
	template<typename T>
	void addChild(T t);

	/**
	 * @fn	void Engine::drawMenuGraphics();
	 *
	 * @brief	Draw menu graphics.
	 */
	void drawMenuGraphics();

	/**
	 * @fn	void Engine::updateMenuGraphics();
	 *
	 * @brief	Updates the menu graphics.
	 */
	void updateMenuGraphics();

	float deltaTime = 1.f; //we save deltaTime as seconds
	unsigned int previousTime = 1; //previous time is stored as milliseconds for precision

	int selectedTool;
	int selectedLevel;

	bool levelLoaded = false;
	bool consoleactivated = false;
	
	Console console;

	std::unique_ptr<ActorFactory> actorFactory;
	std::unique_ptr<GraphicsHandler> graphicsHandler;
	std::unique_ptr<PhysicsHandler> physicsHandler;
	std::unique_ptr<TextureHandler> textureHandler;
	std::unique_ptr<AudioHandler> audioHandler;
	std::unique_ptr<MeshHandler> meshHandler;
	std::unique_ptr<GuiFactory> guiFactory;
	std::unique_ptr<ShaderHandler> shaderHandler;
	std::unique_ptr<EventHandler> eventHandler;
	std::unique_ptr<FontHandler> fontHandler;
	std::unique_ptr<TerrainHandler> terrainHandler;
	std::unique_ptr<TerrainHandler> waterHandler;
	std::unique_ptr<SkyboxHandler> skyboxHandler;
	std::unique_ptr<WeatherParticles> weatherParticles;
	std::unique_ptr<ParticleSystem> particleSystem;
	std::unique_ptr<XmlHandler> xmlHandler;
	std::unique_ptr<WorldGenerator> worldGenerator;
	std::unique_ptr<UserFeedback> userFeedback;
	std::unique_ptr<DatabaseHandler> databaseHandler;
	std::unique_ptr<LevelGenerator> levelLoader;

	std::unique_ptr<AttemptX> attemptX;


	//std::unique_ptr<RandomSpawn> randomSpawn;
#ifdef __ANDROID__
	std::unique_ptr<krem::app::SdlAndroidActivity> sdlAndroidActivity;
	std::unique_ptr<krem::android::AssetManager> assetMgr;
	JNIEnv *javaEnv;
	jobject javaAssetManager;
#else
	std::unique_ptr<NetworkClientHandler> networkClientHandler;
	std::unique_ptr<NetworkServerHandler> networkServerHandler;
	//std::unique_ptr<LightHandler> lightHandler;
#endif

	bool hosting;
	bool joining;
	int gameState;	
	int nextGameState;
	bool running;
	std::string input;
	Timer netUpdateTimer;
	Timer oneSecondTimer;
	int fps;

	unsigned int randomSeed;

	std::string IP;
	int32_t localPort = 0;
	int32_t remotePort = 0;
	std::string message;

};
