#include "CombatComponent.h"
#include "Actor.h"
#include "Engine.h"
#include "Timer.h"
#include "ActorFactory.h"

CombatComponent::CombatComponent(ActorData::ComponentData * newComponentData)
	:ICombatComponent(newComponentData)
{
	this->invincibilityTimer.setDuration(3.f);
	this->invincibilityTimer.start();
	this->health = this->componentData.maxHealth;
	this->lifetimeTimer = this->componentData.lifetime;
}

CombatComponent::~CombatComponent()
{
}

void CombatComponent::init()
{
	this->parentEngine = parentEngine;
	if (componentData.lifetime > 0)
	{
		lifetimeTimer.setDuration(componentData.lifetime);
		lifetimeTimer.start();
	}
}

void CombatComponent::update(float deltaTime)
{
	if (this->invincibilityTimer.hasEnded())
	{
		this->invincible = false;
	}
	if (lifetimeTimer.hasEnded() || health <= 0)
	{
		actorID temp = actor->getActorId();
		if (temp.actorName == "Player")
		{
			this->parentEngine->setGameState(STATE_GAMEOVER);
		}
		else
		{
			this->actor->onDeath();
			this->parentEngine->getActorFactory()->addToDeleteQueue(actor); //should rewrite this somehow
		}
	}
}

void CombatComponent::changeHealth(int modifier)
{
	if (!invincible)
	{
		printf("Ouch, you took damage!\n");

		parentEngine->setGameState(STATE_GAMEOVER);
		this->health += modifier;
		this->invincibilityTimer.restart();
	}
	this->invincible = true;

	if (this->health < 0)
	{
		this->health = 0;
	}

	if (this->health > this->componentData.maxHealth)
	{
		this->health = this->componentData.maxHealth;
	}
}

int CombatComponent::getHealth()
{
	return this->health;
}

int CombatComponent::getDamage()
{
	return this->componentData.damage;
}

void CombatComponent::kill()
{
	this->health = 0;
	this->actor->onDeath();
	this->parentEngine->getActorFactory()->addToDeleteQueue(actor); //should rewrite this somehow
}
