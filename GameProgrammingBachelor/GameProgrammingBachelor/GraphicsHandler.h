#pragma once
#include "IGraphicsHandler.h"
#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#ifdef __ANDROID__
#include <GLES3/gl31.h>
#else
#include <GL\glew.h>
#endif
#include <glm\glm.hpp>
#include <vector>
#include <memory>
#include <string>

class Camera;
/**
 * @class	GraphicsHandler
 *
 * @brief	The graphics handler is primarily used to clear/draw everything.
 * @details	Holds the cameraPosition and SDL_Window
 * 			Also initializes everything related to graphics
 */
class GraphicsHandler : public IGraphicsHandler
{
public:

	/**
	 * @fn	GraphicsHandler::GraphicsHandler(const std::string& windowname);
	 *
	 * @brief	Constructor.
	 *
	 * @param	windowname	Title of window.
	 */
	GraphicsHandler(const std::string& windowname);

	/**
	 * @fn	GraphicsHandler::~GraphicsHandler();
	 *
	 * @brief	Destructor.
	 *
	 */
	~GraphicsHandler();

	/**
	 * @fn	void GraphicsHandler::init(const std::string& windowname);
	 *
	 * @brief	Initialises this object.
	 *
	 * @param	windowname	Title of window.
	 */
	void init(const std::string& windowname);

	/**
	 * @fn	void GraphicsHandler::swapBuffers() override;
	 *
	 * @brief	Swap buffers to display current frame.
	 */
	void swapBuffers() override;

	/**
	 * @fn	void GraphicsHandler::clearWindow() override;
	 *
	 * @brief	Clears the window with a predefined color.
	 *
	 */
	void clearWindow() override;

	/**
	 * @fn	Camera * GraphicsHandler::getCamera();
	 *
	 * @brief	Gets the camera.
	 *
	 * @return	pointer to camera.
	 */
	Camera * getCamera();

	/**
	 * @fn	glm::vec2 GraphicsHandler::getDesktopResolution();
	 *
	 * @brief	Gets desktop resolution.
	 *
	 * @return	The desktop resolution.
	 */
	glm::vec2 getDesktopResolution();

	/**
	 * @fn	glm::vec2 GraphicsHandler::getWindowResolution();
	 *
	 * @brief	Gets window resolution.
	 *
	 * @return	The window resolution.
	 */
	glm::vec2 getWindowResolution();

	/**
	 * @fn	void GraphicsHandler::setCameraDirection(glm::vec2 position, float dt);
	 *
	 * @brief	Sets camera direction.
	 *
	 * @param	position	The position.
	 * @param	dt			The deltatime.
	 */
	void setCameraDirection(glm::vec2 position, float dt);

	/**
	 * @fn	void GraphicsHandler::setCameraPosition(bool horizontalDirection, float forwardMotion);
	 *
	 * @brief	Sets camera position.
	 *
	 * @param	horizontalDirection	true to horizontal direction.
	 * @param	forwardMotion	   	The forward motion.
	 */
	void setCameraPosition(bool horizontalDirection, float forwardMotion);

	/**
	 * @fn	void GraphicsHandler::setCameraPosition(glm::vec3 cameraPosition);
	 *
	 * @brief	Sets camera position.
	 *
	 * @param	cameraPosition	The camera position.
	 */
	void setCameraPosition(glm::vec3 cameraPosition);

	/**
	 * @fn	void GraphicsHandler::updateWindow(Uint32 flag);
	 *
	 * @brief	Updates the window described by flag.
	 *
	 * @param	flag	SDL flags.
	 */
	void updateWindow(Uint32 flag);

	/**
	 * @fn	float GraphicsHandler::getViewDistance();
	 *
	 * @brief	Gets view distance.
	 *
	 * @return	The view distance.
	 */
	float getViewDistance();
	//Also updates Pixels per unit and units on screen
	void setViewDistance(float newViewDistance);

	/**
	 * @fn	glm::vec2 GraphicsHandler::getUnitsOnScreen();
	 *
	 * @brief	Gets units(tiles) on screen.
	 *
	 * @return	The amount of tiles on screen.
	 */
	glm::vec2 getUnitsOnScreen();

	/**
	 * @fn	void GraphicsHandler::calculatePixelsPerUnitAndUnitsOnScreen();
	 *
	 * @brief	Calculates the pixels per unit and units on screen.
	 */
	void calculatePixelsPerUnitAndUnitsOnScreen();
private:
	SDL_Window* window {};

	SDL_GLContext glContext;
	std::unique_ptr<Camera> mainCamera;
	
	glm::vec2 resolution;

	glm::vec2 unitsOnScreen;
	glm::vec2 pixelsPerUnit;

	float vFov;
	float hFov;
	float viewDistance;
};
