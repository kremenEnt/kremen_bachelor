#pragma once
#include <string>
#include <SDL.h>

#ifdef __ANDROID__
#include <GLES3/gl31.h>
#include "android\AssetFile.h"
#include "android\AssetManager.h"
#include <android\asset_manager.h>
#else
#include <GL\glew.h>
//I don't think the SDL_opengl.h is really necessary.
//#include <SDL_opengl.h>
#endif
#include <glm\glm.hpp>
//#include <vector>

typedef struct _TTF_Font TTF_Font;

//
//	This font class is really CPU intensive. It needs rewriting.
// 

/**
 * @class	Font
 *
 * @brief	A font.
 */
class Font
{
public:

	/**
	 * @fn	Font::Font(std::string filePath);
	 *
	 * @brief	Constructor.
	 *
	 * @param	filePath	Full pathname of the file.
	 */
	Font(std::string filePath);

	/**
	 * @fn	Font::~Font();
	 *
	 * @brief	Destructor.
	 */
	~Font();

	/**
	 * Updates this object.
	 *
	 * @param	text	 	The text.
	 * @param	textColor	The text color.
	 */
	void update(std::string text, glm::vec3 textColor);

	/**
	 * Binds the given unit.
	 *
	 * @param	unit	Texture number unit.
	 */
	void bind(unsigned int unit);
private:
	int fontSize;
	TTF_Font* font;
	SDL_Color color;
	GLuint texture;
};

