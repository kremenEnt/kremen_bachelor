#pragma once
#include "ActorData.h"
#include <memory>
#include <glm\glm.hpp>
#include "common.h"

class Actor;
class Engine;
/**
 * @class	ActorComponent
 *
 * @brief	The base class for all components. Can't be instantiated.
 */
class ActorComponent
{
private:
	virtual void init() = 0; //RECONSIDER: does this need to be pure virtual?
public:

	/**
	 * @fn	ActorComponent::ActorComponent()
	 *
	 * @brief	Default constructor.
	 */
	ActorComponent() :alive(true) {}

	/**
	 * @fn	ActorComponent::ActorComponent(ActorData::ComponentData* componentData);
	 *
	 * @brief	Constructor.
	 *
	 * @param [in,out]	componentData	If non-null, information describing the component.
	 */
	ActorComponent(ActorData::ComponentData* componentData);

	/**
	 * @fn	virtual ActorComponent::~ActorComponent();
	 *
	 * @brief	Destructor.
	 */
	virtual ~ActorComponent();

	/**
	 * @fn	virtual void ActorComponent::init(Engine* parentEngine, Actor* actor);
	 *
	 * @param [in,out]	parentEngine	If non-null, the parent engine.
	 * @param [in,out]	actor			If non-null, the actor.
	 */
	virtual void init(Engine* parentEngine, Actor* actor);

	/**
	 * @fn	virtual void ActorComponent::update(float deltaTime)
	 *
	 * @brief	Updates the component.
	 *
	 * @param	deltaTime	The delta time.
	 */	
	virtual void update(float deltaTime) {};

	/**
	 * @fn	virtual void ActorComponent::onDeath()
	 *
	 * @brief	Executes the death action.
	 */
	virtual void onDeath() {};

	/**
	 * @fn	virtual void ActorComponent::onSpawn()
	 *
	 * @brief	Executes the spawn action.
	 */
	virtual void onSpawn() {};

	/**
	 * @fn	void ActorComponent::setParent(Actor& parentPtr);
	 *
	 * @brief	Sets a parent.
	 *
	 * @param [in,out]	parentPtr	The parent pointer.
	 */
	void setParent(Actor& parentPtr);
	
	/**
	* @fn	void ActorComponent::isAlive();
	*
	* @brief	Returns life status of actor.
	*
	* @return	Returns life status of actor.
	*/
	bool isAlive();
protected:
	Actor* actor;
	Engine* parentEngine;
	bool alive;
private:
};