#pragma once
#include "ActorComponent.h"
#include "CustomActorData.h"
#include "Timer.h"

class FlashOnHitComponent : public ActorComponent
{
public:
	FlashOnHitComponent(ActorData::ComponentData* componentData)
	{
		this->componentData = *static_cast<CustomActorData::FlashOnHit*>(componentData);
	}
	void init() override
	{
		invulnerabilityTimer = componentData.invulnerabilityTime;
		blinkingTimer.setDuration(this->invulnerabilityTimer);
		blinkingTimer.start();
	}
	void update(float deltaTime) override
	{
		if (!blinkingTimer.hasEnded())
		{
			color = cos(counter += deltaTime * blinkRate);
			if (counter >= glm::two_pi<float>()) counter -= glm::two_pi<float>();
			static_cast<GraphicsComponent*>(actor->getComponent("GraphicsComponent"))->setColor({ 1.0, color, color, 1.0 });
		}
		else
		{
			static_cast<GraphicsComponent*>(actor->getComponent("GraphicsComponent"))->setColor({ 1.0, 1.0, 1.0, 1.0 });
		}
	}
protected:
	CustomActorData::FlashOnHit componentData;
	float invulnerabilityTimer;
	Timer blinkingTimer;
	float color;
	float counter = 0.f;
	float blinkRate = 25.f;
};