#include "ParticleComponent.h"
#include "Engine.h"
#include "TextureHandler.h"
#include "Actor.h"

ParticleComponent::ParticleComponent(ActorData::ComponentData * componentData)
	:componentData(*static_cast<ActorData::Particle*>(componentData))
{
}

ParticleComponent::~ParticleComponent()
{
}

void ParticleComponent::init()
{
	ParticleSystem::init(parentEngine, componentData.size, parentEngine->getTextureHandler()->loadTexture(componentData.texturePath), componentData.maxVelocity, this->gravity);
}

void ParticleComponent::update(float deltaTime)
{
	glm::vec3 position = actor->getPosition();
	createPoint(componentData.particlesPerSecond, deltaTime, position, componentData.maxLife, componentData.color, componentData.velocity);

	for (std::size_t i = 0; i < particles.size(); i++)
	{
		if (gravity == true)
		{
			float maximum = componentData.maxVelocity * componentData.gravityMultiplier;
			if (particles[i].velocity.y > -maximum)
			{
				particles[i].velocity.y -= maximum / 4.f;
			}
			else
			{
				particles[i].velocity.y = -maximum;
			}
		}
		positions[i] += (particles[i].velocity * deltaTime);

		particles[i].life -= (1.f * deltaTime);
		if (particles[i].life <= 0.f)
		{
			particles.erase(particles.begin() + i);
			colors.erase(colors.begin() + i);
			positions.erase(positions.begin() + i);
		}
	}
}

void ParticleComponent::draw(glm::mat4 viewProjection)
{
	ParticleSystem::draw(viewProjection);
}
