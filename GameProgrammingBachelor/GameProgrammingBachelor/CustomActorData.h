#pragma once
#include "ActorData.h"

class CustomActorData : public ActorData
{
public:
	struct FlashOnHit : public ComponentData
	{
		float invulnerabilityTime;
	};
	CustomActorData(std::string xmlPath);
};