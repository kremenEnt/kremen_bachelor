#pragma once
#include "GuiElement.h"

class Engine;
/** A bar. Examples: health bar, mana bar, loading bar */
class Bar : public GuiElement
{
public:
	Bar(GuiData::ElementData* elementData);
	virtual ~Bar();

	/** Updates this object. */
	void update() override;

	/**
	* Sets the background texture of the bar.
	*
	* @param	texture		the texture path that is used.
	*/
	void setBackgroundTexture(std::string texturePath);

	/**
	* Sets the color of the bar; example: green for health.
	*
	* @param	color		the color that is used in rgb + alpha values.
	*/
	void setColor(glm::vec4 colorChange);

	/**
	* Sets the texture of the bar, used if you don't want just a solid color.
	*
	* @param	texture		the texture path that is used.
	*/
	void setBarTexture(std::string texturePath);

	/**
	 * Sets current value of the bar.
	 *
	 * @param	size	The current value of the bar.
	 */
	void setCurrentValue(float size);

	/**
	 * Sets maximum value of the bar
	 *
	 * @param	size	The maximum size of the bar.
	 */
	void setMaxValue(float size);

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The gui view projection.
	 */
	void draw(glm::mat4 &viewProjection) override;

	/**
	 * Gets the name of the bar.
	 *
	 * @return	The name.
	 */
	std::string getName();

	/**
	* Sets the background texture of the bar to a new one on run time.
	*
	* @param	texture		the texture path that its changing to.
	*/
	void changeBackgroundTexture(std::string newTexture);

	/**
	* Sets the  texture of the bar to a new one in runtime.
	*
	* @param	texture		the texture path that is used when changing.
	*/
	void changeBarTexture(std::string newTexture);

	/**
	* Sets the bar to visible or not, also deactivates interactions.
	*
	* @param	bool		true or false for visible or not.
	*/
	void setVisible(bool visible);
private:
	GuiData::Bar elementData;

	float maxValue;
	float currentValue;

	float width;
	float height;

	bool horizontal;
	bool textureOnBar;
	bool visible;

	glm::vec4 color;

	std::string name;

	Transform defaultBarTransform;
	Transform backgroundTransform;
	Transform barTransform;

	Mesh *mesh;

	Shader *shader;
	Shader *shaderBar;

	Texture *backgroundTexture;
	Texture *barTexture;

	virtual void init() override;
};

