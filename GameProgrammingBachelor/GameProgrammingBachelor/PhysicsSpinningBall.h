#pragma once
#include "PhysicsComponent.h"
#include "Actor.h"
#include <glm\gtc\constants.hpp>
#include "CombatComponent.h"
#include <iostream>
#include <Box2D\Box2D.h>
#include "Engine.h"
#include "ActorFactory.h"

class PhysicsSpinningBall : public PhysicsComponent
{
public:
	PhysicsSpinningBall(ActorData::ComponentData* componentData)
		: PhysicsComponent(componentData)
	{

	}
	void onCollision(int fixtureUserData, Actor* collidedActor) override
	{
		switch (fixtureUserData)
		{
		case CATEGORY_CLONE:
			std::cout << "Ouch, you took " << damage << " damage!\n";
			dynamic_cast<CombatComponent*>(collidedActor->getComponent("CombatComponent"))->changeHealth(-damage);
			break;
		default:
			break;
		}
	}
	void update(float deltaTime) override
	{
		if (!init)
		{
			ballActor = parentEngine->getActorFactory()->createActor(dynamic_cast<ActorData*>(&ActorData("res/actors/spinningBallBase.xml")), glm::vec3{ actor->getPosition().x, actor->getPosition().y, 0.f });
			b2RevoluteJointDef jointDef;

			jointDef.bodyA = body;	//motor
			jointDef.bodyB = dynamic_cast<PhysicsComponent*>(ballActor->getComponent("PhysicsComponent"))->getB2Body();

			jointDef.collideConnected = false;
			jointDef.enableMotor = true;
			jointDef.maxMotorTorque = 100.f;
			jointDef.motorSpeed = 180.f * (glm::pi<float>() / 180.f);

			jointDef.localAnchorA.Set(2.0f, 0);
			jointDef.localAnchorB.Set(0, 0);
			joint = static_cast<b2RevoluteJoint*>(body->GetWorld()->CreateJoint(&jointDef));
			init = true;
		}
		PhysicsComponent::update(deltaTime);
	}
private:
	int damage = 10;
	bool init = false;
	b2RevoluteJoint* joint;
	Actor* ballActor;
};