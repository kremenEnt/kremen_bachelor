#pragma once
#include "INetworkHandler.h"

#include <RakPeerInterface.h>

#include <MessageIdentifiers.h>
#include <BitStream.h>
#include <RakNetTypes.h>

class InputComponent;

/**
 * @class	NetworkClientHandler
 *
 * @brief	A network client handler.
 */
class NetworkClientHandler : public INetworkHandler
{
public:

	/**
	 * @fn	NetworkClientHandler::NetworkClientHandler();
	 *
	 * @brief	Default constructor.
	 */
	NetworkClientHandler();

	/**
	 * @fn	NetworkClientHandler::~NetworkClientHandler();
	 *
	 * @brief	Destructor.
	 */
	~NetworkClientHandler();

	/**
	 * @fn	void NetworkClientHandler::init(std::string hostIP);
	 *
	 * @brief	Inits the given host IP.
	 *
	 * @param	hostIP	The host IP.
	 */
	void init(std::string hostIP);

	/**
	 * @fn	void NetworkClientHandler::sendPacket(int gameState) override;
	 *
	 * @brief	Sends a packet.
	 *
	 * @param	gameState	State of the game.
	 */
	void sendPacket(int gameState) override;

	/**
	 * @fn	void NetworkClientHandler::receivePacket(int gameState) override;
	 *
	 * @brief	Receive packet.
	 *
	 * @param	gameState	State of the game.
	 */
	void receivePacket(int gameState) override;
	void receiveActorStructure();

	/**
	 * @fn	actorID NetworkClientHandler::getPlayerActorID();
	 *
	 * @brief	Gets player actor identifier.
	 *
	 * @return	The player actor identifier.
	 */
	actorID getPlayerActorID();
	//void sendInit();

	/**
	 * @fn	void NetworkClientHandler::sendJoined();
	 *
	 * @brief	Sends the joined.
	 */
	void sendJoined();

	/**
	 * @fn	void NetworkClientHandler::sendRunning();
	 *
	 * @brief	Sends the running.
	 */
	void sendRunning();

	/**
	 * @fn	void NetworkClientHandler::receiveJoining();
	 *
	 * @brief	Receive joining.
	 */
	void receiveJoining();

	/**
	 * @fn	void NetworkClientHandler::receiveJoined();
	 *
	 * @brief	Receive joined.
	 */
	void receiveJoined();

	/**
	 * @fn	void NetworkClientHandler::receiveRunning();
	 *
	 * @brief	Receive running.
	 */
	void receiveRunning();

	/**
	 * @fn	void NetworkClientHandler::receiveGameOver();
	 *
	 * @brief	Receive game over.
	 */
	void receiveGameOver();

private:
	InputComponent* inputComponent;
	RakNet::RakPeerInterface* peerInterface;
	RakNet::SocketDescriptor socket;
	RakNet::Packet* packet;
	RakNet::SystemAddress hostSystemAddress;
	bool initialized;
	static const int MAXINPUT = 5;
	int input[MAXINPUT];
	actorID playerActorID;
	int packetLength;
	NetworkActorData actorFromServer[200];
	int numberOfHostActors;
	int numberOfitems;
};