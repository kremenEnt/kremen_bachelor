#pragma once
#include <unordered_map>
#include "tinyxml2-master\tinyxml2.h"

#include "Handler.h"
#include <memory>
#ifdef __ANDROID__
#include "android\AssetFile.h"
#include "android\AssetManager.h"
#endif

/**
 * @class	XmlHandler
 *
 * @brief	An XML handler.
 */
//using namespace tinyxml2;
class XmlHandler : public Handler
{
public:

	/**
	 * @fn	XmlHandler::XmlHandler();
	 *
	 * @brief	Default constructor.
	 */
	XmlHandler();

	/**
	 * @fn	XmlHandler::~XmlHandler();
	 *
	 * @brief	Destructor.
	 */
	~XmlHandler();

	/**
	 * @fn	tinyxml2::XMLDocument* XmlHandler::loadXml(const std::string& filePath);
	 *
	 * @brief	Loads an XML.
	 *
	 * @param	filePath	Full pathname of the file.
	 *
	 * @return	null if it fails, else returns pointer to the XML.
	 */
	tinyxml2::XMLDocument* loadXml(const std::string& filePath);
#ifdef __ANDROID__
	const std::string getXmlString(std::string path, krem::android::AssetManager * assetmgr);
#endif
private:
	std::unique_ptr<tinyxml2::XMLDocument> doc;
	std::unordered_map<std::string, std::unique_ptr<tinyxml2::XMLDocument>> xmlDocuments;
};