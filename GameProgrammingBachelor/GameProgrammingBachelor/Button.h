#pragma once
#include "GuiElement.h"

class Engine;

/** A button. Used to create buttons that returns true or false if they are clicked or not */
class Button : public GuiElement
{
public:
	Button(GuiData::ElementData* elementData);
	virtual ~Button();

	/** Updates this object. */
	void update() override;

	/**
	 * Determines if mouse click left is inside the button.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickLeft() override;

	/**
	 * Determines if mouse click right is inside the button.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickRight() override;

	/**
	 * Determines if mouse release is inside the button.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseRelease() override;

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection) override;

	/**
	 * Gets the name.
	 *
	 * @return	The name.
	 */
	std::string getName();

	/**
	* Sets the  texture of the button to a new one in runtime.
	*
	* @param	texture		the texture path that is used when changing.
	*/
	void changeTexture(std::string newTexture);

	/**
	* Sets the button to visible or not, also deactivates interactions.
	*
	* @param	bool		true or false for visible or not.
	*/
	void setVisible(bool visible);
private:
	GuiData::Button elementData;

	std::string name;
	std::string text;

	bool clicked;
	bool fontDisplay;
	bool visible; 

	int currentTexture;
	int textureAtlasSize;

	Transform transform;
	Font * font;
	Mesh *mesh;
	Shader *shader;
	Texture *texture;

	virtual void init() override;
};

