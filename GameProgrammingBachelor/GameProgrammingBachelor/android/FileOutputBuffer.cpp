/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "FileOutputBuffer.h"

#include <cassert>

namespace krem { namespace android {

FileOutputBuffer::FileOutputBuffer(JNIEnv* javaEnv, jobject fileOutputStreamJava, const std::size_t bufferSize):
	buffer(bufferSize),
	javaEnv(javaEnv),
	fileOutputStreamJava(fileOutputStreamJava)
{
	assert(javaEnv != nullptr);

	this->resetPointers();

	const auto bufferSizeJava = static_cast<jsize>(bufferSize);
	this->bufferJava = javaEnv->NewByteArray(bufferSizeJava);

	this->fileOutputStreamClass = this->javaEnv->FindClass("java/io/FileOutputStream");
	this->writeMethodId = this->javaEnv->GetMethodID(this->fileOutputStreamClass, "write", "([BII)V");
}

FileOutputBuffer::FileOutputBuffer(JNIEnv* javaEnv, jobject fileOutputStreamJava):
	FileOutputBuffer(javaEnv, fileOutputStreamJava, 256)
{
}

FileOutputBuffer::~FileOutputBuffer()
{
	this->flush();

	this->javaEnv->DeleteLocalRef(this->bufferJava);
}

FileOutputBuffer::int_type FileOutputBuffer::overflow(int_type ch)
{
	if (ch != traits_type::eof())
	{
		assert(std::less_equal<char *>()(this->pptr(), this->epptr()));

		this->flush();
		this->resetPointers();

		return ch;
	}

	return traits_type::eof();
}

int FileOutputBuffer::sync()
{
	this->flush();
	this->resetPointers();

	return 0;
}

void FileOutputBuffer::flush()
{
	const auto numBytesToWrite = this->pptr() - this->pbase();
	const auto numBytesToWriteJava = static_cast<jsize>(numBytesToWrite);
	const auto bufferStart = reinterpret_cast<jbyte*>(this->buffer.data());
	this->javaEnv->SetByteArrayRegion(this->bufferJava, 0, numBytesToWriteJava, bufferStart);
	this->javaEnv->CallVoidMethod(this->fileOutputStreamJava, this->writeMethodId, this->bufferJava, 0, numBytesToWrite);
}

void FileOutputBuffer::resetPointers()
{
	this->setp(this->buffer.data(), this->buffer.data() + this->buffer.size());
}

} }
