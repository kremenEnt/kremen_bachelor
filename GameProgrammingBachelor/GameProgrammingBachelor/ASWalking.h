#pragma once
#include "IActorState.h"
#include "ASDefault.h"

class ASWalking : public IActorState, public ASDefault
{
public:
	ASWalking() {};
	ASWalking(Actor* actor) : ASDefault(actor) {};
	virtual int update() override;
	virtual void enter() override;
	virtual void leave() override;
};