#pragma once
#include "Shader.h"
#include <unordered_map>
#include "Handler.h"
#ifdef __ANDROID__
#include "android\AssetFile.h"
#include "android\AssetManager.h"
#endif
/**
 * @class	ShaderHandler
 *
 * @brief	A shader handler.
 */
class ShaderHandler : public Handler
{
public:

	/**
	 * @fn	ShaderHandler::ShaderHandler();
	 *
	 * @brief	Default constructor.
	 *
	 */
	ShaderHandler();

	/**
	 * @fn	ShaderHandler::~ShaderHandler();
	 *
	 * @brief	Destructor.
	 *
	 */
	~ShaderHandler();

	/**
	 * @fn	Shader* ShaderHandler::loadShader(const std::string& filePath);
	 *
	 * @brief	Loads a shader.
	 *
	 * @param	filePath	Full pathname of the file.
	 *
	 * @return	pointer to shader.
	 */
	Shader* loadShader(const std::string& filePath);
private:
	std::unordered_map<std::string, std::unique_ptr<Shader>> shaders;
};

