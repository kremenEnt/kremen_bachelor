#pragma once
#include <vector>
#include <map>
#include <SDL.h>
#include <glm\glm.hpp>
#include "Texture.h"
#include "Shader.h"
#include "Transform.h"
#include "Mesh.h"

class Engine;

/**
 * @class	CustomCursor
 *
 * @brief	A custom cursor implementation. Handles drawing, updating and storage of a custom cursor. Meant to be a standalone module with no code outside of the function.
 */
class CustomCursor
{
public:

	/**
	 * @fn	CustomCursor::CustomCursor();
	 *
	 * @brief	Default constructor.
	 */
	CustomCursor();

	/**
	 * @fn	CustomCursor::~CustomCursor();
	 *
	 * @brief	Destructor.
	 */
	~CustomCursor();

	/**
	 * @fn	void CustomCursor::toggleCursorOnOff();
	 *
	 * @brief	Toggles cursor on or off.
	 */
	void toggleCursorOnOff();

	/**
	 * @fn	bool CustomCursor::toggleCursorTexture(std::string filePath, Engine * parentEngine);
	 *
	 * @brief	Toggles cursor texture. Also adds the texture to the cursorMap.
	 *
	 * @param	filePath				Full pathname of the file.
	 * @param [in,out]	parentEngine	Pointer to the Engine.
	 *
	 * @return	Really just for the code to check if there is a texture. It is also meant as an escape from the code to avoid branching
	 */
	bool toggleCursorTexture(std::string filePath, Engine * parentEngine);

	/**
	 * @fn	void CustomCursor::updateCursor(Engine * parentEngine);
	 *
	 * @brief	Updates the cursor.
	 *
	 * @param [in,out]	parentEngine	Pointer to the Engine.
	 */	
	void updateCursor(Engine * parentEngine);

	/**
	 * @fn	void CustomCursor::draw();
	 *
	 * @brief	Draws this cursor.
	 */
	void draw();

	/**
	 * @fn	void CustomCursor::initCursor(Engine * parentEngine);
	 *
	 * @brief	Initializes cursor.
	 *
	 * @param [in,out]	parentEngine	Pointer to the Engine.
	 */
	void initCursor(Engine * parentEngine);

private:
	std::map<std::string, Texture *> cursorMap;
	std::string currentTexture;
	Transform transform;
	Mesh *mesh;
	Shader *shader;
	Texture *texture;

	/**
	 * @fn	bool CustomCursor::addCursorToMap(std::string filePath, Engine * parentEngine);
	 *
	 * @brief	Adds a cursor to map to 'parentEngine'.
	 *
	 * @param	filePath				Full pathname of the file.
	 * @param [in,out]	parentEngine	Pointer to the Engine.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool addCursorToMap(std::string filePath, Engine * parentEngine);

	/**
	* @fn	Texture * CustomCursor::getCursorTexture();
	*
	* @brief	Gets cursor texture.
	*
	* @return	null if it fails, else the cursor texture.
	*/
	Texture * getCursorTexture();
};

