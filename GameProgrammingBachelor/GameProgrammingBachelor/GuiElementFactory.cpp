#include "GuiElementFactory.h"

GuiElementCreator * GuiElementFactory::getCreator(const std::string & elementType)
{
	this->table;
	auto it = table.find(elementType);
	if (it != table.end())
	{
		return it->second;
	}
	else
	{
		return nullptr;
	}
}

void GuiElementFactory::registerIt(const std::string & elementType, GuiElementCreator * guiElementCreator)
{
	table[elementType] = guiElementCreator;
}