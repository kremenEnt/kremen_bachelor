#pragma once
#include "GuiElement.h"

class Engine;
class GuiElement;

/** A text box. Used to create a text field on the screen that the user can input text into */
class TextBox : public GuiElement
{
public:
	TextBox(GuiData::ElementData* elementData);
	virtual ~TextBox();
	/** Updates this object. */
	void update() override;

	/**
	 * Executes the focus action.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickLeft() override;

	/**
	 * runs if focus is lost.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool lostFocus();

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection);

	/**
	 * Initialises this object.
	 *
	 * @param [in,out]	parentEngine	If non-null, the parent engine.
	 */

	void setText(std::string newText);
	
	/**
	 * Gets the name.
	 *
	 * @return	The name.
	 */
	std::string getName();

	/**
	* Sets the  texture of the text box to a new one in runtime.
	*
	* @param	texture		the texture path that is used when changing.
	*/
	void changeTexture(std::string newTexture);

	/**
	* Sets the text box to visible or not, also deactivates interactions.
	*
	* @param	bool		true or false for visible or not.
	*/
	void setVisible(bool visible);
private:
	GuiData::TextBox elementData;

	std::string name;
	std::string input;
	std::string defaultValue;

	bool focus;
	bool visible;

	int currentTexture;
	int maxLength;

	Transform transform;
	Font * font;
	Mesh *mesh;
	Shader *shader;
	Texture *texture;

	virtual void init() override;
};

