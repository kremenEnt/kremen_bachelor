#pragma once
#include <string>
#include <memory>
#include <map>
#include <unordered_map>
#include <vector>
#include <glm\glm.hpp>
#include "ComponentFactoryFactory.h"
#include "engineComponents.h"
#include <string>
class Engine;
class Actor;
struct ActorData::ComponentData;
/**
 * @class	ActorFactory
 *
 * @brief	The Actor Factory creates and contains all actors.
 * @details	There is only one ActorFactory, held by engine.
 * 			All actors need to be created by calling createActor.
 * 			All actors are created from ActorData.
 */
class ActorFactory
{
public:
	ActorFactory();
	~ActorFactory();

	/**
	* @fn	void ActorFactory::createActor(const ActorData* actorData, const glm::vec3 position, const glm::vec3 velocity =
	*
	* @brief	Creates an actor.
	*
	* @param	actorData 	Actor data to base actor on.
	* @param	position	The position.
	* @param	velocity	Initial velocity of the created actor
	*/
	Actor* createActor(const ActorData* actorData, const glm::vec3 position, const glm::vec3 velocity = { 0, 0, 0 });


	/**
	* @fn	void ActorFactory::createActorComponent(ActorData::ComponentData* componentData);
	*
	* @brief	Creates components for actors. Only used by createActor
	*
	* @param	componentData 	componentData data to base component on.
	*/
	std::unique_ptr<ActorComponent> createActorComponent(ActorData::ComponentData* componentData);

	/**
	 * @fn	void ActorFactory::createActor(const ActorData* actorData, const glm::vec3 position, const glm::vec2 tilePosition =
	 *
	 * @brief	Creates an actor with graphics from a tileAtlas.
	 *
	 * @param	actorData 	Helper class that contains all general info on the actor.
	 * @param	position	The position.
	 * @param	tilePosition	Position of the tile in the atlas(in x and y). size per tile is specified in the xml file
	 */
	Actor* createActor(const ActorData* actorData, const glm::vec3 position, const glm::vec2 tilePosition);

	/**
	 * @fn	void ActorFactory::deleteActor(actorID actorId);
	 *
	 * @brief	Deletes the actor.
	 * @details Deletes the actor. Should only be called when deletequeue is cleaned unless you know what you're doing.
	 *
	 * @param	actorToDelete will be deleted.
	 */
	void deleteActor(Actor* actorToDelete);

	/**
	 * @fn	void ActorFactory::updateAll(const float deltaTime);
	 *
	 * @brief	Updates all actors in actorMap.
	 *
	 * @param	deltaTime	The delta time.
	 */
	void updateAll(const float deltaTime);
	
	/**
	 * @fn	void ActorFactory::init();
	 *
	 * @brief	Initialises the actorFactory.
	 */
	void init();

	/**
	 * @fn	Actor* ActorFactory::getActor(actorID actorId);
	 *
	 * @brief	Gets an actor.
	 *
	 * @param	actorId	Identifier for the actor.
	 *
	 * @return	null if it fails, else the actor.
	 */
	Actor* getActor(actorID actorId);



	/**
	* @fn	Actor* ActorFactory::getActor(string actorName);
	*
	* @brief	Gets first actor with given actorName.
	*
	* @param	actorName Name of the actor.
	*
	* @return	null if it fails, else the actor.
	*/
	Actor* getActor(std::string actorName);

	/**
	 * @fn	Actor* ActorFactory::getActor(int actorIdNumber);
	 *
	 * @brief	Gets an actor.
	 *
	 * @param	actorIdNumber	The actor identifier number.
	 *
	 * @return	null if it fails, else the actor.
	 */
	Actor* getActor(int actorIdNumber);

	/**
	 * @fn	void ActorFactory::setParent(Engine& parentPtr);
	 *
	 * @brief	Sets a parent.
	 *
	 * @param [in,out]	parentPtr	The parent pointer.
	 */
	void setParent(Engine& parentPtr);

	/**
	 * @fn	Engine* ActorFactory::getParent();
	 *
	 * @brief	Gets the parent of this item.
	 *
	 * @return	null if it fails, else the parent.
	 */
	Engine* getParent();
	


	std::vector<Actor*> getActors();
	std::vector<Actor*> getSceneryActors();
	
	unsigned int getCurrentActorID(void);

	/**
	 * @fn	void ActorFactory::addMessageListener(actorID actorId);
	 *
	 * @brief	Adds a message listener.
	 *
	 * @param	actorId	Identifier for the actor.
	 */
	void addMessageListener(actorID actorId);

	/**
	 * @fn	void ActorFactory::draw();
	 *
	 * @brief	Draws this object.
	 */
	void draw();

	/**
	 * @fn	void ActorFactory::addToDeleteQueue(Actor* actorToDelete);
	 *
	 * @brief	Adds to the delete queue.
	 *
	 * @param [in,out]	actorToDelete	If non-null, the actor to delete.
	 */
	void addToDeleteQueue(Actor* actorToDelete);

	/**
	 * @fn	void ActorFactory::addToSceneryDeleteQueue(int actorToDelete);
	 *
	 * @brief	Adds to the scenery delete queue.
	 *
	 * @param	actorToDelete	The actor to delete.
	 */
	void addToSceneryDeleteQueue(int actorToDelete);

	/**
	 * @fn	void ActorFactory::addToSwapActiveQueue(Actor* actorToSwapActive);
	 *
	 * @brief	Adds to the swap active queue.
	 *
	 * @param [in,out]	actorToSwapActive	If non-null, the actor to swap active.
	 */
	void addToSwapActiveQueue(Actor* actorToSwapActive);

	/**
	 * @fn	void ActorFactory::clearFactory();
	 *
	 * @brief	Clears the factory.
	 */
	void clearFactory();

	/**
	 * @fn	ComponentFactoryFactory* ActorFactory::getComponentFactoryFactory();
	 *
	 * @brief	Gets component factory.
	 *
	 * @return	null if it fails, else the component factory.
	 */
	ComponentFactoryFactory* getComponentFactoryFactory();
private:
	unsigned int currentActorId;
	unsigned int getNextActorId(void);
	std::map<actorID, std::unique_ptr<Actor>> actorMap; //contains all movable objects (player(s), enemies, projectiles++)
	std::vector<std::unique_ptr<Actor>> actorVector; //contains all static objects (tiles, flowers, static lights++)
	std::vector<Actor*> deleteQueue;
	std::vector<int> sceneryDeleteQueue;
	std::vector<Actor*> activeSwapQueue;
	Engine* parentEngine;

	std::unique_ptr<ComponentFactoryFactory> componentFactory;

	ComponentFactoryCreatorImplementation<PhysicsComponent> physicsFactory;
	ComponentFactoryCreatorImplementation<GraphicsComponent> graphicsFactory;
	ComponentFactoryCreatorImplementation<InputComponent> inputFactory;
	ComponentFactoryCreatorImplementation<AIComponent> aIFactory;
	ComponentFactoryCreatorImplementation<AudioComponent> audioFactory;
	ComponentFactoryCreatorImplementation<AnimationComponent> animationFactory;
	ComponentFactoryCreatorImplementation<ParticleComponent> particleFactory;
	ComponentFactoryCreatorImplementation<CombatComponent> combatFactory;
	
};