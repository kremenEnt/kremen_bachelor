#include "CustomActorData.h"

CustomActorData::CustomActorData(std::string xmlPath) :
	ActorData(xmlPath)
{
	tinyxml2::XMLElement* componentElement;
	if (componentsElement->FirstChildElement("FlashOnHitComponent"))
	{
		FlashOnHit flashOnHit;
		flashOnHit.type = "FlashOnHitComponent";
		componentElement = componentsElement->FirstChildElement("FlashOnHitComponent");

		if (componentElement->FirstChildElement("category"))
		{
			flashOnHit.identifier = componentElement->FirstChildElement("category")->GetText();
		}
		if (componentElement->FirstChildElement("flashTimer"))
		{
			componentElement->FirstChildElement("flashTimer")->QueryFloatText(&flashOnHit.invulnerabilityTime);
		}
		componentDataVector.push_back(std::make_unique<FlashOnHit>(flashOnHit));
	}
}
