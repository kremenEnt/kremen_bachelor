#pragma once
#include "ActorComponent.h"
#include <unordered_map>
#include "Animation.h"

class IAnimationComponent : public ActorComponent
{
public:
	IAnimationComponent(ActorData::ComponentData *componentData)
		: ActorComponent(componentData) {};
	virtual ~IAnimationComponent() {};

protected:
	std::unordered_map<int, Animation> animations;
	ActorData::Animation componentData;
};
