#pragma once
#include "PhysicsComponent.h"
#include "Engine.h"
#include "WorldGenerator.h"

class PhysicsGoal : public PhysicsComponent
{
public:
	PhysicsGoal(ActorData::ComponentData* componentData)
		: PhysicsComponent(componentData)
	{

	}

	~PhysicsGoal()
	{

	}

	void onCollision(int fixtureUserData, Actor* collidedActor) override
	{
		switch (fixtureUserData)
		{
		case CATEGORY_CLONE:
			if (doUserFeedback)
			{
				parentEngine->setGameState(STATE_GAMEOVER);
			}
			else
			{
				newLevel = true;
			}
			break;
		default:
			break;
		}
	}

	void update(float deltaTime) override
	{
		angle += 1.f * deltaTime;
		dynamic_cast<GraphicsComponent*>(actor->getComponent("GraphicsComponent"))->setRotation(angle, 'y');

		if (newLevel)
		{
			parentEngine->getWorldGenerator()->newLevel();
			newLevel = false;
		}
	}
	
private:
	bool newLevel = false;
	float angle = 0.f;
};