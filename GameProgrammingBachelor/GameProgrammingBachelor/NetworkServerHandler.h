#pragma once
#include "INetworkHandler.h"
#include <vector>
#include "Observable.h"

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>
#include <RakNetTypes.h>


class InputComponent;

/**
 * @struct	Player
 *
 * @brief	A player.
 */
struct Player
{
	std::string name;
	InputComponent* input;
};

/**
 * @class	NetworkServerHandler
 *
 * @brief	A network server handler.
 */
class NetworkServerHandler : public  INetworkHandler, public IObservable
{
public:

	/**
	 * @fn	NetworkServerHandler::NetworkServerHandler();
	 *
	 * @brief	Default constructor.
	 */
	NetworkServerHandler();

	/**
	 * @fn	NetworkServerHandler::~NetworkServerHandler();
	 *
	 * @brief	Destructor.
	 */
	~NetworkServerHandler();

	/**
	 * @fn	void NetworkServerHandler::init();
	 *
	 * @brief	initializes this object.
	 */
	void init();

	/**
	 * @fn	void NetworkServerHandler::initClient();
	 *
	 * @brief	Initializes client.
	 */
	void initClient();

	/**
	 * @fn	void NetworkServerHandler::removeClient();
	 *
	 * @brief	Removes the client.
	 */
	void removeClient();

	/**
	 * @fn	void NetworkServerHandler::sendPacket(int gameState) override;
	 *
	 * @brief	Sends a packet.
	 *
	 * @param	gameState	State of the game.
	 */
	void sendPacket(int gameState) override;

	/**
	 * @fn	void NetworkServerHandler::receivePacket(int gameState) override;
	 *
	 * @brief	Receive packet.
	 *
	 * @param	gameState	State of the game.
	 */
	void receivePacket(int gameState) override;

	/**
	 * @fn	void NetworkServerHandler::receiveHosting();
	 *
	 * @brief	Receive hosting state.
	 */
	void receiveHosting();

	/**
	 * @fn	void NetworkServerHandler::receiveRunning();
	 *
	 * @brief	Receive running state.
	 */
	void receiveRunning();

	/**
	 * @fn	void NetworkServerHandler::sendStart();
	 *
	 * @brief	Sends the start state.
	 */
	void sendStart();

	/**
	 * @fn	void NetworkServerHandler::sendHosting();
	 *
	 * @brief	Sends the hosting state.
	 */
	void sendHosting();

	/**
	 * @fn	void NetworkServerHandler::sendRunning();
	 *
	 * @brief	Sends the running state.
	 */
	void sendRunning();

private:
	const int MAXCLIENTS = 7;
	RakNet::RakPeerInterface* peerInterface;
	RakNet::SocketDescriptor socket;
	//used for receiving and sending packets
	RakNet::Packet* packet;
	
	std::map<RakNet::RakNetGUID, Player> players;
};