#pragma once
#include "ActorComponent.h"
#include "Timer.h"

/**
 * @class	ICombatComponent
 *
 * @brief	The combat component interface. Used by all actors who engage in some sort of combat or can "die".
 * @defails	Tracks damage and health.
 * 			Dies whenever health or timer reacher zero(whichever occurs first).
 */
class ICombatComponent : public ActorComponent
{
public:
	ICombatComponent(ActorData::ComponentData* componentData):ActorComponent(componentData){ this->componentData = *static_cast<ActorData::Combat*>(componentData); };
	virtual ~ICombatComponent(){};
protected:
	ActorData::Combat componentData;
	bool alive;
	bool invincible;
	int health;
	Timer lifetimeTimer;
	Timer invincibilityTimer;
};

