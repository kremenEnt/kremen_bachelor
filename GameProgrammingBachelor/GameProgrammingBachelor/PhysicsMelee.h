#pragma once
#include "PhysicsComponent.h"

class PhysicsMelee final : public PhysicsComponent
{
public:
	PhysicsMelee(tinyxml2::XMLElement *xmlElement) : PhysicsComponent(xmlElement) {};
	void onCollision(int fixtureUserData, Actor* collidedActor);
private:
};