#pragma once
#include <glm\glm.hpp>
#ifdef __ANDROID__
#include <GLES3/gl31.h>
#else
#include <GL\glew.h>
#endif
#include <vector>
#include "common.h"
#include "Handler.h"
#include "Transform.h"

class Engine;
class Texture;
class Shader;

/**
 * @class	TerrainHandler
 *
 * @brief	A terrain handler.
 */
class TerrainHandler : public Handler
{
public:

	/**
	 * @fn	TerrainHandler::TerrainHandler();
	 *
	 * @brief	Default constructor.
	 */
	TerrainHandler();

	/**
	 * @fn	TerrainHandler::~TerrainHandler();
	 *
	 * @brief	Destructor.
	 */
	~TerrainHandler();

	/**
	 * @fn	void TerrainHandler::init();
	 *
	 * @brief	Initializes this object.
	 */
	void init();

	/**
	 * @fn	void TerrainHandler::generateTerrain(float gridX, float gridZ);
	 *
	 * @brief	Generates a terrain.
	 *
	 * @param	gridX	The grid x coordinate.
	 * @param	gridZ	The grid z coordinate.
	 */
	void generateTerrain(float gridX, float gridZ);

	/**
	 * @fn	void TerrainHandler::update();
	 *
	 * @brief	Updates the terrain.
	 */
	void update();

	/**
	 * @fn	int TerrainHandler::getVertexCount() const;
	 *
	 * @brief	Gets vertex count.
	 *
	 * @return	The amount of vertices.
	 */
	int getVertexCount() const;

	/**
	 * @fn	void TerrainHandler::draw(glm::mat4 & viewProjection);
	 *
	 * @brief	Draws with the perspective given from view projection matrix.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 & viewProjection);

private:
	GLuint vertexArrayObject;
	GLuint vertexArrayBuffer[NUM_BUFFERS];
	glm::vec4 sunPosition;

	unsigned int drawCount;

	const int SIZE;
	const int VERTEX_COUNT;

	float sunDistance = 100.f;
	float x = 0.f;	//in world grid/chunk
	float z = 0.f;	//in world grid/chunk

	Texture *top;
	Texture *slopes;
	Texture *bottom;
	Shader *shader;
	Transform transform;
};

