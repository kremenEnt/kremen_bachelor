#pragma once
#include "Animation.h"
/**
 * @class	Animation
 *
 * @brief	Utility class to help select the snap of which sprite to draw.
 */
class Animation
{
public:

	/**
	 * @fn	Animation::Animation();
	 *
	 * @brief	Default constructor.
	 */
	Animation();

	/**
	 * @fn	Animation::~Animation();
	 *
	 * @brief	Destructor.
	 */
	~Animation();

	/**
	 * @fn	void Animation::config(float animRate, int animFrames, int frameStart);
	 *
	 * @brief	Configs.
	 *
	 * @param	animRate  	The animation rate.
	 * @param	animFrames	The animation frames.
	 * @param	frameStart	The frame start.
	 */
	void config(float animationSpeed, int amountOfFrames, int firstFrame);

	/**
	 * @fn	void Animation::update(float deltaTime);
	 *
	 * @brief	Updates with the given deltaTime.
	 *
	 * @param	deltaTime	The delta time.
	 */
	void update(float deltaTime);

	/**
	 * @fn	void Animation::reset();
	 *
	 * @brief	Resets this object.
	 */
	void reset();

	/**
	 * @fn	int Animation::getFrame();
	 *
	 * @brief	Gets the frame.
	 *
	 * @return	The frame.
	 */
	int	getFrame();

	/**
	 * @fn	float Animation::getSpeed();
	 *
	 * @brief	Gets the speed.
	 *
	 * @return	The speed.
	 */
	float getSpeed();

	/**
	 * @fn	int Animation::getAmount();
	 *
	 * @brief	Gets the amount.
	 *
	 * @return	The amount.
	 */
	int getAmount();

	/**
	 * @fn	int Animation::getFirst();
	 *
	 * @brief	Gets the first.
	 *
	 * @return	The first.
	 */
	int getFirst();

private:
	float currentFrame = 0.f;	//Current animation
	float animationSpeed = 0.f;	//Frames played per second
	int	amountOfFrames = 1; //Total frames to be played
	int	firstFrame = 0; //Offset into the sprite sheet
};