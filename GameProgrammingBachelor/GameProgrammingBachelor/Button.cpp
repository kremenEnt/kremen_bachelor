#include "Button.h"
#include "Engine.h"
#include "GuiElement.h"
#include "FontHandler.h"
#include "EventHandler.h"
#include "MeshHandler.h"
#include "ShaderHandler.h"
#include "TextureHandler.h"
#include "GuiFactory.h"

Button::Button(GuiData::ElementData* elementData) : GuiElement(elementData)
{
	this->elementData = *dynamic_cast<GuiData::Button*>(elementData);
	currentTexture = 0;
	clicked = false;
	fontDisplay = false;
	text = "";
}

Button::~Button()
{
}

void Button::update()
{
	//checks for mouseover
	// TODO: REMOVE MAGIC NUMBERS
	if (parentEngine->getEventHandler()->mouseInside(transform) && !clicked)
	{
		currentTexture = 2;
	}
	else if (clicked)
	{
		currentTexture = 1;
	}
	else
	{
		currentTexture = 0;
	}
	//updates the button text if it exists
}

bool Button::checkMouseClickLeft()
{
	clicked = false;
	if (visible && !parentEngine->getGuiFactory()->getElementOpen())
	{
		if (parentEngine->getEventHandler()->mouseInside(transform))
		{
			clicked = true;

		}
	}
	return clicked;
}

bool Button::checkMouseClickRight()
{
	return false;
}

bool Button::checkMouseRelease()
{
	if (visible && clicked)
	{
		if (parentEngine->getEventHandler()->mouseInside(transform))
		{
			const char * tempName = name.c_str();
			//posts a message with its own name so things subscribed to this get the state the button is in to run functions based on the state
			this->postMessage(Message(this, tempName, true));
			clicked = false;
			return true;
		}
	}
	return false;
}

void Button::draw(glm::mat4 &viewProjection)
{
	if (visible)
	{
		shader->bind();
		shader->loadTransform(transform, viewProjection);
		shader->loadVec2(U_SIZE, glm::vec2(textureAtlasSize, textureAtlasSize));
		shader->loadInt(U_TEXTURE0, 0);
		shader->loadInt(U_SPRITE_NO, currentTexture); //finds what texture to draw
		shader->loadFloat(U_SCALE, 1.5f);
		if (fontDisplay)
		{
			font->update(text, glm::vec3(255, 255, 255));
			shader->loadInt(U_TEXTURE1, 1);
			font->bind(1);
		}
		texture->bind(0);
		mesh->draw();
	}
}	

void Button::init()
{
	float tempPosX;
	float tempPosY;
	float tempWidth;
	float tempHeight;

	name = elementData.name;
	tempPosX = elementData.positionX;
	tempPosY = elementData.positionY;
	tempWidth = elementData.width;
	tempHeight = elementData.height;
	tempHeight = (tempHeight / aspectRatio.y)*aspectRatio.x;
	mesh = parentEngine->getMeshHandler()->loadModel(elementData.meshPath);
	texture = parentEngine->getTextureHandler()->loadTexture(elementData.texturePath);
	shader = parentEngine->getShaderHandler()->loadShader(elementData.shaderPath);
	visible = elementData.visible;
	textureAtlasSize = elementData.atlasSize;

	text = elementData.text;
	if (!text.empty())
	{
		fontDisplay = true;
		font = parentEngine->getFontHandler()->loadFont(elementData.fontPath);
	}

	this->transform.setPos(glm::vec3(tempPosX, tempPosY, 0.999f));
	this->transform.setScale(glm::vec3(tempWidth, tempHeight, 1.0f));
}

std::string Button::getName()
{
	return name;
}

void Button::changeTexture(std::string newTexture)
{
	texture = parentEngine->getTextureHandler()->loadTexture(newTexture);
}

void Button::setVisible(bool visible)
{
	this->visible = visible;
}
