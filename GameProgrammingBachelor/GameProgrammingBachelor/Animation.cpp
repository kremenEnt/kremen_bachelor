#include "Animation.h"


Animation::Animation()
{
}
Animation::~Animation()
{
}

void Animation::config(float animationSpeed, int amountOfFrames, int firstFrame)
{
	this->animationSpeed = animationSpeed;
	this->amountOfFrames = amountOfFrames;
	this->firstFrame = firstFrame;
}

void Animation::update(float deltaTime)
{
	this->currentFrame += this->animationSpeed * deltaTime;
}

void Animation::reset()
{
	this->currentFrame = 0.0f;
}

int Animation::getFrame()
{
	return this->firstFrame + (static_cast<int>(this->currentFrame) % this->amountOfFrames);
}

float Animation::getSpeed()
{
	return this->animationSpeed;
}

int Animation::getAmount()
{
	return this->amountOfFrames;
}

int Animation::getFirst()
{
	return this->firstFrame;
}
