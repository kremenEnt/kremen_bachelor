#include "Astar.h"

Astar::Astar()
{
}


Astar::~Astar()
{
}

std::vector<Node*> Astar::findPath(krem::Vector2i start, krem::Vector2i goal, Grid & world)
{
	nodes.clear();
	std::vector<Node*> openList;
	std::vector<Node*> closedList;
	double distance = start.getDistanceTo(goal);
	if (distance == 0)
	{
		std::cout << "Already at goal node.\n";
		return std::vector<Node*>();
	}
	nodes.emplace_back(std::make_unique<Node>(start, nullptr, 0.0, distance));
	Node* current = nodes[0].get();
	openList.emplace_back(current);

	while (!openList.empty())
	{
		std::sort(openList.begin(), openList.end(), Node::sortCost);

		current = openList[0];

		if (current->getPos() == goal)
		{
			std::vector<Node*>path;
			while (current->getParent() != nullptr)	//go from goal back to finish
			{
				path.emplace_back(current);	//fill the vector with all nodes to visit
				current = current->getParent();	//set node to it's parent
			}
			openList.clear();
			closedList.clear();
			return path;
		}
		openList.erase(openList.begin() + 0);
		closedList.emplace_back(current);
		for (int i = 0; i < 9; i++)
		{
			if (i == 4)
			{
				continue; //skip current tile
			}

			int x = current->getPos().x;
			int y = current->getPos().y;
			int xdir = (i % 3) - 1;		//direction from center -1/0/+1
			int ydir = (i / 3) - 1;		//direction from center -1/0/+1
			if (xdir != 0 && ydir != 0)
			{
				continue; //ignore diagonal movement
			}

			Grid::Tile* at = world.getTile({ x + xdir, y + ydir });
			if (at == nullptr)	//out of bounds
			{
				continue;
			}
			if (at->type == Grid::WALL)	//can't walk on walls
			{
				continue;
			}

			//atPos = current tile we're considering
			krem::Vector2i atPos{ x + xdir, y + ydir };
			double gCost = current->getGCost() + current->getPos().getDistanceTo(atPos);
			double hCost = atPos.getDistanceTo(goal);
			nodes.emplace_back(std::make_unique<Node>(atPos, current, gCost, hCost));
			Node* node = nodes.back().get();
			if (vectorInList(closedList, atPos) && gCost >= current->getGCost())
			{
				continue;
			}
			if (!vectorInList(openList, atPos) || gCost < current->getGCost())
			{
				openList.emplace_back(node);
			}
		}
	}
	closedList.clear();
	std::cout << "No path found.\n";
	return std::vector<Node*>();
}

void Astar::test(Grid* grid, int length)
{
	std::vector<Node*>path;
	path.clear();
	grid->fillMap();
	path = findPath({ 0, 0 }, { length-1, length-1 }, *grid);

	if (path.size() != 0)
	{
		std::cout << "Found a path.\n";

		//visualize path
		for (int i = 0; i < length; i++)
		{
			for (int j = 0; j < length; j++)
			{

				Grid::Tile* tile = grid->getTile({ j,i });
				bool found = false;
				for (auto it : path)
				{
					if (it->getPos().x == j && it->getPos().y == i)
					{
						found = true;
						if (it == path[0])
						{
							std::cout << "G "; //goal
							break;
						}
						if (it == path.back())
						{
							std::cout << "S "; //start
							break;
						}
						std::cout << "* "; //path
						break;
					}
				}
				if (!found)
				{
					if (tile->type == Grid::GROUND)
					{
						std::cout << "  ";
					}
					if (tile->type == Grid::WALL)
					{
						std::cout << "[]";
					}
				}

				if (j == length-1) //new line for visualizing
				{
					std::cout << "\n";
				}
			}
		}
		//end of visual
	}
}

bool Astar::vectorInList(std::vector<Node*> list, krem::Vector2i vector)
{
	for (Node* n : list)
	{
		if (n->getPos() == vector)
		{
			return true;
		}
	}
	return false;
}
