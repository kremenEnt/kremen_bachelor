#include "InputComponent.h"
#include "Engine.h"
#include "EventHandler.h"

InputComponent::InputComponent(ActorData::ComponentData * componentData)
{
	this->componentData = *static_cast<ActorData::Input*>(componentData);
}

InputComponent::~InputComponent()
{
	parentEngine->getEventHandler()->removeSubscriber(this);
}

void InputComponent::init()
{
	parentEngine->getEventHandler()->addSubscriber(this);
}

void InputComponent::receiveNetworkInput(InputValues receivedInput)
{
	defaultInputValues = receivedInput;
}

InputValues InputComponent::getInputValues()
{
	return defaultInputValues;
}
