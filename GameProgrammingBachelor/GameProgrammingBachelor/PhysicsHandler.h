#pragma once
#include "IPhysicsHandler.h"
#include "box2d\box2d.h"
#include <glm\glm.hpp>
#include "ContactListener.h"
struct ActorData::Physics::Definition;
/**
 * @class	PhysicsHandler
 *
 * @brief	The physics handler.
 */
class PhysicsHandler : public IPhysicsHandler
{
public:

	/**
	 * @fn	PhysicsHandler::PhysicsHandler();
	 *
	 * @brief	Default constructor. Creates the world
	 */
	PhysicsHandler();

	/**
	 * @fn	PhysicsHandler::~PhysicsHandler() override;
	 *
	 * @brief	Destroys all bodies and the world.
	 */
	~PhysicsHandler() override;

	/**
	 * @fn	void PhysicsHandler::update(float deltaSeconds) override;
	 *
	 * @brief	Updates with the given deltaSeconds.
	 *
	 * @param	deltaSeconds	The delta in seconds between each frame.
	 */
	void update(float deltaSeconds) override;

	/**
	 * @fn
	 * b2Body* PhysicsHandler::createBox(glm::vec2 position, ActorData::Physics::Definition physicsDefinition);
	 *
	 * @brief	Creates a box.
	 *
	 * @param	position		 	The position of the box you want to create.
	 * @param	physicsDefinition	The physics definition.
	 *
	 * @return	The new box.
	 */
	b2Body* createBox(glm::vec2 position, ActorData::Physics::Definition physicsDefinition);

	/**
	 * @fn	b2RevoluteJoint* PhysicsHandler::addJoint(b2Body* bodyA, b2Body* bodyB);
	 *
	 * @brief	Adds a joint to 'bodyB', currently generic and should probably not be used.
	 *
	 * @param [in,out]	bodyA	If non-null, the body a.
	 * @param [in,out]	bodyB	If non-null, the body b.
	 *
	 * @return	A pointer to a b2RevoluteJoint.
	 */
	b2RevoluteJoint* addJoint(b2Body* bodyA, b2Body* bodyB);

	/**
	 * @fn	void PhysicsHandler::dump();
	 *
	 * @brief	Dumps the whole physics data.
	 */
	void dump();

	/**
	 * @fn	void PhysicsHandler::clear();
	 *
	 * @brief	Clears this object to its blank/initial state. Has same code as the constructor
	 */
	void clear();

	/**
	 * @fn	void PhysicsHandler::applyTerrainCollision(std::vector<CollisionData> collisionDataVector);
	 *
	 * @brief	Applies the terrain collision described by collisionDataVector. Uses the marching squares algorithm. We don't use linear interpolation at the end because we want our ground to feel edgy, and not rounded. We also don't cut corners.
	 *
	 * @param	collisionDataVector	The vector which should contain data on every block in the terrain. 
	 */
	void applyTerrainCollision(std::vector<CollisionData> collisionDataVector);
	
	//Requires an at least one vertex BEFORE the function call (in the chainVector) as a reference point.

	/**
	 * @fn
	 * std::vector<b2Vec2> PhysicsHandler::createChain(signed char** cellData, bool** cellDataTraversed,int i, int j);
	 *
	 * @brief	Creates a chain for terrain collision. This is and SHOULD ONLY used by the applyTerrainCollision function. 
	 *
	 * @param [in,out]	cellData		 	If non-null, information describing the cell.
	 * @param [in,out]	cellDataTraversed	If non-null, the cell data traversed.
	 * @param	i						 	Width range.
	 * @param	j						 	Height range.
	 *
	 * @return	The new chain.
	 */
	std::vector<b2Vec2> createChain(signed char** cellData, bool** cellDataTraversed,int i, int j);

	/**
	 * @fn	void PhysicsHandler::setCollisionCategory(b2FixtureDef *fixtureDef, std::string category);
	 *
	 * @brief	Sets collision category.
	 *
	 * @param [in,out]	fixtureDef	If non-null, the fixture definition.
	 * @param	category		  	The category.
	 */
	void setCollisionCategory(b2FixtureDef *fixtureDef, std::string category);
private:
	
	ContactListener contactListener;
	float scale;
	b2Vec2 gravity;
	b2World world;
};
