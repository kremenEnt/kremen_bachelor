#pragma once
#include "ParticleSystem.h"

/**
 * @class	WeatherParticles
 *
 * @brief	A weather particles.
 */
class WeatherParticles : public ParticleSystem
{
public:

	/**
	 * @fn	WeatherParticles::WeatherParticles()
	 *
	 * @brief	Default constructor.
	 */
	WeatherParticles()
		: ParticleSystem() {};

	/**
	 * @fn	void WeatherParticles::update(float deltaTime) override;
	 *
	 * @brief	Updates with the given deltaTime.
	 *
	 * @param	deltaTime	The delta time.
	 */
	void update(float deltaTime) override;

	/**
	 * @fn	void WeatherParticles::createPoint(int amount, glm::vec3 position, float maxLife);
	 *
	 * @brief	Creates a point.
	 *
	 * @param	amount  	The amount.
	 * @param	position	The position.
	 * @param	maxLife 	The maximum life.
	 */
	void createPoint(int amount, glm::vec3 position, float maxLife);
};