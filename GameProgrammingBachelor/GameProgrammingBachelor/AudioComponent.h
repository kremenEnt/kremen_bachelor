#pragma once
#include "IAudioComponent.h"

/**
 * @class	AudioComponent
 *
 * @brief	An audio component.
 */
class AudioComponent : public IAudioComponent
{
private:

	/**
	 * @fn	virtual void AudioComponent::init() override;
	 *
	 * @brief	S this object.
	 */
	virtual void init() override;
public:

	/**
	 * @fn	AudioComponent::AudioComponent(ActorData::ComponentData* componentData);
	 *
	 * @brief	Constructor.
	 *
	 * @param [in,out]	componentData	If non-null, information describing the component.
	 */
	AudioComponent(ActorData::ComponentData* componentData);

	/**
	 * @fn	virtual AudioComponent::~AudioComponent();
	 *
	 * @brief	Destructor.
	 */
	virtual ~AudioComponent();

	/**
	 * @fn	void AudioComponent::onSpawn() override;
	 *
	 * @brief	Executes the spawn action.
	 */
	void onSpawn() override;

	/**
	 * @fn	void AudioComponent::onDeath() override;
	 *
	 * @brief	Executes the death action.
	 */
	void onDeath() override;

	/**
	 * @fn	void AudioComponent::onHit();
	 *
	 * @brief	Executes the hit action.
	 */
	void onHit();
protected:
	ActorData::Audio componentData;
};