#pragma once
#include "PhysicsComponent.h"

class PhysicsRock final : public PhysicsComponent
{
public:
	PhysicsRock(tinyxml2::XMLElement *xmlElement) : PhysicsComponent(xmlElement) {};
	void onCollision(int fixtureUserData, Actor* collidedActor);
private:
};