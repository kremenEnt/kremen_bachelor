#pragma once
#include "PhysicsComponent.h"

class PhysicsPickup final : public PhysicsComponent
{
public:
	PhysicsPickup(tinyxml2::XMLElement *xmlElement) : PhysicsComponent(xmlElement) {};
	void onCollision(int fixtureUserData, Actor* collidedActor);
private:
};