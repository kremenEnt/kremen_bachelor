#include "TerrainHandler.h"
#include "Engine.h"
#include <time.h>
#include <noise.h>
#include "Texture.h"
#include "Shader.h"
#include "obj_loader.h"
#include "SkyboxHandler.h"
#include "TextureHandler.h"
#include "ShaderHandler.h"
#include <glm/gtc/constants.hpp>
#include <iostream>

TerrainHandler::TerrainHandler()
	:SIZE(64), VERTEX_COUNT(256)
{
}


TerrainHandler::~TerrainHandler()
{

}

void TerrainHandler::init()
{
	this->top = parentEngine->getTextureHandler()->loadTexture("res/textures/snow.jpg");
	this->slopes = parentEngine->getTextureHandler()->loadTexture("res/textures/rock2.jpg");
	this->bottom = parentEngine->getTextureHandler()->loadTexture("res/textures/grass.png");
	this->shader = parentEngine->getShaderHandler()->loadShader("res/shaders/terrainShader");
}

void TerrainHandler::generateTerrain(float gridX, float gridZ)
{
	noise::module::Perlin noise;
	noise.SetSeed(parentEngine->getSeed());
	noise.SetOctaveCount(6);
	noise.SetFrequency(1.0); //1-16

	std::cout << "Generating terrain at (" << gridX << ", " << gridZ << ")" << std::endl;
	x = gridX * SIZE;
	z = gridZ * SIZE;

	unsigned long count = VERTEX_COUNT * VERTEX_COUNT;

	std::vector<glm::vec3> positions;
	std::vector<glm::vec2> textureCoords;
	std::vector<glm::vec3> normals;
	std::vector<unsigned int> indices;

	glm::vec3 position;
	glm::vec2 texture;
	glm::vec3 normal;
	unsigned int index;
	for (int i = 0; i < VERTEX_COUNT; i++) {
		for (int j = 0; j < VERTEX_COUNT; j++) {

			float jj = static_cast<float>(j);
			float ii = static_cast<float>(i);
			float vertexCount = static_cast<float>(VERTEX_COUNT - 1);

			position.x = jj / vertexCount * static_cast<float>(SIZE) + x;
			position.z = ii / vertexCount *  static_cast<float>(SIZE) + z;
			position.y = static_cast<float>(noise.GetValue(position.x / 4.f, 0.f, position.z / 4.f));
			// perlinNoise.generateHeight(static_cast<int>(position.x), static_cast<int>(position.z));

			positions.push_back(position);

			normal.x = 0;
			normal.y = 1;
			normal.z = 0;
			normals.push_back(normal);

			texture.x = jj / vertexCount;
			texture.y = ii / vertexCount;
			textureCoords.push_back(texture);
		}
	}	

	for (int gz = 0; gz < VERTEX_COUNT - 1; gz++) {
		for (int gx = 0; gx < VERTEX_COUNT - 1; gx++) {
			int topLeft = (gz*VERTEX_COUNT) + gx;
			int topRight = topLeft + 1;
			int bottomLeft = ((gz + 1)*VERTEX_COUNT) + gx;
			int bottomRight = bottomLeft + 1;

			index = topLeft;
			indices.push_back(index);

			index = bottomLeft;
			indices.push_back(index);

			index = topRight;
			indices.push_back(index);

			index = topRight;
			indices.push_back(index);

			index = bottomLeft;
			indices.push_back(index);

			index = bottomRight;
			indices.push_back(index);
		}
	}

	IndexedModel model;
	model.positions = positions;
	model.texCoords = textureCoords;
	model.normals = normals;
	model.indices = indices;

	model.CalcNormals();

	drawCount = indices.size();
	
	glGenVertexArrays(1, &vertexArrayObject);
	glBindVertexArray(vertexArrayObject);

	glGenBuffers(NUM_BUFFERS, vertexArrayBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, vertexArrayBuffer[POSITION_VB]);
	glBufferData(GL_ARRAY_BUFFER, model.positions.size()*sizeof(model.positions[0]), &model.positions[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, vertexArrayBuffer[TEXCOORD_VB]);
	glBufferData(GL_ARRAY_BUFFER, model.positions.size()*sizeof(model.texCoords[0]), &model.texCoords[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, vertexArrayBuffer[NORMAL_VB]);
	glBufferData(GL_ARRAY_BUFFER, model.normals.size()*sizeof(model.normals[0]), &model.normals[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexArrayBuffer[INDEX_VB]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, model.indices.size()*sizeof(model.indices[0]), &model.indices[0], GL_STATIC_DRAW);

	glBindVertexArray(0);
	std::cout << "Terrain generated." << std::endl;
}

void TerrainHandler::update()
{
	float rotation = parentEngine->getSkyboxHandler()->getRotation();
	rotation -= glm::half_pi<float>();
	float posX = cos(rotation) * sunDistance;
	float posY = sin(rotation) * sunDistance;
	sunPosition = glm::vec4(posX, posY, 0.f, 1.f);
	//this->transform.getRot().y = rotation;
}

int TerrainHandler::getVertexCount() const
{
	return this->VERTEX_COUNT;
}

void TerrainHandler::draw(glm::mat4 & viewProjection)
{
	this->shader->bind();
	this->shader->loadTransform(transform, viewProjection);
	this->shader->loadMat4(U_VIEW, viewProjection);
	this->shader->loadVec4(U_LIGHT_POS, this->sunPosition);
	this->shader->loadInt(U_TEXTURE0, 0);
	this->shader->loadInt(U_TEXTURE1, 1);
	this->shader->loadInt(U_TEXTURE2, 2);
	this->top->bind(0);
	this->slopes->bind(1);
	this->bottom->bind(2);

	//draw
	glBindVertexArray(vertexArrayObject);
	glDrawElements(GL_TRIANGLES, drawCount, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}
