#pragma once
#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

#include <mysql_driver.h>
#include <mysql.h>

#include <iostream>
#include <string>
#include "Handler.h"
#include <vector>
#include <typeinfo>
#include <typeindex>

/**
 * @class	DatabaseHandler
 *
 * @brief	A database handler.
 */
class DatabaseHandler : public Handler
{
public:

	/**
	 * @fn	DatabaseHandler::DatabaseHandler();
	 *
	 * @brief	Default constructor.
	 */
	DatabaseHandler();

	/**
	 * @fn	DatabaseHandler::~DatabaseHandler();
	 *
	 * @brief	Destructor.
	 */
	~DatabaseHandler();

	/**
	 * @fn
	 * void DatabaseHandler::init(std::string hostAddress, std::string username, std::string password, std::string database);
	 *
	 * @brief	inits all the info used to connect to the database.
	 *
	 * @param	hostAddress	The host address.
	 * @param	username   	The username.
	 * @param	password   	The password.
	 * @param	database   	The database.
	 */
	void init(std::string hostAddress, std::string username, std::string password, std::string database);

	/**
	 * @fn	void DatabaseHandler::connect();
	 *
	 * @brief	Connects to the database.
	 */
	void connect();

	/**
	 * @fn	void DatabaseHandler::disconnect();
	 *
	 * @brief	Disconnects from the database.
	 */
	void disconnect();

	/**
	 * @fn
	 * void DatabaseHandler::setVariable(sql::PreparedStatement *preparedStatement, int number, int variable);
	 *
	 * @brief	Sets a variable.
	 *
	 * @param 	preparedStatement			PreparedStatement.
	 * @param	number					 	Table number.
	 * @param	variable				 	The variable.
	 */
	void setVariable(sql::PreparedStatement *preparedStatement, int number, int variable);

	/**
	 * @fn
	 * void DatabaseHandler::setVariable(sql::PreparedStatement *preparedStatement, int number, float variable);
	 *
	 * @brief	Sets a variable.
	 *
	 * @param 	preparedStatement			PreparedStatement.
	 * @param	number					 	Table number.
	 * @param	variable				 	The variable.
	 */
	void setVariable(sql::PreparedStatement *preparedStatement, int number, float variable);

	/**
	 * @fn
	 * void DatabaseHandler::setVariable(sql::PreparedStatement *preparedStatement, int number, std::string variable);
	 *
	 * @brief	Sets a variable.
	 *
	 * @param 	preparedStatement			PreparedStatement.
	 * @param	number					 	Table number.
	 * @param	variable				 	The variable.
	 */
	void setVariable(sql::PreparedStatement *preparedStatement, int number, std::string variable);

	/**
	 * @fn
	 * void DatabaseHandler::setVariable(sql::PreparedStatement *preparedStatement, int number, bool variable);
	 *
	 * @brief	Sets a variable.
	 *
	 * @param 	preparedStatement			PreparedStatement.
	 * @param	number					 	Table number.
	 * @param	variable				 	The variable.
	 */
	void setVariable(sql::PreparedStatement *preparedStatement, int number, bool variable);

	/**
	 * @fn	void DatabaseHandler::setVariables(sql::PreparedStatement *preparedStatement, int number);
	 *
	 * @brief	Empty function to escape recursion.
	 *
	 * @param 	preparedStatement			PreparedStatement.
	 * @param	number					 	Table number.
	 */
	void setVariables(sql::PreparedStatement *preparedStatement, int number);

	/**
	 * @fn
	 * template<typename Variable, typename ... Args> void DatabaseHandler::setVariables(sql::PreparedStatement *preparedStatement, int &number, Variable variable, Args ... args)
	 *
	 * @brief	Sets the variables.
	 *
	 * @tparam	Variable	Type of the variable.
	 * @tparam	Args		Type of the arguments.
	 * @param 	preparedStatement			PreparedStatement.
	 * @param 	number			 			Table number.
	 * @param	variable				 	Current Variable to execute
	 * @param	args					 	Remaining Variables to execute
	 */
	template<typename Variable, typename ... Args>
	void setVariables(sql::PreparedStatement *preparedStatement, int &number, Variable variable, Args ... args)
	{
		setVariable(preparedStatement, number, variable);
		number++;
		setVariables(preparedStatement, number, args...);
	}

	/**
	 * @fn
	 * template<typename ... Args> void DatabaseHandler::insertPreperedStatement(std::string preparedStatement, Args ... args)
	 *
	 * @brief	Inserts a prepered statement to the database.
	 *
	 * @tparam	Args	Type of the arguments.
	 * @param	preparedStatement	The prepared statement.
	 * @param	args			 	the variables used in the prepared statement.
	 */
	template<typename ... Args>
	void insertPreperedStatement(std::string preparedStatement, Args ... args)
	{
	#ifdef DEBUG
	#else
	try
		{
		//connect();

		sql::PreparedStatement  *prep_stmt;

		prep_stmt = connection->prepareStatement(preparedStatement);

		int i = 1;

		setVariables(prep_stmt, i, args...);
		
		prep_stmt->execute();

		delete prep_stmt;
		//delete connection;
		}
	catch (sql::SQLException &e) {
			/*
			MySQL Connector/C++ throws three different exceptions:

			- sql::MethodNotImplementedException (derived from sql::SQLException)
			- sql::InvalidArgumentException (derived from sql::SQLException)
			- sql::SQLException (derived from std::runtime_error)
			*/
			std::cout << "# ERR: SQLException in " << __FILE__;
			std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
			/* what() (derived from std::runtime_error) fetches error message */
			std::cout << "# ERR: " << e.what();
			std::cout << " (MySQL error code: " << e.getErrorCode();
			std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
		}
	#endif // !DEBUG
	}

	/**
	 * @fn
	 * void DatabaseHandler::getVariable(sql::ResultSet * result, std::string column, std::vector<int> &variables);
	 *
	 * @brief	Gets a variable.
	 *
	 * @param [out]	result		 	If non-null, the result.
	 * @param	column			 	The column.
	 * @param [in,out]	variables	The variables.
	 */
	void getVariable(sql::ResultSet * result, std::string column, std::vector<int> &variables);

	/**
	 * @fn
	 * void DatabaseHandler::getVariable(sql::ResultSet * result, std::string column, std::vector<bool> &variables);
	 *
	 * @brief	Gets a variable.
	 *
	 * @param 	preparedStatement			PreparedStatement.
	 * @param	number					 	Table number.
	 * @param	variable				 	The variable.
	 */
	void getVariable(sql::ResultSet * result, std::string column, std::vector<bool> &variables);

	/**
	 * @fn
	 * void DatabaseHandler::getVariable(sql::ResultSet * result, std::string column, std::vector<float> &variables);
	 *
	 * @brief	Gets a variable.
	 *
	* @param 	preparedStatement			PreparedStatement.
	 * @param	number					 	Table number.
	 * @param	variable				 	The variable.
	 */
	void getVariable(sql::ResultSet * result, std::string column, std::vector<float> &variables);

	/**
	 * @fn
	 * void DatabaseHandler::getVariable(sql::ResultSet * result, std::string column, std::vector<std::string> &variables);
	 *
	 * @brief	Gets a variable.
	 *
	 * @param 	preparedStatement			PreparedStatement.
	 * @param	number					 	Table number.
	 * @param	variable				 	The variable.
	 */
	void getVariable(sql::ResultSet * result, std::string column, std::vector<std::string> &variables);

	/**
	 * @fn
	 * void DatabaseHandler::getVariables(sql::ResultSet *result, std::vector<std::string> column, int &vectorPosition);
	 *
	 * @brief	Gets the variables.
	 *
	 * @param 	preparedStatement			PreparedStatement.
	 * @param	number					 	Table number.
	 * @param	variable				 	The variable.
	 */
	void getVariables(sql::ResultSet *result, std::vector<std::string> column, int &vectorPosition);

	/**
	 * @fn
	 * template<typename Variable, typename ... Args> void DatabaseHandler::getVariables(sql::ResultSet *result, std::vector<std::string> column, int &vectorPosition, Variable& variable, Args& ... args)
	 *
	 * @brief	Gets the variables.
	 *
	 * @tparam	Variable	Type of the variable.
	 * @tparam	Args		Type of the arguments.
	 * @param 	result			  		Resultset.
	 * @param	column				  	The column name.
	 * @param	vectorPosition			The vector position, table number.
	 * @param 	variable			  	Current Variable to execute.
	 * @param	args				  	Remaining variables to execute.
	 */
	template<typename Variable, typename ... Args>
	void getVariables(sql::ResultSet *result, std::vector<std::string> column, int &vectorPosition, Variable& variable, Args& ... args)
	{
		getVariable(result, column[vectorPosition], variable);
		vectorPosition++;
		getVariables(result, column, vectorPosition, args...);
	}

	/**
	 * @fn
	 * template<typename ... Args> void DatabaseHandler::retrieveData(std::string sqlStatement, std::vector<std::string> column, Args& ... args)
	 *
	 * @brief	Retrieves a data.
	 *
	 * @tparam	Args	Type of the arguments.
	 * @param	sqlStatement	The SQL statement.
	 * @param	column			The column.
	 * @param	args			Variables to send to get variables.
	 */
	template<typename ... Args>
	void retrieveData(std::string sqlStatement, std::vector<std::string> column, Args& ... args)
	{
	#ifdef DEBUG
	#else
	try
		{
		//connect();
		sql::Statement *statement;
		sql::ResultSet  *result;

		statement = connection->createStatement();

		result = statement->executeQuery(sqlStatement);
		int counter = 0;
		while (result->next()) {
			getVariables(result, column, counter, args...);
			counter = 0;
		}

		delete result;
		delete statement;
		//delete connection;
	}
	catch (sql::SQLException &e) {
		/*
		MySQL Connector/C++ throws three different exceptions:

		- sql::MethodNotImplementedException (derived from sql::SQLException)
		- sql::InvalidArgumentException (derived from sql::SQLException)
		- sql::SQLException (derived from std::runtime_error)
		*/
		std::cout << "# ERR: SQLException in " << __FILE__;
		std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
		/* what() (derived from std::runtime_error) fetches error message */
		std::cout << "# ERR: " << e.what();
		std::cout << " (MySQL error code: " << e.getErrorCode();
		std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
	}
	#endif // !DEBUG
	}
private:
	std::string hostAddress;
	std::string username;
	std::string password;
	std::string database;

	sql::mysql::MySQL_Driver *driver;
	sql::Connection *connection;
};