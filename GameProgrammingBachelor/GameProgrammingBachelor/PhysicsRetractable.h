#pragma once
#include "PhysicsComponent.h"
#include "Actor.h"
#include "Engine.h"
#include <math.h>
#include <glm\gtc\constants.hpp>
#include "CombatComponent.h"
#include <Box2D\Box2D.h>
#include <iostream>

class PhysicsRetractable : public PhysicsComponent
{
public:
	
	PhysicsRetractable(ActorData::ComponentData* componentData)
		: PhysicsComponent(componentData)
	{
		this->position = 0.0f;
		this->speed = 1.f;
		this->direction.x = 0.f;
		this->direction.y = 0.5f;
	}
	
	void onCollision(int fixtureUserData, Actor* collidedActor) override
	{
		switch (fixtureUserData)
		{
		case CATEGORY_CLONE:
			dynamic_cast<CombatComponent*>(collidedActor->getComponent("CombatComponent"))->changeHealth(-damage);
			break;
		default:
			break;
		}
	}
	
	void update(float deltaTime) override
	{
		PhysicsComponent::update(deltaTime);
		float pos = std::cos(this->position += (this->speed * deltaTime));

		if (this->position >= glm::two_pi<float>())
		{
			this->position -= glm::two_pi<float>();
		}
		if (this->position <= -glm::two_pi<float>())
		{
			this->position += glm::two_pi<float>();
		}

		body->SetLinearVelocity({ this->direction.x * pos, this->direction.y * pos });
	}
private:
	float speed = 0.0f;
	float position = 0.0f;
	int damage = 10;
	glm::vec2 direction;
};