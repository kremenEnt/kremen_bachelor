#pragma once

#include "common.h"
#include <SDL.h>
#include <string>
#include "Transform.h"
#include "MousePicker.h"
#include "Handler.h"
#include "Observable.h"

/**
 * @class	EventHandler
 *
 * @brief	An event handler for window and keyboard/mouse events.
 * @details 
 */
class EventHandler : public Handler, public IObservable
{
public:

	/**
	 * @fn	EventHandler::EventHandler();
	 *
	 * @brief	Default constructor.
	 */
	EventHandler();

	/**
	 * @fn	EventHandler::~EventHandler();
	 *
	 * @brief	Destructor.
	 */
	~EventHandler();

	/**
	 * @fn	void EventHandler::handleEvents();
	 *
	 * @brief	Handles the events.
	 */
	void handleEvents();

	/**
	 * @fn	void EventHandler::init();
	 *
	 * @brief	Initializes this object.
	 */
	void init();

	/**
	 * @fn	void EventHandler::handleWindowEvents(SDL_Event &SDLevent);
	 *
	 * @brief	Handles the window events.
	 *
	 * @param [in,out]	SDLevent	The SDL event.
	 */
	void handleWindowEvents(SDL_Event &SDLevent);

	/**
	 * @fn	void EventHandler::handleMouseMotion(SDL_Event &SDLEvent);
	 *
	 * @brief	Handles mouse motion.
	 *
	 * @param [in,out]	SDLEvent	The SDL event.
	 */
	void handleMouseMotion(SDL_Event &SDLEvent);

	/**
	 * @fn	void EventHandler::handleMouseDown(SDL_Event &SDLEvent);
	 *
	 * @brief	Handles mouse event mouse held down.
	 *
	 * @param [in,out]	SDLEvent	The SDL event.
	 */
	void handleMouseDown(SDL_Event &SDLEvent);

	/**
	 * @fn	void EventHandler::handleMouseUp(SDL_Event &SDLEvent);
	 *
	 * @brief	Handles mouse event mouse up.
	 *
	 * @param [in,out]	SDLEvent	The SDL event.
	 */
	void handleMouseUp(SDL_Event &SDLEvent);

	/**
	 * @fn	void EventHandler::handleMouseWheel(SDL_Event &SDLEvent);
	 *
	 * @brief	Handles mouse wheel.
	 *
	 * @param [in,out]	SDLEvent	The SDL event.
	 */
	void handleMouseWheel(SDL_Event &SDLEvent);

	/**
	 * @fn	void EventHandler::updateMousePosition(SDL_Event & SDLEvent);
	 *
	 * @brief	Updates the mouse position.
	 *
	 * @param [in,out]	SDLEvent	The SDL event.
	 */
	void updateMousePosition(SDL_Event & SDLEvent);

	/**
	 * @fn	bool EventHandler::mouseInside(Transform trans);
	 *
	 * @brief	Checks if the mouse is inside a square, used for GUI elements.
	 *
	 * @param	trans	Transform object.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool mouseInside(Transform trans);

	/**
	 * @fn	void EventHandler::handleConsoleTextActions(SDL_Event &SDLevent);
	 *
	 * @brief	Handles console text actions.
	 *
	 * @param [in,out]	SDLevent	The SDL event.
	 */
	void handleConsoleTextActions(SDL_Event &SDLevent);

	/**
	 * @fn	void EventHandler::handleConsoleInputText(SDL_Event &SDLevent);
	 *
	 * @brief	Handles console text input.
	 *
	 * @param [in,out]	SDLevent	The SDL event.
	 */
	void handleConsoleInputText(SDL_Event &SDLevent);

	/**
	 * @fn	void EventHandler::handleTextActions(SDL_Event &SDLevent);
	 *
	 * @brief	Handles text actions.
	 *
	 * @param [in,out]	SDLevent	The SDL event.
	 */
	void handleTextActions(SDL_Event &SDLevent);

	/**
	 * @fn	void EventHandler::handleInputText(SDL_Event &SDLevent);
	 *
	 * @brief	Handles text input.
	 *
	 * @param [in,out]	SDLevent	The SDL event.
	 */
	void handleInputText(SDL_Event &SDLevent);

	/**
	 * @fn	void EventHandler::swapConsoleStatus();
	 *
	 * @brief	Swap console status. True/false
	 */
	void swapConsoleStatus();

	/**
	 * @fn	void EventHandler::setInputStatus(bool status);
	 *
	 * @brief	Sets input status true/false. Whether or not to take text input
	 *
	 * @param	status	sets inputActive.
	 */
	void setInputStatus(bool status);

	/**
	 * @fn	glm::ivec2 EventHandler::getMousePos();
	 *
	 * @brief	Gets mouse position.
	 *
	 * @return	The mouse position.
	 */
	glm::ivec2 getMousePos();

	/**
	 * @fn	glm::vec3 EventHandler::getMouseOpenGLPos();
	 *
	 * @brief	Gets mouse position in openGL space.
	 *
	 * @return	The mouse position in openGL space.
	 */
	glm::vec3 getMouseOpenGLPos();

	/**
	 * @fn	std::string EventHandler::getInputText();
	 *
	 * @brief	Gets input text.
	 *
	 * @return	The input text.
	 */
	std::string getInputText();
private:
	glm::vec3 mousePos; //in 3d opengl space
	MousePicker mousePicker;
	int screenResolutionX, screenResolutionY;
	int mouseX, mouseY; //X Y position of mouse, have to be ints, in window pixels
	int prevMouseX, prevMouseY;
	bool consoleActive;
	bool inputActive;
	std::string inputText;
	std::string consoleInputText;

#ifdef __ANDROID__
	SDL_Point touchLocation;
#endif
};



