#include "Texture.h"
#include <typeinfo>
#include "stb_image.h"
#include <iostream>

#ifdef __ANDROID__
#include <android/log.h>
Texture::Texture(const std::string & filename, krem::android::AssetManager * assetmgr)
#else
Texture::Texture(const std::string & filename)
#endif
{
	int width, height, numComponents;
	unsigned char* imageData;
#ifdef __ANDROID__
	auto tempCharVec = getSTBString(filename, assetmgr);
	unsigned char* tempUnChar = reinterpret_cast<unsigned char*>(tempCharVec.data());
	imageData = stbi_load_from_memory(tempUnChar, sizeof(tempUnChar) * tempCharVec.size(), &width, &height, &numComponents, 4);
#else
	imageData = stbi_load(filename.c_str(), &width, &height, &numComponents, 4);
#endif
	if (imageData == NULL)
	{
#ifdef __ANDROID__
		__android_log_print(ANDROID_LOG_VERBOSE, "RogueLike : ", "Something went wrong with imageData in Texture.cpp");
#endif
		std::cerr << "Texture loading failed for texture: " << filename << std::endl;
	}

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	GLint majVers, minVers;
	glGetIntegerv(GL_MAJOR_VERSION, &majVers);
	glGetIntegerv(GL_MINOR_VERSION, &minVers);


	if (majVers >= 4 && minVers >= 2)	//officially supported since 4.2
	{
		GLint num_mipmaps = 4;	//4 seems to be the sweet spot

		glTexStorage2D(GL_TEXTURE_2D, num_mipmaps, GL_RGBA8, width, height);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);	//GL_LINEAR for non-mipmap, GL_LINEAR_MIPMAP_LINEAR for mipmap
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);		//linear or nearest
		//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -0.4f);	
	}
#ifdef __ANDROID__
#else
	else if ((majVers == 1 && minVers >= 4) || majVers >= 2) //opengl 1.4 or higher
	{
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}
#endif
	else //you should probably upgrade your computer
	{
		//gluBuild2DMipmaps(GL_TEXTURE_2D, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
		//this should enable mipmapping on older hardware but got nothing to test it on.
		//requires hardware from 2000 or down as 1.3 was introduces in 2001
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	GLfloat largest_supported_anisotropy = 0.f;														
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &largest_supported_anisotropy);					//usually 16x filtering
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, largest_supported_anisotropy);	//anisotropic filtering
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
	stbi_image_free(imageData);
}

Texture::~Texture()
{
	glDeleteTextures(1, &texture);
}

void Texture::bind(unsigned int unit)
{
	//GLint whichID;
	//glGetIntegerv(GL_TEXTURE_BINDING_2D, &whichID);
	//if (whichID != texture)		//potensial fps increase
	assert(unit >= 0 && unit <= 31);
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(GL_TEXTURE_2D, texture);
}

#ifdef __ANDROID__
#include "android\AssetFile.h"
#include "android\AssetManager.h"
#include <android\log.h>
const std::vector<char> Texture::getSTBString(std::string path, krem::android::AssetManager * assetmgr)
{
	krem::android::AssetFile AF = assetmgr->open(path);
	auto tempBuffer = AF.readAll();
	return tempBuffer;
}
#endif