#include "ContactListener.h"
#include "Actor.h"

ContactListener::ContactListener()
{

}

void ContactListener::BeginContact(b2Contact * contact)
{
	fixturePtrA = contact->GetFixtureA();
	fixturePtrB = contact->GetFixtureB();

	fixtureUserDataA = fixturePtrA->GetUserData();
	fixtureUserDataB = fixturePtrB->GetUserData();

	bodyUserDataA = fixturePtrA->GetBody()->GetUserData();
	bodyUserDataB = fixturePtrB->GetBody()->GetUserData();

	bool aIsSensor = isSensor(fixtureUserDataA);
	bool bIsSensor = isSensor(fixtureUserDataB);

	if (!aIsSensor && !bIsSensor)
	{
		if (reinterpret_cast<int>(fixtureUserDataA) == CATEGORY_SCENERY)
		{
			static_cast<PhysicsComponent*>(bodyUserDataB)->onCollision(reinterpret_cast<int>(fixtureUserDataA), nullptr);	//must be reinterpret because box2d wants void*
		}
		else if (reinterpret_cast<int>(fixtureUserDataB) == CATEGORY_SCENERY)
		{
			static_cast<PhysicsComponent*>(bodyUserDataA)->onCollision(reinterpret_cast<int>(fixtureUserDataB), nullptr);
		}
		else
		{
			static_cast<PhysicsComponent*>(bodyUserDataA)->onCollision(reinterpret_cast<int>(fixtureUserDataB), static_cast<PhysicsComponent*>(bodyUserDataB)->getactor());
			static_cast<PhysicsComponent*>(bodyUserDataB)->onCollision(reinterpret_cast<int>(fixtureUserDataA), static_cast<PhysicsComponent*>(bodyUserDataA)->getactor());
		}
	}
	else
	{
		if (aIsSensor && ((reinterpret_cast<int>(fixtureUserDataB) == CATEGORY_SCENERY || ((reinterpret_cast<int>(fixtureUserDataB) == CATEGORY_SPECIAL)))))
		{
			bodyUserDataA = fixturePtrA->GetBody()->GetUserData();
			static_cast<PhysicsComponent*>(bodyUserDataA)->startContact(fixtureUserDataA);
		}
		if (bIsSensor && ((reinterpret_cast<int>(fixtureUserDataA) == CATEGORY_SCENERY || ((reinterpret_cast<int>(fixtureUserDataB) == CATEGORY_SPECIAL)))))
		{
			bodyUserDataB = fixturePtrB->GetBody()->GetUserData();
			static_cast<PhysicsComponent*>(bodyUserDataB)->startContact(fixtureUserDataB);
		}
	}
}

void ContactListener::EndContact(b2Contact * contact)
{
	fixturePtrA = contact->GetFixtureA();
	fixturePtrB = contact->GetFixtureB();

	fixtureUserDataA = fixturePtrA->GetUserData();
	fixtureUserDataB = fixturePtrB->GetUserData();

	bodyUserDataA = fixturePtrA->GetBody()->GetUserData();
	bodyUserDataB = fixturePtrB->GetBody()->GetUserData();

	bool aIsSensor = isSensor(fixtureUserDataA);
	bool bIsSensor = isSensor(fixtureUserDataB);

	PhysicsComponent* physicsComponentA = static_cast<PhysicsComponent*>(bodyUserDataA);
	PhysicsComponent* physicsComponentB = static_cast<PhysicsComponent*>(bodyUserDataB);

	if (!aIsSensor && !bIsSensor)
	{
		if (reinterpret_cast<int>(fixtureUserDataA) == CATEGORY_SCENERY)
		{
			static_cast<PhysicsComponent*>(bodyUserDataB)->endCollision(reinterpret_cast<int>(fixtureUserDataA), nullptr);
		}
		else if (reinterpret_cast<int>(fixtureUserDataB) == CATEGORY_SCENERY)
		{
			static_cast<PhysicsComponent*>(bodyUserDataA)->endCollision(reinterpret_cast<int>(fixtureUserDataB), nullptr);
		}
		else
		{
			static_cast<PhysicsComponent*>(bodyUserDataA)->endCollision(reinterpret_cast<int>(fixtureUserDataB), static_cast<PhysicsComponent*>(bodyUserDataB)->getactor());
			static_cast<PhysicsComponent*>(bodyUserDataB)->endCollision(reinterpret_cast<int>(fixtureUserDataA), static_cast<PhysicsComponent*>(bodyUserDataA)->getactor());
		}
	}
	else
	{
		if (aIsSensor && (reinterpret_cast<int>(fixtureUserDataB) == CATEGORY_SCENERY || reinterpret_cast<int>(fixtureUserDataB) == CATEGORY_SPECIAL))
		{
			bodyUserDataA = fixturePtrA->GetBody()->GetUserData();
			static_cast<PhysicsComponent*>(bodyUserDataA)->endContact(fixtureUserDataA);
		}
		if (bIsSensor && (reinterpret_cast<int>(fixtureUserDataA) == CATEGORY_SCENERY || reinterpret_cast<int>(fixtureUserDataB) == CATEGORY_SPECIAL))
		{
			bodyUserDataB = fixturePtrB->GetBody()->GetUserData();
			static_cast<PhysicsComponent*>(bodyUserDataB)->endContact(fixtureUserDataB);
		}
	}
}

bool ContactListener::isSensor(void * fixtureUserData)
{
	if (fixtureUserData == "jump")
	{
		return true;
	}
	if (fixtureUserData == "wallLeft")
	{
		return true;
	}
	if (fixtureUserData == "wallRight")
	{
		return true;
	}
	return false;
}
