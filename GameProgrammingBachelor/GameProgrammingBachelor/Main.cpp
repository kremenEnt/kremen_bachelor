#include "Engine.h"
#include "Astar.h"
#include "InputClone.h"

#include "PhysicsClone.h"
#include "PhysicsMovingPlatform.h"
#include "PhysicsRetractable.h"
#include "PhysicsGoal.h"
#include "PhysicsSpring.h"
#include "PhysicsTimedBlock.h"
#include "PhysicsSpinningBall.h"
#include "PhysicsTotemMissile.h"

#include "AISorceress.h"
#include "AIWizard.h"
#include "AITotem.h"

#include "FlashOnHitComponent.h"

#include "Astar.h"

#include "DatabaseHandler.h"


int main(int argc, char* args[])
{
	Astar star;
	Grid grid(20, 20);
	grid.fillMap();
	star.test(&grid, 20);

	Engine engine;
	
	engine.init();

	//replace this with something hashed from a file
	engine.getDatabaseHandler()->init("teamkremen.mysql.domeneshop.no", "teamkremen", "YXYMZZok8Z4FTgg", "teamkremen");
	//engine.getDatabaseHandler()->init("tcp://160.153.16.17", "bachelorKremen", "oOlkyGTDD3t[", "db_levelgenerator");

	
	//custom components
	ComponentFactoryCreatorImplementation<FlashOnHitComponent> flashOnHitFactory("FlashOnHitComponent", engine.getActorFactory()->getComponentFactoryFactory());

	//custom actors
	ComponentCreatorImplementation<PhysicsClone> physicsComponent0("Clone", engine.getActorFactory()->getComponentFactoryFactory()->getFactory("PhysicsComponent"));
	ComponentCreatorImplementation<PhysicsMovingPlatform> physicsComponent1("MovingPlatform", engine.getActorFactory()->getComponentFactoryFactory()->getFactory("PhysicsComponent"));
	ComponentCreatorImplementation<PhysicsRetractable> physicsComponent2("MovingSpike", engine.getActorFactory()->getComponentFactoryFactory()->getFactory("PhysicsComponent"));
	ComponentCreatorImplementation<PhysicsGoal> physicsComponent3("Goal", engine.getActorFactory()->getComponentFactoryFactory()->getFactory("PhysicsComponent"));
	ComponentCreatorImplementation<PhysicsSpring> physicsComponent4("Spring", engine.getActorFactory()->getComponentFactoryFactory()->getFactory("PhysicsComponent"));
	ComponentCreatorImplementation<PhysicsTimedBlock> physicsComponent5("TimedBlock", engine.getActorFactory()->getComponentFactoryFactory()->getFactory("PhysicsComponent"));
	ComponentCreatorImplementation<PhysicsSpinningBall> physicsComponent6("SpinningBall", engine.getActorFactory()->getComponentFactoryFactory()->getFactory("PhysicsComponent"));
	ComponentCreatorImplementation<PhysicsTotemMissile> physicsComponent7("TotemMissile", engine.getActorFactory()->getComponentFactoryFactory()->getFactory("PhysicsComponent"));

	ComponentCreatorImplementation<InputClone> inputComponent0("Clone", engine.getActorFactory()->getComponentFactoryFactory()->getFactory("InputComponent"));

	ComponentCreatorImplementation<AISorceress> aiComponent0("Sorceress", engine.getActorFactory()->getComponentFactoryFactory()->getFactory("AIComponent"));
	ComponentCreatorImplementation<AIWizard> aiComponent1("Wizard", engine.getActorFactory()->getComponentFactoryFactory()->getFactory("AIComponent"));
	ComponentCreatorImplementation<AITotem> aiComponent2("Totem", engine.getActorFactory()->getComponentFactoryFactory()->getFactory("AIComponent"));

	engine.run();

	return 0;
}