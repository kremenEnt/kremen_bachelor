#pragma once
#include <string>
#ifdef __ANDROID__
#include <GLES3/gl31.h>
#include <glu.h>
#include <GLES/glext.h>
#include "android\AssetFile.h"
#include "android\AssetManager.h"
#include <SDL.h>
#else
#include <GL\glew.h>
#endif
#include <cassert>
#include <vector>
/**
 * @class	Texture
 *
 * @brief	A texture.
 * @details	Holds a spesific texture for later use to prevent loading same files multiple times.
 */
class Texture
{
public:
#ifdef __ANDROID__
	Texture(const std::string& filename, krem::android::AssetManager * assetmgr);
#else

	/**
	 * @fn	Texture::Texture(const std::string& filename);
	 *
	 * @brief	Constructor.
	 *
	 * @param	filename	Filepath of the file.
	 */
	Texture(const std::string& filename);
#endif

	/**
	 * @fn	Texture::~Texture();
	 *
	 * @brief	Destructor.
	 */
	~Texture();

	/**
	 * @fn	void Texture::bind(unsigned int unit);
	 *
	 * @brief	Binds the given texture.
	 *
	 * @param	unit	Texture number unit.
	 */
	void bind(unsigned int unit);
protected:
private:
#ifdef __ANDROID__
	const std::vector<char> getSTBString(std::string path, krem::android::AssetManager * assetmgr);
	unsigned int texture;
#else
	GLuint texture;
#endif
};
