#pragma once
#include <string>
#include <glm\glm.hpp>
#include "common.h"
#ifdef __ANDROID__
#include <GLES3/gl31.h>
#include "android\AssetFile.h"
#include "android\AssetManager.h"
#else
#include <GL\glew.h>
#endif

class Transform;

/**
 * @class	Shader
 *
 * @brief	A shader.
 * @details	Holds a spesific shader for later use to prevent loading same files multiple times.
 */
class Shader
{
public:
#ifdef __ANDROID__
	Shader(const std::string & fileName, krem::android::AssetManager * assetmgr);
#else

	/**
	 * @fn	Shader::Shader(const std::string& fileName);
	 *
	 * @brief	Constructor.
	 *
	 * @param	fileName	Filepath of the file.
	 */
	Shader(const std::string& fileName);
#endif

	/**
	 * @fn	virtual Shader::~Shader();
	 *
	 * @brief	Destructor.
	 */
	virtual ~Shader();

	/**
	 * @fn	GLuint Shader::createShader(const std::string& text, GLenum shaderType);
	 *
	 * @brief	Creates a shader.
	 *
	 * @param	text	  	The filepath.
	 * @param	shaderType	Type of the shader.
	 */
	GLuint createShader(const std::string& text, GLenum shaderType);

	/**
	 * @fn	void Shader::bind();
	 *
	 * @brief	Binds this object.
	 */
	void bind();

	//Loads uniforms

	/**
	 * @fn	void Shader::loadTransform(const Transform& transform, const glm::mat4 viewProjection);
	 *
	 * @brief	Loads a transform.
	 *
	 * @param	transform	  	The transform.
	 * @param	viewProjection	The view projection.
	 */
	void loadTransform(const Transform& transform, const glm::mat4 viewProjection);

	/**
	 * @fn	void Shader::loadMat4(int location, glm::mat4 matrix);
	 *
	 * @brief	Loads matrix 4.
	 *
	 * @param	location	The location.
	 * @param	matrix  	The matrix.
	 */
	void loadMat4(int location, glm::mat4 matrix);

	/**
	 * @fn	void Shader::loadInt(int location, int integer);
	 *
	 * @brief	Loads an int.
	 *
	 * @param	location	The location.
	 * @param	integer 	The integer.
	 */
	void loadInt(int location, int integer);

	/**
	 * @fn	void Shader::loadFloat(int location, float floating);
	 *
	 * @brief	Loads a float.
	 *
	 * @param	location	The location.
	 * @param	floating	The floating.
	 */
	void loadFloat(int location, float floating);

	/**
	 * @fn	void Shader::loadVec4(int location, glm::vec4 vector);
	 *
	 * @brief	Loads vector 4.
	 *
	 * @param	location	The location.
	 * @param	vector  	The vector.
	 */
	void loadVec4(int location, glm::vec4 vector);

	/**
	 * @fn	void Shader::loadVec2(int location, glm::vec2 vector);
	 *
	 * @brief	Loads vector 2.
	 *
	 * @param	location	The location.
	 * @param	vector  	The vector.
	 */
	void loadVec2(int location, glm::vec2 vector);



protected:
private:
	enum
	{
		VERT,
		FRAG,
		NUM_SHADERS
	};
	GLuint program; //shader program
	GLuint shaders[NUM_SHADERS];
	GLuint uniforms[NUM_UNIFORMS];

	/**
	 * @fn	std::string Shader::loadShader(const std::string & filename);
	 *
	 * @brief	Loads a shader.
	 *
	 * @param	filename	Filepath of the file.
	 *
	 * @return	The shader.
	 */
	std::string loadShader(const std::string & filename);

	/**
	 * @fn	void Shader::checkShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string & errorMessage);
	 *
	 * @brief	Check shader error.
	 *
	 * @param	shader			The shader.
	 * @param	flag			The flag.
	 * @param	isProgram   	true if this object is program.
	 * @param	errorMessage	Message describing the error.
	 */
	void checkShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string & errorMessage);
#ifdef __ANDROID__
	const std::string getShaderString(std::string path, krem::android::AssetManager * assetmgr);
#endif
	//	Shader(const Shader& other) {}
	//void operator=(const Shader& other) {};
};