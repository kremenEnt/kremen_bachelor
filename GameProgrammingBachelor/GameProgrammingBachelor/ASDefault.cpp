#include "ASDefault.h"
#include "Actor.h"
#include "PhysicsComponent.h"
#include "AnimationComponent.h"

ASDefault::ASDefault(Actor * actor)
{
	this->actor = actor;
	if (static_cast<PhysicsComponent*>(actor->getComponent("PhysicsComponent")) != nullptr)
	{
		this->physicsComponent = static_cast<PhysicsComponent*>(actor->getComponent("PhysicsComponent"));
	}
	else
	{
		physicsComponent = nullptr;
	}
	if (static_cast<AnimationComponent*>(actor->getComponent("AnimationComponent")) != nullptr)
	{
		this->animationComponent = static_cast<AnimationComponent*>(actor->getComponent("AnimationComponent"));
	}
	else
	{
		animationComponent = nullptr;
	}
}
