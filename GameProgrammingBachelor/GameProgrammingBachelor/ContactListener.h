#pragma once
#include <typeinfo>

#include <Box2D\Box2D.h>
#include "PhysicsComponent.h"

/**
 * @class	ContactListener
 *
 * @brief	A physics contact listener.
 * @details Listens to when objects start touching and stop touching.
 * 			Used for physicssensors like checking if an actor can jump.
 * 			Also used for dealing with generic collision like if bullet hits an actor.
 * @see		PhysicsComponent
 */
class ContactListener : public b2ContactListener
{
public: 
	ContactListener();
private:

	/**
	 * @fn	void ContactListener::BeginContact(b2Contact* contact);
	 *
	 * @brief	Begins a contact. Do not rename. Is inherited from b2ContactListener
	 *
	 * @param [in,out]	contact	If non-null, the contact.
	 */
	void BeginContact(b2Contact* contact) override;
	
	/**
	 * @fn	void ContactListener::EndContact(b2Contact* contact);
	 *
	 * @brief	Ends a contact. Do not rename. Is inherited from b2ContactListener
	 *
	 * @param [in,out]	contact	If non-null, the contact.
	 */
	void EndContact(b2Contact* contact) override;
	

	/**
	* @fn	void ContactListener::isSensor(void* fixtureUserData);
	*
	* @brief	checks if fixtureUserData is a sensor (by comparing strings! NEEDS WORK!)
	* 			
	*
	*/
	bool isSensor(void* fixtureUserData);
private:
	b2Fixture* fixturePtrA;
	b2Fixture* fixturePtrB;
	void* fixtureUserDataA;
	void* fixtureUserDataB;
	void* bodyUserDataA;
	void* bodyUserDataB;

	b2Fixture* triggeredFixturePtrA;
	b2Fixture* triggeredFixturePtrB;
	void* triggeredFixtureUserDataA;
	void* triggeredFixtureUserDataB;
	void* triggeredBodyUserDataA;
	void* triggeredBodyUserDataB;

};