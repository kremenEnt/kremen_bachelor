#pragma once
#include "IParticleComponent.h"
#include <memory>

/**
 * @class	ParticleComponent
 *
 * @brief	A particle component.
 */
class ParticleComponent : public IParticleComponent, ParticleSystem
{
private:
	virtual void init() override;
public:

	/**
	 * @fn	ParticleComponent::ParticleComponent(ActorData::ComponentData* componentData);
	 *
	 * @brief	Constructor.
	 *
	 * @param [in,out]	componentData	If non-null, information describing the component.
	 */
	ParticleComponent(ActorData::ComponentData* componentData);

	/**
	 * @fn	ParticleComponent::~ParticleComponent();
	 *
	 * @brief	Destructor.
	 */
	~ParticleComponent();

	/**
	 * @fn	void ParticleComponent::update(float deltaTime) override;
	 *
	 * @brief	Updates the given deltaTime.
	 *
	 * @param	deltaTime	The delta time.
	 */
	void update(float deltaTime) override;

	/**
	 * @fn	void ParticleComponent::draw(glm::mat4 viewProjection);
	 *
	 * @brief	Draws the given view projection.
	 *
	 * @param	viewProjection	The view projection.
	 */
	void draw(glm::mat4 viewProjection);

private:
	ActorData::Particle componentData;
};