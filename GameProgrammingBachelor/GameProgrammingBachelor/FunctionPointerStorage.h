#pragma once
#include <map>
#include <string>
#include <typeindex>
#include <typeinfo>
#include <cassert>

/**
 * @class	FunctionPointerStorage
 *
 * @brief	A function pointer storage.
 */
class FunctionPointerStorage
{
private:
	typedef void(*funcPtr)(void);
	std::map<std::string, std::pair<funcPtr, std::type_index>> funcs;

public:

	/**
	 * @fn	template<typename T> void FunctionPointerStorage::insert(std::string s1, T f1)
	 *
	 * @brief	Inserts.
	 *
	 * @tparam	T	Generic type parameter.
	 * @param	s1	Function name String.
	 * @param	f1	Function pointer.
	 */
	template<typename T>
	void insert(std::string s1, T f1)
	{
		auto tt = std::type_index(typeid(f1));
		funcs.insert(std::make_pair(s1, std::make_pair((funcPtr)f1, tt)));
	}

	/**
	 * @fn
	 * template<typename T, typename... Args> T FunctionPointerStorage::searchAndCall(std::string s1, Args&&... args)
	 *
	 * @brief	Searches for the function and calls it.
	 *
	 * @tparam	T   	Generic type parameter.
	 * @tparam	Args	Type of the arguments.
	 * @param	s1  	Function name String
	 * @param	args	Function arguments.
	 *
	 * @return	The found and call.
	 */
	template<typename T, typename... Args>
	T searchAndCall(std::string s1, Args&&... args)
	{
		auto mapIter = funcs.find(s1);

		auto mapVal = mapIter->second;

		auto TypeCastedFunc = (T(*)(Args ...))(mapVal.first);

		assert(mapVal.second == std::type_index(typeid(TypeCastedFunc)));
		return TypeCastedFunc(std::forward<Args>(args)...);
	}

};