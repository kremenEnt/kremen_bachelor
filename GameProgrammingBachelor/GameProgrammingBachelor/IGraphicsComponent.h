#pragma once
#include <string>
#include "ActorComponent.h"
#include "Animation.h"

/**
* @class	GraphicsComponent
*
* @brief	The graphics component interface. everything that needs to be drawn needs this
* @details	Holds pointers to all data needed to draw the actor.
* 			All pointers are owned by their respective handlers
*/
class IGraphicsComponent : public ActorComponent
{
public:
	IGraphicsComponent(ActorData::ComponentData* componentData) { this->componentData = *static_cast<ActorData::Graphics*>(componentData); };
	virtual ~IGraphicsComponent(){};
protected:
	ActorData::Graphics componentData;
	bool flipped = false;
	glm::vec2 tilePosition;
	Animation anim;	
	float xFrames;
	float yFrames;
};