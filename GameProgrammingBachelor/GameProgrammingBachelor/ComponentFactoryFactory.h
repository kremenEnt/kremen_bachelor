#pragma once
#include <map>
#include <string>
#include "ComponentFactoryCreator.h"

class ComponentFactoryFactory
{
public:
	ComponentFactoryCreator* getFactory(const std::string& componentType);

	void registerIt(const std::string& componentType, ComponentFactoryCreator* componentCreator);

private:
	std::map<std::string, ComponentFactoryCreator*> table;
};