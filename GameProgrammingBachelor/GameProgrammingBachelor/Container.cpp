#include "Container.h"
#include "Engine.h"
#include "GuiElement.h"
#include "TextureHandler.h"
#include "EventHandler.h"
#include "MeshHandler.h"
#include "ShaderHandler.h"

Container::Container(GuiData::ElementData* elementData) : GuiElement(elementData)
{
	this->elementData = *dynamic_cast<GuiData::Container*>(elementData);
	visible = true;
	selectedBox = -1;
	activatedBox = -1;
	firstFreeSlot = 0;
}

Container::~Container()
{
}

void Container::setBoxTexture(std::string texturePath)
{
	textureBoxes = parentEngine->getTextureHandler()->loadTexture(texturePath);
}

void Container::update()
{
	if (selectedBox != -1 && movableItems)
	{
		transformContainedItems[selectedBox].setPos(glm::vec3(parentEngine->getEventHandler()->getMouseOpenGLPos().x, parentEngine->getEventHandler()->getMouseOpenGLPos().y, 1.0f));
	}
}

bool Container::checkMouseClickLeft()
{
	if (clickable)
	{ 
		if (visible)
		{
			if (movableItems)
			{
				return movableMouseClick();
			}
			else
			{
				for (int i = 0; i < numberOfBoxes; i++)
				{
					if (parentEngine->getEventHandler()->mouseInside(transformBoxes[i]))
					{
						selectedBox = i;
						const char * tempName = name.c_str();
						this->postMessage(Message(this, tempName, "boxSelected", selectedBox));
						return true;
					}
				}
				if (parentEngine->getEventHandler()->mouseInside(transform))
				{
					return true;
				}
			}
		}
	}
	return false;
}

bool Container::checkMouseClickRight()
{
	if (visible)
	{
		if (clickable)
		{
			if (activatable)
			{
				for (int i = 0; i < numberOfBoxes; i++)
				{
					if (parentEngine->getEventHandler()->mouseInside(transformBoxes[i]))
					{
						for (auto it : containedItems)
						{
							if (i == it.first)
							{
								activatedBox = i;
								const char * tempName = name.c_str();

								//sends a message to all its subscribe with its name and the currently activated box
								this->postMessage(Message(this, tempName, "boxActivated", activatedBox));
								return true;
							}	
						}			
					}
				}
			}
		}
	}
	activatedBox = -1;
	return false;
}

bool Container::movableMouseClick()
{
	for (int i = 0; i < numberOfBoxes; i++)
	{
		if (parentEngine->getEventHandler()->mouseInside(transformBoxes[i]))
		{
			selectedBox = i;
			for (auto it : containedItems)
			{
				if (i == it.first)
				{
					selectedBox = it.first;
				}
			}
			return true;
		}
	}
	if (parentEngine->getEventHandler()->mouseInside(transform))
	{
		selectedBox = -1;
		return true;
	}
	return false;
}

bool Container::checkMouseRelease()
{
	if (visible)
	{
		if (movableItems)
		{
			return movableMouseRelease();
		}
		else
		{
			for (int i = 0; i < numberOfBoxes; i++)
			{
				if (parentEngine->getEventHandler()->mouseInside(transformBoxes[i]))
				{
					return true;
				}
			}
			if (parentEngine->getEventHandler()->mouseInside(transform))
			{
				return true;
			}
		}
	}
	
	return false;
}

bool Container::movableMouseRelease()
{
	for (int i = 0; i < numberOfBoxes; i++)
	{
		if (parentEngine->getEventHandler()->mouseInside(transformBoxes[i]))
		{
			if (selectedBox != -1)
			{
				transformContainedItems[selectedBox].setPos(transformBoxes[selectedBox].getPos());
				bool move = true;
				for (auto it : containedItems)
				{
					if (i == it.first)
					{
						move = false;
						swapItems(selectedBox, i);		
						if (activatable && activatedBox == selectedBox)
						{
							activatedBox = i;
						}
						else if (activatable && activatedBox == i)
						{
							activatedBox = selectedBox;
						}
					}
				}
				if (move)
				{
					sortItems(selectedBox, i);
					if (activatable && activatedBox == selectedBox)
					{
						activatedBox = i;
					}
				}
			}
			selectedBox = -1;
			return true;
		}
	}
	if (parentEngine->getEventHandler()->mouseInside(transform))
	{
		if (selectedBox != -1)
		{
			transformContainedItems[selectedBox].setPos(transformBoxes[selectedBox].getPos());
			selectedBox = -1;
			return true;
		}
	}

	if (destroyable)
	{
		if (selectedBox != -1)
		{
			transformContainedItems[selectedBox].setPos(transformBoxes[selectedBox].getPos());

				std::map<int, Texture*>::iterator it = containedItems.find(selectedBox);

				if (it != containedItems.end())
				{
						int dropItem = it->first;
						containedItems.erase(it->first);
						if (activatable && activatedBox == dropItem)
						{
							activatedBox = -1;
						}
				

						const char * tempName = name.c_str();
						this->postMessage(Message(this, tempName, "destroy", dropItem));
				}
				selectedBox = -1;
				return true;
		}
	}
	else
	{
		if (selectedBox != -1)
		{
			transformContainedItems[selectedBox].setPos(transformBoxes[selectedBox].getPos());
			selectedBox = -1;
			return true;
		}
	}

	selectedBox = -1;
	return false;
}

void Container::sortItems(int item1, int item2)
{
	const char * tempName = name.c_str();
	this->postMessage(Message(this, tempName, "move", item1, item2));

		std::map<int, Texture*>::iterator it = containedItems.find(item1);

		if (it != containedItems.end())
		{
			Texture* temp = it->second;
			containedItems.erase(it->first);

			containedItems.insert(std::make_pair(item2, temp));
		}
}

void Container::swapItems(int item1, int item2)
{
	std::map<int, Texture*>::iterator it = containedItems.find(item2);

	if (it != containedItems.end())
	{
		Texture * tempItem = containedItems[item1];

		containedItems[item1] = it->second;

		it->second = tempItem;

		const char * tempName = name.c_str();
		this->postMessage(Message(this, tempName, "swap", item1, item2));
	}
}

void Container::setSelectedBox(int box)
{
	selectedBox = box;
	const char * tempName = name.c_str();
	this->postMessage(Message(this, tempName, "boxSelected", selectedBox));
}

void Container::receiveMessage(const Message & message)
{
	//TODO: needs rework. COULD potentially make the engine support and include inventory as a base component, but inventorycomponent needs work in that case. probably better to program it game spesific

	/*if (message.getSenderType() == typeid(InventoryComponent))
	{
		const char* messageType = message.getVariable(0);
		if (strcmp(messageType,"addItemToContainer") == 0)
		{
			Texture* newItem = message.getVariable(1);

			bool exists = false;

			int i = 0;

			if (containedItems.empty())
			{
				firstFreeSlot = 0;
				containedItems.insert(std::make_pair(0, newItem));
			}
			else
			{
				bool newSlot = false;
				for (auto it : containedItems)
				{
					if (it.first != i && !newSlot)
					{
						if (i <= firstFreeSlot)
						{
							firstFreeSlot = i;
							newSlot = true;
						}
					}
					i++;
				}
				if (!newSlot)
				{
					firstFreeSlot = i;
					containedItems.insert(std::make_pair(firstFreeSlot, newItem));
				}
				else
				{
					containedItems.insert(std::make_pair(firstFreeSlot, newItem));
				}
			}
		}
	}*/
}

void Container::draw(glm::mat4 &viewProjection)
{
	if (visible)
	{

		for (std::size_t i = 0; i < transformBoxes.size(); i++)
		{
			for (auto it : containedItems)
			{
				if (i == it.first)
				{
					shader->bind();
					shader->loadTransform(transformContainedItems[i], viewProjection);
					shader->loadInt(U_TEXTURE0, 0);
					shader->loadFloat(U_SCALE, 1.5f);
					it.second->bind(0);
					mesh->draw();
				}
			}
		}

		for (std::size_t i = 0; i < transformBoxes.size(); i++)
		{
			shaderBoxes->bind();
			shaderBoxes->loadTransform(transformBoxes[i], viewProjection);
			shaderBoxes->loadVec2(U_SIZE, glm::vec2(textureAtlasSizeX, textureAtlasSizeY));
			shaderBoxes->loadInt(U_TEXTURE0, 0);
			if (activatable && activatedBox == i)
			{
				shaderBoxes->loadInt(U_SPRITE_NO, 2);
			}
			else if (selectedBox == i)
			{
				shaderBoxes->loadInt(U_SPRITE_NO, 1);
			}
			else
			{
				shaderBoxes->loadInt(U_SPRITE_NO, 0);
			}
			shaderBoxes->loadFloat(U_SCALE, 1.5f);
			textureBoxes->bind(0);
			mesh->draw();
		}

		shader->bind();
		shader->loadTransform(transform, viewProjection);
		shader->loadInt(U_TEXTURE0, 0);
		shader->loadFloat(U_SCALE, 1.5f);
		textureContainer->bind(0);
		mesh->draw();
	}
}

void Container::init()
{
	float tempPosX = 0.f;
	float tempPosY = 0.f;
	float tempWidth = 0.f;
	float tempHeight = 0.f;

	name = elementData.name;
	tempPosX = elementData.positionX;
	tempPosY = elementData.positionY;
	tempWidth = elementData.heightBoxes;
	tempHeight = elementData.widthBoxes;
	tempHeight = (tempHeight / aspectRatio.y)*aspectRatio.x;

	mesh = parentEngine->getMeshHandler()->loadModel(elementData.meshPath);
	textureContainer = parentEngine->getTextureHandler()->loadTexture(elementData.texturePath);
	shader = parentEngine->getShaderHandler()->loadShader(elementData.shaderPath);

	shaderBoxes = parentEngine->getShaderHandler()->loadShader(elementData.shaderBoxes);
	textureBoxes = parentEngine->getTextureHandler()->loadTexture(elementData.textureBoxes);

	textureAtlasSizeX = elementData.atlasSizeX;
	textureAtlasSizeY = elementData.atlasSizeY;

	numberOfBoxesX = elementData.numberOfBoxesX;
	numberOfBoxesY = elementData.numberOfBoxesY;

	distanceBetweenBoxes = elementData.distanceBetweenBoxes;
	borderTop = elementData.borderTop;
	borderBottom = elementData.borderBottom;
	borderLeft = elementData.borderLeft;
	borderRight = elementData.borderRight;

	clickable = elementData.clickable;
	visible = elementData.visible;
	movableItems = elementData.movable;
	activatable = elementData.activatable;
	destroyable = elementData.destroyable;

	float buttonsWidth = 0;
	float buttonsHeight = 0;
	numberOfBoxes = numberOfBoxesX*numberOfBoxesY;

	transform.setPos(glm::vec3(tempPosX, tempPosY, 0.999f));
	transform.setScale(glm::vec3((tempWidth / 2) + borderLeft + borderRight + (distanceBetweenBoxes*numberOfBoxesX), (tempHeight / 2) + borderTop + borderBottom + (distanceBetweenBoxes*numberOfBoxesY), 1.0f));

	buttonsWidth += numberOfBoxesX * tempWidth;
	buttonsHeight += numberOfBoxesY * tempHeight;
	transform.setScale(glm::vec3(transform.getScale().x + buttonsWidth, transform.getScale().y + buttonsHeight, 1.0f));

	Transform tempBox;
	tempBox.setPos(transform.getPos());

	tempPosX = transform.getPos().x - (transform.getScale().x / 2) + ((tempWidth / 2) / 2);
	tempPosY = transform.getPos().y;
	if (borderLeft != 0)
	{
		tempPosX += borderLeft + (tempWidth / 2);
	}
	else
	{
		tempPosX += tempWidth / 2;
	}
	if (borderTop != 0)
	{
		tempPosY -= borderTop;
	}

	float defaultPosX = tempPosX;
	for (int i = 0; i < numberOfBoxesY; i++)
	{
		for (int j = 0; j < numberOfBoxesX; j++)
		{
			tempBox.setScale(glm::vec3(tempWidth, tempHeight, 1.0f));
			tempBox.setPos(glm::vec3(tempPosX, tempPosY, 0.999f));
			tempPosX += tempWidth;
			tempPosX += distanceBetweenBoxes;
			transformBoxes.emplace_back(tempBox);
			/*if (clickable)
			{*/
			tempBox.setScale(glm::vec3(tempBox.getScale().x * 0.85, tempBox.getScale().y * 0.85, 1.0f));
			/*	}
				else
				{
					tempBox.setScale(glm::vec3(tempBox.getScale().x, tempBox.getScale().y, 1.0f));
				}*/
			transformContainedItems.emplace_back(tempBox);
		}
		tempPosY -= distanceBetweenBoxes + tempHeight;
		tempPosX = defaultPosX;
	}


	if (!elementData.presetItems.empty())
	{

		std::string tempCheck;
		Texture* tempTexture;
		int numberOfPresetItems = 0;

		if (clickable)
		{
			selectedBox = 0;
		}
		else
		{
			selectedBox = -1;
		}
		for (std::size_t i = 0; i < elementData.presetItems.size(); i++)
		{
			tempTexture = parentEngine->getTextureHandler()->loadTexture(elementData.presetItems[i]);
			containedItems.insert(std::make_pair(numberOfPresetItems, tempTexture));
			numberOfPresetItems++;
		}
	}
}

void Container::flipVisible()
{
	visible = !visible;
}

void Container::setTexture(int slot, std::string texturePath)
{
	Texture* tempTexture;		
	std::map<int, Texture*>::iterator it = containedItems.find(slot);	
		
	if (it != containedItems.end())
	{
		tempTexture = parentEngine->getTextureHandler()->loadTexture(texturePath);
		it->second = tempTexture;
	}
}

std::string Container::getName()
{
	return name;
}

void Container::setVisible(bool visible)
{
	this->visible = visible;
}

