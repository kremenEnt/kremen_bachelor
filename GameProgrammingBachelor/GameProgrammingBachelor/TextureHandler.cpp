#include "TextureHandler.h"
#include "Engine.h"
#include "Texture.h"
#include <iostream>

TextureHandler::TextureHandler()
{
	textures.clear();
}


TextureHandler::~TextureHandler()
{
	textures.clear();
}

Texture* TextureHandler::loadTexture(const std::string & filePath)
{
	std::unordered_map<std::string, std::unique_ptr<Texture>>::iterator it = textures.find(filePath);
	if (it == textures.end())
	{
		std::cout << "Texture: " << filePath << std::endl;
#ifdef __ANDROID__
		krem::android::AssetManager * mgr = parentEngine->getAssetMgr();
		this->textures[filePath] = std::make_unique<Texture>(filePath, mgr);
#else
		this->textures[filePath] = std::make_unique<Texture>(filePath);
#endif
	}
	return textures[filePath].get();
}