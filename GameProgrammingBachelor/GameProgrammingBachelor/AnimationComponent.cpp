#include "AnimationComponent.h"
#include "common.h"
#include "Engine.h"
#include "Actor.h"
#include "GraphicsComponent.h"

AnimationComponent::AnimationComponent(ActorData::ComponentData* componentData)
	:IAnimationComponent(componentData)
{
	this->componentData = *static_cast<ActorData::Animation*>(componentData);
}

AnimationComponent::~AnimationComponent()
{
	animations.clear();
}

void AnimationComponent::changeAnimation(int animation)
{
		static_cast<GraphicsComponent*>(actor->getComponent("GraphicsComponent"))->updateSpritePositions(componentData.animations[animation]);
}

void AnimationComponent::init()
{	
	Animation anim;
}