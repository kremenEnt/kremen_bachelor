#include "ComponentFactoryFactory.h"

ComponentFactoryCreator* ComponentFactoryFactory::getFactory(const std::string & componentType)
{
	this->table;
	auto it = table.find(componentType);
	if (it != table.end())
	{
		return it->second;
	}
	else
	{
		return nullptr;
	}
}

void ComponentFactoryFactory::registerIt(const std::string & componentType, ComponentFactoryCreator* componentCreator)
{
	table[componentType] = componentCreator;
}