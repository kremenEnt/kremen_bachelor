#include "DatabaseHandler.h"



DatabaseHandler::DatabaseHandler()
{
	/*DatabaseHandler db("tcp://160.153.16.17", "bachelorKremen", "oOlkyGTDD3t[", "db_levelgenerator");
	db.insertData("INSERT INTO Users(username, age, city) VALUES('NewFlobo', 24 ,'Molde')");
	db.retrieveData("SELECT username, age, city FROM Users ORDER BY username ASC");*/
}

void DatabaseHandler::init(std::string hostAddress, std::string username, std::string password, std::string database)
{
	this->hostAddress = hostAddress;
	this->username = username;
	this->password = password;
	this->database = database;
}

DatabaseHandler::~DatabaseHandler()
{
	//delete connection;
}

void DatabaseHandler::connect()
{
#ifdef DEBUG
#else
	driver = sql::mysql::get_mysql_driver_instance();
		
	connection = driver->connect(hostAddress + "/" + database, username, password);
#endif // !DEBUG
}

void DatabaseHandler::disconnect()
{
	delete connection;
}

void DatabaseHandler::setVariables(sql::PreparedStatement * preparedStatement, int number)
{
}

void DatabaseHandler::setVariable(sql::PreparedStatement *preparedStatement, int number, int variable)
{
	preparedStatement->setInt(number, variable);
}

void DatabaseHandler::setVariable(sql::PreparedStatement *preparedStatement, int number, float variable)
{
	preparedStatement->setDouble(number, variable);
}

void DatabaseHandler::setVariable(sql::PreparedStatement *preparedStatement, int number, std::string variable)
{
	preparedStatement->setString(number, variable);
}
void DatabaseHandler::setVariable(sql::PreparedStatement *preparedStatement, int number, bool variable)
{
	preparedStatement->setBoolean(number, variable);
}


void DatabaseHandler::getVariable(sql::ResultSet * result, std::string column, std::vector<int>& variables)
{
	variables.emplace_back(result->getInt(column));
}

void DatabaseHandler::getVariable(sql::ResultSet * result, std::string column, std::vector<bool>& variables)
{
	variables.emplace_back(result->getBoolean(column));
}

void DatabaseHandler::getVariable(sql::ResultSet * result, std::string column, std::vector<float>& variables)
{
	variables.emplace_back(result->getDouble(column));
}

void DatabaseHandler::getVariable(sql::ResultSet * result, std::string column, std::vector <std::string> & variables)
{
	variables.emplace_back(result->getString(column));
}

void DatabaseHandler::getVariables(sql::ResultSet * result, std::vector<std::string> column, int &vectorPosition)
{
}



