#pragma once
#include "ICombatComponent.h"
/**
* @class	ILifeComponent
*
* @brief	A life component.
* @defails	Tracks whether the actor is alive.
* 			Dies whenever health or timer reacher zero(whichever occurs first).	
*/
class CombatComponent : public ICombatComponent
{
private:

	/**
	 * @fn	virtual void CombatComponent::init() override;
	 *
	 * @brief	S this object.
	 */
	virtual void init() override;
public:

	/**
	 * @fn	CombatComponent::CombatComponent(ActorData::ComponentData* componentData);
	 *
	 * @brief	Constructor.
	 *
	 * @param [in,out]	componentData	If non-null, information describing the component.
	 */
	CombatComponent(ActorData::ComponentData* componentData);

	/**
	 * @fn	virtual CombatComponent::~CombatComponent();
	 *
	 * @brief	Destructor.
	 */
	virtual ~CombatComponent();

	/**
	 * @fn	void CombatComponent::update(float deltaTime) override;
	 *
	 * @brief	Updates the given deltaTime.
	 *
	 * @param	deltaTime	The delta time.
	 */
	void update(float deltaTime) override;

	/**
	 * @fn	void CombatComponent::changeHealth(int modifier);
	 *
	 * @brief	Change health.
	 *
	 * @param	modifier	The modifier.
	 */
	void changeHealth(int modifier);

	/**
	 * @fn	void CombatComponent::changeMana(int modifier);
	 *
	 * @brief	Change mana.
	 *
	 * @param	modifier	The modifier.
	 */
	void changeMana(int modifier);

	/**
	 * @fn	int CombatComponent::getHealth();
	 *
	 * @brief	Gets the health.
	 *
	 * @return	The health.
	 */
	int getHealth();

	/**
	 * @fn	int CombatComponent::getDamage();
	 *
	 * @brief	Gets the damage.
	 *
	 * @return	The damage.
	 */
	int getDamage();

	/**
	 * @fn	void CombatComponent::kill();
	 *
	 * @brief	Kills this object.
	 */
	void kill();
public:
};

