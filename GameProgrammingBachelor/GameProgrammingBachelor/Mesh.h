#pragma once
#include <glm/glm.hpp>
#include <string>
#include "common.h"
#ifdef __ANDROID__
#include <GLES3/gl31.h>
#include <GLES/glext.h>
#include "android\AssetFile.h"
#include "android\AssetManager.h"
#else
#include <GL\glew.h>
#endif
#include <vector>

class Vertex;
class IndexedModel;
/**
 * @class	Mesh
 *
 * @brief	The mesh.
 */
class Mesh
{
public:

	/**
	 * @fn	Mesh::Mesh(Vertex* vertices, unsigned int numVertices, unsigned int* indices, unsigned int numIndices);
	 *
	 * @brief	Constructor.
	 *
	 * @param [in]	vertices		Vertex data.
	 * @param	numVertices			Number of vertices.
	 * @param [in]	indices 		Index data.
	 * @param	numIndices			Number of indices.
	 */
	Mesh(Vertex* vertices, unsigned int numVertices, unsigned int* indices, unsigned int numIndices);
#ifdef __ANDROID__
	Mesh(const std::string & filename, krem::android::AssetManager * assetmgr);
#else

	/**
	 * @fn	Mesh::Mesh(const std::string& filename);
	 *
	 * @brief	Constructor./
	 *
	 * @param	filename	Filepath of the file.
	 */
	Mesh(const std::string& filename);
#endif

	/**
	 * @fn	virtual Mesh::~Mesh();
	 *
	 * @brief	Destructor.
	 */
	virtual ~Mesh();

	/**
	 * @fn	void Mesh::draw();
	 *
	 * @brief	Draws this object.
	 */
	void draw();

protected:
private:

	/**
	 * @fn	void Mesh::initMesh(const IndexedModel& model);
	 *
	 * @brief	Initialises the mesh.
	 *
	 * @param	model	The model.
	 */
	void initMesh(const IndexedModel& model);
	GLuint vertexArrayObject;
	GLuint vertexArrayBuffer[NUM_BUFFERS];
	unsigned int drawCount;
};