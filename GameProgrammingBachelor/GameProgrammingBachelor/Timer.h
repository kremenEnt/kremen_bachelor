#pragma once

/**
 * @class	Timer
 *
 * @brief	A timer.
 * @details	Can be used to find elapsed time since creation.
 * 			Can be created with a duration, where hasEnded() will be true after the given time(in seconds)
 * @param	durationTakes an optional float as duration in seconds
 * @see		Timer::hasEnded()
 */
class Timer
{
public:
	//Initializes variables
	Timer(float duration = 0);
	~Timer() {};
	//The various clock actions

	/**
	 * @fn	void Timer::start();
	 *
	 * @brief	Starts this object.
	 */
	void start();

	/**
	 * @fn	void Timer::stop();
	 *
	 * @brief	Stops this object.
	 */
	void stop();
	/**
	* @fn	void Timer::restart();
	*
	* @brief	Restarts timer. Duration is the same.
	*
	*/
	void restart();

	/**
	 * @fn	void Timer::pause();
	 *
	 * @brief	Pauses this object.
	 */
	void pause();

	/**
	 * @fn	void Timer::unpause();
	 *
	 * @brief	Unpauses this object.
	 */
	void unpause();
	/**
	* @fn	void Timer::restart();
	*
	* @brief	Sets Duration. Will overwrite default duration.
	* 
	* @param	duration	Desired duration.
	*/
	void setDuration(float duration);

	//Gets the timer's time

	/**
	 * @fn	float Timer::getTicks();
	 *
	 * @brief	Gets the ticks.
	 *
	 * @return	The ticks.
	 */
	float getTicks();

	//Checks the status of the timer

	/**
	 * @fn	bool Timer::isStarted();
	 *
	 * @brief	Query if this object is started.
	 *
	 * @return	true if started, false if not.
	 */
	bool isStarted();

	/**
	 * @fn	bool Timer::isPaused();
	 *
	 * @brief	Query if this object is paused.
	 *
	 * @return	true if paused, false if not.
	 */
	bool isPaused();

	/**
	 * @fn	bool Timer::hasEnded();
	 *
	 * @brief	Query if this object has ended.
	 *
	 * @return	true if ended, false if not.
	 */
	bool hasEnded();
private:
	//The clock time when the timer started
	unsigned __int32 startTicks;

	//The ticks stored when the timer was paused
	unsigned __int32 pausedTicks;
	float duration;
	//The timer status
	bool paused;
	bool started;
	
};
