#include "CheckBox.h"
#include "Engine.h"
#include "GuiElement.h"
#include "MeshHandler.h"
#include "ShaderHandler.h"
#include "TextureHandler.h"
#include "EventHandler.h"
#include "GuiFactory.h"

CheckBox::CheckBox(GuiData::ElementData* elementData) : GuiElement(elementData)
{
	this->elementData = *dynamic_cast<GuiData::CheckBox*>(elementData);
	currentTexture = 0;
	clicked = false;
}

CheckBox::~CheckBox()
{
}
 
void CheckBox::update()
{
	//checks for mouseover
	if (!status)
	{
		//TODO: REMOVE MAGIC NUMBERS
		if (parentEngine->getEventHandler()->mouseInside(transform) && !clicked)
		{
			currentTexture = 1;
		}
		else if (clicked)
		{
			currentTexture = 3;
		}
		else
		{
			currentTexture = 0;
		}
	}
	else
	{
		if (parentEngine->getEventHandler()->mouseInside(transform) && !clicked)
		{
			currentTexture = 3;
		}
		else if (clicked)
		{
			currentTexture = 1;
		}
		else
		{
			currentTexture = 2;
		}
	}
	const char * tempName = name.c_str();
	//post a message with the name of the check box and if it is currently true or false to run function based on the state. 
	this->postMessage(Message(this, tempName, status));
}

bool CheckBox::checkMouseClickLeft()
{
	if (visible && !parentEngine->getGuiFactory()->getElementOpen())
	{
		if (parentEngine->getEventHandler()->mouseInside(transform))
		{
			clicked = true;
			return clicked;
		}
	}
	
	clicked = false;
	return clicked;
}

bool CheckBox::checkMouseClickRight()
{
	return false;
}

bool CheckBox::checkMouseRelease()
{
	if (visible && clicked)
	{
		if (parentEngine->getEventHandler()->mouseInside(transform))
		{
			if (status)
			{
				status = false;
			}
			else
			{
				status = true;
			}
			clicked = false;
			return true;
		}
	}
	return false;
}


void CheckBox::draw(glm::mat4 &viewProjection)
{
	if (visible)
	{
		shader->bind();
		shader->loadTransform(transform, viewProjection);
		shader->loadVec2(U_SIZE, glm::vec2(textureAtlasSizeX, textureAtlasSizeY));
		shader->loadInt(U_TEXTURE0, 0);
		shader->loadInt(U_TEXTURE1, 1);
		shader->loadInt(U_SPRITE_NO, currentTexture); //finds what texture to draw
		shader->loadFloat(U_SCALE, 1.5f);
		texture->bind(0);
		mesh->draw();
	}
}

void CheckBox::init()
{
	float tempPosX;
	float tempPosY;
	float tempWidth;
	float tempHeight;

	std::string pathTemp;

	name = elementData.name;
	tempPosX = elementData.positionX;
	tempPosY = elementData.positionY;
	tempWidth = elementData.width;
	tempHeight = elementData.height;
	tempHeight = (tempHeight / aspectRatio.y)*aspectRatio.x;
	visible = elementData.visible;
	mesh = parentEngine->getMeshHandler()->loadModel(elementData.meshPath);
	texture = parentEngine->getTextureHandler()->loadTexture(elementData.texturePath);
	shader = parentEngine->getShaderHandler()->loadShader(elementData.shaderPath);

	textureAtlasSizeX = elementData.atlasSizeX;
	textureAtlasSizeY = elementData.atlasSizeY;
	
	defaultStatus = elementData.defaultStatus;

	transform.setPos(glm::vec3(tempPosX, tempPosY, 0.999f));
	transform.setScale(glm::vec3(tempWidth, tempHeight, 1.0f));
	status = defaultStatus;
}

std::string CheckBox::getName()
{
	return name;
}

void CheckBox::changeTexture(std::string newTexture)
{
	texture = parentEngine->getTextureHandler()->loadTexture(newTexture);
}

void CheckBox::setVisible(bool visible)
{
	this->visible = visible;
}
