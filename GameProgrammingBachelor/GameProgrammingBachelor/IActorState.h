#pragma once

/**
 * @class	IActorState
 *
 * @brief	An actor state.
 */
class IActorState
{
public:
	virtual ~IActorState() {};
	virtual int update() = 0;
	//IActorState* update();
	//void handleInput();
	virtual void enter() = 0;
	virtual void leave() = 0;
};