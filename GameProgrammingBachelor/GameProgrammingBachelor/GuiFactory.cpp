#include "GuiFactory.h"
#include "Engine.h"
#include "Message.h"
#include "GraphicsHandler.h"
#include "AudioHandler.h"
#include "WorldGenerator.h"
#include "UserFeedback.h"

//using namespace tinyxml2;

GuiFactory::GuiFactory() :
	guiElementFactory(std::make_unique<GuiElementFactory>()),
	barCreator("bar", guiElementFactory.get()),
	buttonCreator("button", guiElementFactory.get()),
	checkBoxCreator("checkBox", guiElementFactory.get()),
	containerCreator("container", guiElementFactory.get()),
	dropDownCreator("dropDown", guiElementFactory.get()),
	panelCreator("panel", guiElementFactory.get()),
	sliderCreator("slider", guiElementFactory.get()),
	textBoxCreator("textBox", guiElementFactory.get())
{
	GuiData::init();
}

GuiFactory::~GuiFactory()
{
	for (auto& it : currentGuiState)
	{
		it.second->removeSubscriber(this);
	}
}

void GuiFactory::clear()
{
	currentGuiState.clear();
}

//creates a new GUI state. I.e main menu, ingame, option screen by reading xml files
void GuiFactory::createState(const GuiData* stateData)
{
	std::string identifier;
	clear();
	
	for (auto &it : stateData->elementDataVector)
	{
		std::unique_ptr<GuiElement> newElement(guiElementFactory->getCreator(it->type)->create(it.get()));
		identifier = it->name;
		newElement->init(parentEngine);
		newElement->addSubscriber(this);
		currentGuiState.insert(std::make_pair(identifier, std::move(newElement)));
	}
}

void GuiFactory::draw()
{
	//Enable this to get UI in perspective to look good on any monitor, no matter aspect ratio

	/*glm::mat4 projectionMatrix = glm::perspective(70.f, 16.f/9.f, 0.01f, 1000.f);
	glm::vec3 position = { 0.f, 0.f, 15.f };
	glm::vec3 forward = { 0, 0, -1 };
	glm::vec3 up = { 0, 1, 0 };
	glm::mat4 viewMatrix = glm::mat4(glm::lookAt(position, position + forward, up));*/

	glm::mat4 staticCamera =	//don't want UI to float in 3D space but locked to view projection
	{
	1.f, 0, 0, 0,
	0, 1.f, 0, 0,
	0, 0, -1.f, 0,
	0, 0, 0, 1.f
	};

	//staticCamera = projectionMatrix * viewMatrix;
	glDisable(GL_DEPTH);
	
	for (auto& it : currentGuiState)
	{
		it.second->draw(staticCamera);
	}
	glEnable(GL_DEPTH);
}

void GuiFactory::update()
{
	for (auto& it : currentGuiState)
	{
		it.second->update();
	}
}

bool GuiFactory::checkMouseClickLeft()
{
	bool check = false;
	for (auto& it : currentGuiState)
	{
		if (!check)
		{
			check = it.second->checkMouseClickLeft();
		}
	}
	return check;
}

bool GuiFactory::checkMouseClickRight()
{
	bool check = false;
	for (auto& it : currentGuiState)
	{
		if (!check)
		{
			check = it.second->checkMouseClickRight();
		}
	}
	return check;
}

bool GuiFactory::checkMouseRelease()
{
	bool check = false;
	for (auto& it : currentGuiState)
	{
		if (!check)
		{
			check = it.second->checkMouseRelease();
		}
	}
	return check;
}

GuiElement * GuiFactory::getElement(std::string elementName)
{
	auto it = currentGuiState.find(elementName);
	if (it != currentGuiState.end())
	{
		return it->second.get();
	}
	else
	{
		return nullptr;
	}
}

////TODO: same as function above. Remove the shit out of this so it is in a better place
//// All of these are messaged received from different gui elements 
void GuiFactory::receiveMessage(const Message & message)
{
	if (message.getSenderType() == typeid(Button))
	{
		const char * tempMessage = message.getVariable(0);
		if (strcmp(tempMessage, "play") == 0)
		{
			parentEngine->setGameState(STATE_RUNNING);
		}
		if (strcmp(tempMessage, "settings") == 0)
		{
			parentEngine->setGameState(STATE_SETTINGS);
		}
		if (strcmp(tempMessage, "exitgame") == 0)
		{
			parentEngine->setGameState(STATE_QUITTING);
		}
		if (strcmp(tempMessage, "join") == 0)
		{
			parentEngine->setGameState(STATE_JOINING);
		}
		if (strcmp(tempMessage, "host") == 0)
		{
			parentEngine->setGameState(STATE_HOSTING);
		}
		if (strcmp(tempMessage, "testing") == 0)
		{
			parentEngine->setGameState(STATE_GAMEOVER);
		}
		if (strcmp(tempMessage, "mainmenu") == 0)
		{
			parentEngine->setGameState(STATE_MAINMENU);
		}
		if (strcmp(tempMessage, "startgame") == 0)
		{
			parentEngine->setGameState(STATE_RUNNING);
		}
		if (strcmp(tempMessage, "newlevel") == 0)
		{
			parentEngine->getWorldGenerator()->newLevel();
		}

		if (strcmp(tempMessage, "starthosting") == 0)
		{
#ifndef __ANDROID__
			//parentEngine->networkServerHandler->sendStart();
#endif
			parentEngine->setGameState(STATE_RUNNING);
		}
		if (strcmp(tempMessage, "connect") == 0)
		{
			parentEngine->initConnection();
		}

		if (strcmp(tempMessage, "yesbutton") == 0)
		{
			parentEngine->getUserFeedback()->saveAnswer(true);
			parentEngine->getUserFeedback()->update();
		}
		if (strcmp(tempMessage, "nobutton") == 0)
		{
			parentEngine->getUserFeedback()->saveAnswer(false);
			parentEngine->getUserFeedback()->update();
		}
		if (strcmp(tempMessage, "confirmbutton") == 0)
		{
			parentEngine->getUserFeedback()->update();
		}
	}
	if (message.getSenderType() == typeid(CheckBox))
	{
		const char * tempMessage = message.getVariable(0);
		bool tempStatus = message.getVariable(1);
		if (strcmp(tempMessage, "vsync") == 0)
		{
			if (tempStatus)
			{
				SDL_GL_SetSwapInterval(1);
			}
			else
			{
				SDL_GL_SetSwapInterval(0);
			}
		}

		parentEngine->getWorldGenerator()->setBool(tempStatus, tempMessage);
	}

	if (message.getSenderType() == typeid(DropDown))
	{
		const char * tempMessage = message.getVariable(0);
		const char * temp = message.getVariable(1);
		if (strcmp(tempMessage, "resolution") == 0)
		{

			glm::vec2 resolution = parentEngine->getGraphicsHandler()->getWindowResolution();
			//glViewport(0, 0, 640, 360);
		}
		if (strcmp(tempMessage, "display") == 0)
		{
			if (strcmp(temp, "Fullscreen") == 0)
			{
				//graphicsHandler->updateWindow(SDL_WINDOW_FULLSCREEN);
			}
			else if (strcmp(temp, "Windowed") == 0)
			{
				parentEngine->getGraphicsHandler()->updateWindow(0);
			}
			else if (strcmp(temp, "Borderless") == 0)
			{
				parentEngine->getGraphicsHandler()->updateWindow(SDL_WINDOW_BORDERLESS);
			}
		}
	}
	if (message.getSenderType() == typeid(Slider))
	{
		const char * tempMessage = message.getVariable(0);
		int temp = message.getVariable(1);
		if (strcmp(tempMessage, "mastervolume") == 0)
		{
			parentEngine->getAudioHandler()->setMasterVolume(temp);
		}
		if (strcmp(tempMessage, "musicvolume") == 0)
		{
			parentEngine->getAudioHandler()->setMusicVolume(temp);
		}
		if (strcmp(tempMessage, "soundvolume") == 0)
		{
			parentEngine->getAudioHandler()->setSoundVolume(temp);
		}

	}
	if (message.getSenderType() == typeid(Container))
	{
		const char * tempMessage = message.getVariable(0);

		if (strcmp(tempMessage, "actionBar") == 0)
		{
			tempMessage = message.getVariable(1);
			if (strcmp(tempMessage, "boxSelected") == 0)
			{
				/*selectedTool = message.getVariable(2);*/
			}
		}
		

		if (strcmp(tempMessage, "rating") == 0)
		{
			tempMessage = message.getVariable(1);
			if (strcmp(tempMessage, "boxSelected") == 0)
			{
				int rating = message.getVariable(2);
				parentEngine->getUserFeedback()->saveRating(rating);
			}
		}
	}
}

bool GuiFactory::getElementOpen()
{
	bool check = false;
	for (auto& it : currentGuiState)
	{
		if (!check)
		{
			check = it.second->checkOpen();
		}	
	}
	return check;
}

