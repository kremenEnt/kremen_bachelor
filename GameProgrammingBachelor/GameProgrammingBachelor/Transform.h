#pragma once
#include <glm/glm.hpp>
#ifdef __ANDROID__
#include <GLES3/gl31.h> //CHRISTER TODO: is this needed?
#else
#endif
/**
 * @class	Transform
 *
 * @brief	Form for viewing the transaction. 
 */
class Transform
{
public:

	/**
	 * @fn	Transform::Transform(const glm::vec3& pos, const glm::vec3& rot, const glm::vec3& scale);
	 *
	 * @brief	Constructor.
	 *
	 * @param	pos  	The position.
	 * @param	rot  	The rotation.
	 * @param	scale	The scale.
	 */
	Transform(const glm::vec3& pos, const glm::vec3& rot, const glm::vec3& scale);

	 /**
	  * @fn	glm::mat4 Transform::getBillboardModelViewMatrix(glm::mat4 viewMatrix) const;
	  *
	  * @brief	Gets billboard model view matrix.
	  *
	  * @param	viewMatrix	The view matrix.
	  *
	  * @return	The billboard model view matrix.
	  */
	 glm::mat4 getBillboardModelViewMatrix(glm::mat4 viewMatrix) const;

	/**
	 * @fn	Transform::Transform()
	 *
	 * @brief	Default constructor.
	 */
	Transform() { rot = glm::vec3(0, 0, 0); };

	/**
	 * @fn	glm::mat4 Transform::getModel() const;
	 *
	 * @brief	Gets the model.
	 *
	 * @return	The model matrix.
	 */
	glm::mat4 getModel() const;

	/**
	 * @fn	glm::vec3& Transform::getPos();
	 *
	 * @brief	Gets the position.
	 *
	 * @return	The position.
	 */
	glm::vec3& getPos();

	/**
	 * @fn	glm::vec3& Transform::getRot();
	 *
	 * @brief	Gets the rot.
	 *
	 * @return	The rot.
	 */
	glm::vec3& getRot();

	/**
	 * @fn	glm::vec3& Transform::getScale();
	 *
	 * @brief	Gets the scale.
	 *
	 * @return	The scale.
	 */
	glm::vec3& getScale();

	/**
	 * @fn	void Transform::setPos(glm::vec3 pos);
	 *
	 * @brief	Sets a position.
	 *
	 * @param	pos	The position.
	 */
	void setPos(glm::vec3 pos);

	/**
	 * @fn	void Transform::setRot(glm::vec3 rot);
	 *
	 * @brief	Sets a rot.
	 *
	 * @param	rot	The rot.
	 */
	void setRot(glm::vec3 rot);

	/**
	 * @fn	void Transform::setScale(glm::vec3 scale);
	 *
	 * @brief	Sets a scale.
	 *
	 * @param	scale	The scale.
	 */
	void setScale(glm::vec3 scale);

protected:
private:
	glm::vec3 pos = glm::vec3(0.f, 0.f, 0.f);
	glm::vec3 rot = glm::vec3(0.f, 0.f, 0.f);
	glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f);
};