#pragma once
#include <SDL_mixer.h>
#include <iostream>
#include <unordered_map>
#include "Handler.h"

class AudioHandler : public Handler
{
public:
	AudioHandler();
	~AudioHandler();

	/**
	 * Loads music files from a path.
	 *
	 * @param	musicPath	Full pathname of the music file.
	 */
	void loadMusic(std::string musicPath);

	/**
	 * Play sound mapped to the path
	 *
	 * @param	soundPath	Full pathname of the sound file.
	 */
	void playSound(std::string soundPath);

	/**
	 * Play music mapped to the path
	 *
	 * @param	musicPath	Full pathname of the music file.
	 */
	void playMusic(std::string musicPath);

	/*
	 * @fn	void AudioHandler::stopMusic();
	 *
	 * @brief	Stops the current music file that is playing.
	 */	
	void stopMusic();
	
	/**
	 * Sets master volume.
	 *
	 * @param	volume	The volume.
	 */
	void setMasterVolume(int volume);

	/**
	 * Sets sound volume.
	 *
	 * @param	volume	The volume.
	 */
	void setSoundVolume(int volume);

	/**
	 * Sets music volume.
	 *
	 * @param	volume	The volume.
	 */
	void setMusicVolume(int volume);
protected:

private:
	int masterVolume;
	std::string currentMusic;
	std::unordered_map<std::string, Mix_Chunk*> soundEffects;
	std::unordered_map<std::string, Mix_Music*> music;
};