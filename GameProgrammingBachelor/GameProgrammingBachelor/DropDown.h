#pragma once
#include "GuiElement.h"

class Engine;
class GuiElement;

/** A drop down menu. Used to display a lot of options when clicked. Clicking away hides them */
class DropDown : public GuiElement
{
public:
	DropDown(GuiData::ElementData* elementData);
	virtual ~DropDown();

	/** Updates this object. */
	void update() override;

	/**
	 * Determines if mouse click left is inside the drop down.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickLeft() override;

	/**
	 * Determines if mouse click right is inside the drop down.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickRight() override;

	/**
	 * Determines if mouse release is inside the drop down.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseRelease() override;

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection) override;

	/**
	 * Gets the name.
	 *
	 * @return	The name.
	 */
	std::string getName();

	/**
	* Sets the  texture of the drop down menu to a new one in runtime.
	*
	* @param	texture		the texture path that is used when changing.
	*/
	void changeTexture(std::string newTexture);

	/**
	* Sets the drop down menu to visible or not, also deactivates interactions.
	*
	* @param	bool		true or false for visible or not.
	*/
	void setVisible(bool visible);

	/** Checks if the drop down is already open. */
	bool checkOpen() override;
private:
	GuiData::DropDown elementData;

	std::string name;
	std::string currentChoice;
	std::string defaultChoice;

	std::vector<std::string> choices;

	int numberOfChoices;

	bool open;
	bool closing;
	bool fontDisplay;
	bool visible;

	Transform transform;
	std::vector<Transform> choicesTransform;
	std::vector<Mesh*> choicesMesh;

	Font * font;
	Mesh *mesh;
	Shader *shader;

	Texture *defaultTexture;
	Texture *choiceTexture;

	virtual void init() override;
};

