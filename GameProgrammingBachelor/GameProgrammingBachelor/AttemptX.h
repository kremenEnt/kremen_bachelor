#pragma once
#include <map>
#include "Handler.h"

/**
 * @struct	SlotValues
 *
 * @brief	A slot values.
 */
struct SlotValues
{
	float horizontal;
	float vertical;
};

const int xChunkSize = 10;
const int yChunkSize = 10;

/**
 * @struct	Chunk
 *
 * @brief	A chunk.
 */
struct Chunk
{
	SlotValues scores[xChunkSize][yChunkSize];
};

const int amountOfChunks = 5;

/**
 * @struct	MapIdentifier
 *
 * @brief	A map identifier.
 */
struct MapIdentifier
{
	int xPosition;
	int yPosition;
	int chunkNumber;

	/**
	 * @fn	bool operator<(const MapIdentifier&n) const
	 *
	 * @brief	Less-than comparison operator.
	 *
	 * @param	n	The MapIdentifier to process.
	 *
	 * @return	true if the first parameter is less than the second.
	 */
	bool operator<(const MapIdentifier&n) const
	{
		if (chunkNumber < n.chunkNumber)
		{
			return true;
		}
		if (yPosition < n.yPosition)
		{
			return true;
		}
		if (yPosition > n.yPosition)
		{
			return false;
		}
		//if both y-axis are equal
		if (xPosition < n.xPosition)
		{
			return true;
		}

		//default if something fails, but this should never happen
		return false;
	}
};

/**
 * @struct	LevelData
 *
 * @brief	A level data.
 */
struct LevelData
{
	float gapSize;
	float gapDensity;
	float enemyDensity;
	Chunk chunks[amountOfChunks];
};

class Engine;

/**
 * @class	AttemptX
 *
 * @brief	An attempt x coordinate.
 */
class AttemptX : public Handler
{
public:

	/**
	 * @fn	void AttemptX::loadLevelDataFromDB();
	 *
	 * @brief	Loads level data from database.
	 */
	void loadLevelDataFromDB();

	/**
	 * @fn	void AttemptX::saveResultsToDB(bool answer);
	 *
	 * @brief	Saves the results to database.
	 *
	 * @param	answer	true to answer.
	 */
	void saveResultsToDB(bool answer);

	/**
	 * @fn	LevelData AttemptX::getDataForLevel();
	 *
	 * @brief	Gets data for level.
	 *
	 * @return	The data for level.
	 */
	LevelData getDataForLevel();

	/**
	 * @fn	void AttemptX::setSelectedTiles(std::map<MapIdentifier, SlotValues> selectedStartingTiles);
	 *
	 * @brief	Sets selected tiles.
	 *
	 * @param	selectedStartingTiles	The selected starting tiles.
	 */
	void setSelectedTiles(std::map<MapIdentifier, SlotValues> selectedStartingTiles);
private:
	LevelData oldLevelData[5]; //this would store level data which would be older
	std::map<MapIdentifier, SlotValues> selectedStartingTiles;
	
	LevelData levelData;
};