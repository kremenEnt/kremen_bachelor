#include "Engine.h"
#include "Message.h"
#include "Actor.h"
#include "GuiData.h"

#include "DatabaseHandler.h"
#include "GraphicsHandler.h"
#include "PhysicsHandler.h"
#include "AudioHandler.h"
#include "TextureHandler.h"
#include "MeshHandler.h"
#include "ShaderHandler.h"
#include "EventHandler.h"
#include "FontHandler.h"
#include "TerrainHandler.h"
#include "SkyboxHandler.h"

#include "ActorFactory.h"
#include "GuiFactory.h"

#include "WorldGenerator.h"
#include "UserFeedback.h"
#include "LevelGenerator.h"

#include "Camera.h"
#include "WeatherParticles.h"

#ifndef __ANDROID__
#include "ParticleSystem.h"
#include "NetworkServerHandler.h"
#include "NetworkClientHandler.h"
//#include "LightHandler.h"

#include "AttemptX.h"

#endif

void Engine::initMainmenu()
{
	hosting = false;
	joining = false;
	audioHandler->loadMusic("res/sounds/mainMenu.wav");
	getGuiFactory()->createState(&GuiData("res/gui/states/MAINMENU.xml"));
	//audioHandler->playMusic("res/sounds/mainMenu.wav");
}

void Engine::updateMainmenu()
{
	while (gameState == STATE_MAINMENU)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		genericDraw();
	}
	return;
}

void Engine::initSettings()
{
	getGuiFactory()->createState(&GuiData("res/gui/states/SETTINGS.xml"));
}

void Engine::updateSettings()
{
	while (gameState == STATE_SETTINGS)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		genericDraw();
	}
	return;
}

void Engine::initLevelSelect()
{
	getGuiFactory()->createState(&GuiData("res/gui/states/LEVELSELECT.xml"));
}

void Engine::updateLevelSelect()
{
	while (gameState == STATE_LEVELSELECT)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		genericDraw();
	}
	return;
}

void Engine::initHosting()
{
	getGuiFactory()->createState(&GuiData("res/gui/states/HOSTING.xml"));
#ifdef __ANDROID__
#else
	srand(randomSeed);
	networkServerHandler = std::make_unique<NetworkServerHandler>();
	hosting = true;
	addChild(networkServerHandler.get());
	networkServerHandler->init();
#endif
}

void Engine::updateHosting()
{
#ifdef __ANDROID__
#else
	while (gameState == STATE_HOSTING)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		networkServerHandler->receivePacket(gameState);
		if (netUpdateTimer.hasEnded())
		{
			networkServerHandler->sendPacket(gameState);
			netUpdateTimer.restart();
		}
		genericDraw();
	}
	//this allows users to continue joining even after the game has "started". not sure how to do this in a good way. Tried multithreading, but it fucked up
	return;
#endif
}

void Engine::initJoining()
{
	getGuiFactory()->createState(&GuiData("res/gui/states/JOINING.xml"));
	networkClientHandler = std::make_unique<NetworkClientHandler>();
}

void Engine::initConnection()
{
#ifndef __ANDROID__
	std::string hostIP;
	joining = true;
	addChild(networkClientHandler.get());
	std::cout << "\nEnter remote IP ( 127.0.0.1  for local connections ) : ";
	std::cin >> hostIP;
	//std::cin >> remotePort; //fix l8r
	networkClientHandler->init(hostIP);
#endif
}

void Engine::updateJoining()
{
#ifdef __ANDROID__
#else
	while (gameState == STATE_JOINING)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		networkClientHandler->receivePacket(gameState);
		genericDraw();
	}
#endif
}

void Engine::updateJoined()
{
	while (gameState == STATE_JOINED)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		networkClientHandler->receivePacket(gameState);
		genericDraw();
	}
}
void Engine::initRunning()
{
	getGuiFactory()->createState(&GuiData("res/gui/states/RUNNING.xml"));
	levelLoaded = true;
	eventHandler->setInputStatus(false);
	userFeedback->reset();
	printf("seed: is %i\n", randomSeed);
	srand(randomSeed);

	attemptX->loadLevelDataFromDB();
	levelLoader->setActorFactory(actorFactory.get());
	levelLoader->setPhysicsHandler(physicsHandler.get());
	attemptX->setSelectedTiles(levelLoader->loadLevel(attemptX->getDataForLevel()));
	
	worldGenerator->setActorFactory(actorFactory.get());

	if (physicsDebug)
	{
		physicsHandler->dump();
	}
}

void Engine::updateRunning()
{
	while (gameState == STATE_RUNNING)
	{
		genericUpdate();
#ifndef __ANDROID__
		if (netUpdateTimer.hasEnded())
		{
			if (hosting)
			{
				networkServerHandler->sendPacket(gameState);
			}
			if (joining)
			{
				networkClientHandler->sendPacket(gameState);
			}
			netUpdateTimer.restart();
		}
		if (hosting)
		{
			networkServerHandler->receivePacket(gameState);
		}
		if (joining)
		{
			networkClientHandler->receivePacket(gameState);
		}		
#endif
		glm::mat4 viewProjection = graphicsHandler->getCamera()->getViewProjection();
		this->physicsHandler->update(this->deltaTime);
		this->actorFactory->updateAll(this->deltaTime);		
		this->skyboxHandler->update(viewProjection, this->deltaTime);
		weatherParticles->update(this->deltaTime);
		particleSystem->update(this->deltaTime);
		this->worldGenerator->update();

		this->skyboxHandler->draw(viewProjection);	//updates and draws
		this->actorFactory->draw();

#ifndef __ANDROID__
		//this->lightHandler->update(viewProjection);
#endif
		this->weatherParticles->draw(viewProjection);
		this->particleSystem->draw(viewProjection);
		
		////// test light
		/*Transform transform;
		transform.setPos(glm::vec3(5, -5, 0));
		transform.setScale(glm::vec3(5, 5, 1));
		Mesh *mesh = getMeshHandler()->loadModel("res/models/quad.obj");
		Shader *shader = getShaderHandler()->loadShader("res/shaders/basicShader");
		shader->loadTransform(transform, viewProjection);
		mesh->draw();*/
		genericDraw();
	}

	actorFactory->clearFactory();
	physicsHandler->clear();
	
	return;
}

void Engine::initGameover()
{
	getGuiFactory()->createState(&GuiData("res/gui/states/GAMEOVER.xml"));
}

void Engine::updateGameover()
{
	while (gameState == STATE_GAMEOVER)
	{
		updateMenuGraphics();
		drawMenuGraphics();
		genericUpdate();
		genericDraw();
	}
	return;
}

void Engine::initQuitting()
{
	physicsHandler->clear();
}

void Engine::updateQuitting()
{

}



Engine::Engine()
	:
	deltaTime(0),
	previousTime(0),
	netUpdateTimer(1.f / 30.f), //sets network to update every 1.f/x.f second (or x times a second). NOTE: this feels unprecise. should get rid of sdl timers anyways 1
	oneSecondTimer(1),
	joining(false),
	hosting(false)
{
	randomSeed = static_cast<unsigned int>(time(NULL));
	srand(randomSeed);
	running = true;
	
	actorFactory = std::make_unique<ActorFactory>();
	graphicsHandler = std::make_unique<GraphicsHandler>("Smart Level Generator");
	physicsHandler = std::make_unique<PhysicsHandler>();
	audioHandler = std::make_unique<AudioHandler>();
	textureHandler = std::make_unique<TextureHandler>();
	meshHandler = std::make_unique<MeshHandler>();
	guiFactory = std::make_unique<GuiFactory>();
	shaderHandler = std::make_unique<ShaderHandler>();
	eventHandler = std::make_unique<EventHandler>();
	fontHandler = std::make_unique<FontHandler>();
	terrainHandler = std::make_unique<TerrainHandler>();
	skyboxHandler = std::make_unique<SkyboxHandler>();
	xmlHandler = std::make_unique<XmlHandler>();
	worldGenerator = std::make_unique<WorldGenerator>();
	userFeedback = std::make_unique<UserFeedback>();
	databaseHandler = std::make_unique<DatabaseHandler>();

	levelLoader = std::make_unique<LevelGenerator>();


	attemptX = std::make_unique<AttemptX>();

	
	ActorData::init(); //initiates xmlloader for this class only. more efficient to go through map.
#ifdef __ANDROID__
	sdlAndroidActivity = std::make_unique<krem::app::SdlAndroidActivity>();
	sdlAndroidActivity->initialize();
	javaEnv = sdlAndroidActivity->getJavaEnvironment();
	javaAssetManager = sdlAndroidActivity->getJavaAssetManager();
	assetMgr = std::make_unique<krem::android::AssetManager>(javaEnv, javaAssetManager);
#else
	//lightHandler = std::make_unique<LightHandler>();
#endif
	weatherParticles = std::make_unique<WeatherParticles>();
	particleSystem = std::make_unique<ParticleSystem>();
	gameState = STATE_MAINMENU;
}

Engine::~Engine()
{
}

void Engine::init()
{
	addChild(fontHandler.get());
	addChild(shaderHandler.get());
	addChild(meshHandler.get());
	addChild(actorFactory.get());
	addChild(guiFactory.get());
	addChild(eventHandler.get());
	addChild(terrainHandler.get());
	addChild(skyboxHandler.get());
	addChild(textureHandler.get());
	addChild(xmlHandler.get());
	addChild(worldGenerator.get());
	addChild(userFeedback.get());
	addChild(databaseHandler.get());
	addChild(attemptX.get());

#ifdef __ANDROID__
#else
	//addChild(lightHandler.get());
#endif
    eventHandler->init();
	terrainHandler->init();
	terrainHandler->generateTerrain(0, 0);
	skyboxHandler->init();
	skyboxHandler->loadTexture("res/textures/skybox/", SKYBOX_DAY);
	skyboxHandler->loadTexture("res/textures/nightbox/", SKYBOX_NIGHT);

	weatherParticles->init(this, 0.12f, this->textureHandler->loadTexture("res/textures/snowflake.png"), 0.35f, true);
	//weatherParticles->createPoint(1000, glm::vec3(0.f, 0.f, 0.f), 0.01f);
	particleSystem->init(this, 0.02f, this->textureHandler->loadTexture("res/textures/snowball.png"), 0.50f, true);

	//lightHandler->init();
	//lightHandler->addLight(glm::vec3(0.f, 0.f, 0.f), 15.f);

	this->deltaTime = 0.f;

	previousTime = SDL_GetTicks();

}

void Engine::run()
{
	SDL_StartTextInput(); //for console and picking up key input to text input thing from eids� when you could write own name for multiplayer 
	while (gameState != STATE_QUITTING)
	{
		switch (gameState)
		{
		case STATE_MAINMENU:
			initMainmenu();
			updateMainmenu();
			break;
		case STATE_LEVELSELECT:
			initLevelSelect();
			updateLevelSelect();
			break;
		case STATE_HOSTING:
			initHosting();
			updateHosting();
			break;
		case STATE_JOINING:
			initJoining();
			updateJoining();
			break;
		case STATE_JOINED:
			updateJoined();
			break;
		case STATE_RUNNING:
			initRunning();
			if (levelLoaded)
			{
				updateRunning();
			}
			break;
		case STATE_GAMEOVER:
			initGameover();
			updateGameover();
			break;
		case STATE_SETTINGS:
			initSettings();
			updateSettings();
			break;
		default:
			break;
		}
	}
	//one function should be enough
	initQuitting();
	//updateQuitting();
	
	physicsHandler.reset(nullptr);
	SDL_StopTextInput();
}


void Engine::genericUpdate()
{
	eventHandler->handleEvents();
	deltaTime = (SDL_GetTicks() - previousTime) / 1000.f; //1000 = ms per second
	previousTime = SDL_GetTicks();
	guiFactory->update();
	
	if (hosting)
	{
		networkServerHandler->receivePacket(gameState);
		if (netUpdateTimer.hasEnded())
		{
			networkServerHandler->sendPacket(gameState);
			netUpdateTimer.restart();
		}
	}

	if (joining && netUpdateTimer.hasEnded())
	{
		networkClientHandler->receivePacket(gameState);
		if (netUpdateTimer.hasEnded())
		{
			networkClientHandler->sendPacket(gameState);
			netUpdateTimer.restart();
		}
	}

	if (debug)
	{
		if (oneSecondTimer.hasEnded())
		{
			printf("FPS: %i\n", fps);
			fps = 0;
			oneSecondTimer.restart();
		}
		else
		{
			fps++;
		}
	}
}

void Engine::genericDraw()
{
	guiFactory->draw();
	graphicsHandler->swapBuffers();
	graphicsHandler->clearWindow();
}

ActorFactory* Engine::getActorFactory() const
{
	return actorFactory.get();
}

PhysicsHandler* Engine::getPhysicsHandler() const
{
	return physicsHandler.get();
}

AudioHandler* Engine::getAudioHandler() const
{
	return audioHandler.get();
}

GraphicsHandler* Engine::getGraphicsHandler() const
{
	return graphicsHandler.get();
}

TextureHandler* Engine::getTextureHandler() const
{
	return textureHandler.get();
}

MeshHandler* Engine::getMeshHandler() const
{
	return meshHandler.get();
}

GuiFactory* Engine::getGuiFactory() const
{
	return guiFactory.get();
}

ShaderHandler* Engine::getShaderHandler() const
{
	return shaderHandler.get();
}

EventHandler* Engine::getEventHandler() const
{
	return eventHandler.get();
}

FontHandler * Engine::getFontHandler() const
{
	return fontHandler.get();
}
TerrainHandler * Engine::getTerrainHandler() const
{
	return terrainHandler.get();
}
SkyboxHandler * Engine::getSkyboxHandler() const
{
	return skyboxHandler.get();
}
ParticleSystem * Engine::getParticleSystem() const
{
	return particleSystem.get();
}
WorldGenerator * Engine::getWorldGenerator() const
{
	return worldGenerator.get();
}
DatabaseHandler * Engine::getDatabaseHandler() const
{
	return databaseHandler.get();
}
AttemptX * Engine::getAttemptX() const
{
	return attemptX.get();
}
UserFeedback * Engine::getUserFeedback() const
{
	return userFeedback.get();
}
#ifdef __ANDROID__
#else
NetworkClientHandler * Engine::getClientHandler() const
{
	return networkClientHandler.get();
}

NetworkServerHandler * Engine::getServerHandler() const
{
	return networkServerHandler.get();
}
#endif


LevelGenerator * Engine::getLevelLoader() const
{
	return levelLoader.get();
}

XmlHandler * Engine::getXmlHandler() const
{
	return xmlHandler.get();
}

#ifdef __ANDROID__
krem::android::AssetManager * Engine::getAssetMgr() const
{
	return assetMgr.get();
}
#endif

template<typename T>
void Engine::addChild(T t)
{
	t->setParent(*this);
}

int Engine::getSelectedTool() const
{
	return selectedTool;
}

int Engine::getSelectedLevel() const
{
	return selectedLevel;
}

void Engine::setGameState(int gameState)
{
	this->gameState = gameState;
}

int Engine::getGameState()
{
	return gameState;
}

void Engine::setSeed(unsigned int seed)
{
	randomSeed = seed;
	srand(randomSeed);
}

unsigned int Engine::getSeed()
{
	return randomSeed;
}

float Engine::getDeltaTime()
{
	return deltaTime;
}

bool Engine::getJoining()
{
	return joining;
}

void Engine::drawMenuGraphics()
{
	glm::mat4 viewProjection = graphicsHandler->getCamera()->getViewProjection();
	this->skyboxHandler->draw(viewProjection);
	//this->terrainHandler->draw(viewProjection);
	this->weatherParticles->draw(viewProjection);
}

void Engine::updateMenuGraphics()
{
	glm::mat4 viewProjection = graphicsHandler->getCamera()->getViewProjection();
	this->skyboxHandler->update(viewProjection, this->deltaTime);
	//this->terrainHandler->update();
	this->weatherParticles->update(this->deltaTime);
}
