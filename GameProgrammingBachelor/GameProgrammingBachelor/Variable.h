#pragma once

#include <typeinfo>
#include <assert.h>
#include <cstddef>

class Variable
{
public:
	//Default constructor
	Variable() : internalContainer(NULL) { }

	//Regular constructor initialising its value
	template<typename T> Variable(const T &data)
	{
		//Initialise container with data
		this->internalContainer = new Container<T>(data);
	}

	//Copy constructor
	Variable(const Variable &other)
	{
		//Copy content from source
		this->internalContainer = (other.internalContainer != NULL) ? other.internalContainer->Clone() : NULL;
	}

	//Move constructor
	Variable(Variable && var)
	{
		//Copy pointer from source
		this->internalContainer = var.internalContainer;

		//Release pointer from source to avoid double-free
		var.internalContainer = NULL;
	}

	//Releases the allocated memory
	~Variable()
	{
		if (this->internalContainer)
		{
			delete this->internalContainer;
			this->internalContainer = NULL;
		}
	}

	//Copy assignment operator

	template<typename T> Variable& operator = (const T &rhs)
	{
		if (static_cast<const void*>(this) != static_cast<const void*>(&rhs))
		{
			//Free previous values
			if (this->internalContainer != NULL) delete this->internalContainer;

			//Initialise container with data
			this->internalContainer = new Container<T>(rhs);

			//Check if valid
			assert(this->internalContainer != NULL);
		}

		return *this;
	}

	/*template<typename T> Variable& operator=(const T &rhs)
	{
		if (this != &rhs)
		{
			//Free previous values
			if (this->internalContainer != NULL) delete this->internalContainer;

			//Initialise container with data
			this->internalContainer = new Container<T>(data);
		}
		
		return *this;
	}*/

	//Copy assignment operator
	Variable& operator=(const Variable &other)
	{
		if (this != &other)
		{
			//Free previous values
			if (this->internalContainer != NULL) delete this->internalContainer;

			//Copy content from source
			this->internalContainer = other.internalContainer->Clone();
		}

		return *this;
	}

	//Move assignment operator
	Variable& operator=(Variable && other)
	{
		if (this != &other)
		{
			//Free previous values
			if (this->internalContainer != NULL) delete this->internalContainer;

			//Copy pointer from source
			this->internalContainer	= other.internalContainer;

			//Release pointer from source to avoid double-free
			other.internalContainer = NULL;
		}

		return *this;
	}

	template<typename T>
	inline operator T() const
	{
		assert(this->internalContainer != NULL);
		assert(this->internalContainer->TypeID() == typeid(T));

		return (static_cast<Container<T>*>(this->internalContainer))->content;
	}

	//Returns the type of the stored value
	const std::type_info& GetType() const
	{
		if (this->internalContainer != NULL) return this->internalContainer->TypeID();

		return typeid(void);
	}

	//Returns the value stored in the variable
	template<typename T> T& Get() const
	{
		assert(this->interalContainer != NULL);
		assert(this->internalContainer->TypeID() == typeid(T));

		return (static_cast<Container<T>*>(this->internalContainer))->content;
	}

private:
	class BaseContainer
	{
	public:
		virtual ~BaseContainer() {}

		virtual const std::type_info& TypeID()	const = 0;

		virtual BaseContainer* Clone()		const = 0;
	};

	template<typename T>
	class Container : public BaseContainer
	{
	public:
		Container(const T &data) : content(data) {}

		const std::type_info& TypeID() const
		{
			return typeid(T);
		}

		BaseContainer* Clone() const
		{
			return new Container<T>(content);
		}

		const T content;
	};

	BaseContainer* internalContainer; //The container that actually holds the value of this variable
};