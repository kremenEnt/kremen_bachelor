#pragma once
#include "AIComponent.h"
#include "Timer.h"
#include "Actor.h"
#include "Engine.h"
#include "Message.h"
#include "PhysicsComponent.h"
#include "ActorFactory.h"
#include "GraphicsComponent.h"
class AITotem final : public AIComponent
{
public:
	AITotem(ActorData::ComponentData* componentData)
		: AIComponent(componentData),
		direction(-1),
		missileTimer(4.f)
	{
		missileTimer.start();
	}
	void update(float deltatime) override
	{
		if (!init)
		{
			static_cast<GraphicsComponent*>(actor->getComponent("GraphicsComponent"))->setRotation(glm::half_pi<float>(), 'x');
			xSpawnPosition = actor->getPosition().x;
			init = true;
		}
		if (missileTimer.hasEnded())
		{
			Actor* playerCube = parentEngine->getActorFactory()->getActor("playerCube");
			if (playerCube != nullptr)
			{
				glm::vec3 playerPos = playerCube->getPosition();
				float distance = glm::distance(playerPos, actor->getPosition());

				if (distance < 5)
				{
					if (playerCube->getPosition().x + 1 > actor->getPosition().x)
					{
						//static_cast<GraphicsComponent*>(actor->getComponent("GraphicsComponent"))->setRotation(glm::pi<float>(), 'y');
						direction = 1;
					}

					if (playerCube->getPosition().x - 1 < actor->getPosition().x)
					{
						//static_cast<GraphicsComponent*>(actor->getComponent("GraphicsComponent"))->setRotation(0.f, 'y');
						direction = -1;
					}
					parentEngine->getActorFactory()->createActor(dynamic_cast<ActorData*>(&ActorData("res/actors/totemMissile.xml")), { actor->getPosition().x + direction,actor->getPosition().y,0.f }, glm::vec3(direction*3.5f, 0.f, 0.f));
					missileTimer.restart();
				}
			}
		}

	}
private:
	int direction;
	float xSpawnPosition;
	bool init;
	Timer missileTimer;
};
