#pragma once

//Forward-declare the Message class
class Message;

//Declare the Receiver interface
class IReceiver
{
public:
	virtual ~IReceiver();
	
	virtual void receiveMessage(const Message &msg) = 0;
};

inline IReceiver::~IReceiver() {}

