#pragma once
#include <unordered_map>
#include "Handler.h"
#include <memory>
class Mesh;
/**
 * @class	MeshHandler
 *
 * @brief	A mesh handler.
 */
class MeshHandler : public Handler
{
public:

	/**
	 * @fn	MeshHandler::MeshHandler();
	 *
	 * @brief	Default constructor.
	 *
	 */
	MeshHandler();

	/**
	 * @fn	MeshHandler::~MeshHandler();
	 *
	 * @brief	Destructor.
	 *
	 */
	~MeshHandler();

	/**
	 * @fn	Mesh* MeshHandler::loadModel(const std::string & filePath);
	 *
	 * @brief	Loads a model into memory.
	 *
	 * @param	filePath	Full pathname of the file.
	 *
	 * @return	pointer to model.
	 */
	Mesh* loadModel(const std::string & filePath);

private:
	std::unordered_map<std::string, std::unique_ptr<Mesh>> meshes;
};

