#include "ActorFactory.h"
#include "Engine.h"
#include "Texture.h"
#include "Actor.h"
#include "ActorData.h"
#include "common.h"
#include "GraphicsHandler.h"
#include "Camera.h"
#include <iostream>


ActorFactory::ActorFactory():
	currentActorId(-1),
	componentFactory(std::make_unique<ComponentFactoryFactory>()),
	physicsFactory("PhysicsComponent", componentFactory.get()),
	graphicsFactory("GraphicsComponent", componentFactory.get()),
	inputFactory("InputComponent", componentFactory.get()),
	aIFactory("AIComponent", componentFactory.get()),
	audioFactory("AudioComponent", componentFactory.get()),
	particleFactory("ParticleComponent", componentFactory.get()),
	combatFactory("CombatComponent", componentFactory.get()),
	animationFactory("AnimationComponent", componentFactory.get())
{
}

ActorFactory::~ActorFactory()
{
}

Actor * ActorFactory::createActor(const ActorData* actorData, const glm::vec3 position, const glm::vec3 velocity)
{
	Actor* returnPointer = nullptr;
	actorID newActorID{ actorData->actorName, getNextActorId() };
	std::unique_ptr<Actor> newActor(std::make_unique<Actor>(parentEngine));
	newActor->setPosition(position);
	std::string actorType = actorData->actorType;
	newActor->setActorId(newActorID);
	
	for (auto &it : actorData->componentDataVector)
	{
		newActor->addComponent(createActorComponent(it.get()), it->type);
	}
	
	if (dynamic_cast<PhysicsComponent*>(newActor->getComponent("PhysicsComponent")) != nullptr)
	{
		dynamic_cast<PhysicsComponent*>(newActor->getComponent("PhysicsComponent"))->setVelocity(velocity);
	}
	newActor->postInit();
	actorMap[newActorID] = std::move(newActor);
	returnPointer = actorMap[newActorID].get();
			
	if (debug)
	{
		printf("Created actor with ID:%i\n", currentActorId);
	}
		
	return returnPointer;
}

std::unique_ptr<ActorComponent> ActorFactory::createActorComponent(ActorData::ComponentData* componentData)
{
	return componentFactory->getFactory(componentData->type)->create(componentData->identifier, componentData);
}

Actor* ActorFactory::createActor(const ActorData* actorData, glm::vec3 position, glm::vec2 tilePosition)
{
	std::unique_ptr<Actor> newActor(std::make_unique<Actor>(parentEngine));
	newActor->setPosition(position);
	std::string actorType = actorData->actorType;
	
	for (auto &it : actorData->componentDataVector)
	{
		newActor->addComponent(createActorComponent(it.get()), it->type);
		if (it->type == "GraphicsComponent")
		{
			static_cast<GraphicsComponent*>(newActor->getComponent("GraphicsComponent"))->setTilePosition(tilePosition);
		}
	}
	actorVector.push_back(std::move(newActor));
	currentActorId++;
	if (debug)
	{
		printf("Created actor with ID:%i\n", currentActorId);
	}
	return actorVector.back().get();
}

void ActorFactory::deleteActor(Actor* actorToDelete)
{
	actorID actorId = actorToDelete->getActorId();
	auto it = actorMap.find(actorId);
	if (it != actorMap.end())
	{
		if (debug)
		{
			printf("Deleted actor: %s, %i\n", actorId.actorName.c_str(), actorId.actorId);
		}

		it->second.reset();	//reset (kills) object
		
		actorMap.erase(it);
	}
}

void ActorFactory::updateAll(float deltaTime)
{
 	for (auto it: deleteQueue)
	{
		deleteActor(it);
	}

	for (auto it : activeSwapQueue)
	{
		it->setActive(!it->isActive());
	}
	activeSwapQueue.clear();

	deleteQueue.clear();
	sceneryDeleteQueue.clear();
	for (auto& it : actorMap)
	{
		if (it.second->isActive())
		{
			//printf("%s\n%",it.first.actorName.c_str()); //prints out actornames
			it.second->update(deltaTime);
		}
	}
}

Actor* ActorFactory::getActor(actorID actorId)
{
	auto it = actorMap.find(actorId);
	if (it != actorMap.end())
	{
		return it->second.get();
	}
	else
	{
		std::cout << "Invalid actorID." << std::endl;
		return nullptr;
	}
}

Actor * ActorFactory::getActor(std::string actorName)
{
	for (auto &it : actorMap)
	{
		if (it.first.actorName == actorName)
		{
			return it.second.get();
		}
	}
	std::cout << "Invalid actorName." << std::endl;
	return nullptr;
}

Actor* ActorFactory::getActor(int actorIdNumber)
{
	for (auto &it : actorMap)
	{
		if (it.first.actorId == actorIdNumber)
		{
			return it.second.get();
		}
	}
	std::cout << "Invalid actorID." << std::endl;
	return nullptr;
}

void ActorFactory::clearFactory()
{
	currentActorId = -1;
	deleteQueue.clear();
	sceneryDeleteQueue.clear();
	actorMap.erase(actorMap.begin(), actorMap.end());
	actorVector.erase(actorVector.begin(), actorVector.end());
}

ComponentFactoryFactory * ActorFactory::getComponentFactoryFactory()
{
	return componentFactory.get();
}

unsigned int ActorFactory::getCurrentActorID(void)
{
	return currentActorId;
}


std::vector<Actor*> ActorFactory::getActors()
{
	std::vector<Actor*> temp;

	for (auto& it : actorMap)
	{
		temp.emplace_back(it.second.get());
	}
	return temp;
}

std::vector<Actor*> ActorFactory::getSceneryActors()
{
	std::vector<Actor*> temp;

	for (auto& it : actorVector)
	{
		temp.emplace_back(it.get());
	}
	return temp;
}


void ActorFactory::draw()
{
	Camera * mainCamera = parentEngine->getGraphicsHandler()->getCamera();
	glm::mat4 viewProjection = mainCamera->getProjectionMatrix() * mainCamera->getViewMatrix();
	for (auto& it : actorMap)
	{
		if (it.second->isActive())
		{
			if (dynamic_cast<GraphicsComponent*>(it.second->getComponent("GraphicsComponent")) != nullptr)
			{
				dynamic_cast<GraphicsComponent*>(it.second->getComponent("GraphicsComponent"))->draw(viewProjection);
			}

			if (dynamic_cast<ParticleComponent*>(it.second->getComponent("ParticleComponent")) != nullptr)
			{
				dynamic_cast<ParticleComponent*>(it.second->getComponent("ParticleComponent"))->draw(viewProjection);
			}
		}
	}

	for (auto& it : actorVector)
	{
		//if (it->isActive()) //will we ever deactivate static objects?
		{
			if (dynamic_cast<GraphicsComponent*>(it->getComponent("GraphicsComponent")) != nullptr)
			{
				dynamic_cast<GraphicsComponent*>(it->getComponent("GraphicsComponent"))->draw(viewProjection);
			}

			if (dynamic_cast<ParticleComponent*>(it->getComponent("ParticleComponent")) != nullptr)
			{
				dynamic_cast<ParticleComponent*>(it->getComponent("ParticleComponent"))->draw(viewProjection);
			}
		}
		
	}
}

void ActorFactory::addToDeleteQueue(Actor* actorToDelete)
{
	for (auto it : deleteQueue)
	{
		if (it == actorToDelete)
		{
			return;
		}
	}
	this->deleteQueue.emplace_back(actorToDelete);
}

//DOESNT WORK BUT I CBA FIX IT FOR NOW
void ActorFactory::addToSceneryDeleteQueue(int actorToDelete)
{
	for (auto it : sceneryDeleteQueue)
	{
		if (it == actorToDelete)
		{
			return;
		}
	}
	this->sceneryDeleteQueue.emplace_back(actorToDelete);
}

void ActorFactory::addToSwapActiveQueue(Actor* actorToSwapActive)
{
	for (auto it : activeSwapQueue)
	{
		if (it == actorToSwapActive)
		{
			return;
		}
	}
	this->activeSwapQueue.emplace_back(actorToSwapActive);
}

unsigned int ActorFactory::getNextActorId(void)
{
	return ++currentActorId;
}

void ActorFactory::setParent(Engine& parentPtr)
{
	this->parentEngine = &parentPtr;
}

Engine* ActorFactory::getParent()
{
	return parentEngine;
}
