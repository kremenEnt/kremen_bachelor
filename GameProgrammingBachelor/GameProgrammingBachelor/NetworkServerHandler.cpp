#include "NetworkServerHandler.h"
#include "Engine.h"
#include "Actor.h"
#include "Message.h"
#include <iostream>
#include <stdio.h>
#include <string>
#include "EventHandler.h"
#include "ActorFactory.h"
#include "GuiFactory.h"

NetworkServerHandler::NetworkServerHandler()
{
}

NetworkServerHandler::~NetworkServerHandler()
{
	RakNet::RakPeerInterface::DestroyInstance(peerInterface);
}

void NetworkServerHandler::init()
{
	peerInterface = RakNet::RakPeerInterface::GetInstance();
	socket = RakNet::SocketDescriptor(PORT, 0);

	peerInterface->Startup(MAXCLIENTS, &socket, 1);
	peerInterface->SetMaximumIncomingConnections(MAXCLIENTS); //probably not necessary?
	players[peerInterface->GetMyGUID()].name = "HereComesThePainTrain";
	//oldActors = parentEngine->getActorFactory()->getActors();
}

void NetworkServerHandler::initClient()
{
	printf("initing client2\n");
	//creates new actor and stores inputcomponent for easy access later
	players[packet->guid].name = "DummyName";

	dynamic_cast<TextBox*>(parentEngine->getGuiFactory()->getElement("playerOneName"))->setText("DummyName");

	RakNet::BitStream bsOut;
	bsOut.Write((RakNet::MessageID)(ID_USER_PACKET_ENUM + STATE_JOINING));

	bsOut.Write(parentEngine->getSeed());
	peerInterface->Send(&bsOut, HIGH_PRIORITY, RELIABLE, 0, packet->guid,false);
}

void NetworkServerHandler::removeClient()
{
	//remove disconnected players
	players.erase(packet->guid);
}

void NetworkServerHandler::sendPacket(int gameState)
{
	if (networkDebug)
	{
		printf("starting to send packet from server\n");
	}
		switch (gameState)
	{
	case STATE_HOSTING:
		sendHosting();
		return;
	case STATE_RUNNING:
		sendRunning();
		break;
	case STATE_GAMEOVER:
		//sendRunning();
		break;
	default:
		printf("invalid gamestate for network play");
		break;
	}
}


void NetworkServerHandler::receivePacket(int gameState)
{	
	for (packet = peerInterface->Receive(); packet; peerInterface->DeallocatePacket(packet), packet = peerInterface->Receive())
	{
		switch (packet->data[0])
		{
		case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			printf("Another client has disconnected.\n");
			removeClient();
			break;
		case ID_REMOTE_CONNECTION_LOST:
			printf("Another client has lost the connection.\n");
			removeClient();
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION:
			printf("Another client has connected.\n");
			break;
		case ID_NEW_INCOMING_CONNECTION:
			printf("A connection is incoming.\n");
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			printf("The server is full.\n");
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			printf("A client has disconnected.\n");
			break;
		case ID_CONNECTION_LOST:
			printf("A client lost the connection.\n");
			break;
		case ID_USER_PACKET_ENUM+STATE_HOSTING:
			receiveHosting();
			break;
		case ID_USER_PACKET_ENUM+STATE_RUNNING:
			receiveRunning();
			break;
		case ID_USER_PACKET_ENUM + STATE_JOINING:
			if (gameState == STATE_HOSTING)
			{
				initClient();
			}
			break;
		}
	}
}

void NetworkServerHandler::receiveHosting()
{
	RakNet::BitStream bsIn;
	RakNet::RakString rs;
	bsIn.Read(rs);
	players[packet->guid].name = rs.C_String();
}

void NetworkServerHandler::receiveRunning()
{
	
	InputValues receivedInput;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(receivedInput);
	players[packet->guid].input->receiveNetworkInput(receivedInput);
	if (networkDebugTwo)
	{
		//I think there's an issue with inputcomponent where some shit isn't reset, which is why these outputs are wrong in certain cases
		printf("received packet I think\n:");
		printf(receivedInput.w ? "w: true\n" : "w: false\n");
		printf(receivedInput.s ? "s: true\n" : "s: false\n");
		printf(receivedInput.a ? "a: true\n" : "a: false\n");
		printf(receivedInput.d ? "d: true\n" : "d: false\n");
		printf(receivedInput.q ? "q: true\n" : "q: false\n");
		printf(receivedInput.space ? "space: true\n" : "space: false\n");
		printf(receivedInput.mouse ? "mouse: true\n" : "mouse: false\n");
		printf("mousepos: %f, %f", receivedInput.mousePosition.x, receivedInput.mousePosition.y);
	}
}

void NetworkServerHandler::sendStart()
{
	for (auto playerToReceive : players)
	{
		Actor* newActor = parentEngine->getActorFactory()->createActor(&ActorData("res/actors/clone.xml"), { 20.f,0.f,0.f });
		for (auto playerToGetActor : players)
		{
			RakNet::BitStream bsOut;
			RakNet::RakString rs = "res/actors/clone.xml";

			bsOut.Write((RakNet::MessageID)CREATE_ACTOR);
			bsOut.Write(rs);
			bsOut.WriteVector(20.f, 0.f, 0.f);
			peerInterface->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, playerToReceive.first, false);		
			players[playerToReceive.first].input = dynamic_cast<InputComponent*>((newActor)->getComponent("InputComponent"));
		}
		if (playerToReceive.first == peerInterface->GetMyGUID())
		{
			//parentEngine->setPlayer(*newActor);
			continue;
		}
		//TODO: should probably rewrite this. no need to create a full "player" on host, for other players. they should be technically different
		parentEngine->getEventHandler()->removeSubscriber(players[playerToReceive.first].input);
		
		RakNet::BitStream bsOut;
		bsOut.Write((RakNet::MessageID)START_GAME);
		bsOut.Write(newActor->getActorId());
		peerInterface->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, playerToReceive.first, false);
	}
}

void NetworkServerHandler::sendHosting()
{
	for (auto playerToReceive : players)
	{
		RakNet::BitStream bsOut;
		bsOut.Write((RakNet::MessageID)(ID_USER_PACKET_ENUM + STATE_JOINED));
		for (auto playerInfo : players)
		{
			bsOut.Write(playerInfo.second.name);
		}
		peerInterface->Send(&bsOut, MEDIUM_PRIORITY, RELIABLE, 0, playerToReceive.first, false);
	}
}

void NetworkServerHandler::sendRunning()
{
	std::vector<NetworkActorData> serverData;

	oldActors = parentEngine->getActorFactory()->getActors();
	for (auto it : oldActors)
	{
		if (it->isActive())
		{
			NetworkActorData newNetworkData;
			//packActorId(&newNetworkData, it);
			newNetworkData.actorIdNumber = it->getActorId().actorId;
			newNetworkData.actorPosition = static_cast<PhysicsComponent*>(it->getComponent("PhysicsComponent"))->getPosition();
											
			newNetworkData.actorVelocity = { static_cast<PhysicsComponent*>(it->getComponent("PhysicsComponent"))->getVelocity().x,
												static_cast<PhysicsComponent*>(it->getComponent("PhysicsComponent"))->getVelocity().y,
												0 };
			serverData.emplace_back(newNetworkData);
		}
	}
	
	for (auto playerToReceive : players)
	{
		if (playerToReceive.first == peerInterface->GetMyGUID())
		{
			continue;
		}
		RakNet::BitStream bsOut;
		bsOut.Write((RakNet::MessageID)(ID_USER_PACKET_ENUM + STATE_RUNNING));
		bsOut.Write((int)serverData.size());
		for (auto it : serverData)
		{
			bsOut.Write(it);
		}
		peerInterface->Send(&bsOut, HIGH_PRIORITY, RELIABLE, 0, playerToReceive.first, false);
	}
}
