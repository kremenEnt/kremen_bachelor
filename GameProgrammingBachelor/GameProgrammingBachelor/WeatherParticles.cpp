#include "WeatherParticles.h"

void WeatherParticles::update(float deltaTime)
{
	float color = (rand() % 200 / 1000.f + 0.801f);
	for (std::size_t i = 0; i < this->particles.size(); i++)
	{
		this->particles[i].life -= (1.f * deltaTime);	//lose 1hp/sec

		if (this->particles[i].life <= 0.f)	//move(reuse) particle
		{
			this->positions[i].y = -((rand() % 100 / 100.f) * 50.f) + 25.f;
			this->positions[i].x = ((rand() % 100 / 100.f) * 150.f) - 50.f;
			this->positions[i].z = ((rand() % 100 / 100.f) * 50.f) - 25.f;
			this->particles[i].life = static_cast<float>(rand() % 100);
			this->colors[i] = glm::vec4(color, color, color*1.25f, 1.f);
		}

		if (this->gravity == true)
		{
			if (this->particles[i].velocity.y > -this->maxVelocity)
			{
				this->particles[i].velocity.y -= this->maxVelocity * deltaTime; //1 second to reach maximum velocity
			}
			else
			{
				this->particles[i].velocity.y = -this->maxVelocity;
			}
		}

		this->positions[i] += (this->particles[i].velocity * deltaTime);
	}
}

void WeatherParticles::createPoint(int amount, glm::vec3 position, float maxLife)
{
	Particle particle;
	glm::vec4 color{ 1.f, 1.f, 1.f, 1.f, };

	for (int i = 0; i < amount; i++)
	{
		particle.life = (rand() % static_cast<int>(maxLife * 100)) / 100.f;
		particle.velocity =
		{
			particle.velocity.x + ((rand() % 200 / 100.f) - 1.f) * 0.01f,
			particle.velocity.y,
			particle.velocity.z + ((rand() % 200 / 100.f) - 1.f) * 0.01f
		};
		particles.emplace_back(particle);
		positions.emplace_back(position);
		colors.emplace_back(color);
	}
}
