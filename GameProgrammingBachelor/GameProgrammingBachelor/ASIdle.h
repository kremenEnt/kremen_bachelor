#pragma once
#include "IActorState.h"
#include "ASDefault.h"


class ASIdle : public IActorState, public ASDefault
{
public:
	ASIdle() {};
	ASIdle(Actor* actor) : ASDefault(actor) {};
	virtual int update() override;
	virtual void enter() override;
	virtual void leave() override;
};