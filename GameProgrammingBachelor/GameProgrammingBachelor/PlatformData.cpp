#include "PlatformData.h"


//used to add custom size to actors, but currently defaults to 1.0 in all axis.
PlatformData::PlatformData(std::string actor):
	ActorData(actor)
{
	for (auto &it : componentDataVector)
	{
		if (it->type == "PhysicsComponent")
		{
			dynamic_cast<Physics*>(it.get())->definition.size = { 1.f, 1.f };
		}
		if (it->type == "GraphicsComponent")
		{
			dynamic_cast<Graphics*>(it.get())->size = { 1.f, 1.f, 1.f };
		}
	}
}


PlatformData::~PlatformData()
{
}
