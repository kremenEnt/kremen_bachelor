#pragma once
#include "Handler.h"
#include <unordered_map>
#include <memory>

class Texture;
/**
 * @class	TextureHandler
 *
 * @brief	A texture handler.
 */
class TextureHandler : public Handler
{
public:

	/**
	 * @fn	TextureHandler::TextureHandler();
	 *
	 * @brief	Default constructor.
	 *
	 */
	TextureHandler();

	/**
	 * @fn	TextureHandler::~TextureHandler();
	 *
	 * @brief	Destructor.
	 *
	 */
	~TextureHandler();

	/**
	 * @fn	Texture* TextureHandler::loadTexture(const std::string& filePath);
	 *
	 * @brief	Loads a texture.
	 *
	 * @param	filePath	Full pathname of the file.
	 *
	 * @return	returns pointer to texture.
	 */
	Texture* loadTexture(const std::string& filePath);
private:
	std::unordered_map<std::string, std::unique_ptr<Texture>> textures;
};