#include "GuiData.h"
#include <iostream>

GuiData::GuiData()
{
}

GuiData::GuiData(std::string xmlPath)
{
	tinyxml2::XMLDocument* xmlDocument = guiXMLHandler->loadXml(xmlPath);
	tinyxml2::XMLElement* rootElement = xmlDocument->FirstChildElement();
	tinyxml2::XMLElement* itemElement;
	std::string temp = "";
	
	if (rootElement->FirstChildElement("Button"))
	{

		itemElement = rootElement->FirstChildElement("Button");
		temp = rootElement->FirstChildElement("Button")->Value();
		while (temp == "Button")
		{
			createButton(itemElement);

			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of buttons";
			}
		}
	}
	else if (xmlDebug)
	{
		printf("Missing button\n");
	}

	if (rootElement->FirstChildElement("Slider"))
	{
		itemElement = rootElement->FirstChildElement("Slider");
		temp = rootElement->FirstChildElement("Slider")->Value();
		while (temp == "Slider")
		{
			createSlider(itemElement);

			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of sliders";
			}

		}
	}
	else if (xmlDebug)
	{
		printf("Missing sliders\n");
	}

	if (rootElement->FirstChildElement("Panel"))
	{
		itemElement = rootElement->FirstChildElement("Panel");
		temp = rootElement->FirstChildElement("Panel")->Value();
		while (temp == "Panel")
		{
			createPanel(itemElement);
			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of panels";
			}

		}
	}
	else if (xmlDebug)
	{
		printf("Missing panels\n");
	}

	if (rootElement->FirstChildElement("Textbox"))
	{
		itemElement = rootElement->FirstChildElement("Textbox");
		temp = rootElement->FirstChildElement("Textbox")->Value();
		while (temp == "Textbox")
		{
			createTextBox(itemElement);
			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of textboxes";
			}

		}
	}
	else if (xmlDebug)
	{
		printf("Missing textboxes\n");
	}

	if (rootElement->FirstChildElement("Checkbox"))
	{
		itemElement = rootElement->FirstChildElement("Checkbox");
		temp = rootElement->FirstChildElement("Checkbox")->Value();
		while (temp == "Checkbox")
		{
			createCheckBox(itemElement);
			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of checkboxes";
			}

		}
	}
	else if (xmlDebug)
	{
		printf("Missing checkboxes\n");
	}

	if (rootElement->FirstChildElement("Dropdown"))
	{
		itemElement = rootElement->FirstChildElement("Dropdown");
		temp = rootElement->FirstChildElement("Dropdown")->Value();
		while (temp == "Dropdown")
		{
			createDropDown(itemElement);
			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of dropdowns";
			}
			
		}
	}
	else if (xmlDebug)
	{
		printf("Missing dropdowns\n");
	}

	if (rootElement->FirstChildElement("Container"))
	{
		itemElement = rootElement->FirstChildElement("Container");
		temp = rootElement->FirstChildElement("Container")->Value();
		while (temp == "Container")
		{
			createContainer(itemElement);
			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of containers";
			}

		}
	}
	else if (xmlDebug)
	{
		printf("Missing containers\n");
	}

	if (rootElement->FirstChildElement("Bar"))
	{
		itemElement = rootElement->FirstChildElement("Bar");
		temp = rootElement->FirstChildElement("Bar")->Value();
		while (temp == "Bar")
		{
			createBar(itemElement);
			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				temp = itemElement->Value();
			}
			else
			{
				temp = "end of Bar";
			}

		}
	}
	else if (xmlDebug)
	{
		printf("Missing Bar\n");
	}
}

void GuiData::init()
{
	guiXMLHandler = std::make_unique<XmlHandler>();
}


void GuiData::createBar(tinyxml2::XMLElement *originNode)
{
	Bar bar;
	bar.type = "bar";
	
	if (originNode->FirstChildElement("name"))
	{
		bar.name =  originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (originNode->FirstChildElement("horizontal"))
	{
		originNode->FirstChildElement("horizontal")->QueryBoolText(&bar.horizontal);
	}
	else if (xmlDebug)
	{
		printf("Missing orientation\n");
	}

	if (originNode->FirstChildElement("positionX"))
	{
		originNode->FirstChildElement("positionX")->QueryFloatText(&bar.positionX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionX\n");
	}

	if (originNode->FirstChildElement("positionY"))
	{
		originNode->FirstChildElement("positionY")->QueryFloatText(&bar.positionY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}

	if (originNode->FirstChildElement("widthInPixels"))
	{
		originNode->FirstChildElement("widthInPixels")->QueryFloatText(&bar.width);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixels\n");
	}

	if (originNode->FirstChildElement("heightInPixels"))
	{
		originNode->FirstChildElement("heightInPixels")->QueryFloatText(&bar.height);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}

	if (originNode->FirstChildElement("meshVerticesPath"))
	{
		bar.meshPath =  originNode->FirstChildElement("meshVerticesPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing meshVerticesPath\n");
	}

	if (originNode->FirstChildElement("texturePath"))
	{
		bar.texturePath =  originNode->FirstChildElement("texturePath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (originNode->FirstChildElement("shaderPath"))
	{
		bar.shaderPath =  originNode->FirstChildElement("shaderPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}

	if (originNode->FirstChildElement("textureBarPath"))
	{
		bar.texturePathBar =  originNode->FirstChildElement("textureBarPath")->GetText();
	}
	else if (xmlDebug)
	{
		std::cout << "Missing textureBarPath\n";
	}
	else
	{
		if (originNode->FirstChildElement("colorR"))
		{
			originNode->FirstChildElement("colorR")->QueryFloatText(&bar.color.r);
		}
		else if (xmlDebug)
		{
			std::cout << "Missing colorR\n";
		}

		if (originNode->FirstChildElement("colorG"))
		{
			originNode->FirstChildElement("colorG")->QueryFloatText(&bar.color.g);
		}
		else if (xmlDebug)
		{
			std::cout << "Missing colorG\n";
		}

		if (originNode->FirstChildElement("colorB"))
		{
			originNode->FirstChildElement("colorB")->QueryFloatText(&bar.color.b);
		}
		else if (xmlDebug)
		{
			std::cout << "Missing colorB\n";
		}
	}
	if (originNode->FirstChildElement("visible"))
	{
		originNode->FirstChildElement("visible")->QueryBoolText(&bar.visible);
	}
	else if (xmlDebug)
	{
		printf("Missing visible\n");
	}
	elementDataVector.emplace_back(std::make_unique<Bar>(bar));
}

void GuiData::createButton(tinyxml2::XMLElement *originNode)
{
	Button button;
	button.type = "button";
	
	if (originNode->FirstChildElement("name"))
	{
		button.name =  originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (originNode->FirstChildElement("text"))
	{
		button.text =  originNode->FirstChildElement("text")->GetText();
		if ( originNode->FirstChildElement("fontPath"))
		{
			button.fontPath =  originNode->FirstChildElement("fontPath")->GetText();
		}
		else if (xmlDebug)
		{
			printf("Missing fontPath\n");
		}
	}
	else if (xmlDebug)
	{
		printf("Missing text\n");
	}

	if (originNode->FirstChildElement("positionX"))
	{
		originNode->FirstChildElement("positionX")->QueryFloatText(&button.positionX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionX\n");
	}

	if (originNode->FirstChildElement("positionY"))
	{
		originNode->FirstChildElement("positionY")->QueryFloatText(&button.positionY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}

	if (originNode->FirstChildElement("widthInPixels"))
	{
		originNode->FirstChildElement("widthInPixels")->QueryFloatText(&button.width);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixels\n");
	}

	if (originNode->FirstChildElement("heightInPixels"))
	{
		originNode->FirstChildElement("heightInPixels")->QueryFloatText(&button.height);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}

	if (originNode->FirstChildElement("meshVerticesPath"))
	{
		button.meshPath =  originNode->FirstChildElement("meshVerticesPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing meshVericesPath\n");
	}

	if (originNode->FirstChildElement("texturePath"))
	{
		button.texturePath =  originNode->FirstChildElement("texturePath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (originNode->FirstChildElement("shaderPath"))
	{

		button.shaderPath =  originNode->FirstChildElement("shaderPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}

	if (originNode->FirstChildElement("textureAtlasSize"))
	{

		originNode->FirstChildElement("textureAtlasSize")->QueryIntText(&button.atlasSize);
	}
	else if (xmlDebug)
	{
		printf("Missing textureAtlasSize\n");
	}
	if (originNode->FirstChildElement("visible"))
	{
		originNode->FirstChildElement("visible")->QueryBoolText(&button.visible);
	}
	else if (xmlDebug)
	{
		printf("Missing visible\n");
	}

	elementDataVector.emplace_back(std::make_unique<Button>(button));
}

void GuiData::createCheckBox(tinyxml2::XMLElement *originNode)
{
	CheckBox checkBox;
	checkBox.type = "checkBox";

	if (originNode->FirstChildElement("name"))
	{
		checkBox.name =  originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (originNode->FirstChildElement("positionX"))
	{
		originNode->FirstChildElement("positionX")->QueryFloatText(&checkBox.positionX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionX\n");
	}

	if (originNode->FirstChildElement("positionY"))
	{
		originNode->FirstChildElement("positionY")->QueryFloatText(&checkBox.positionY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}

	if (originNode->FirstChildElement("widthInPixels"))
	{
		originNode->FirstChildElement("widthInPixels")->QueryFloatText(&checkBox.width);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixels\n");
	}

	if (originNode->FirstChildElement("heightInPixels"))
	{
		originNode->FirstChildElement("heightInPixels")->QueryFloatText(&checkBox.height);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}

	if (originNode->FirstChildElement("defaultStatus"))
	{
		originNode->FirstChildElement("defaultStatus")->QueryBoolText(&checkBox.defaultStatus);
	}
	else if (xmlDebug)
	{
		printf("Missing defaultStatus\n");
	}

	if (originNode->FirstChildElement("meshVerticesPath"))
	{
		checkBox.meshPath =  originNode->FirstChildElement("meshVerticesPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing meshVerticesPath\n");
	}

	if (originNode->FirstChildElement("texturePath"))
	{
		checkBox.texturePath =  originNode->FirstChildElement("texturePath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (originNode->FirstChildElement("shaderPath"))
	{
		checkBox.shaderPath =  originNode->FirstChildElement("shaderPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}

	if (originNode->FirstChildElement("textureAtlasSizeX"))
	{
		originNode->FirstChildElement("textureAtlasSizeX")->QueryIntText(&checkBox.atlasSizeX);
	}
	else if (xmlDebug)
	{
		printf("Missing textureAtlasSizeX\n");
	}

	if (originNode->FirstChildElement("textureAtlasSizeY"))
	{
		originNode->FirstChildElement("textureAtlasSizeY")->QueryIntText(&checkBox.atlasSizeY);
	}
	else if (xmlDebug)
	{
		printf("Missing textureAtlasSizeY\n");
	}
	if (originNode->FirstChildElement("visible"))
	{
		originNode->FirstChildElement("visible")->QueryBoolText(&checkBox.visible);
	}
	else if (xmlDebug)
	{
		printf("Missing visible\n");
	}
	elementDataVector.emplace_back(std::make_unique<CheckBox>(checkBox));
}

void GuiData::createContainer(tinyxml2::XMLElement *originNode)
{
	tinyxml2::XMLElement* itemElement = originNode->FirstChildElement();
	Container container;
	container.type = "container";

	if (originNode->FirstChildElement("name"))
	{
		container.name =  originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (originNode->FirstChildElement("positionX"))
	{
		originNode->FirstChildElement("positionX")->QueryFloatText(&container.positionX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionX\n");
	}

	if (originNode->FirstChildElement("positionY"))
	{
		originNode->FirstChildElement("positionY")->QueryFloatText(&container.positionY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}

	if (originNode->FirstChildElement("meshVerticesPath"))
	{
		container.meshPath =  originNode->FirstChildElement("meshVerticesPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing meshVerticesPath\n");
	}

	if (originNode->FirstChildElement("texturePath"))
	{
		container.texturePath =  originNode->FirstChildElement("texturePath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (originNode->FirstChildElement("shaderPath"))
	{
		container.shaderPath =  originNode->FirstChildElement("shaderPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}

	if (originNode->FirstChildElement("shaderPathBoxes"))
	{
		container.shaderBoxes =  originNode->FirstChildElement("shaderPathBoxes")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPathBoxes\n");
	}
	if (originNode->FirstChildElement("texturePathBoxes"))
	{
		container.textureBoxes =  originNode->FirstChildElement("texturePathBoxes")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing texturePathBoxes\n");
	}

	if (originNode->FirstChildElement("textureAtlasSizeX"))
	{
		originNode->FirstChildElement("textureAtlasSizeX")->QueryIntText(&container.atlasSizeX);
	}
	else if (xmlDebug)
	{
		printf("Missing textureAtlasSizeX\n");
	}

	if (originNode->FirstChildElement("textureAtlasSizeY"))
	{
		originNode->FirstChildElement("textureAtlasSizeY")->QueryIntText(&container.atlasSizeY);
	}
	else if (xmlDebug)
	{
		printf("Missing textureAtlasSizeY\n");
	}

	if (originNode->FirstChildElement("numberOfBoxesX"))
	{
		originNode->FirstChildElement("numberOfBoxesX")->QueryIntText(&container.numberOfBoxesX);
	}
	else if (xmlDebug)
	{
		printf("Missing numberOfBoxesX\n");
	}

	if (originNode->FirstChildElement("numberOfBoxesY"))
	{
		originNode->FirstChildElement("numberOfBoxesY")->QueryIntText(&container.numberOfBoxesY);
	}
	else if (xmlDebug)
	{
		printf("Missing numberOfBoxesY\n");
	}

	if (originNode->FirstChildElement("distanceBetweenBoxes"))
	{
		originNode->FirstChildElement("distanceBetweenBoxes")->QueryFloatText(&container.distanceBetweenBoxes);
	}
	else if (xmlDebug)
	{
		printf("Missing distanceBetweenBoxes\n");
	}

	if (originNode->FirstChildElement("borderTop"))
	{
		originNode->FirstChildElement("borderTop")->QueryFloatText(&container.borderTop);
	}
	else if (xmlDebug)
	{
		printf("Missing borderTop\n");
	}

	if (originNode->FirstChildElement("borderBottom"))
	{
		originNode->FirstChildElement("borderBottom")->QueryFloatText(&container.borderBottom);
	}
	else if (xmlDebug)
	{
		printf("Missing borderBottom\n");
	}

	if (originNode->FirstChildElement("borderLeft"))
	{
		originNode->FirstChildElement("borderLeft")->QueryFloatText(&container.borderLeft);
	}
	else if (xmlDebug)
	{
		printf("Missing borderLeft\n");
	}

	if (originNode->FirstChildElement("borderRight"))
	{
		originNode->FirstChildElement("borderRight")->QueryFloatText(&container.borderRight);
	}
	else if (xmlDebug)
	{
		printf("Missing borderRight\n");
	}

	if (originNode->FirstChildElement("widthInPixelsBoxes"))
	{
		originNode->FirstChildElement("widthInPixelsBoxes")->QueryFloatText(&container.widthBoxes);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixelsBoxes\n");
	}

	if (originNode->FirstChildElement("heightInPixelsBoxes"))
	{
		originNode->FirstChildElement("heightInPixelsBoxes")->QueryFloatText(&container.heightBoxes);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixelsBoxes\n");
	}

	if (originNode->FirstChildElement("clickable"))
	{
		originNode->FirstChildElement("clickable")->QueryBoolText(&container.clickable);
	}
	else if (xmlDebug)
	{
		printf("Missing clickable\n");
	}

	if (originNode->FirstChildElement("visible"))
	{
		originNode->FirstChildElement("visible")->QueryBoolText(&container.visible);
	}
	else if (xmlDebug)
	{
		printf("Missing visible\n");
	}

	if (originNode->FirstChildElement("movableItems"))
	{
		originNode->FirstChildElement("movableItems")->QueryBoolText(&container.movable);
	}
	else if (xmlDebug)
	{
		printf("Missing movableItems\n");
	}

	if (originNode->FirstChildElement("destroyable"))
	{
		originNode->FirstChildElement("destroyable")->QueryBoolText(&container.destroyable);
	}
	else if (xmlDebug)
	{
		printf("Missing destroyable\n");
	}

	if (originNode->FirstChildElement("activatable"))
	{
		originNode->FirstChildElement("activatable")->QueryBoolText(&container.activatable);
	}
	else if (xmlDebug)
	{
		printf("Missing activatable\n");
	}

	if (originNode->FirstChildElement("presetItems"))
	{
		std::string tempCheck;
		
		itemElement = originNode->FirstChildElement("presetItems")->FirstChildElement("texturePath");
		tempCheck = originNode->FirstChildElement("presetItems")->FirstChildElement("texturePath")->Value();
		while (tempCheck == "texturePath")
		{
			std::string tempPresetItem;
			tempPresetItem = itemElement->GetText();

			container.presetItems.emplace_back(tempPresetItem);

			itemElement = itemElement->NextSiblingElement();
			if (itemElement != NULL)
			{
				tempCheck = itemElement->Value();
			}
			else
			{
				tempCheck = "end of texturePaths";
			}
		}
	}
	else if (xmlDebug)
	{
		printf("Missing presetItems\n");
	}

	elementDataVector.emplace_back(std::make_unique<Container>(container));
}

void GuiData::createDropDown(tinyxml2::XMLElement *originNode)
{
	tinyxml2::XMLElement* itemElement = originNode->FirstChildElement();
	DropDown dropDown;
	dropDown.type = "dropDown";

	if (originNode->FirstChildElement("name"))
	{
		dropDown.name =  originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (originNode->FirstChildElement("fontPath"))
	{
		dropDown.fontPath =  originNode->FirstChildElement("fontPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing fontPath\n");
	}

	if (originNode->FirstChildElement("positionX"))
	{
		originNode->FirstChildElement("positionX")->QueryFloatText(&dropDown.positionX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionX\n");
	}

	if (originNode->FirstChildElement("positionY"))
	{
		originNode->FirstChildElement("positionY")->QueryFloatText(&dropDown.positionY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}

	if (originNode->FirstChildElement("widthInPixels"))
	{
		originNode->FirstChildElement("widthInPixels")->QueryFloatText(&dropDown.width);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixels\n");
	}

	if (originNode->FirstChildElement("heightInPixels"))
	{
		originNode->FirstChildElement("heightInPixels")->QueryFloatText(&dropDown.height);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}

	if (originNode->FirstChildElement("meshVerticesPath"))
	{
		dropDown.meshPath =  originNode->FirstChildElement("meshVerticesPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing meshVerticesPath\n");
	}

	if (originNode->FirstChildElement("shaderPath"))
	{
		dropDown.shaderPath =  originNode->FirstChildElement("shaderPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}

	if (originNode->FirstChildElement("texturePath"))
	{
		dropDown.texturePath =  originNode->FirstChildElement("texturePath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (originNode->FirstChildElement("textureChoicePath"))
	{
		dropDown.textureChoice =  originNode->FirstChildElement("textureChoicePath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing textureChoicePath\n");
	}

	if (originNode->FirstChildElement("defaultChoice"))
	{
		dropDown.defaultChoice =  originNode->FirstChildElement("defaultChoice")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing defaultChoice\n");
	}

	if (originNode->FirstChildElement("choices"))
	{
		std::string tempCheck;
		itemElement = originNode->FirstChildElement("choices")->FirstChildElement("choice");
		if (originNode->FirstChildElement("choices")->FirstChildElement("choice"))
		{
			tempCheck = originNode->FirstChildElement("choices")->FirstChildElement("choice")->Value();
			while (tempCheck == "choice")
			{
				std::string tempChoice;
				tempChoice = itemElement->GetText();
				dropDown.choices.emplace_back(tempChoice);

				itemElement = itemElement->NextSiblingElement();
				if (itemElement != NULL)
				{
					tempCheck = itemElement->Value();
				}
				else
				{
					tempCheck = "end of choices";
				}
			}
		}
		else if (xmlDebug)
		{
			printf("Missing choice\n");
		}
	}
	else if (xmlDebug)
	{
		printf("Missing choices\n");
	}
	if (originNode->FirstChildElement("visible"))
	{
		originNode->FirstChildElement("visible")->QueryBoolText(&dropDown.visible);
	}
	else if (xmlDebug)
	{
		printf("Missing visible\n");
	}
	elementDataVector.emplace_back(std::make_unique<DropDown>(dropDown));
}

void GuiData::createPanel(tinyxml2::XMLElement *originNode)
{
	Panel panel;
	panel.type = "panel";
	if (originNode->FirstChildElement("name"))
	{
		panel.name =  originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (originNode->FirstChildElement("positionX"))
	{
		originNode->FirstChildElement("positionX")->QueryFloatText(&panel.positionX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionX\n");
	}

	if (originNode->FirstChildElement("positionY"))
	{
		originNode->FirstChildElement("positionY")->QueryFloatText(&panel.positionY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}

	if (originNode->FirstChildElement("widthInPixels"))
	{
		originNode->FirstChildElement("widthInPixels")->QueryFloatText(&panel.width);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixels\n");
	}

	if (originNode->FirstChildElement("heightInPixels"))
	{
		originNode->FirstChildElement("heightInPixels")->QueryFloatText(&panel.height);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}

	if (originNode->FirstChildElement("meshVerticesPath"))
	{
		panel.meshPath =  originNode->FirstChildElement("meshVerticesPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing meshVerticesPath\n");
	}

	if (originNode->FirstChildElement("texturePath"))
	{
		panel.texturePath =  originNode->FirstChildElement("texturePath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (originNode->FirstChildElement("shaderPath"))
	{
		panel.shaderPath =  originNode->FirstChildElement("shaderPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}
	if (originNode->FirstChildElement("visible"))
	{
		originNode->FirstChildElement("visible")->QueryBoolText(&panel.visible);
	}
	else if (xmlDebug)
	{
		printf("Missing visible\n");
	}
	elementDataVector.emplace_back(std::make_unique<Panel>(panel));
}

void GuiData::createSlider(tinyxml2::XMLElement *originNode)
{
	Slider slider;
	slider.type = "slider";
	if (originNode->FirstChildElement("name"))
	{
		slider.name =  originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (originNode->FirstChildElement("fontPath"))
	{
		slider.fontPath =  originNode->FirstChildElement("fontPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing fontPath\n");
	}

	if (originNode->FirstChildElement("positionY"))
	{
		originNode->FirstChildElement("positionY")->QueryFloatText(&slider.positionY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}

	if (originNode->FirstChildElement("positionSliderX"))
	{
		originNode->FirstChildElement("positionSliderX")->QueryFloatText(&slider.positionX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionSliderX\n");
	}

	if (originNode->FirstChildElement("positionLeftX"))
	{
		originNode->FirstChildElement("positionLeftX")->QueryFloatText(&slider.positionLeftX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionLeftX\n");
	}

	if (originNode->FirstChildElement("positionRightX"))
	{
		originNode->FirstChildElement("positionRightX")->QueryFloatText(&slider.positionRightX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionRightX\n");
	}

	if (originNode->FirstChildElement("widthInPixels"))
	{
		originNode->FirstChildElement("widthInPixels")->QueryFloatText(&slider.width);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixels\n");
	}

	if (originNode->FirstChildElement("heightInPixels"))
	{
		originNode->FirstChildElement("heightInPixels")->QueryFloatText(&slider.height);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}

	if (originNode->FirstChildElement("minValue"))
	{
		originNode->FirstChildElement("minValue")->QueryIntText(&slider.minValue);
	}
	else if (xmlDebug)
	{
		printf("Missing minValue\n");
	}

	if (originNode->FirstChildElement("maxValue"))
	{
		originNode->FirstChildElement("maxValue")->QueryIntText(&slider.maxValue);
	}
	else if (xmlDebug)
	{
		printf("Missing maxValue\n");
	}

	if (originNode->FirstChildElement("defaultValue"))
	{
		originNode->FirstChildElement("defaultValue")->QueryIntText(&slider.defaultValue);
	}
	else if (xmlDebug)
	{
		printf("Missing defaultValue\n");
	}

	if (originNode->FirstChildElement("valueInterval"))
	{
		originNode->FirstChildElement("valueInterval")->QueryIntText(&slider.interval);
	}
	else if (xmlDebug)
	{
		printf("Missing valueInterval\n");
	}

	if (originNode->FirstChildElement("meshVerticesPath"))
	{
		slider.meshPath =  originNode->FirstChildElement("meshVerticesPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing meshVerticesPath\n");
	}

	if (originNode->FirstChildElement("texturePath"))
	{
		slider.texturePath =  originNode->FirstChildElement("texturePath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (originNode->FirstChildElement("shaderPath"))
	{
		slider.shaderPath =  originNode->FirstChildElement("shaderPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}

	if (originNode->FirstChildElement("textureAtlasSize"))
	{
		originNode->FirstChildElement("textureAtlasSize")->QueryIntText(&slider.atlasSize);
	}
	else if (xmlDebug)
	{
		printf("Missing textureAtlasSize\n");
	}
	if (originNode->FirstChildElement("visible"))
	{
		originNode->FirstChildElement("visible")->QueryBoolText(&slider.visible);
	}
	else if (xmlDebug)
	{
		printf("Missing visible\n");
	}
	elementDataVector.emplace_back(std::make_unique<Slider>(slider));
}

void GuiData::createTextBox(tinyxml2::XMLElement *originNode)
{
	TextBox textBox;
	textBox.type = "textBox";

	if (originNode->FirstChildElement("name"))
	{
		textBox.name =  originNode->FirstChildElement("name")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing name\n");
	}

	if (originNode->FirstChildElement("fontPath"))
	{
		textBox.fontPath =  originNode->FirstChildElement("fontPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing fontPath\n");
	}

	if (originNode->FirstChildElement("maxLength"))
	{
		originNode->FirstChildElement("maxLength")->QueryIntText(&textBox.maxLength);
	}
	else if (xmlDebug)
	{
		printf("Missing maxLength\n");
	}

	if (originNode->FirstChildElement("defaultValue"))
	{
		textBox.defaultValue =  originNode->FirstChildElement("defaultValue")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing defaultValue\n");
	}

	if (originNode->FirstChildElement("positionX"))
	{
		originNode->FirstChildElement("positionX")->QueryFloatText(&textBox.positionX);
	}
	else if (xmlDebug)
	{
		printf("Missing positionX\n");
	}

	if (originNode->FirstChildElement("positionY"))
	{
		originNode->FirstChildElement("positionY")->QueryFloatText(&textBox.positionY);
	}
	else if (xmlDebug)
	{
		printf("Missing positionY\n");
	}

	if (originNode->FirstChildElement("widthInPixels"))
	{
		originNode->FirstChildElement("widthInPixels")->QueryFloatText(&textBox.width);
	}
	else if (xmlDebug)
	{
		printf("Missing widthInPixels\n");
	}

	if (originNode->FirstChildElement("heightInPixels"))
	{
		originNode->FirstChildElement("heightInPixels")->QueryFloatText(&textBox.height);
	}
	else if (xmlDebug)
	{
		printf("Missing heightInPixels\n");
	}

	if (originNode->FirstChildElement("meshVerticesPath"))
	{
		textBox.meshPath =  originNode->FirstChildElement("meshVerticesPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing meshVerticesPath\n");
	}

	if (originNode->FirstChildElement("texturePath"))
	{
		textBox.texturePath =  originNode->FirstChildElement("texturePath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing texturePath\n");
	}

	if (originNode->FirstChildElement("shaderPath"))
	{
		textBox.shaderPath =  originNode->FirstChildElement("shaderPath")->GetText();
	}
	else if (xmlDebug)
	{
		printf("Missing shaderPath\n");
	}
	if (originNode->FirstChildElement("visible"))
	{
		originNode->FirstChildElement("visible")->QueryBoolText(&textBox.visible);
	}
	else if (xmlDebug)
	{
		printf("Missing visible\n");
	}
	elementDataVector.emplace_back(std::make_unique<TextBox>(textBox));
}