#include "Observable.h"
#include "Receiver.h"
#include "Message.h"
#include <assert.h>

void IObservable::addSubscriber(IReceiver* receiver)
{
	assert(receiver != NULL);

	this->observerList.push_back(receiver);
}

void IObservable::removeSubscriber(IReceiver* receiver)
{
	assert(receiver != NULL);
	std::vector<IReceiver*>::iterator position = std::find(this->observerList.begin(), this->observerList.end(), receiver);
	if (position != observerList.end())
	{
		this->observerList.erase(position);
	}
}

void IObservable::postMessage(const Message &msg) const
{
	for (auto i = this->observerList.begin(); i != this->observerList.end(); i++)
	{
		(*i)->receiveMessage(msg);
	}
}