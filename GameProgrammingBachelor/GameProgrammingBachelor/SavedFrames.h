#pragma once
#include <string>
#include <memory>
#include <map>
#include <vector>
#include <list>
#include "tinyxml2-master\tinyxml2.h"
#include <glm\glm.hpp>
#include "common.h"
#include <iostream>

/**
 * @class	SavedFrames
 *
 * @brief	A saved frames. NOT IN USE, probably not functional currently either.
 */
class SavedFrames
{
public:

	/**
	 * @fn	SavedFrames::SavedFrames();
	 *
	 * @brief	Default constructor.
	 */
	SavedFrames();

	/**
	 * @fn	SavedFrames::~SavedFrames();
	 *
	 * @brief	Destructor.
	 */
	~SavedFrames();

	/**
	 * @fn	void SavedFrames::setPosition(glm::vec3 position);
	 *
	 * @brief	Sets a position.
	 *
	 * @param	position	The position.
	 */
	void setPosition(glm::vec3 position);

	/**
	 * @fn	void SavedFrames::setRotation(float rotation);
	 *
	 * @brief	Sets a rotation.
	 *
	 * @param	rotation	The rotation.
	 */
	void setRotation(float rotation);

	/**
	 * @fn	void SavedFrames::setVelocity(glm::vec2 velocity);
	 *
	 * @brief	Sets a velocity.
	 *
	 * @param	velocity	The velocity.
	 */
	void setVelocity(glm::vec2 velocity);

	/**
	 * @fn	void SavedFrames::setFrameSpawned(int frameSpawned);
	 *
	 * @brief	Sets frame spawned.
	 *
	 * @param	frameSpawned	The frame spawned.
	 */
	void setFrameSpawned(int frameSpawned);

	/**
	 * @fn	void SavedFrames::MoveFrameSpawned();
	 *
	 * @brief	Move frame spawned.
	 */
	void MoveFrameSpawned();

	/**
	 * @fn	void SavedFrames::removeTimeFrame(int timeFrame);
	 *
	 * @brief	Removes the time frame described by timeFrame.
	 *
	 * @param	timeFrame	The time frame.
	 */
	void removeTimeFrame(int timeFrame);

	/**
	 * @fn	glm::vec3 SavedFrames::getPosition(int timeFrame);
	 *
	 * @brief	Gets a position.
	 *
	 * @param	timeFrame	The time frame.
	 *
	 * @return	The position.
	 */
	glm::vec3 getPosition(int timeFrame);

	/**
	 * @fn	float SavedFrames::getRotation(int timeFrame);
	 *
	 * @brief	Gets a rotation.
	 *
	 * @param	timeFrame	The time frame.
	 *
	 * @return	The rotation.
	 */
	float getRotation(int timeFrame);

	/**
	 * @fn	glm::vec2 SavedFrames::getVelocity(int timeFrame);
	 *
	 * @brief	Gets a velocity.
	 *
	 * @param	timeFrame	The time frame.
	 *
	 * @return	The velocity.
	 */
	glm::vec2 getVelocity(int timeFrame);

	/**
	 * @fn	int SavedFrames::getFrameSpawned();
	 *
	 * @brief	Gets frame spawned.
	 *
	 * @return	The frame spawned.
	 */
	int getFrameSpawned();
private:	
	int frameSpawned;
	std::vector<glm::vec3> positionVector;
	std::vector<float> rotationVector;
	std::vector<glm::vec2> velocityVector;
};

