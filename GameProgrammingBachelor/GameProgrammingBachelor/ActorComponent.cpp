#include "ActorComponent.h"
#include "ActorData.h"
#include <glm\glm.hpp>

ActorComponent::ActorComponent(ActorData::ComponentData* componentData)
	: alive(true)
{
}

ActorComponent::~ActorComponent(void)
{ 
}

void ActorComponent::init(Engine * parentEngine, Actor * actor)
{
	this->parentEngine = parentEngine; 
	this->actor = actor;
	init();
}

void ActorComponent::setParent(Actor& parentPtr)
{
	this->actor = &parentPtr;
}

bool ActorComponent::isAlive()
{
	return alive;
}
