#pragma once
#include "IGraphicsComponent.h"
#include "Transform.h"
#include <string>
class Mesh;
class Texture;
class Shader;

/**
 * @class	GraphicsComponent
 *
 * @brief	The graphics component for each actor.
 * @details	Holds pointers to all data needed to draw the actor.
 * 			All pointers are owned by their respective handlers
 */
class GraphicsComponent : public IGraphicsComponent
{
private:

	/**
	 * @fn	virtual void GraphicsComponent::init() override;
	 *
	 * @brief	S this object.
	 */
	virtual void init() override;
public:

	/**
	 * @fn	GraphicsComponent::GraphicsComponent(ActorData::ComponentData* componentData);
	 *
	 * @brief	Constructor.
	 *
	 * @param [in,out]	componentData	If non-null, information describing the component.
	 */
	GraphicsComponent(ActorData::ComponentData* componentData);

	/**
	 * @fn	GraphicsComponent::~GraphicsComponent() override;
	 *
	 * @brief	Destructor.
	 */
	~GraphicsComponent() override;

	/**
	 * @fn	void GraphicsComponent::draw(const glm::mat4& viewProjection);
	 *
	 * @brief	Draws the given view projection.
	 *
	 * @param	viewProjection	The view projection.
	 */
	void draw(const glm::mat4& viewProjection);

	/**
	 * @fn	void GraphicsComponent::update(float deltaTime) override;
	 *
	 * @brief	Updates the given deltaTime.
	 *
	 * @param	deltaTime	The delta time.
	 */
	void update(float deltaTime) override;

	/**
	 * @fn	void GraphicsComponent::setPosition(glm::vec3 position);
	 *
	 * @brief	Sets a position.
	 *
	 * @param	position	The position.
	 */
	void setPosition(glm::vec3 position);

	/**
	 * @fn	void GraphicsComponent::setScale(glm::vec3 scale);
	 *
	 * @brief	Sets a scale.
	 *
	 * @param	scale	The scale.
	 */
	void setScale(glm::vec3 scale);

	/**
	 * @fn	void GraphicsComponent::setTilePosition(glm::vec2 tilePosition);
	 *
	 * @brief	Sets tile position.
	 *
	 * @param	tilePosition	The tile position.
	 */
	void setTilePosition(glm::vec2 tilePosition);

	//TODO: NEEDS MAJOR REWORK
//	void receiveMessage(const Message &message);

	/**
	 * @fn	void GraphicsComponent::setTexture(Texture * texture);
	 *
	 * @brief	Sets a texture.
	 *
	 * @param [in,out]	texture	If non-null, the texture.
	 */
	void setTexture(Texture * texture);

	/**
	 * @fn	void GraphicsComponent::setTexture(std::string texturePath);
	 *
	 * @brief	Sets a texture.
	 *
	 * @param	texturePath	Full pathname of the texture file.
	 */
	void setTexture(std::string texturePath);

	/**
	 * @fn	void GraphicsComponent::setRotation(float degrees);
	 *
	 * @brief	Sets a rotation.
	 *
	 * @param	degrees	The degrees.
	 * @param	axis Rotation around axis.
	 */
	void setRotation(float degrees, char axis);

	/**
	 * @fn	void GraphicsComponent::setFrames(glm::vec2 frames);
	 *
	 * @brief	Sets the frames.
	 *
	 * @param	frames	The frames.
	 */
	void setFrames(glm::vec2 frames);

	/**
	 * @fn
	 * void GraphicsComponent::updateSpritePositions(ActorData::Animation::AnimationSequence animationSequence);
	 *
	 * @brief	Updates the sprite positions described by animationSequence.
	 *
	 * @param	animationSequence	The animation sequence.
	 */
	void updateSpritePositions(ActorData::Animation::AnimationSequence animationSequence);

	/**
	 * @fn	glm::vec3 GraphicsComponent::getPosition();
	 *
	 * @brief	Gets the position.
	 *
	 * @return	The position.
	 */
	glm::vec3 getPosition();

	/**
	 * @fn	Texture* GraphicsComponent::getTexture();
	 *
	 * @brief	Gets the texture.
	 *
	 * @return	null if it fails, else the texture.
	 */
	Texture* getTexture();

	/**
	 * @fn	void GraphicsComponent::setColor(glm::vec4 color);
	 *
	 * @brief	Sets a color.
	 *
	 * @param	color	The color.
	 */
	void setColor(glm::vec4 color);

private:
	float currentFrame = 0.f;
	Transform transform;
	Mesh *mesh;
	Shader *shader;
	Texture *texture;
	glm::mat4 modelMatrix;
	glm::vec4 color;
};