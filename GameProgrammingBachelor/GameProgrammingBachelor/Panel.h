#pragma once
#include "GuiElement.h"

class Engine;
class GuiElement;

/** A panel. Can be used to display a panel in game. For example a panel behind buttons or really anything you just want to display as a square with a texture in gui */
class Panel : public GuiElement
{
public:
	Panel(GuiData::ElementData* elementData);
	virtual ~Panel();

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection) override;

	/**
	* Sets the  texture of the panel to a new one in runtime.
	*
	* @param	texture		the texture path that is used when changing.
	*/
	void changeTexture(std::string texturePath);

	/**
	* Gets the name.
	*
	* @return	The name.
	*/
	std::string getName();

	/**
	* Sets the panel to visible or not, also deactivates interactions.
	*
	* @param	bool		true or false for visible or not.
	*/
	void setVisible(bool visible);
private:
	GuiData::Panel elementData;

	std::string name;

	bool visible;

	Transform transform;
	Mesh *mesh;
	Shader *shader;
	Texture *texture;

	virtual void init() override;
};

