#pragma once
#include <memory>
#include "ActorData.h"
#include "ActorComponent.h"

class ComponentFactoryFactory;
class ComponentCreator;

/**
 * @class	ComponentFactoryCreator
 *
 * @brief	A component factory creator.
 */
class ComponentFactoryCreator
{
public:

	/**
	 * @fn
	 * ComponentFactoryCreator::ComponentFactoryCreator(const std::string& componentType, ComponentFactoryFactory* componentFactory);
	 *
	 * @brief	Constructor.
	 *
	 * @param	componentType				Type of the component.
	 * @param [in,out]	componentFactory	If non-null, the component factory.
	 */
	ComponentFactoryCreator(const std::string& componentType, ComponentFactoryFactory* componentFactory);

	/**
	 * @fn
	 * void ComponentFactoryCreator::registerIt(const std::string& typeCategory, ComponentCreator* componentCreator);
	 *
	 * @brief	Registers the iterator.
	 *
	 * @param	typeCategory				Category the type belongs to.
	 * @param [in,out]	componentCreator	If non-null, the component creator.
	 */
	void registerIt(const std::string& typeCategory, ComponentCreator* componentCreator);

	/**
	 * @fn
	 * virtual std::unique_ptr<ActorComponent> ComponentFactoryCreator::create(const std::string& typeCategory, ActorData::ComponentData* componentData) = 0;
	 *
	 * @brief	Creates a new std::unique_ptr&lt;ActorComponent&gt;
	 *
	 * @param	typeCategory		 	Category the type belongs to.
	 * @param [in,out]	componentData	If non-null, information describing the component.
	 *
	 * @return	A std::unique_ptr&lt;ActorComponent&gt;
	 */
	virtual std::unique_ptr<ActorComponent> create(const std::string& typeCategory, ActorData::ComponentData* componentData) = 0;
protected:
	std::map<std::string, ComponentCreator*> table;
};

/**
 * @class	ComponentFactoryCreatorImplementation
 *
 * @brief	A component factory creator implementation.
 *
 * @tparam	T	Generic type parameter.
 */
template <class T>
class ComponentFactoryCreatorImplementation : public ComponentFactoryCreator
{
public:

	/**
	 * @fn
	 * ComponentFactoryCreatorImplementation::ComponentFactoryCreatorImplementation(const std::string& componentType, ComponentFactoryFactory* componentFactory)
	 *
	 * @brief	Constructor.
	 *
	 * @param	componentType				Type of the component.
	 * @param [in,out]	componentFactory	If non-null, the component factory.
	 */
	ComponentFactoryCreatorImplementation(const std::string& componentType, ComponentFactoryFactory* componentFactory) : ComponentFactoryCreator(componentType, componentFactory) {};

	//needs some buffs! 

	/**
	 * @fn
	 * virtual std::unique_ptr<ActorComponent> ComponentFactoryCreatorImplementation::create(const std::string& typeCategory, ActorData::ComponentData* componentData) override
	 *
	 * @brief	Creates a new std::unique_ptr&lt;ActorComponent&gt;
	 *
	 * @param	typeCategory		 	Category the type belongs to.
	 * @param [in,out]	componentData	If non-null, information describing the component.
	 *
	 * @return	A std::unique_ptr&lt;ActorComponent&gt;
	 */
	virtual std::unique_ptr<ActorComponent> create(const std::string& typeCategory, ActorData::ComponentData* componentData) override
	{
		if (typeCategory == "")
		{
			return std::make_unique<T>(componentData);
		}
		auto it = table.find(typeCategory);
		if (it != table.end())
		{
			return it->second->create(componentData);
		}
		return nullptr;
	}
};

/**
 * @class	ComponentCreator
 *
 * @brief	A component creator.
 */
class ComponentCreator
{
public:

	/**
	 * @fn
	 * ComponentCreator::ComponentCreator(const std::string& typeCategory, ComponentFactoryCreator* componentFactoryCreator)
	 *
	 * @brief	Constructor.
	 *
	 * @param	typeCategory				   	Category the type belongs to.
	 * @param [in,out]	componentFactoryCreator	If non-null, the component factory creator.
	 */
	ComponentCreator(const std::string& typeCategory, ComponentFactoryCreator* componentFactoryCreator)
	{
		componentFactoryCreator->registerIt(typeCategory, this);
	};

	/**
	 * @fn
	 * virtual std::unique_ptr<ActorComponent> ComponentCreator::create(ActorData::ComponentData* componentData) = 0;
	 *
	 * @brief	Creates a new std::unique_ptr&lt;ActorComponent&gt;
	 *
	 * @param [in,out]	componentData	If non-null, information describing the component.
	 *
	 * @return	A std::unique_ptr&lt;ActorComponent&gt;
	 */
	virtual std::unique_ptr<ActorComponent> create(ActorData::ComponentData* componentData) = 0;
};

/**
 * @class	ComponentCreatorImplementation
 *
 * @brief	A component creator implementation.
 *
 * @tparam	T	Generic type parameter.
 */
template <class T>
class ComponentCreatorImplementation : public ComponentCreator
{
public:

	/**
	 * @fn
	 * ComponentCreatorImplementation::ComponentCreatorImplementation(const std::string& typeCategory, ComponentFactoryCreator* componentFactoryCreator)
	 *
	 * @brief	Constructor.
	 *
	 * @param	typeCategory				   	Category the type belongs to.
	 * @param [in,out]	componentFactoryCreator	If non-null, the component factory creator.
	 */
	ComponentCreatorImplementation(const std::string& typeCategory, ComponentFactoryCreator* componentFactoryCreator) : ComponentCreator(typeCategory, componentFactoryCreator) {};

	/**
	 * @fn
	 * virtual std::unique_ptr<ActorComponent> ComponentCreatorImplementation::create(ActorData::ComponentData* componentData) override
	 *
	 * @brief	Creates a new std::unique_ptr&lt;ActorComponent&gt;
	 *
	 * @param [in,out]	componentData	If non-null, information describing the component.
	 *
	 * @return	A std::unique_ptr&lt;ActorComponent&gt;
	 */
	virtual std::unique_ptr<ActorComponent> create(ActorData::ComponentData* componentData) override
	{
		return std::make_unique<T>(componentData);
		return nullptr;
	}
};