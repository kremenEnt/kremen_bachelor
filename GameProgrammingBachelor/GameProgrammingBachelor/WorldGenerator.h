#pragma once
#include "Handler.h"
#include "ActorFactory.h"
#include "PhysicsHandler.h"
#include "PlatformData.h"
#include "Actor.h"
#include "Astar.h"
#include <vector>
#include <memory>

/**
 * @class	WorldGenerator for noise approach
 *
 * @brief	A world generator using the noise approach.
 */
class WorldGenerator : public Handler 
{
public:

	/**
	 * @fn	WorldGenerator::WorldGenerator();
	 *
	 * @brief	Default constructor.
	 */
	WorldGenerator();

	/**
	 * @fn	WorldGenerator::~WorldGenerator();
	 *
	 * @brief	Destructor.
	 */
	~WorldGenerator();

	/**
	 * @fn	void WorldGenerator::setActorFactory(ActorFactory * actorFactory);
	 *
	 * @brief	Sets actor factory.
	 *
	 * @param [in,out]	actorFactory	If non-null, the actor factory.
	 */
	void setActorFactory(ActorFactory * actorFactory);

	/**
	 * @fn	void WorldGenerator::generate();
	 *
	 * @brief	Generates level.
	 */
	void generate();

	/**
	 * @fn	void WorldGenerator::newLevel();
	 *
	 * @brief	Creates a new level.
	 */
	void newLevel();

	/**
	 * @fn	void WorldGenerator::setBool(bool state, std::string name);
	 *
	 * @brief	Sets a bool.
	 *
	 * @param	state	true to state.
	 * @param	name 	The name.
	 */
	void setBool(bool state, std::string name);

	/**
	 * @fn	void WorldGenerator::update();
	 *
	 * @brief	Updates this object.
	 */
	void update();
private:
	const unsigned int length;
	ActorFactory* actorFactory;
	PhysicsHandler* physicsHandler;
	std::vector<Actor*>actors;
	std::vector<CollisionData> collisionDataVector;
	bool gap;
	bool spike;
	bool spring;
	bool everything;
	bool timedBlock;
	std::unique_ptr<Grid> grid; //grid representation of world
	bool generateNewLevel;
};

