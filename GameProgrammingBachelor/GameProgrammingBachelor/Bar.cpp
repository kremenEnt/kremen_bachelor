#include "Bar.h"
#include "Engine.h"
#include "GuiElement.h"
#include "Message.h"
#include "MeshHandler.h"
#include "ShaderHandler.h"
#include "TextureHandler.h"


Bar::Bar(GuiData::ElementData* elementData) : GuiElement(elementData)
{
	this->elementData = *dynamic_cast<GuiData::Bar*>(elementData);
	name = "";
	color = { 1.f, 1.f, 1.f, 1.f };
	maxValue = 0;
	currentValue = 0;
}

Bar::~Bar()
{
}

void Bar::update()
{
	float tempScale = (currentValue / maxValue);

	float tempPos; 

	//checks if the bar is at 100% or more. If it is the bar shouldnt draw outside its limits
	if (horizontal)
	{
		tempPos = (width * tempScale / 2.f);
		if (tempScale <= 1.0)
		{
			barTransform.setPos(glm::vec3(defaultBarTransform.getPos().x + tempPos - width / 2.f, barTransform.getPos().y, 1.0f));
			barTransform.setScale(glm::vec3(defaultBarTransform.getScale().x * tempScale, barTransform.getScale().y, 1.0f));
		}
		else
		{
			barTransform = defaultBarTransform;
			barTransform.setPos(defaultBarTransform.getPos());
		}
	}
	else
	{
		tempPos = (height * tempScale / 2.f);

		if (tempScale <= 1.0)
		{
			barTransform.setPos(glm::vec3(barTransform.getPos().x , defaultBarTransform.getPos().y + tempPos - height / 2.f, 1.0f));
			barTransform.setScale(glm::vec3(barTransform.getScale().x , defaultBarTransform.getScale().y* tempScale, 1.0f));
		}
		else
		{
			barTransform = defaultBarTransform;
			barTransform.setPos(defaultBarTransform.getPos());
		}
	}
}

void Bar::setBackgroundTexture(std::string texturePath)
{
	backgroundTexture = parentEngine->getTextureHandler()->loadTexture(texturePath);
}

void Bar::setColor(glm::vec4 colorChange)
{
	color = colorChange;
}

void Bar::setBarTexture(std::string texturePath)
{
	barTexture = parentEngine->getTextureHandler()->loadTexture(texturePath);
}


void Bar::setCurrentValue(float size)
{
	currentValue = size;
}

void Bar::setMaxValue(float size)
{
	maxValue = size;
	currentValue = maxValue;
}

void Bar::draw(glm::mat4 &viewProjection)
{
	if (visible)
	{
		if (textureOnBar)
		{
			shader->bind();
			shader->loadTransform(barTransform, viewProjection);
			shader->loadInt(U_TEXTURE0, 0);
			shader->loadFloat(U_SCALE, 1.5f);
			barTexture->bind(0);
			mesh->draw();
		}
		else
		{
			shaderBar->bind();
			shaderBar->loadTransform(barTransform, viewProjection);
			shaderBar->loadVec4(U_COLOR, color);
			mesh->draw();
		}

		shader->bind();
		shader->loadTransform(backgroundTransform, viewProjection);
		shader->loadInt(U_TEXTURE0, 0);
		shader->loadFloat(U_SCALE, 1.5f);
		backgroundTexture->bind(0);
		mesh->draw();
	}
}

void Bar::init()
{
	float tempPosX;
	float tempPosY;
	std::string pathTemp;
	name = elementData.name;
	
	tempPosX = elementData.positionX;
	tempPosY = elementData.positionY;
	width = elementData.width;
	height = elementData.height;
	height = (height / aspectRatio.y)*aspectRatio.x;
	visible = elementData.visible;

	this->mesh = parentEngine->getMeshHandler()->loadModel(elementData.meshPath);
	this->shader = parentEngine->getShaderHandler()->loadShader(elementData.shaderPath);
	this->backgroundTexture = parentEngine->getTextureHandler()->loadTexture(elementData.texturePath);

	pathTemp = elementData.texturePathBar;
	if (!pathTemp.empty())
	{
		textureOnBar = true;
		this->barTexture = parentEngine->getTextureHandler()->loadTexture(elementData.texturePathBar);
	}
	else
	{
		color = elementData.color;
	}
	horizontal = elementData.horizontal;
	

	this->shaderBar = parentEngine->getShaderHandler()->loadShader("res/shaders/basicColorShader");

	backgroundTransform.setPos(glm::vec3(tempPosX, tempPosY, 0.999f));
	backgroundTransform.setScale(glm::vec3(width, height, 1.0f));
	
	if (horizontal)
	{
		barTransform.setScale(glm::vec3(backgroundTransform.getScale().x * 0.92f, backgroundTransform.getScale().y * 0.85f, 1.0f));
		barTransform.setPos(glm::vec3(backgroundTransform.getPos().x - (backgroundTransform.getScale().x - barTransform.getScale().x) / 2.f + 0.01125f, backgroundTransform.getPos().y, backgroundTransform.getPos().z));
	}
	else
	{
		barTransform.setScale(glm::vec3(backgroundTransform.getScale().x * 0.92f, backgroundTransform.getScale().y, 1.0f));
		barTransform.setPos(glm::vec3(backgroundTransform.getPos().x, backgroundTransform.getPos().y - (backgroundTransform.getScale().y - barTransform.getScale().y) / 2.f + 0.005f, backgroundTransform.getPos().z));
	}	
	defaultBarTransform = barTransform;
 }

std::string Bar::getName()
{
	return name;
}

void Bar::changeBackgroundTexture(std::string newTexture)
{
	backgroundTexture = parentEngine->getTextureHandler()->loadTexture(newTexture);

}

void Bar::changeBarTexture(std::string newTexture)
{
	barTexture = parentEngine->getTextureHandler()->loadTexture(newTexture);
}

void Bar::setVisible(bool visible)
{
	this->visible = visible;
}
