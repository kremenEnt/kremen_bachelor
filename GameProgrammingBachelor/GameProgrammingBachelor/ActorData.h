#pragma once
#include <string>
#include <unordered_map>
#include <vector>
#include <glm\glm.hpp>
#include "XmlHandler.h"
#include <map>
#include "common.h"


class Actor;
class ActorFactory;
class ComponentFactory;

static std::unique_ptr<XmlHandler> actorXMLHandler;

/**
 * @class	ActorData
 *
 * @brief	Data for Actors.
 */
class ActorData
{
private:
	friend class ActorFactory;
	friend class ComponentFactory;
	std::string actorType;
	std::string actorName;
	std::string actorSource;
	std::string category;
	int netSyncLevel;
public:

	/**
	 * @fn	ActorData::ActorData();
	 *
	 * @brief	Default constructor.
	 */
	ActorData();

	/**
	 * @fn	ActorData::ActorData(std::string xmlPath);
	 *
	 * @brief	Constructor.
	 *
	 * @param	xmlPath	Full pathname of the XML file.
	 */
	ActorData(std::string xmlPath);

	/**
	 * @fn	virtual ActorData::~ActorData()
	 *
	 * @brief	Destructor.
	 */
	virtual ~ActorData() {};

	/**
	 * @fn	static void ActorData::init();
	 *
	 * @brief	Initializes this object to have access to xml resource manager. is static because this they all use the same handler
	 */
	static void init();

	/**
	 * @struct	ComponentData
	 *
	 * @brief	Base component data.
	 */
	struct ComponentData
	{
		virtual ~ComponentData() {};
		//used to know which componentType to create from actorFactory
		std::string type;
		//used to know which spesific f
		std::string identifier = "";
	};
protected:
	tinyxml2::XMLElement* componentsElement;
	std::vector<std::unique_ptr<ComponentData>> componentDataVector;
public:

	/**
	 * @struct	Animation
	 *
	 * @brief	An animation component.
	 */
	struct Animation : public ComponentData
	{
		/**
		 * @fn	Animation()
		 *
		 * @brief	Default constructor.
		 */
		Animation() {};

		/**
		 * @struct	AnimationSequence
		 *
		 * @brief	An animation sequence.
		 */
		struct AnimationSequence
		{
			
			int firstFrame;
			int amountOfFrames;
			float animationSpeed;
			AnimationSequence() : firstFrame(-1), amountOfFrames(-1), animationSpeed(-1) {};
		};
		
		std::map<int, AnimationSequence> animations;
	};

	/**
	 * @struct	ArtificialIntelligence
	 *
	 * @brief	An artificial intelligence component.
	 */
	struct ArtificialIntelligence : public ComponentData
	{
		std::string scriptPath;
		ArtificialIntelligence() : scriptPath("") {};
	};

	/**
	 * @struct	Audio
	 *
	 * @brief	An audio component.
	 */
	struct Audio : public ComponentData
	{
		//identifier, filepath
		std::unordered_map<std::string, std::string> soundMap;
		//identifier, filepath
		std::unordered_map<std::string, std::string> musicMap;
	};

	/**
	 * @struct	Combat
	 *
	 * @brief	A combat component.
	 */
	struct Combat : public ComponentData
	{
		int maxHealth = 1;
		int damage = 0;
		float lifetime = 0;
		Combat():maxHealth(1), damage(0), lifetime(0) {};
	};

	/**
	 * @struct	Graphics
	 *
	 * @brief	A graphics component.
	 */
	struct Graphics : public ComponentData
	{
		glm::vec3 size;
		glm::ivec2 noSpriteFrames;
		bool animated;
		std::string meshPath;
		std::string texturePath;
		std::string shaderPath;
		Graphics() :size{ 1.f,1.f,1.f }, noSpriteFrames(1, 1), animated(false), meshPath(""), texturePath(""), shaderPath("res/shaders/basicShader"){};
	};

	/**
	* @struct	Input
	*
	* @brief	An input component.
	*/
	struct Input : public ComponentData
	{
		//input just needs to exist. will always be too game spesific, but it's better to be somewhat handeled by engine because of message system and subscribing
	};

	/**
	 * @struct	Physics
	 *
	 * @brief	A physics component.
	 */
	struct Physics : public ComponentData
	{
		struct Definition
		{
			enum
			{
				BOX,
				CIRCLE,
				POLYGON
			};
			
			int bodyType = 0;
			std::vector<glm::vec2> polygonVertices;
			int shape = BOX;
			bool isSensor = false;
			float density = 1.f;
			glm::vec2 size{ 1.f,1.f };
			float gravity = 1.f;
			bool jumpSensor = false;
			bool wallSensor = false;
			bool fixedRotation = true;
			float restitution = 0.f;
			float friction = 0.2f;
			std::string collisionCategory = "";
		};
		Definition definition;
		float desiredSpeed;
		Physics() :desiredSpeed(0) {};
	};

	/**
	 * @struct	Particle
	 *
	 * @brief	A particle component.
	 */
	struct Particle : public ComponentData
	{
		float particlesPerSecond = 100.f;
		float maxLife = 1.f;
		glm::vec3 velocity = { 1.f,1.f,1.f };
		float maxVelocity = 0.f;
		float size = 0.1f;
		glm::vec4 color{ 1.f,1.f,1.f,1.f };
		bool gravity = true;
		float gravityMultiplier = 0.5f;
		std::string texturePath = "";
	};
};