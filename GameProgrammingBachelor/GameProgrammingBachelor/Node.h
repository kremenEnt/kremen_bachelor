#pragma once
#include "common.h"

/**
 * @class	Node
 *
 * @brief	A node.
 */
class Node
{
public:

	/**
	 * @fn	Node::Node(krem::Vector2i pos, Node* parent, double gCost, double hCost);
	 *
	 * @brief	Constructor.
	 *
	 * @param	pos			  	The position.
	 * @param [in,out]	parent	If non-null, the parent.
	 * @param	gCost		  	The cost to nearest goal.
	 * @param	hCost		  	The heuristic cost.
	 */
	Node(krem::Vector2i pos, Node* parent, double gCost, double hCost);

	/**
	 * @fn	Node::~Node();
	 *
	 * @brief	Destructor.
	 */
	~Node();

	/**
	 * @fn	Node* Node::getParent();
	 *
	 * @brief	Gets the parent of this item.
	 *
	 * @return	nullptr if it fails, else the parent.
	 */
	Node* getParent();

	/**
	 * @fn	double Node::getFCost() const;
	 *
	 * @brief	Gets f cost.
	 *
	 * @return	The f cost.
	 */
	double getFCost() const;

	/**
	 * @fn	double Node::getGCost() const;
	 *
	 * @brief	Gets g cost.
	 *
	 * @return	The g cost.
	 */
	double getGCost() const;

	/**
	 * @fn	krem::Vector2i Node::getPos() const;
	 *
	 * @brief	Gets the position.
	 *
	 * @return	The position.
	 */
	krem::Vector2i getPos() const;

	/**
	 * @fn	static bool Node::sortCost(const Node* first, const Node* second);
	 *
	 * @brief	Sort cost.
	 *
	 * @param	first 	The first.
	 * @param	second	The second.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	static bool sortCost(const Node* first, const Node* second);

private:
	krem::Vector2i pos;	//position
	Node* parent;
	double gCost;		//from start to finish
	double hCost;		//distance from current to next node
	double fCost;		//combined cost from g and h
};