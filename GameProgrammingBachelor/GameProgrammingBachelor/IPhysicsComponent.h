#pragma once
#include "ActorComponent.h"
/**
 * @class	IPhysicsComponent
 *
 * @brief	The physics component interface. Used by most game objects
 */
class IPhysicsComponent : public ActorComponent
{
public:
	IPhysicsComponent(ActorData::ComponentData* componentData) : ActorComponent(componentData) {};
	IPhysicsComponent() {};
	//virtual ~IPhysicsComponent() = 0;
	//virtual void setPosition(glm::vec2 position) = 0;
	//virtual void applyForce() = 0;
	//virtual void createShape(int shapeIndex) = 0;
protected:
	ActorData::Physics* componentData;
	//glm::vec3 position;
	glm::vec3 newPosition;

	float angle;
	float newAngle;
};

