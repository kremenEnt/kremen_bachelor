#include "Actor.h"
#include "Engine.h"
#include "IActorState.h"
#include "ASDropping.h"
#include "ASIdle.h"
#include "ASWalking.h"
#include "ASRising.h"
#include "ASDead.h"
#include "GraphicsComponent.h"
#include "PhysicsComponent.h"
#include <Box2D\Box2D.h>
#ifdef __ANDROID__
#include <SDL.h>
#define printf SDL_Log
#endif

Actor::Actor(Engine* parentEngine):
	active(true),
	category(""),
	currentState(AS_WALKING)
{
	this->parentEngine = parentEngine;
}

//this is kind of horritrash. rewrite once templates are proper
Actor::Actor(const Actor & newActor):
	position(newActor.position),
	actorId(newActor.actorId),
	category(newActor.category),
	parentEngine(newActor.parentEngine),
	active(newActor.active)
{

}

Actor& Actor::operator=(const Actor& newActor)
{
	return *this;
}

Actor::~Actor()
{
	for (auto &it : actorComponentMap)
	{
		it.second.reset();
	}
	actorComponentMap.clear();
}

void Actor::addComponent(std::unique_ptr<ActorComponent>& actorComponent, const std::string identifier)
{	
	addChild(actorComponent.get());
	actorComponent->init(parentEngine, this);
	actorComponentMap.insert(std::make_pair(identifier, std::move(actorComponent)));
}

void Actor::postInit()
{
	auto i = 0;
	actorStates.insert(std::make_pair(i, std::move(std::make_unique<ASIdle>(this))));
	i++;
	actorStates.insert(std::make_pair(i, std::move(std::make_unique<ASWalking>(this))));
	i++;
	actorStates.insert(std::make_pair(i, std::move(std::make_unique<ASRising>(this))));
	i++;
	actorStates.insert(std::make_pair(i, std::move(std::make_unique<ASDropping>(this))));
	i++;
	actorStates.insert(std::make_pair(i, std::move(std::make_unique<ASDead>(this))));
	actorState.push(actorStates[AS_IDLE].get());
	actorState.top()->enter();
	onSpawn();
}

void Actor::update(float deltaTime)
{
	//this is slow and inefficient, and should be rewritten somehow in the next iteration
	if (actorState.size() > 0)
	{
		int newState = actorState.top()->update();
		if (newState != currentState)
		{
			currentState = newState;
			actorState.top()->leave();
			actorState.pop();
			actorState.push(actorStates[newState].get());
			actorState.top()->enter();
		}
	}
	
	for (auto& it : actorComponentMap)
	{
		it.second->update(deltaTime);
	}
}

void Actor::onDeath()
{
	for (auto& it : actorComponentMap)
	{
		it.second->onDeath();
	}
}

void Actor::onHit()
{
	for (auto& it : actorComponentMap)
	{
		//it.second->onHit();
	}
}

void Actor::onSpawn()
{
	for (auto& it : actorComponentMap)
	{
		it.second->onSpawn();
	}
}

ActorComponent* Actor::getComponent(std::string componentId)
{
	auto it = actorComponentMap.find(componentId);
	if (it != actorComponentMap.end())
	{
		return it->second.get();
	}
	else
	{
		return nullptr;
	}
}

actorID Actor::getActorId()
{
	return actorId;
}

void Actor::setActorId(actorID actorId)
{
	this->actorId = actorId;
}

std::string Actor::getCategory()
{
	return category;
}

void Actor::setCategory(std::string category)
{
	this->category = category;
}

void Actor::setPosition(glm::vec3 newPosition)
{
	position = newPosition;
	PhysicsComponent* physicsComponentptr = dynamic_cast<PhysicsComponent*>(getComponent("PhysicsComponent"));
	if (physicsComponentptr != nullptr)
	{
		physicsComponentptr->getB2Body()->SetTransform({ newPosition.x,newPosition.y }, physicsComponentptr->getB2Body()->GetAngle());
	}
	GraphicsComponent* graphicsComponentptr = dynamic_cast<GraphicsComponent*>(getComponent("GraphicsComponent"));
	if (graphicsComponentptr != nullptr)
	{
		graphicsComponentptr->setPosition(newPosition);
	}
}

glm::vec3 Actor::getPosition()
{
	return position;
}

void Actor::movePosition(glm::vec3 direction)
{
	position += direction;
	if (getComponent("PhysicsComponent") != nullptr)
	{
		dynamic_cast<PhysicsComponent*>(getComponent("PhysicsComponent"))->setPosition(position);
	}
	else
	{
		if (getComponent("GraphicsComponent") != nullptr)
		{
			dynamic_cast<GraphicsComponent*>(getComponent("GraphicsComponent"))->setPosition(position);
		}
	}
}

void Actor::setActive(bool flag)
{
	PhysicsComponent* physicsComponentptr = dynamic_cast<PhysicsComponent*>(getComponent("PhysicsComponent"));
	if (physicsComponentptr != nullptr)
	{
		physicsComponentptr->getB2Body()->SetActive(flag);
	}
	active = flag;
}

void Actor::setPhysicsActive(bool flag)
{
	PhysicsComponent* physicsComponentptr = dynamic_cast<PhysicsComponent*>(getComponent("PhysicsComponent"));
	if (physicsComponentptr != nullptr)
	{
		physicsComponentptr->getB2Body()->SetActive(flag);
	}
}

bool Actor::isActive()
{
	return active;
}

void Actor::addChild(ActorComponent* actorComponent)
{
	actorComponent->setParent(*this);
}
