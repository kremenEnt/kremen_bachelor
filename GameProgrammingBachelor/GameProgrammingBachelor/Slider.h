#pragma once
#include "GuiElement.h"

class Engine;
class GuiElement;
/** A slider. Used to create sliders that go from a minimum to a maximum value. */
class Slider : public GuiElement
{
public:
	Slider(GuiData::ElementData* elementData);
	virtual ~Slider();

	/** Updates this object. */
	void update() override;

	/** Check mouse over to change the texture to give feedback to the user*/
	void checkMouseOver();

	/**
	 * Determines if mouse click left is inside either left/right arrow of slider or the slider it self..
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickLeft() override;

	/**
	 * Determines if mouse click right is inside either left/right arrow of slider or the slider it self..
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickRight() override;

	/**
	 * Determines if mouse release is inside either left/right arrow of slider or the slider it self.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseRelease() override;

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection) override;

	/**
	 * Gets the name.
	 *
	 * @return	The name.
	 */
	std::string getName();

	/**
	* Sets the  texture of the slider to a new one in runtime.
	*
	* @param	texture		the texture path that is used when changing.
	*/
	void changeTexture(std::string newTexture);

	/**
	* Sets the slider to visible or not, also deactivates interactions.
	*
	* @param	bool		true or false for visible or not.
	*/
	void setVisible(bool visible);
private:
	GuiData::Slider elementData;

	std::vector<float> values;

	tinyxml2::XMLElement *originNode;

	std::string name;

	bool slider;
	bool left;
	bool right;
	bool fontDisplay;
	bool visible;

	float distance;

	int maxValue;
	int minValue;
	int currentValue;
	int valueInterval;
	int textureAtlasSize;
	int currentSliderTexture;
	int currentRightTexture;
	int currentLeftTexture;

	//TODO: rewrite this to be dynamic as fuck
	Transform transform[3];
	Font * font;
	Mesh *mesh;
	Shader *shader;
	Texture *texture;

	virtual void init() override;
};

