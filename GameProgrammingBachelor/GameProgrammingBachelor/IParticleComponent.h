#pragma once
#include "ActorComponent.h"
#include "ParticleSystem.h"

class IParticleComponent : public ActorComponent
{
public:
	IParticleComponent() {};
	virtual ~IParticleComponent() {};
};