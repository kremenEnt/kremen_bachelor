#include "Node.h"

Node::Node(krem::Vector2i pos, Node * parent, double gCost, double hCost)
{
	this->pos = pos;
	this->parent = parent;
	this->gCost = gCost;
	this->hCost = hCost;
	this->fCost = this->gCost + this->hCost;
}

Node::~Node()
{
	this->pos = { 0,0 };
	this->parent = nullptr;
	this->gCost = 0.0;
	this->hCost = 0.0;
	this->fCost = 0.0;
}

Node * Node::getParent()
{
	return this->parent;
}

double Node::getFCost() const
{
	return this->fCost;
}

double Node::getGCost() const
{
	return this->gCost;
}

krem::Vector2i Node::getPos() const
{
	return this->pos;
}

bool Node::sortCost(const Node * first, const Node * second)
{
	return (first->fCost < second->fCost);
}