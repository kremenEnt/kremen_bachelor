#include "AttemptX.h"
#include "Engine.h"
#include "DatabaseHandler.h"
#include <string>

void AttemptX::loadLevelDataFromDB()
{
	//OUT COMMENTED CODE IS USED TO RESET DATABASE :D
	//parentEngine->getDatabaseHandler()->connect();

	//parentEngine->getDatabaseHandler()->insertPreperedStatement("UPDATE LevelScores SET Input1 = 0, Input2 = 0, Input3 = 0, Input4 = 0, Input5 = 0 WHERE Id % 2 = 0");// fix for fuckups in database
	//parentEngine->getDatabaseHandler()->insertPreperedStatement("UPDATE LevelScores SET Input1 = 1, Input2 = 1, Input3 = 1, Input4 = 1, Input5 = 1 WHERE Id % 2 = 1");// fix for fuckups in database
	//parentEngine->getDatabaseHandler()->disconnect();

	parentEngine->getDatabaseHandler()->connect();
	std::vector<std::string> columns;
	columns.emplace_back("Input1");
	columns.emplace_back("Input2");
	columns.emplace_back("Input3");
	columns.emplace_back("Input4");
	columns.emplace_back("Input5");
	std::vector<float> scores[5];
	parentEngine->getDatabaseHandler()->retrieveData("SELECT Input1, Input2, Input3, Input4, Input5 FROM LevelScores", columns, scores[0], scores[1], scores[2], scores[3], scores[4]);
	int chunkcounter = 0;
	int i = 0;
	for (auto eachScoreTable = 0; eachScoreTable < 5; eachScoreTable++)
	{
		for (auto i = 0; i < 1000;)
		{
			for (auto j = 0; j < 5; j++)
			{
				for (auto y = 0; y < 10; y++)
				{
					for (auto x = 0; x < 10; x++)
					{
						oldLevelData[eachScoreTable].chunks[j].scores[y][x].horizontal = scores[eachScoreTable][i++];
						oldLevelData[eachScoreTable].chunks[j].scores[y][x].vertical = scores[eachScoreTable][i++];
					}
				}
			}
		}
	}
	std::vector<std::string> levelInfo;
	levelInfo.emplace_back("AverageGap");
	levelInfo.emplace_back("GapDensity");
	levelInfo.emplace_back("EnemyDensity");
	std::vector<float> localAverageGap;
	std::vector<float> localGapDensity;
	std::vector<float> localEnemyDensity;

	parentEngine->getDatabaseHandler()->retrieveData("SELECT AverageGap, GapDensity, EnemyDensity FROM LevelInfo", levelInfo, localAverageGap, localGapDensity, localEnemyDensity);
	parentEngine->getDatabaseHandler()->disconnect();
	for (auto i = 0; i < 5; i++)
	{
		oldLevelData[i].gapSize = localAverageGap[0];
		oldLevelData[i].gapDensity = localGapDensity[0];
		oldLevelData[i].enemyDensity = localEnemyDensity[0];
	}
	levelData = oldLevelData[rand() % 5];
}


LevelData AttemptX::getDataForLevel()
{
	return levelData;
}

void AttemptX::setSelectedTiles(std::map<MapIdentifier, SlotValues> selectedStartingTiles)
{
	this->selectedStartingTiles = selectedStartingTiles;
}

void AttemptX::saveResultsToDB(bool likedLevel)
{
	int xRising; //positive = rising
	int yRising; //positive = rising
	parentEngine->getDatabaseHandler()->connect();
	for (auto it : selectedStartingTiles)
	{
		xRising = 0;
		yRising = 0;
		for (auto i = 0; i < 5; i++)
		{
			//Hopefully temporary. gets ugly quick
			if (oldLevelData[i].chunks[it.first.chunkNumber].scores[it.first.xPosition][it.first.yPosition].horizontal < 
				oldLevelData[i + 1].chunks[it.first.chunkNumber].scores[it.first.xPosition][it.first.yPosition].horizontal)
			{
				xRising++;
			}
			else
			{
				xRising--;
			}

			if (oldLevelData[i].chunks[it.first.chunkNumber].scores[it.first.xPosition][it.first.yPosition].vertical < 
				oldLevelData[i + 1].chunks[it.first.chunkNumber].scores[it.first.xPosition][it.first.yPosition].vertical)
			{
				yRising++;
			}
			else
			{
				yRising--;
			}
		}
	
		int id = (it.first.chunkNumber * 100*2) + (it.first.xPosition*2) + (it.first.yPosition * 10*2) +1; //100 = chunksizex*y. 10 = chunksizex. +1 because IDs in database start from 1 instead of 0
		//printf("ID TO UPDATE: %i\n", id);
		//if (LEVEL WAS LIKED)
		if (likedLevel)
		{
			if (xRising > 0)
			{
				parentEngine->getDatabaseHandler()->insertPreperedStatement
					("UPDATE LevelScores SET Input" + std::to_string((rand() % 5) + 1) + " = "+ std::to_string(it.second.horizontal+1.f) +"5 WHERE Id = "+std::to_string(id));	
			}

			if (xRising <= 0)
			{
				if (it.second.horizontal > 1)
				{
					parentEngine->getDatabaseHandler()->insertPreperedStatement
						("UPDATE LevelScores SET Input" + std::to_string((rand() % 5) + 1) + " = " + std::to_string(it.second.horizontal - 1.f) + "5 WHERE Id = " + std::to_string(id));
				}
				
			}		
			if (yRising > 0)
			{
				if ((it.first.yPosition + it.second.vertical) < 10)
				{
					parentEngine->getDatabaseHandler()->insertPreperedStatement
						("UPDATE LevelScores SET Input" + std::to_string((rand() % 5) + 1) + " = " + std::to_string(it.second.vertical + 1.f) + "5 WHERE Id = " + std::to_string(id + 1));
				}
				
			}

			if (yRising <= 0)
			{
				if ((it.first.yPosition - it.second.vertical) >= 0)
				{
					parentEngine->getDatabaseHandler()->insertPreperedStatement
						("UPDATE LevelScores SET Input" + std::to_string((rand() % 5) + 1) + " = " + std::to_string(it.second.vertical - 1.f) + "5 WHERE Id = " + std::to_string(id + 1));
				}
			}
		}
		
		//if (LEVEL WAS DISLIKED)
		if (!likedLevel)
		{
			if (xRising > 0)
			{
				if (it.second.horizontal > 1)
				{
					parentEngine->getDatabaseHandler()->insertPreperedStatement
						("UPDATE LevelScores SET Input" + std::to_string((rand() % 5) + 1) + " = " + std::to_string(it.second.horizontal - 1.f) + "5 WHERE Id = " + std::to_string(id));
				}
			}
			if (xRising <= 0)
			{
				parentEngine->getDatabaseHandler()->insertPreperedStatement
					("UPDATE LevelScores SET Input" + std::to_string((rand() % 5) + 1) + " = " + std::to_string(it.second.horizontal + 1.f) + "5 WHERE Id = " + std::to_string(id));
			}

			if (yRising > 0)
			{
				if ((it.first.yPosition + it.second.vertical) < 10)
				{
					parentEngine->getDatabaseHandler()->insertPreperedStatement
						("UPDATE LevelScores SET Input" + std::to_string((rand() % 5) + 1) + " = " + std::to_string(it.second.vertical - 1.f) + "5 WHERE Id = " + std::to_string(id + 1));
				}

			}

			if (yRising <= 0)
			{
				if ((it.first.yPosition + it.second.vertical) < 10)
				
				{
					parentEngine->getDatabaseHandler()->insertPreperedStatement
						("UPDATE LevelScores SET Input" + std::to_string((rand() % 5) + 1) + " = " + std::to_string(it.second.vertical + 1.f) + "5 WHERE Id = " + std::to_string(id + 1));
				}
			}
		}
		//levelData.chunks[it.first.chunkNumber].scores[it.first.xPosition][it.first.yPosition].horizontal -= xRising;
		//levelData.chunks[it.first.chunkNumber].scores[it.first.xPosition][it.first.yPosition].vertical -= yRising;
	

	}	

	parentEngine->getDatabaseHandler()->disconnect();
	 
	//parentEngine->getDatabaseHandler()->insertPreperedStatement
	//	("UPDATE LevelInfo SET Input"+std::to_string(rand() % 5)+" = 5 WHERE Id = 2");

	//UPDATE LevelInfo SET Input1 = 5422435 WHERE ROW_NUMBER() = 5

	/*for (auto i = 0; i < 200; i++)
	{
		if (i % 2 == 0)
		{
			parentEngine->getDatabaseHandler()->insertPreperedStatement
				("INSERT INTO LevelLoader(Input"+ std::to_string(rand() % 5)+")VALUES(?)",
				1.f);
		}
	}*/
	
//	parentEngine->getDatabaseHandler()->disconnect();

	//send newest entry to DB
	//db has to make sure the oldest gets deleted

	//other statistics will also require tracking. enemy density, gap density, gap size, level length. store this in DB and compare.

}