#include "ASRising.h"
#include "Actor.h"
#include "PhysicsComponent.h"
#include "AnimationComponent.h"

int ASRising::update()
{
	//printf("rising\n");
	if (physicsComponent->getVelocity().x == 0 && physicsComponent->getVelocity().y == 0)
	{
		return AS_IDLE;
	}
	if (physicsComponent->getVelocity().y < 0)
	{
		return AS_DROPPING;
	}
	if (physicsComponent->getVelocity().x != 0 && physicsComponent->getVelocity().y == 0)
	{
		return AS_WALKING;
	}

	return AS_RISING;
}

void ASRising::enter()
{
	if (animationComponent != nullptr)
	{
		animationComponent->changeAnimation(JUMPING);
	}
}

void ASRising::leave()
{
}
