#pragma once
#include "IAnimationComponent.h"

/**
 * @class	AnimationComponent
 *
 * @brief	An animation component.
 */
class AnimationComponent : public IAnimationComponent
{
private:

	/**
	 * @fn	virtual void AnimationComponent::init() override;
	 *
	 * @brief	S this object.
	 */
	virtual void init() override;
public:

	/**
	 * @fn	AnimationComponent::AnimationComponent(ActorData::ComponentData* componentData);
	 *
	 * @brief	Constructor.
	 *
	 * @param [in,out]	componentData	If non-null, information describing the component.
	 */
	AnimationComponent(ActorData::ComponentData* componentData);

	/**
	 * @fn	AnimationComponent::~AnimationComponent() override;
	 *
	 * @brief	Destructor.
	 */
	~AnimationComponent() override;

	/**
	 * @fn	void AnimationComponent::changeAnimation(int animation);
	 *
	 * @brief	Change animation.
	 *
	 * @param	animation	The animation.
	 */
	void changeAnimation(int animation);
	//void loadFromXML(Animation &anim, char* firstChild);
	//void receiveMessage(const Message &message);
private:
};