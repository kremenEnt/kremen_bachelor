#include "GuiElementCreator.h"
#include "GuiElementFactory.h"

GuiElementCreator::GuiElementCreator(const std::string & elementType, GuiElementFactory * guiElementFactory)
{
	guiElementFactory->registerIt(elementType, this);
}

void GuiElementCreator::registerIt(const std::string & typeCategory, GuiElementCreator * guiElementCreator)
{
	table[typeCategory] = guiElementCreator;
}