#pragma once
#include "IActorState.h"
#include "ASDefault.h"


class ASDropping : public IActorState, public ASDefault
{
public:
	ASDropping() {};
	ASDropping(Actor* actor) : ASDefault(actor) {};
	virtual int update() override;
	virtual void enter() override;
	virtual void leave() override;
};