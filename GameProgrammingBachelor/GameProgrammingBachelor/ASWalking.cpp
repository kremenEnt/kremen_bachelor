#include "ASWalking.h"
#include "Actor.h"
#include "PhysicsComponent.h"
#include "AnimationComponent.h"
#include <Box2D\Box2D.h>
int ASWalking::update()
{
	//printf("walking\n");
	if (physicsComponent == nullptr)
	{
		return AS_WALKING;
	}
	if (physicsComponent->getVelocity().x == 0 && physicsComponent->getSensorContacts().jump > 0)
	{
		return AS_IDLE;
	}
	if (physicsComponent->getSensorContacts().jump > 0)
	{
		return AS_WALKING;
	}
	if (physicsComponent->getVelocity().y > 0)
	{
		return AS_RISING;
	}
	if (physicsComponent->getVelocity().y < 0)
	{
		return AS_DROPPING;
	}
	return AS_WALKING;
}

void ASWalking::enter()
{
	if (animationComponent != nullptr)
	{
		animationComponent->changeAnimation(WALKING);
	}
}

void ASWalking::leave()
{
}
