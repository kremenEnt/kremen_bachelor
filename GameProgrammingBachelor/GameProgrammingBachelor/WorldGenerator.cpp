#include "WorldGenerator.h"
#include "Engine.h"
#include <noise.h>
#include "noiseutils.h"
#include "PhysicsMovingPlatform.h"
#include "CustomActorData.h"
#include "Astar.h"

//noise approach
WorldGenerator::WorldGenerator()
	: length(100), spike(false), gap(false),spring(false), everything(false)
{
	grid = std::make_unique<Grid>(length, length);
}


WorldGenerator::~WorldGenerator()
{
}

void WorldGenerator::setActorFactory(ActorFactory* actorFactory)
{
	this->actorFactory = actorFactory;
	physicsHandler = parentEngine->getPhysicsHandler();
}

void WorldGenerator::generate()
{
	noise::module::Perlin noise;
	noise.SetSeed(static_cast<int>(time(NULL)));
	noise.SetOctaveCount(6);
	noise.SetFrequency(1.0); //1-16
	noise.SetPersistence(1.0 / 2.0); // 1/4, 1/2, 3/4
	double height = 3.0;
	int heightToFill = 4;

	for (unsigned int i = 0; i < length; i++)
	{
		glm::vec3 layer2{ i, static_cast<int>((noise.GetValue(static_cast<double>((i) / 4.0), height, 0.0) * height)), 0.0 };
		if (layer2.y >= 0)
		{
			this->actors.emplace_back(actorFactory->createActor(dynamic_cast<ActorData*>(&PlatformData("res/actors/tileAtlases/castleFg_rand.xml")), { static_cast<int>(layer2.x), static_cast<int>((layer2.y + (height)) * 1.5f), layer2.z }, glm::vec2(0, 0)));
			collisionDataVector.push_back(CollisionData{ { static_cast<int>(layer2.x), static_cast<int>((layer2.y + (height)) * 1.5f) }, 1.f });
		}
	}

	for (unsigned int i = 0; i < length; i++)
	{
			glm::vec3 position{ i, static_cast<int>((noise.GetValue(static_cast<double>(i / 4.0), 0.0, 0.0) * height)), 0.0 };
			//fill the world under each block to fill the gap underneath the platforms
			for (int j = 1; j <= heightToFill; j++)
			{
				this->actors.emplace_back(actorFactory->createActor(dynamic_cast<ActorData*>(&PlatformData("res/actors/tileAtlases/castleFg_rand.xml")), { position.x, position.y - j, position.z }, glm::vec2(0, 0)));
				collisionDataVector.push_back(CollisionData{ { static_cast<int>(position.x), static_cast<int>(position.y - j) }, 1.f });
			}

			int blocksToCheck = 5;
			//if spike is checked off ingame
			if (spike)
			{
				glm::vec3 previousPosition = { i - 1, static_cast<int>((noise.GetValue(static_cast<double>((i - 1) / 4.0), 0.0, 0.0) * height)), 0.0 };
				glm::vec3 nextPosition;

				if (previousPosition.y > position.y)
				{
					int spikesToPlace = 1;
					for (int h = 1; h < blocksToCheck; h++)
					{
						nextPosition = { i + h, static_cast<int>((noise.GetValue(static_cast<double>((i + h) / 4.0), 0.0, 0.0) * height)), 0.0 };
						if (nextPosition.y == position.y)
						{
							spikesToPlace++;
						}
						else //next block moved up or down
						{
							h = blocksToCheck;
						}
					}
					if (nextPosition.y > position.y) //next block moved up
					{
						for (int g = 0; g < spikesToPlace; g++) //fill the gap from original position to last checked position
						{
							this->actors.emplace_back(actorFactory->createActor(&ActorData("res/actors/movingSpike.xml"), { position.x + g, position.y - 0.5f, position.z }));
							for (int k = 1; k <= heightToFill + 1; k++)
							{
								this->actors.emplace_back(actorFactory->createActor(dynamic_cast<ActorData*>(&PlatformData("res/actors/tileAtlases/castleFg_rand.xml")), { position.x + g, position.y - k, position.z }, glm::vec2(0, 0)));
								collisionDataVector.push_back(CollisionData{ { static_cast<int>(position.x + g), static_cast<int>(position.y - k) }, 1.f });
							}
						}
						i += (spikesToPlace-1);
					}
				}
			}

			//if gap is checked off ingame
			if (gap)
			{
				glm::vec3 nextPosition;
				int blocksChecked = 0;
				for (int h = 1; h <= blocksToCheck; h++)
				{
					nextPosition = { i + h, static_cast<int>((noise.GetValue(static_cast<double>((i + h) / 4.0), 0.0, 0.0) * height)), 0.0 };
					if (nextPosition.y == position.y)
					{
						blocksChecked++;
					}
				}
				//if gap is wide enough
				if (blocksChecked == blocksToCheck)
				{
					i += blocksChecked-1;
					this->actors.emplace_back(actorFactory->createActor(dynamic_cast<ActorData*>(&PlatformData("res/actors/movingPlatform.xml")), { position.x + (blocksChecked-1.75f), position.y, position.z }));
					static_cast<PhysicsMovingPlatform*>(actors.back()->getComponent("PhysicsComponent"))->setBlocks(static_cast<float>(blocksChecked));

				}
			}

			//if spring is checked off ingame
			if (spring)
			{
				//10% chance to spawn a spring randomly at any point
				if (rand() % 10 == 0)
				{
					this->actors.emplace_back(actorFactory->createActor(dynamic_cast<ActorData*>(&PlatformData("res/actors/spring.xml")), { position.x, position.y - 0.9f, position.z }, glm::vec2(0, 0)));
				}	
			}
	}

	//spawn the player
	actorFactory->createActor(dynamic_cast<ActorData*>(&CustomActorData("res/actors/clone.xml")), glm::vec3(0.0f, static_cast<int>((noise.GetValue(0.0, 0.0, 0.0) * height)) + 1.0f, 0.0f));
	//spawn the goal
	actorFactory->createActor(&ActorData("res/actors/goal.xml"), glm::vec3(length - 1.f, static_cast<int>((noise.GetValue(static_cast<double>((length - 1) / 4.0), 0.0, 0.0) * height)) + 1.0f, 0.0f));
	physicsHandler->applyTerrainCollision(collisionDataVector);
	collisionDataVector.clear();
}

void WorldGenerator::newLevel()
{
	generateNewLevel = true;	//new level at next frame to avoid crash
}

void WorldGenerator::setBool(bool state, std::string name)
{
	if (name == "gap")
	{
		this->gap = state;
	}

	if (name == "spike")
	{
		this->spike = state;
	}
	
	if (name == "timedBlock")
	{
		this->timedBlock = state;
	}

	if (name == "spring")
	{
		this->spring = state;
	}

	if (name == "everything")
	{
		this->everything = state;
	}

	//everything must be last
	if (this->everything)
	{
		this->gap = true;
		this->spike = true;
		this->timedBlock = true;
		this->spring = true;
	}
}

void WorldGenerator::update()
{
	if (generateNewLevel)
	{
		actorFactory->clearFactory();
		physicsHandler->clear();
		//grid->fillMap(); //to check path with A*
		generate();

		generateNewLevel = false;
	}
}
