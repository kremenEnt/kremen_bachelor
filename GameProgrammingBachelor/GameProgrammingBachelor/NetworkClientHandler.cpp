#include "NetworkClientHandler.h"
#include "Engine.h"
#include "InputComponent.h"
#include "ActorData.h"
#include <iostream>
#include <string>
#include "ActorFactory.h"
#include "Actor.h"
NetworkClientHandler::NetworkClientHandler()
	:initialized(false)
{
}

NetworkClientHandler::~NetworkClientHandler()
{
	RakNet::RakPeerInterface::DestroyInstance(peerInterface);
}

void NetworkClientHandler::init(std::string hostIP)
{
	peerInterface = RakNet::RakPeerInterface::GetInstance();
	peerInterface->Startup(1, &socket, 1);
	peerInterface->Connect(hostIP.c_str(), PORT, 0, 0);
	initialized = true;
}

void NetworkClientHandler::sendPacket(int gameState)
{
	if (networkDebug)
	{
		printf("starting to send packet from client\n");
	}
	
	switch (gameState)
	{
	case STATE_JOINING:
		//don't need to do anything here atm. maybe later
		//sendJoining();
		break;
	case STATE_JOINED:
		sendJoined();
		break;
	case STATE_RUNNING:
		sendRunning();
		break;
	default:
		printf("invalid gamestate for network play");
		break;
	}
}


void NetworkClientHandler::receivePacket(int gameState)
{
	if (!initialized)
	{
		return;
	}

	for (packet = peerInterface->Receive(); packet; peerInterface->DeallocatePacket(packet), packet = peerInterface->Receive())
	{
		switch (packet->data[0])
		{
		case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			printf("Another client has disconnected.\n");
			break;
		case ID_REMOTE_CONNECTION_LOST:
			printf("Another client has lost the connection.\n");
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION:
			printf("Another client has connected.\n");
			break;
		case ID_CONNECTION_REQUEST_ACCEPTED:
		{
			printf("Our connection request has been accepted.\n");

			RakNet::BitStream bsOut;
			//this tells the host that we are still in the joining gamestate and need approval (improve later)
			bsOut.Write((RakNet::MessageID)(gameState+ID_USER_PACKET_ENUM));
			peerInterface->Send(&bsOut, HIGH_PRIORITY, RELIABLE, 0, packet->systemAddress, false);
			break;
		}
		case ID_NEW_INCOMING_CONNECTION:
			printf("A connection is incoming.\n");
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			printf("The server is full.\n");
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			printf("We have been disconnected.\n");
			break;
		case ID_CONNECTION_LOST:
			printf("Connection lost.\n");
			break;
		case ID_USER_PACKET_ENUM+STATE_JOINING:
			receiveJoining();
			break;
		case ID_USER_PACKET_ENUM + STATE_JOINED:
			receiveJoined();
			break;
		case ID_USER_PACKET_ENUM + STATE_RUNNING:
			receiveRunning();
			break;
		case ID_USER_PACKET_ENUM + STATE_GAMEOVER:
			receiveGameOver();
			break;
		case CREATE_ACTOR:
		{
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			RakNet::RakString xmlPath;
			glm::vec3 position;

			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(xmlPath);
			bsIn.ReadVector(position.x, position.y, position.z);
			parentEngine->getActorFactory()->createActor(&ActorData(xmlPath.C_String()), position);
			break;
		}
		case START_GAME:
		{
			
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			bsIn.Read(playerActorID);
			parentEngine->setGameState(STATE_RUNNING);
			break;
		}
		default:
			printf("Message with identifier %i has arrived.\n", packet->data[0]);
			break;
		}
	}		
}

actorID NetworkClientHandler::getPlayerActorID()
{
	return playerActorID;
}

void NetworkClientHandler::sendJoined()
{
	RakNet::BitStream bsOut;
	RakNet::RakString rs;
	rs = "happetipappeti";
	bsOut.Write(rs);
	peerInterface->Send(&bsOut, MEDIUM_PRIORITY, RELIABLE, 0, hostSystemAddress, false);
}

void NetworkClientHandler::receiveJoining()
{
	int seed;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(playerActorID);
	bsIn.Read(seed);
	hostSystemAddress = packet->systemAddress;
	parentEngine->setSeed(seed);
	parentEngine->setGameState(STATE_JOINED);
}

void NetworkClientHandler::receiveJoined()
{

}

void NetworkClientHandler::receiveRunning()
{
	NetworkActorData dataIn;
	int numberOfActors;
	RakNet::BitStream bsIn(packet->data, packet->length, false);
	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
	bsIn.Read(numberOfActors);
	
	for (auto it = 0; it < numberOfActors; it++)
	{
		bsIn.Read(dataIn);
		Actor* actorToSync = parentEngine->getActorFactory()->getActor(dataIn.actorIdNumber);
		glm::vec3 interPosition = (dataIn.actorPosition - actorToSync->getPosition())*0.5f;
		actorToSync->movePosition(interPosition);
		dynamic_cast<PhysicsComponent*>(actorToSync->getComponent("PhysicsComponent"))->setVelocity(dataIn.actorVelocity);
	}
}

void NetworkClientHandler::receiveGameOver()
{
	//TODO: add stuff
}

void NetworkClientHandler::sendRunning()
{
	RakNet::BitStream bsOut;
	
	bsOut.Write((RakNet::MessageID)(ID_USER_PACKET_ENUM + STATE_RUNNING));
	bsOut.Write(inputComponent->getInputValues());

	peerInterface->Send(&bsOut, MEDIUM_PRIORITY, RELIABLE, 0, hostSystemAddress, false);
	if (networkDebug)
	{
		printf("succeeded sending input from client\n");
	}
}