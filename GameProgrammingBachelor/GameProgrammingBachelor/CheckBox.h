#pragma once
#include "GuiElement.h"

class Engine;
/** A check box. Can be used to create check boxes that is in a true or false state */
class CheckBox : public GuiElement
{
public:
	CheckBox(GuiData::ElementData* elementData);
	virtual ~CheckBox();

	/** Updates this object. */
	void update() override;

	/**
	 * Determines if mouse click left is inside the checkbox.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickLeft() override;

	/**
	 * Determines if mouse click right is inside the checkbox.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickRight() override;

	/**
	 * Determines if mouse release is inside button.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseRelease() override;

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 &viewProjection) override;

	/**
	 * Gets the name.
	 *
	 * @return	The name.
	 */
	std::string getName();

	/**
	* Sets the  texture of the check box to a new one in runtime.
	*
	* @param	texture		the texture path that is used when changing.
	*/
	void changeTexture(std::string newTexture);

	/**
	* Sets the check box to visible or not, also deactivates interactions.
	*
	* @param	bool		true or false for visible or not.
	*/
	void setVisible(bool visible);
private:
	GuiData::CheckBox elementData;

	std::string name;

	bool clicked;
	bool status;
	bool defaultStatus;
	bool visible;

	int currentTexture;
	int textureAtlasSizeX;
	int textureAtlasSizeY;

	Transform transform;
	Font * font;
	Mesh *mesh;
	Shader *shader;
	Texture *texture;

	virtual void init() override;
};

