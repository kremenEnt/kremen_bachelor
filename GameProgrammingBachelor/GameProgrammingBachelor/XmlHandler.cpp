#include "XmlHandler.h"
#include "Engine.h"
#include <iostream>
#include <string>

XmlHandler::XmlHandler()
{
	xmlDocuments.clear();
}


XmlHandler::~XmlHandler()
{
	xmlDocuments.clear();
}

tinyxml2::XMLDocument* XmlHandler::loadXml(const std::string & filePath)
{
	std::unordered_map<std::string, std::unique_ptr<tinyxml2::XMLDocument>>::iterator it = xmlDocuments.find(filePath);
	if (it == xmlDocuments.end())
	{
		doc = std::make_unique<tinyxml2::XMLDocument>();
#ifdef __ANDROID__
		krem::android::AssetManager * mgr = parentEngine->getAssetMgr();
		std::string xmlTemp = getXmlString(filePath, mgr);
		int loadOkay = doc->Parse(xmlTemp.c_str());
#else
		int loadOkay = doc->LoadFile(filePath.c_str());
#endif
		if (!loadOkay == 0)
		{
			printf("Error: XML file %s not working.\n Google XML error %i", filePath.c_str(), loadOkay);
		}
		else
		{
			std::cout << "XmlFile: " << filePath << std::endl;		

			xmlDocuments.insert(std::make_pair(filePath.c_str(), std::move(doc)));
			return xmlDocuments[filePath].get();
		}	
	}
	else
	{
		return xmlDocuments[filePath].get();
	}
	return nullptr;
	
}

#ifdef __ANDROID__
#include "android\AssetFile.h"
#include "android\AssetManager.h"
const std::string XmlHandler::getXmlString(std::string path, krem::android::AssetManager * assetmgr)
{
	krem::android::AssetFile AF = assetmgr->open(path);


	std::vector<char> tempBuffer;

	tempBuffer = AF.readAll();

	std::string returnString = "";
	for (auto it : tempBuffer)
	{
		returnString += it;
	}
	return returnString;
}
#endif