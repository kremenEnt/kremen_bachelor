#pragma once
#include <string>
#include "common.h"
#include "Handler.h"

#ifdef __ANDROID__
#include <GLES3/gl31.h>
#include <glu.h>
#include <GLES/glext.h>
#include "android\AssetFile.h"
#include "android\AssetManager.h"
#include <android\log.h>
#else 
#include <GL\glew.h>
#endif

class Engine;
class Shader;

/**
 * @class	SkyboxHandler
 *
 * @brief	A skybox handler.
 */
class SkyboxHandler : public Handler
{
public:

	/**
	 * @fn	SkyboxHandler::SkyboxHandler();
	 *
	 * @brief	Default constructor.
	 */
	SkyboxHandler();

	/**
	 * @fn	SkyboxHandler::~SkyboxHandler();
	 *
	 * @brief	Destructor.
	 */
	~SkyboxHandler();

	/**
	 * @fn	void SkyboxHandler::init();
	 *
	 * @brief	Initialises this object.
	 */
	void init();
#ifdef __ANDROID__
	const std::vector<char> getSTBString(std::string path, krem::android::AssetManager * assetmgr);
#endif

	/**
	 * @fn	unsigned char* SkyboxHandler::loadSTBTexture(const std::string filename, int &width, int &height, int &numComponents);
	 *
	 * @brief	Loads stb texture.
	 *
	 * @param	filename			 	Filename of the file.
	 * @param [in,out]	width		 	The width.
	 * @param [in,out]	height		 	The height.
	 * @param [in,out]	numComponents	Number of components.
	 *
	 * @return	texture data.
	 */

	unsigned char* loadSTBTexture(const std::string filename, int &width, int &height, int &numComponents);

	/**
	 * @fn	void SkyboxHandler::loadTexture(const std::string & filename, int type);
	 *
	 * @brief	Loads a texture.
	 *
	 * @param	filename	Filename of the file.
	 * @param	type		The type(day/night).
	 */
	void loadTexture(const std::string & filename, int type);

	/**
	 * @fn	void SkyboxHandler::update(glm::mat4& viewProjection, float deltaTime);
	 *
	 * @brief	Updates this object.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 * @param	deltaTime			  	The delta time.
	 */
	void update(glm::mat4& viewProjection, float deltaTime);

	/**
	 * @fn	void SkyboxHandler::draw(glm::mat4 & viewProjection);
	 *
	 * @brief	Draws the given view projection.
	 *
	 * @param [in]	viewProjection	The view projection.
	 */
	void draw(glm::mat4 & viewProjection);

	/**
	 * @fn	float SkyboxHandler::getRotation();
	 *
	 * @brief	Gets the rotation.
	 *
	 * @return	The rotation.
	 */
	float getRotation();
	
private:

	/**
	 * @fn	void SkyboxHandler::loadCube();
	 *
	 * @brief	Loads the cube.
	 */
	void loadCube();

	/**
	 * @fn	void SkyboxHandler::bind(unsigned int unit);
	 *
	 * @brief	Binds the given unit.
	 *
	 * @param	unit	Texture number unit.
	 */
	void bind(unsigned int unit);
	float rotation;

	Shader *shader;
	GLuint texture[NO_OF_SKYBOX_TEXTURES];
	GLuint vertexArrayBuffer[1];
	GLuint vertexArrayObject;
	std::vector<glm::vec3> positions;

	//cube faces
	enum
	{
		RIGHT,
		LEFT,
		TOP,
		BOTTOM,
		BACK,
		FRONT,
		SIDES
	};
};

