#include "TextBox.h"
#include "Engine.h"
#include "GuiElement.h"
#include "EventHandler.h"
#include "FontHandler.h"
#include "MeshHandler.h"
#include "ShaderHandler.h"
#include "TextureHandler.h"

TextBox::TextBox(GuiData::ElementData* elementData) : GuiElement(elementData)
{
	this->elementData = *dynamic_cast<GuiData::TextBox*>(elementData);
	currentTexture = 0;
	focus = false;
	input = " ";
	int maxLength = 100;
}

TextBox::~TextBox()
{
}

void TextBox::update()
{
	if (focus && visible)
	{
		std::string tempUserInput = parentEngine->getEventHandler()->getInputText();
		input = tempUserInput;
		
		//clears the textbox when gaining focus (Not ideal but works for now)
		if (input != "")
		{
			const char * tempInput = input.c_str();
			const char * tempName = name.c_str();

			//sends the name of the text box and its current entered text.
			this->postMessage(Message(this, tempName, tempInput));
		}
		else
		{
			input = defaultValue;
		}
	}
	else
	{
		
	}
	
}

bool TextBox::checkMouseClickLeft()
{
	if (visible)
	{
		if (parentEngine->getEventHandler()->mouseInside(transform))
		{
			focus = true;
			parentEngine->getEventHandler()->setInputStatus(true);
			return focus;
		}
		else
		{
			lostFocus();
		}
	}
	return focus;
}

bool TextBox::lostFocus()
{
	focus = false;
	//parentEngine->getEventHandler()->setInputStatus(false);
	return false;
}

void TextBox::init()
{
	float tempPosX;
	float tempPosY;
	float tempWidth;
	float tempHeight;

	name = elementData.name;
	tempPosX = elementData.positionX;
	tempPosY = elementData.positionY;
	tempWidth = elementData.width;
	tempHeight = elementData.height;
	tempHeight = (tempHeight / aspectRatio.y)*aspectRatio.x;
	visible = elementData.visible;
	font = parentEngine->getFontHandler()->loadFont(elementData.fontPath);
	mesh = parentEngine->getMeshHandler()->loadModel(elementData.meshPath);
	texture = parentEngine->getTextureHandler()->loadTexture(elementData.texturePath);
	shader = parentEngine->getShaderHandler()->loadShader(elementData.shaderPath);
	
	maxLength = elementData.maxLength;
	defaultValue = elementData.defaultValue;

	input = defaultValue;

	transform.setPos(glm::vec3(tempPosX, tempPosY, 0.999f));
	transform.setScale(glm::vec3(tempWidth, tempHeight, 1.f));
}

void TextBox::draw(glm::mat4 &viewProjection)
{
	if (visible)
	{
		shader->bind();
		shader->loadTransform(transform, viewProjection);
		shader->loadInt(U_SPRITE_NO, currentTexture); //finds what texture to draw
		shader->loadFloat(U_SCALE, 1.5f);

		if (input == defaultValue)
		{
			font->update(input, glm::vec3(50, 50, 50));
		}
		else
		{
			font->update(input, glm::vec3(0, 0, 0));
		}

		shader->loadInt(U_TEXTURE1, 1);
		font->bind(1);

		texture->bind(0);
		mesh->draw();
	}
}

void TextBox::setText(std::string newText)
{
	//sends the name of the text box and its current entered text.
	this->postMessage(Message(this, name.c_str(), newText.c_str()));
}

std::string TextBox::getName()
{
	return name;
}

void TextBox::changeTexture(std::string newTexture)
{
	texture = parentEngine->getTextureHandler()->loadTexture(newTexture);
}

void TextBox::setVisible(bool visible)
{
	this->visible = visible;
}
