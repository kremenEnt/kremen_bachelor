#include "AudioHandler.h"

AudioHandler::AudioHandler()
{
	masterVolume = 100;
	Mix_AllocateChannels(32);
	Mix_Volume(-1, masterVolume);
	Mix_VolumeMusic(masterVolume);
}

AudioHandler::~AudioHandler()
{
	soundEffects.clear();
	music.clear();
}

void AudioHandler::loadMusic(std::string musicPath)
{
	auto it = soundEffects.find(musicPath);
	if (it == soundEffects.end())		//check if music already exists in map
	{
		//const char* filePath = musicPath.c_str();
		Mix_Music *tempMusic;
		tempMusic = Mix_LoadMUS(musicPath.c_str());

		this->music[musicPath] = tempMusic; //saves new music if the old one isn't already in the map
	}
}

void AudioHandler::playSound(std::string soundPath)
{
	auto it = soundEffects.find(soundPath);
	if (it != soundEffects.end())		//check if sound already exists in map. adds it if it doesn't exist
	{
		Mix_PlayChannel(-1, it->second, 0);
	}
	else
	{
		//const char* filePath = soundPath.c_str();
		Mix_Chunk *tempSound;
		tempSound = Mix_LoadWAV(soundPath.c_str());
		this->soundEffects[soundPath] = tempSound; //saves new sound if the old one isn't already in the map
		Mix_PlayChannel(-1, soundEffects[soundPath], 0);
		std::cout << "sound doesn't exist. added sound to map\n";
	}
}

void AudioHandler::playMusic(std::string musicPath)
{
	auto it = music.find(musicPath);
	if (it != music.end())		//check if music already exists in map
	{
		if (Mix_PlayingMusic() == 0)
		{
			//Play the music
			Mix_PlayMusic(it->second, -1);
			currentMusic = musicPath;
		}
		else if (currentMusic != musicPath)
		{	
			//Mix_FadeOutMusic(2000);
			//Mix_HaltMusic();
			Mix_FadeInMusic(it->second, -1, 3000);
			//Mix_PlayMusic(it->second, -1);
			currentMusic = musicPath;
		}
	}
	else
	{
		std::cout << "music doesn't exist\n";
	}
}

void AudioHandler::stopMusic()
{
	Mix_HaltMusic();
}

void AudioHandler::setMasterVolume(int volume)
{
	masterVolume = volume;
}

void AudioHandler::setSoundVolume(int volume)
{
	if (volume > masterVolume)
	{
		Mix_Volume(-1, masterVolume);
	}
	else
	{
		Mix_Volume(-1, volume);
	}
	
}

void AudioHandler::setMusicVolume(int volume)
{
	if (volume > masterVolume)
	{
		Mix_VolumeMusic(masterVolume);
	}
	else
	{
		Mix_VolumeMusic(volume);
	}

}

