#pragma once
#include "ParticleSystem.h"

/**
 * @class	ExplosionParticles
 *
 * @brief	An explosion particles.
 */
class ExplosionParticles : public ParticleSystem
{
public:

	/**
	 * @fn	ExplosionParticles::ExplosionParticles()
	 *
	 * @brief	Default constructor.
	 */
	ExplosionParticles()
		: ParticleSystem() {};

	/**
	 * @fn	void ExplosionParticles::update(float deltaTime) override;
	 *
	 * @brief	Updates with the given deltaTime.
	 *
	 * @param	deltaTime	The delta time.
	 */
	void update(float deltaTime) override;
};