#pragma once
class Actor;
class ActorFactory;
class PhysicsHandler;
#include "common.h"
#include <map>

struct LevelData;
struct MapIdentifier;
struct SlotValues;
class LevelGenerator
{
private:
	std::pair<int, int> selectPlatformStart(const std::pair<int, int> endOfLastPlatform);

public:

	LevelGenerator();
	/**
	* @fn	void ContactListener::BeginContact(b2Contact* contact);
	*
	* @brief	Begins a contact. Do not rename. Is inherited from b2ContactListener
	*
	* @param [in,out]	contact	If non-ANNLevelGeneratornull, the contact.
	*/
	void setActorFactory(ActorFactory* actorFactory);
	/**
	* @fn	void ContactListener::BeginContact(b2Contact* contact);
	*
	* @brief	Begins a contact. Do not rename. Is inherited from b2ContactListener
	*
	* @param [in,out]	contact	If non-null, the contact.
	*/
	void setPhysicsHandler(PhysicsHandler* physicsHandler);

	/**
	* @fn	void ContactListener::BeginContact(b2Contact* contact);
	*
	* @brief	Begins a contact. Do not rename. Is inherited from b2ContactListener
	*
	* @param [in,out]	contact	If non-null, the contact.
	*/
	std::map<MapIdentifier,SlotValues> loadLevel(LevelData levelData);
	
private:
	std::vector<CollisionData> collisionDataVector;
	ActorFactory* actorFactory;
	PhysicsHandler* physicsHandler;
};
