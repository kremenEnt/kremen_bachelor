#include "PhysicsComponent.h"
#include "Actor.h"
#include "Engine.h"
#include "PhysicsHandler.h"
#include "GraphicsComponent.h"
#include "Box2D\Box2D.h"
#include <glm\gtc\constants.hpp>

PhysicsComponent::PhysicsComponent(ActorData::ComponentData* newComponentData)
	:IPhysicsComponent(newComponentData)
{
	componentData = dynamic_cast<ActorData::Physics*>(newComponentData);
	sensorContacts.jump = 0;
	sensorContacts.wallLeft = 0;
	sensorContacts.wallRight = 0;
	//forceScale is the scaling for the force exerted. Currently at 60hz because that feels best for now
	forceScale = 1.f / 60.f;
}

PhysicsComponent::~PhysicsComponent()
{
	this->body->GetWorld()->DestroyBody(this->body);
}

void PhysicsComponent::init()
{
	this->body = this->parentEngine->getPhysicsHandler()->createBox(glm::vec2(actor->getPosition().x, actor->getPosition().y), componentData->definition);
	this->body->SetUserData(this);
	if (actor->getComponent("GraphicsComponent") != nullptr)
	{
		graphicsComponent = dynamic_cast<GraphicsComponent*>(actor->getComponent("GraphicsComponent"));
	}
}

void PhysicsComponent::update(float deltaTime)
{
	angle = body->GetAngle();
	bool changed = false;
	if (angle > glm::two_pi<float>() || angle < -glm::two_pi<float>())
	{
		body->SetTransform(body->GetPosition(), 0.f);	//sets body angle to 0 if above or under pi*2 / -pi*2 since transformation sets rotation to 0 if above or below.
		angle = 0.f;									//sets angle to 0
	}
	actor->setPosition({ body->GetPosition().x, body->GetPosition().y, actor->getPosition().z });

	graphicsComponent->setRotation(angle, 'z');
}

void PhysicsComponent::setPosition(glm::vec3 newPosition)
{
	body->SetTransform(b2Vec2(newPosition.x, newPosition.y), this->newAngle);
}

glm::vec3 PhysicsComponent::getPosition()
{
	return glm::vec3(body->GetPosition().x, body->GetPosition().y, 0.f);
}

void PhysicsComponent::setRotation(float angle)
{
	body->SetTransform(body->GetPosition(), angle);
}

void PhysicsComponent::setVelocity(glm::vec3 velocity)
{
	b2Vec2 temp(velocity.x, velocity.y);
	body->SetLinearVelocity(temp);
}

float PhysicsComponent::getRotation()
{
	return body->GetAngle();
}


glm::vec2 PhysicsComponent::getVelocity()
{
	b2Vec2 vel = body->GetLinearVelocity();
	return{ vel.x, vel.y };
}

void PhysicsComponent::applySpeed(glm::vec2 desiredSpeed)
{
	b2Vec2 currentVelocity = body->GetLinearVelocity();
	float desiredXVelocity = 0.f;
	float desiredYVelocity = 0.f;
	float velocityXChange = -currentVelocity.x;
	float velocityYChange = -currentVelocity.y;
	float impulse;

	if (desiredSpeed.y != 0)
	{
		desiredYVelocity = desiredSpeed.y;
		velocityYChange += desiredYVelocity;
		impulse = body->GetMass() * velocityYChange;
		body->ApplyLinearImpulse(b2Vec2(0.f, impulse), body->GetWorldCenter(), true);
	}


	if (desiredSpeed.x < 0.f)
	{
		if (sensorContacts.wallLeft == 0)
		{
			desiredXVelocity = desiredSpeed.x;
			velocityXChange += desiredXVelocity;
			impulse = body->GetMass() * velocityXChange;
			body->ApplyLinearImpulse(b2Vec2(impulse, 0.f), body->GetWorldCenter(), true);
		}
		if (sensorContacts.wallLeft > 0)
		{
			body->SetLinearVelocity((b2Vec2(0.f, currentVelocity.y)));
		}
	}
	if (desiredSpeed.x > 0.f)
	{
		if (sensorContacts.wallRight == 0)
		{
			desiredXVelocity = desiredSpeed.x;
			velocityXChange += desiredXVelocity;
			impulse = body->GetMass() * velocityXChange;
			body->ApplyLinearImpulse(b2Vec2(impulse, 0.f), body->GetWorldCenter(), true);
		}
		else
		{
			body->SetLinearVelocity((b2Vec2(0.f, currentVelocity.y)));
		}
	}
	if (desiredSpeed.x == 0.f)
	{
		if (sensorContacts.jump > 0) //if on ground
		{
			desiredXVelocity = currentVelocity.x*0.5f;
		}
		if (sensorContacts.jump == 0) //if airborne
		{
			desiredXVelocity = currentVelocity.x*0.5f;
		}
		velocityXChange += desiredXVelocity;
		impulse = body->GetMass() * velocityXChange;
		body->ApplyLinearImpulse(b2Vec2(impulse, 0.f), body->GetWorldCenter(), true);
	}
	graphicsComponent->setPosition({ body->GetPosition().x, body->GetPosition().y ,actor->getPosition().z});
}

void PhysicsComponent::applyLinearImpulse(glm::vec2 impulse)
{
	if ((body->GetLinearVelocity().y <= 0) && (sensorContacts.jump > 0))
	{
		this->body->ApplyLinearImpulse(b2Vec2(0, impulse.y), body->GetWorldCenter(), true);
	}
}

std::string PhysicsComponent::getActorName()
{
	return actor->getActorId().actorName;
}

bool PhysicsComponent::wallCollision()
{
	if (body->GetLinearVelocity().x < 0 && sensorContacts.wallLeft > 0)
	{
		return true;
	}
	if (body->GetLinearVelocity().x > 0 && sensorContacts.wallRight > 0)
	{
		return true;
	}
	return false;
}

Actor * PhysicsComponent::getactor()
{
	return actor;
}

b2Body * PhysicsComponent::getB2Body()
{
	return body;
}

void PhysicsComponent::setB2Body(b2Body* body)
{
	this->body = body;
	body->SetUserData(this);
}

PhysicsDefinition PhysicsComponent::getPhysicsDefinition()
{
	return physicsDefinition;
}

SensorContacts PhysicsComponent::getSensorContacts()
{
	return sensorContacts;
}

b2Fixture* PhysicsComponent::applyFixture(krem::Vector2f size, krem::Vector2f relativePosition, void * fixturename, float angle, bool sensor)
{
	b2PolygonShape newFixtureShape;
	newFixtureShape.SetAsBox(size.x, size.y, b2Vec2({ relativePosition.x,relativePosition.y }), angle);
	
	b2FixtureDef newFixtureDefinition;
	newFixtureDefinition.shape = &newFixtureShape;
	newFixtureDefinition.isSensor = sensor;
	
	b2Fixture* newFixture = body->CreateFixture(&newFixtureDefinition);
	newFixture->SetUserData((void*)fixturename);
	return newFixture;
}

void PhysicsComponent::startContact(void* sensorType)
{
	if (sensorType == "jump")
	{
		sensorContacts.jump++;
	}
	if (sensorType == "wallLeft")
	{
		sensorContacts.wallLeft++;
	}
	if (sensorType == "wallRight")
	{
		sensorContacts.wallRight++;
	}
}

void PhysicsComponent::endContact(void* sensorType)
{
	if (sensorType == "jump")
	{
		sensorContacts.jump--;
	}
	if (sensorType == "wallLeft")
	{
		sensorContacts.wallLeft--;
	}
	if (sensorType == "wallRight")
	{
		sensorContacts.wallRight--;
	}
}
