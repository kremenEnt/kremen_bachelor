#pragma once
#include "IActorState.h"

class Actor;
class PhysicsComponent;
class AnimationComponent;

class ASDefault
{
public:
	ASDefault() {};
	ASDefault(Actor* actor);
protected:
	Actor* actor;
	PhysicsComponent* physicsComponent;
	AnimationComponent* animationComponent;
};