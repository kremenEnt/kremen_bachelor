#include "DropDown.h"
#include "Engine.h"
#include "GuiElement.h"
#include "FontHandler.h"
#include "EventHandler.h"
#include "MeshHandler.h"
#include "TextureHandler.h"
#include "ShaderHandler.h"
#include "GuiFactory.h"

DropDown::DropDown(GuiData::ElementData* elementData) : GuiElement(elementData)
{
	this->elementData = *dynamic_cast<GuiData::DropDown*>(elementData);
	open = false;
	closing = false;
	fontDisplay = false;
}

DropDown::~DropDown()
{
}


void DropDown::update()
{
	const char * tempName = name.c_str();
	const char * tempChoice = currentChoice.c_str();

	//sends a message with the current selected option
	this->postMessage(Message(this, tempName, tempChoice));
}

bool DropDown::checkMouseClickLeft()
{
	if (visible)
	{
		if (open)
		{
			for (int i = 0; i < numberOfChoices; i++)
			{
				//checks is one of the options in the drop down is clicked. sets the current value to the new value if it is
				if (parentEngine->getEventHandler()->mouseInside(choicesTransform[i]))
				{
					currentChoice = choices[i];
					open = false;
					closing = true;
					return true;
				}
			}
			//if the mouse is outside the drop down the menu is closed and no new value is set
			if (parentEngine->getEventHandler()->mouseInside(transform))
			{
				{
					open = false;
					return true;
				}
			}
		}
		//if the mouse click is inside the drop down. Open up the options
		if (parentEngine->getEventHandler()->mouseInside(transform) && !parentEngine->getGuiFactory()->getElementOpen())
		{
			{
				open = true;
				return true;
			}
		}
	}
	open = false;
	return false;
}

bool DropDown::checkMouseClickRight()
{
	return false;
}

bool DropDown::checkMouseRelease()
{
	if (visible)
	{
		if (closing)
		{
			for (int i = 0; i < numberOfChoices; i++)
			{
				if (parentEngine->getEventHandler()->mouseInside(choicesTransform[i]))
				{
					closing = false;
					return true;
				}
			}
		}
	}
	
	return false;
}

void DropDown::draw(glm::mat4 &viewProjection)
{
	if (visible)
	{
		shader->bind();
		shader->loadTransform(transform, viewProjection);
		shader->loadInt(U_TEXTURE0, 0);
		shader->loadFloat(U_SCALE, 1.5f);
		if (fontDisplay)
		{
			font->update(currentChoice, glm::vec3(0, 0, 0));
			shader->loadInt(U_TEXTURE1, 1);
			font->bind(1);
		}

		defaultTexture->bind(0);
		mesh->draw();

		if (open)
		{
			for (int i = 0; i < numberOfChoices; i++)
			{
				shader->loadTransform(choicesTransform[i], viewProjection);
				shader->loadInt(U_TEXTURE0, 0);
				shader->loadFloat(U_SCALE, 1.5f);
				if (fontDisplay)
				{
					font->update(choices[i], glm::vec3(0, 0, 0));
					shader->loadInt(U_TEXTURE1, 1);
					font->bind(1);
				}
				choiceTexture->bind(0);

				choicesMesh[i]->draw();
			}
		}
	}

}

void DropDown::init()
{
	float tempPosX;
	float tempPosY;
	float tempWidth;
	float tempHeight;

	std::string pathTemp;

	name = elementData.name;
	tempPosX = elementData.positionX;
	tempPosY = elementData.positionY;
	tempWidth = elementData.width;
	tempHeight = elementData.height;
	tempHeight = (tempHeight / aspectRatio.y)*aspectRatio.x;
	visible = elementData.visible;
	font = parentEngine->getFontHandler()->loadFont(elementData.fontPath);
	mesh = parentEngine->getMeshHandler()->loadModel(elementData.meshPath);
	defaultTexture = parentEngine->getTextureHandler()->loadTexture(elementData.texturePath);
	shader = parentEngine->getShaderHandler()->loadShader(elementData.shaderPath);
	
	choiceTexture = parentEngine->getTextureHandler()->loadTexture(elementData.textureChoice);

	defaultChoice = elementData.defaultChoice;
	currentChoice = defaultChoice;

	transform.setPos(glm::vec3(tempPosX, tempPosY, 0.999f));
	transform.setScale(glm::vec3(tempWidth, tempHeight, 1.f));

	if (!elementData.choices.empty())
	{ 
		std::string tempCheck;
		numberOfChoices = 0;
		fontDisplay = true;
	
		for (std::size_t i = 0; i < elementData.choices.size(); i++)
		{
			numberOfChoices++;
			Transform tempTransform;
			Mesh *tempMesh = mesh;

			choices.emplace_back(elementData.choices[i]);

			tempTransform.setPos(glm::vec3(tempPosX, tempPosY - (tempHeight*numberOfChoices), 1.0f));
			tempTransform.setScale(glm::vec3(tempWidth, tempHeight, 1.f));
			choicesTransform.emplace_back(tempTransform);

			choicesMesh.emplace_back(tempMesh);
		}
	}
}


std::string DropDown::getName()
{
	return name;
}

void DropDown::changeTexture(std::string newTexture)
{
	choiceTexture = parentEngine->getTextureHandler()->loadTexture(newTexture);
}

void DropDown::setVisible(bool visible)
{
	this->visible = visible;
}

bool DropDown::checkOpen()
{
	return open;
}
