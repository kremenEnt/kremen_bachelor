#pragma once
#include <string>
#include <map>
#include <vector>
#include <fstream>
#include <iostream>
#include "GuiElement.h"
#include "tinyxml2-master\tinyxml2.h"
#include "common.h"
#include "Handler.h"
#include "GuiData.h"
#include "Bar.h"
#include "Button.h"
#include "CheckBox.h"
#include "Container.h"
#include "DropDown.h"
#include "Panel.h"
#include "Slider.h"
#include "TextBox.h"
#include "GuiElementFactory.h"
#include "Receiver.h" //i want to remove this in the future

class GuiElement;
class Engine;
/**
 * @class	GuiFactory
 *
 * @brief	A graphical user interface handler.
 */
class GuiFactory : public Handler, public IReceiver
{
public:
	GuiFactory();
	~GuiFactory();

	/** Clears the vectors that holds the gui data. */
	void clear();

	void createState(const GuiData* stateData);
	/**
	 * Draws.
	 *
	 * @param	gamestate	The gamestate.
	 */
	void draw();

	/** Updates this object. */
	void update();

	/**
	 * Checks if any elements in the current state is clicked.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickLeft();

	/**
	 * Checks if any element in the current state is right clicked.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseClickRight();

	/**
	 * Checks if any element in the current state has the mouse released inside of them
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool checkMouseRelease();

	/**
	 * @fn	GuiElement* GuiFactory::getElement(std::string elementName);
	 *
	 * @brief	Gets an element.
	 *
	 * @param	elementName	Name of the element.
	 *
	 * @return	null if it fails, else the element.
	 */
	GuiElement* getElement(std::string elementName);

	/**
	 * @fn	void GuiFactory::receiveMessage(const Message &message);
	 *
	 * @brief	Receive message.
	 *
	 * @param	message	The message.
	 */
	void receiveMessage(const Message &message);

	/**
	 * @fn	bool GuiFactory::getElementOpen();
	 *
	 * @brief	Gets element open.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	bool getElementOpen();
private:
	std::unordered_map<std::string, std::unique_ptr<GuiElement>> currentGuiState;
	std::unique_ptr<GuiElementFactory> guiElementFactory;

	GuiElementCreatorImplementation<Bar> barCreator;
	GuiElementCreatorImplementation<Button> buttonCreator;
	GuiElementCreatorImplementation<CheckBox> checkBoxCreator;
	GuiElementCreatorImplementation<Container> containerCreator;
	GuiElementCreatorImplementation<DropDown> dropDownCreator;
	GuiElementCreatorImplementation<Panel> panelCreator;
	GuiElementCreatorImplementation<Slider> sliderCreator;
	GuiElementCreatorImplementation<TextBox> textBoxCreator;
};