#pragma once
#include "Handler.h"
class b2Body;

/**
 * @class	IPhysicsHandler
 *
 * @brief	The physics handler.
 */
class IPhysicsHandler : public Handler
{
public:
	IPhysicsHandler() {};
	virtual ~IPhysicsHandler(){};
	virtual void update(float deltaSeconds) = 0;
	//virtual std::shared_ptr<b2Body> createBox(int width) = 0;
	//can we add a modular createBox function?:s
	//virtual b2Body* createBox(sf::Vector2f position, int bodyType, float density, sf::Vector2i size) = 0;
	
private:
	
};