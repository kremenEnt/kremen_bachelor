#include "LevelGenerator.h"
#include "ActorFactory.h"
#include "PhysicsHandler.h"
#include "CustomActorData.h"
#include "AttemptX.h"
#include "Engine.h"
#include <noise.h>
#include "PlatformData.h"

LevelGenerator::LevelGenerator()
{
}

void LevelGenerator::setActorFactory(ActorFactory* actorFactory)
{
	this->actorFactory = actorFactory;
}

void LevelGenerator::setPhysicsHandler(PhysicsHandler * physicsHandler)
{
	this->physicsHandler = physicsHandler;
}

std::map<MapIdentifier,SlotValues> LevelGenerator::loadLevel(LevelData leveldata)
{	
	noise::module::Perlin noise;
	noise.SetSeed(static_cast<int>(time(NULL)));
	noise.SetOctaveCount(6);
	noise.SetFrequency(1.0); //1-16
	noise.SetPersistence(1.0 / 2.0); // 1/4, 1/2, 3/4
	double height = 8.0;
	double offset = 1.5;
	double sensitivity = 4.0;

	std::map<MapIdentifier, SlotValues> returnMap;
	int positionX = 0;
	int positionY = 0;
	int chunksize = 10;
	int currentChunk = 0;
	int levelLength = 50;
	while (positionX < levelLength)
	{
		currentChunk = positionX / chunksize;
		glm::vec2 tilePosition{ 1, 0 };
		for (auto j = 0; j < static_cast<int>(leveldata.chunks[currentChunk].scores[positionX%chunksize][positionY].horizontal); j++)
		{
			int random = 0;
			int timedBlocks = rand() % 3 + 2;
			//Enemy generation
			if (positionX > 5)
			{
				random = rand() % 25;
				switch (random)
				{
				case 0:
				case 1:
				case 2:
					actorFactory->createActor(&ActorData("res/actors/movingSpike.xml"), glm::vec3(positionX + j, positionY + 0.5f, 0.f));
					break;
				case 3:
					actorFactory->createActor(&ActorData("res/actors/totemGolem.xml"), glm::vec3(positionX + j, positionY + 1, 0.f));
					break;
				case 4:
					actorFactory->createActor(&ActorData("res/actors/spinningBall.xml"), glm::vec3(positionX + j, positionY + 2, 0.f));
					break;
				case 5:
					for (int i = 0; i < timedBlocks; i++)
					{
						actorFactory->createActor(&ActorData("res/actors/timedBlock.xml"), glm::vec3(positionX + j, positionY, 0.f));
						j++;
					}
				break;
				default:
					break;
				}
			}
			
			if (random != 5)
			{
				actorFactory->createActor(&ActorData("res/actors/tileAtlases/grassCube.xml"), glm::vec3(positionX + j, positionY, 0.f), tilePosition);
				collisionDataVector.push_back(CollisionData{ { static_cast<int>(positionX + j), static_cast<int>(positionY) }, 1.f });
			}
			if (random == 5)
			{
				positionX += timedBlocks-1;
			}
		}

		for (auto j = 0; j < static_cast<int>(leveldata.chunks[currentChunk].scores[positionX%chunksize][positionY].horizontal); j++)
		{
			glm::vec3 layer2{ positionX+j, noise.GetValue(static_cast<double>(positionX + j) / sensitivity, 0.0, 0.0) * offset, 0.0 };
			actorFactory->createActor(dynamic_cast<ActorData*>(&PlatformData("res/actors/tileAtlases/roofCube.xml")), { positionX + j, static_cast<int>((layer2.y + positionY + height)), 0.0f }, glm::vec2(0, 0));
			collisionDataVector.push_back(CollisionData{ { positionX + j, static_cast<int>((layer2.y + positionY + height)) }, 1.f });	//offset + height + height between floor and roof
		}

		int temp = positionX + static_cast<int>(leveldata.chunks[currentChunk].scores[positionX%chunksize][positionY].horizontal); //holds new xposition, where vertical generation starts
	
		for (auto j = 0; j < std::abs(static_cast<int>(leveldata.chunks[currentChunk].scores[positionX%chunksize][positionY].vertical)); j++)
		{
			actorFactory->createActor(&ActorData("res/actors/tileAtlases/grassCube.xml"), glm::vec3(temp, positionY + j, 0.f), tilePosition);
			collisionDataVector.push_back(CollisionData{ { static_cast<int>(temp), static_cast<int>(positionY + j) }, 1.f });
		}
		
		MapIdentifier position{positionX,positionY,currentChunk}; //position in world, based of chunks and tilePosition in chunk
		SlotValues value{ leveldata.chunks[currentChunk].scores[positionX%chunksize][positionY].horizontal, leveldata.chunks[currentChunk].scores[positionX%chunksize][positionY].vertical };
		returnMap[position] = value;
		int currentX = positionX%chunksize;
		positionX += static_cast<int>(leveldata.chunks[currentChunk].scores[positionX%chunksize][positionY].horizontal);
		
		positionY += static_cast<int>(leveldata.chunks[currentChunk].scores[currentX][positionY].vertical); //needs positionX before it is overwritten

		leveldata.chunks[currentChunk].scores[positionX%chunksize][positionY].horizontal;
	}
	actorFactory->createActor(&ActorData("res/actors/goal.xml"), glm::vec3(positionX - 1, positionY + 1, 0.f));
	std::pair<int, int> currentTile(0,0);
	
	actorFactory->createActor(dynamic_cast<ActorData*>(&CustomActorData("res/actors/playerCube.xml")), glm::vec3(0.f,5.f, 0.0f));
	//actorFactory->createActor(&ActorData("res/actors/sorceress.xml"), glm::vec3(14.f, 5.f, 0.f));
	//actorFactory->createActor(&ActorData("res/actors/wizard.xml"), glm::vec3(30.f, 10.f, 0.f));
	//actorFactory->createActor(dynamic_cast<ActorData*>(&CustomActorData("res/actors/wizard.xml")), glm::vec3(30.f, 10.f, 0.f));
	//actorFactory->createActor(dynamic_cast<ActorData*>(&CustomActorData("res/actors/totemGolem.xml")), glm::vec3(40.f, 10.f, 0.f));
	//actorFactory->createActor(dynamic_cast<ActorData*>(&CustomActorData("res/actors/spinningBall.xml")), glm::vec3(5.f, 5.f, 0.f));
	//actorFactory->createActor(&ActorData("res/actors/rightRamp.xml"), glm::vec3(15.f, 6.f, 0.f));
	//actorFactory->createActor(&ActorData("res/actors/leftRamp.xml"), glm::vec3(20.f, 6.f, -0.f));
	//actorFactory->createActor(&ActorData("res/actors/sorceress.xml"), glm::vec3(currentPos.first - 4.f, currentPos.second, 0.f));

	physicsHandler->applyTerrainCollision(collisionDataVector);
	collisionDataVector.clear();
	return returnMap;
}
