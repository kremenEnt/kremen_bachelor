#pragma once
#include "ActorComponent.h"


extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

class Actor;
//class ActorFactory;

/**
 * @class	AIComponent
 *
 * @brief	An ai component.
 */
class AIComponent : public ActorComponent
{
private:

	/**
	 * @fn	virtual void AIComponent::init() override;
	 *
	 * @brief	S this object.
	 */
	virtual void init() override;
public:

	/**
	 * @fn	AIComponent::AIComponent(ActorData::ComponentData* componentData);
	 *
	 * @brief	Constructor.
	 *
	 * @param [in,out]	componentData	If non-null, information describing the component.
	 */
	AIComponent(ActorData::ComponentData* componentData);

	/**
	 * @fn	virtual AIComponent::~AIComponent();
	 *
	 * @brief	Destructor.
	 */
	virtual ~AIComponent();

	/**
	 * @fn	virtual void AIComponent::update(float deltatime);
	 *
	 * @brief	Updates the given deltatime.
	 *
	 * @param	deltatime	The deltatime.
	 */
	virtual void update(float deltatime);

	/**
	 * @fn	virtual void AIComponent::onDeath()
	 *
	 * @brief	Executes the death action.
	 */
	virtual void onDeath() {};
	
protected:
	ActorData::ArtificialIntelligence componentData;
	Actor* player;
	lua_State* luaState;
	int noLuaParameters;
	int noLuaReturnValues;
};