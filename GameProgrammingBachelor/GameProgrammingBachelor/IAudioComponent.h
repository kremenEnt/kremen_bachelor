#pragma once
#include <string>
#include <unordered_map>
#include "ActorComponent.h"

/**
* @class	AudioComponent
*
* @brief	The audio component interface.
*/
class IAudioComponent : public ActorComponent
{
public:
	virtual ~IAudioComponent(){};
	IAudioComponent(ActorData::ComponentData* componentData) : ActorComponent(componentData) {};
protected:
	std::unordered_map<std::string, std::string> soundMap;
	std::unordered_map<std::string, std::string> musicMap;
};