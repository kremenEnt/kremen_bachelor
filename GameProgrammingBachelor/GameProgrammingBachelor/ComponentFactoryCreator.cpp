#include "ComponentFactoryCreator.h"
#include "ComponentFactoryFactory.h"

ComponentFactoryCreator::ComponentFactoryCreator(const std::string & typeCategory, ComponentFactoryFactory * componentFactory)
{	
	componentFactory->registerIt(typeCategory, this);
}

void ComponentFactoryCreator::registerIt(const std::string & typeCategory, ComponentCreator * componentCreator)
{
	table[typeCategory] = componentCreator;
}