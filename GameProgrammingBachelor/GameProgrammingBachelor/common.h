#pragma once
#include <map>
#include <memory>
#include <string>
#include <glm\glm.hpp>
#include <SDL.h>
#include <string>
#include <sstream>
#include <vector>

class Actor;

static bool debug = false; //general debug
static bool xmlDebug = false; //change to true for more printfs
static bool luaDebug = false; 
static bool physicsDebug = false;
static bool networkDebug = false;
static bool networkDebugTwo = false;
static bool doUserFeedback = true;

namespace krem
{
	const float PI = 3.141592f;
	//const float degToRad()
	//{
		//return (PI / 180.f);
	//};

	/**
	 * @struct	Vector2i
	 *
	 * @brief	A vector 2i.
	 */
	struct Vector2i
	{
		int x;
		int y;
		inline bool operator==(const Vector2i& other)
		{
			return (x == other.x && y == other.y);
		}
		double getDistanceTo(const Vector2i& other)
		{
			double dx = this->x - other.x;
			double dy = this->y - other.y;
			return std::sqrt(dx * dx + dy * dy);
		}
	};

	/**
	 * @struct	Vector2f
	 *
	 * @brief	A vector 2f.
	 */
	struct Vector2f
	{
		float x;
		float y;
	};

	/**
	 * @struct	Vector3f
	 *
	 * @brief	A vector 3f.
	 */
	struct Vector3f
	{
		float x;
		float y;
		float z;
	};

	/**
	 * @struct	Vector3i
	 *
	 * @brief	A vector 3i.
	 */
	struct Vector3i
	{
		int x;
		int y;
		int z;
	};
	
	//linear interpolation. amount should be 0.f-1.f
	//float lerp(float a, float b, float weight)
	//{
	//	return a*(1.f - weight) + b*weight;
	//}

	//float dot2d(Vector2f a, Vector2f b)
	//{

	//}

	//float dot3d(Vector3f a, Vector3f b)
	//{
	//	a.x*b.x + a.y*b.y + a.z*b.z;
	//}
}

/**
 * @struct	CollisionData
 *
 * @brief	A collision data.
 */
struct CollisionData
{
	krem::Vector2i position;
	float originalValue;
};

/**
 * @struct	actorID
 *
 * @brief	The identifier for every actor in the actorMap.
 * @see		ActorFactory
 * @see		ActorFactory::CreateActor
 */
struct actorID
{
	std::string actorName;
	unsigned int actorId;
	/**
	 * @fn	bool operator<(const actorID&n) const
	 *
	 * @brief	Less-than comparison operator. Required because actorID is used in a map.
	 *
	 * @param	n	The actorID to process.
	 *
	 * @return	true if the first parameter is less than the second.
	 */
	bool operator<(const actorID&n) const
	{
		if (actorName == n.actorName)
		{
			return (actorId < n.actorId);
		}
		if (actorName < n.actorName)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	bool operator!=(const actorID&n) const
	{
		if (actorName == n.actorName && actorId == n.actorId )
		{
			return false;
		}
		return true;
	}
	bool operator==(const actorID&n) const
	{
		if (actorName == n.actorName && actorId == n.actorId)
		{
			return true;
		}
		return false;
	}
};

/**
 * @struct	InputValues
 *
 * @brief	All supported inputvalues
 * @details	Holds bools for all valid input. is true if the key is pressed down, false if/when it is released
 * @see		EventHandler::handleKeyboardEvents
 * @see		EventHandler::handleMouseEvents
 * @see		InputComponent
 */
struct InputValues
{
	bool w;
	bool s;
	bool a;
	bool d;
	bool i;
	bool q; //for timerewinder
	bool space;
	bool lControl;
	bool mouse;
	krem::Vector2f mousePosition;
};

/**
 * @struct	NetworkActorData
 *
 * @brief	A network actor data.
 */
struct NetworkActorData
{
	int actorIdNumber;
	//char actorIdName[50];
	//char category[50];
	glm::vec3 actorPosition;
	//int actorIdNameLength;
	glm::vec3 actorVelocity;
	//int categoryLength;
//	std::string category;
	
};


/**
 * @struct	PhysicsDefinition
 *
 * @brief	The physics definition.
 * @details	Contains all the data the physicsHandler need to create a body for a physicsComponent
 * @see		PhysicsComponent
 * @see		PhysicsHandler::createBox
 */
struct PhysicsDefinition
{
	enum shapeType {
		BOX,
		CIRCLE,
		POLYGON, //custom vectors
		NO_OF_SHAPES
	};
	int bodyType = 0;
	bool isSensor = false;
	float density = 1.f;
	glm::vec2 size{ 1.f,1.f };
	float gravity = 1.f;
	bool jumpSensor = false;
	bool wallSensor = false;
	bool fixedRotation = false;
	float restitution = 0.f;
	float friction = 0.2f; //I believe this is the default from box2d
	int shape = BOX;

	std::string category;
};

/**
 * @enum	ActorCollisionDataType
 *
 * @brief	Values that represent actor collision data types.
 */
enum ActorCollisionDataType
{
	DATA_SNOWBALL,
	DATA_RABBIT,
	//DATA_BLOB,
	DATA_MEGABLASTER,
	NO_OF_COLLISION_DATA_TYPES
};

#ifdef _WIN32

/**
 * @enum	input
 *
 * @brief	Values that represent inputs.
 */
enum input
{
	KEY_W = SDLK_w,
	KEY_A = SDLK_a
};
#endif
#ifdef _android
enum mobileInput
{
	KEY_W = OPENGLESINPUTW,
	KEY_A = OPENGLESINPUTA
};
#endif

/**
 * @enum	gameState
 *
 * @brief	Values that represent game states.
 */
enum gameState
{
	STATE_MAINMENU,
	STATE_RUNNING,
	STATE_LEVELSELECT,
	STATE_GAMEOVER,
	STATE_JOINING,
	STATE_JOINED,
	STATE_HOSTING,
	STATE_SETTINGS,
	STATE_QUITTING,
	NO_OF_GAMESTATES
};

/**
 * @enum	networkSynchronizationLevel
 *
 * @brief	Values that represent network synchronization levels.
 */
enum networkSynchronizationLevel
{
	NETLEVEL_NOTHING,
	NETLEVEL_POSITION,
	NETLEVEL_ANIMATION,
	NETLEVEL_FULL,
	NETLEVEL_COMPONENT_SPESIFIC
};

//VERY important: move enumerators in eventmessage needs to have the same order as certain commands. can rework this if we want to. can't think of a better way to implement this atm

/**
 * @enum	eventMessage
 *
 * @brief	Values that represent event messages.
 */
enum eventMessage
{
	EVENT_LEFT,
	EVENT_RIGHT,
	EVENT_UP,
	EVENT_DOWN,
	EVENT_LEFTUP,
	EVENT_LEFTDOWN,
	EVENT_RIGHTUP,
	EVENT_RIGHTDOWN,
	EVENT_JUMP
};

/**
 * @enum	commandMessage
 *
 * @brief	Values that represent command messages.
 */
enum commandMessage
{
	COMMAND_LEFT,
	COMMAND_RIGHT,
	COMMAND_UP,
	COMMAND_DOWN,
	COMMAND_LEFTUP,
	COMMAND_LEFTDOWN,
	COMMAND_RIGHTUP,
	COMMAND_RIGHTDOWN,
	COMMAND_JUMP,
	COMMAND_THROW,
	COMMAND_UPDATEPOSITION,
	COMMAND_UPDATEANGLE,
	COMMAND_MOVE
};

/**
 * @enum	vertexBuffers
 *
 * @brief	Values that represent vertex buffers.
 */
enum vertexBuffers
{
	POSITION_VB,
	TEXCOORD_VB,
	NORMAL_VB,
	INDEX_VB,
	NUM_BUFFERS
};

/**
 * @enum	uniforms
 *
 * @brief	Values that represent uniforms.
 */
enum uniforms
{
	U_TRANSFORM,
	U_VIEW,
	U_SIZE,
	U_SPRITE_NO,
	U_LIGHT_POS,
	U_TEXTURE0,
	U_TEXTURE1,
	U_TEXTURE2,
	U_SCALE,
	U_COLOR,
	NUM_UNIFORMS
};

/**
 * @enum	collisionCategories
 *
 * @brief	Values that represent collision categories.
 */
enum collisionCategories {
	CATEGORY_CLONE = 0x0001,
	CATEGORY_GOAL = 0x0002,
	CATEGORY_PICKUP = 0x0004,
	CATEGORY_SCENERY = 0x0008,
	CATEGORY_OBJECT = 0x0010,
	CATEGORY_SPECIAL = 0x0020
};

/**
 * @enum	skyboxTextures
 *
 * @brief	Values that represent skybox textures.
 */
enum skyboxTextures
{
	SKYBOX_DAY,
	SKYBOX_NIGHT,
	NO_OF_SKYBOX_TEXTURES
};

//namespace std
//{
//	template < typename T > std::string to_string(const T& n)
//	{
//		std::ostringstream ss;
//		ss << n;
//		return ss.str();
//	}
//}

/**
 * @enum	actorStates
 *
 * @brief	Values that represent actor states.
 */
enum actorStates
{
	AS_IDLE,
	AS_WALKING,
	AS_RISING,
	AS_DROPPING,
	AS_DEAD,
	NO_OF_ACTORSTATES
};

/**
 * @enum	animations
 *
 * @brief	Values that represent animations.
 */
enum animations
{
	IDLE,
	WALKING,
	DEATH,
	JUMPING,
	FALLING,
	NO_OF_ANIMATIONS
};