#pragma once
#include "Handler.h"

/**
 * @class	IGraphicsHandler
 *
 * @brief	The graphics handler.
 */
class IGraphicsHandler : public Handler
{
public:
	IGraphicsHandler() {};
	virtual ~IGraphicsHandler(){};
	virtual void swapBuffers() = 0;
	virtual void clearWindow() = 0;
	
protected:
private:
	
};