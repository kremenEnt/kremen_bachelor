#pragma once
#include "PhysicsComponent.h"
#include "GraphicsComponent.h"
#include "Actor.h"
#include "Engine.h"
#include "GraphicsHandler.h"
#include "Camera.h"
#include <Box2D\Box2D.h>
#include "CombatComponent.h"
class PhysicsClone : public PhysicsComponent
{
public:
	PhysicsClone(ActorData::ComponentData* componentData)
		: PhysicsComponent(componentData)
	{

	}
	void update(float deltaTime) override
	{
		
		if (body->GetPosition().y < -10.f)
		{
			static_cast<CombatComponent*>(actor->getComponent("CombatComponent"))->changeHealth(-1);
		}

		angle += 10.f * deltaTime;
		if (body->GetLinearVelocity().x > 0.f)
		{
			if (body->GetLinearVelocity().y < 0.5f && body->GetLinearVelocity().y > -0.5f && sensorContacts.jump != 0)
			{
				dynamic_cast<GraphicsComponent*>(actor->getComponent("GraphicsComponent"))->setRotation(cos(angle) / 2.f, 'y');
				dynamic_cast<GraphicsComponent*>(actor->getComponent("GraphicsComponent"))->setRotation(cos(angle) / 2.f, 'x');
			}
		}
		if (body->GetLinearVelocity().x < 0.f)
		{
			if (body->GetLinearVelocity().y < 0.5f && body->GetLinearVelocity().y > -0.5f && sensorContacts.jump != 0)
			{
				dynamic_cast<GraphicsComponent*>(actor->getComponent("GraphicsComponent"))->setRotation(cos(angle) / 2.f, 'y');
				dynamic_cast<GraphicsComponent*>(actor->getComponent("GraphicsComponent"))->setRotation(-cos(angle) / 2.f, 'x');
			}
		}

		if (!inited)
		{
			camera = parentEngine->getGraphicsHandler()->getCamera();
			inited = true;
		}
		camera->setPosition(glm::vec3{ body->GetPosition().x, body->GetPosition().y, 15.f });
		PhysicsComponent::update(deltaTime);
	}
	void onCollision(int fixtureUserData, Actor* collidedActor) override
	{
		switch (fixtureUserData)
		{
		case CATEGORY_OBJECT:
			sensorContacts.jump++;
			break;
		default:
			break;
		}
	};
	void endCollision(int fixtureUserData, Actor * collidedActor) override
	{
		switch (fixtureUserData)
		{
		case CATEGORY_OBJECT:
			sensorContacts.jump--;
			break;
		default:
			break;
		}
	};
private:
	bool inited;
	Camera* camera;
	float angle = 0.f;
};