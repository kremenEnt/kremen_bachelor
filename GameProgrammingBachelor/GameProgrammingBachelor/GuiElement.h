#pragma once
#include <string>
#include <memory>
#include <map>
#include <vector>
#include "Transform.h"
#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"
#include "Font.h"
#include <glm\glm.hpp>
#include "GuiData.h"
#include "Observable.h"
#include "Message.h"

class Engine;
class GuiFactory;

/** A graphical user interface state. This can be a main menu, a setting menu or the gui displayed in game */
class GuiElement : public IObservable
{
public:
	GuiElement(GuiData::ElementData* elementData);
	virtual ~GuiElement() = 0;
	
	/**
	 * Initialises this object.
	 *
	 * @param [in,out]	parentEngine	If non-null, the parent engine.
	 */
	virtual void init(Engine * parentEngine);

	/**
	 * Draws the given view projection.
	 *
	 * @param [in,out]	viewProjection	The view projection.
	 */
	virtual void draw(glm::mat4 &viewProjection) = 0;

	/** Updates this object./ */
	virtual void update() {};

	/**
	 * Checks if any elements in the current game state is left clicked.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	virtual bool checkMouseClickLeft();

	/**
	 * Checks if any elements in the current game state is right clicked.
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	virtual bool checkMouseClickRight();

	/**
	 * Determines if any elements in the current game state has mouse released inside them
	 *
	 * @return	true if it succeeds, false if it fails.
	 */
	virtual bool checkMouseRelease();

	/** Checks if the element is already open. Only used for drop down at the moment*/
	virtual bool checkOpen();
protected:
	Engine* parentEngine;
	glm::vec2 aspectRatio;
	virtual void init() = 0;
};
