#pragma once
#include "PhysicsComponent.h"
#include "Actor.h"
#include "Engine.h"
#include <math.h>
#include <glm/gtc/constants.hpp>
#include <Box2D\Box2D.h>

class PhysicsMovingPlatform : public PhysicsComponent
{
public:
	PhysicsMovingPlatform(ActorData::ComponentData* componentData)
		: PhysicsComponent(componentData)
	{
		this->direction = { 0.f, 0.f };
		this->speed = 1.f;
		this->position = glm::quarter_pi<float>() * 3.f; //start going positive
	}
	
	void update(float deltaTime) override
	{
		PhysicsComponent::update(deltaTime);
		float pos = std::cos(this->position += (this->speed * deltaTime));

		if (this->position >= glm::two_pi<float>())
		{
			this->position -= glm::two_pi<float>();
		}

		body->SetLinearVelocity({ this->direction.x * pos, this->direction.y * pos });
	}
	
	void setBlocks(float blocks)
	{
		direction.x = blocks / glm::pi<float>();
	}
private:
	float speed;
	float position;
	glm::vec2 direction;
};