#pragma once
#include <string>
#include <unordered_map>
#include <vector>
#include <glm\glm.hpp>
#include "XmlHandler.h"
#include <map>
#include "common.h"


class GuiFactory;
class ComponentFactory;

static std::unique_ptr<XmlHandler> guiXMLHandler;

/**
 * @class	GuiData
 *
 * @brief	A class used to read XML-Files for creating gui elements.
 */
class GuiData
{
private:
	friend class GuiFactory;
	friend class ComponentFactory;
public:

	/**
	 * @fn	GuiData::GuiData();
	 *
	 * @brief	Default constructor.
	 */
	GuiData();

	/**
	 * @fn	GuiData::GuiData(std::string xmlPath);
	 *
	 * @brief	Constructor.
	 *
	 * @param	xmlPath	Full pathname of the XML file.
	 */
	GuiData(std::string xmlPath);

	/**
	 * @fn	static void GuiData::init();
	 *
	 * @brief	init the gui from xml.
	 */
	static void init();

	/**
	 * @fn	void GuiData::createBar(tinyxml2::XMLElement *originNode);
	 *
	 * @brief	Creates a bar.
	 *
	 * @param [in,out]	originNode	takes in the current node in the xml file being read.
	 */
	void createBar(tinyxml2::XMLElement *originNode);

	/**
	 * @fn	void GuiData::createButton(tinyxml2::XMLElement *originNode);
	 *
	 * @brief	Creates a button.
	 *
	 * @param [in,out]	originNode	takes in the current node in the xml file being read.
	 */
	void createButton(tinyxml2::XMLElement *originNode);

	/**
	 * @fn	void GuiData::createCheckBox(tinyxml2::XMLElement *originNode);
	 *
	 * @brief	Creates check box.
	 *
	 * @param [in,out]	originNode	takes in the current node in the xml file being read.
	 */
	void createCheckBox(tinyxml2::XMLElement *originNode);

	/**
	 * @fn	void GuiData::createContainer(tinyxml2::XMLElement *originNode);
	 *
	 * @brief	Creates a container.
	 *
	 * @param [in,out]	originNode	takes in the current node in the xml file being read.
	 */
	void createContainer(tinyxml2::XMLElement *originNode);

	/**
	 * @fn	void GuiData::createDropDown(tinyxml2::XMLElement *originNode);
	 *
	 * @brief	Creates drop down.
	 *
	 * @param [in,out]	originNode	takes in the current node in the xml file being read.
	 */
	void createDropDown(tinyxml2::XMLElement *originNode);

	/**
	 * @fn	void GuiData::createPanel(tinyxml2::XMLElement *originNode);
	 *
	 * @brief	Creates a panel.
	 *
	 * @param [in,out]	originNode	takes in the current node in the xml file being read.
	 */
	void createPanel(tinyxml2::XMLElement *originNode);

	/**
	 * @fn	void GuiData::createSlider(tinyxml2::XMLElement *originNode);
	 *
	 * @brief	Creates a slider.
	 *
	 * @param [in,out]	originNode	takes in the current node in the xml file being read.
	 */
	void createSlider(tinyxml2::XMLElement *originNode);

	/**
	 * @fn	void GuiData::createTextBox(tinyxml2::XMLElement *originNode);
	 *
	 * @brief	Creates text box.
	 *
	 * @param [in,out]	originNode	takes in the current node in the xml file being read.
	 */
	void createTextBox(tinyxml2::XMLElement *originNode);

	/**
	 * @struct	ElementData
	 *
	 * @brief	Data that an element may have.
	 */
	struct ElementData
	{
		virtual ~ElementData() {};
		std::string type;

		std::string name;
		std::string fontPath;
		std::string meshPath;
		std::string shaderPath;
		std::string texturePath;

		bool visible;

		float positionX;
		float positionY;

		float width;
		float height;
	};

	/**
	 * @struct	Bar
	 *
	 * @brief	Extra data a bar may have.
	 */
	struct Bar : public ElementData
	{
		std::string texturePathBar;

		bool horizontal;

		glm::vec4 color;
	};

	/**
	 * @struct	Button
	 *
	 * @brief	Extra data a button may have.
	 */
	struct Button : public ElementData
	{
		std::string text;

		int atlasSize;
	};

	/**
	 * @struct	CheckBox
	 *
	 * @brief	Extra data a check box may have.
	 */
	struct CheckBox : public ElementData
	{
		bool defaultStatus;

		int atlasSizeX;
		int atlasSizeY;
	};

	/**
	 * @struct	Container
	 *
	 * @brief	Extra data a container may have.
	 */
	struct Container : public ElementData
	{
		bool visible;
		bool movable;
		bool destroyable;
		bool clickable;
		bool activatable;

		std::string shaderBoxes;
		std::string textureBoxes;
		std::vector<std::string> presetItems;

		float distanceBetweenBoxes;
		float widthBoxes;
		float heightBoxes;
		float borderTop;
		float borderBottom;
		float borderLeft;
		float borderRight;

		int atlasSizeX;
		int atlasSizeY;
		int numberOfBoxesX;
		int numberOfBoxesY;
	};

	/**
	 * @struct	DropDown
	 *
	 * @brief	Extra data a drop down may have.
	 */
	struct DropDown : public ElementData
	{
		std::string textureChoice;
		std::string defaultChoice;
		std::vector<std::string> choices;

		int atlasSize;
	};

	/**
	 * @struct	Panel
	 *
	 * @brief	Extra data a panel may have.
	 */
	struct Panel : public ElementData
	{
	};

	/**
	 * @struct	Slider
	 *
	 * @brief	Extra data a slider may have.
	 */
	struct Slider : public ElementData
	{
		float positionLeftX;
		float positionRightX;

		int minValue;
		int maxValue;
		int defaultValue;
		int interval;

		int atlasSize;
	};

	/**
	 * @struct	TextBox
	 *
	 * @brief	Extra data a text box may have.
	 */
	struct TextBox : public ElementData
	{
		std::string defaultValue;
		int maxLength;

		int atlasSize;
	};
	private: 
		std::vector<std::unique_ptr<ElementData>> elementDataVector;
};