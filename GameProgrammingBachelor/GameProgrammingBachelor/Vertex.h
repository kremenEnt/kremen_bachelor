#pragma once
#include <glm\glm.hpp>

/**
 * @class	Vertex
 *
 * @brief	A vertex.
 */
class Vertex
{
public:

	/**
	 * @fn	Vertex::Vertex(const glm::vec3& pos, const glm::vec2& texCoord, const glm::vec3& normal = glm::vec3(0, 0, 0))
	 *
	 * @brief	Constructor.
	 *
	 * @param	pos			The position.
	 * @param	texCoord	The texture coordinate.
	 * @param	normal  	The normal.
	 */
	Vertex(const glm::vec3& pos, const glm::vec2& texCoord, const glm::vec3& normal = glm::vec3(0, 0, 0))
	{
		this->pos = pos;
		this->texCoord = texCoord;
	}

	/**
	 * @fn	inline glm::vec3* Vertex::getPos()
	 *
	 * @brief	Gets the position.
	 *
	 * @return	null if it fails, else the position.
	 */
	inline glm::vec3* getPos()
	{
		return &pos;
	}

	/**
	 * @fn	inline glm::vec2* Vertex::getTexCoord()
	 *
	 * @brief	Gets tex coordinate.
	 *
	 * @return	null if it fails, else the tex coordinate.
	 */
	inline glm::vec2* getTexCoord()
	{
		return &texCoord;
	}

	/**
	 * @fn	inline glm::vec3* Vertex::getNormal()
	 *
	 * @brief	Gets the normal.
	 *
	 * @return	null if it fails, else the normal.
	 */
	inline glm::vec3* getNormal()
	{
		return &normal;
	}
protected:
private:
	glm::vec3 pos;
	glm::vec2 texCoord;
	glm::vec3 normal;
};