#include "ASDropping.h"
#include "Actor.h"
#include "PhysicsComponent.h"
#include "AnimationComponent.h"

int ASDropping::update()
{
	//printf("dropping\n");
	if (physicsComponent->getVelocity().x == 0 && physicsComponent->getVelocity().y == 0)
	{
		return AS_IDLE;
	}
	if (physicsComponent->getVelocity().y > 0)
	{
		return AS_RISING;
	}
	if (physicsComponent->getVelocity().y < 0)
	{
		return AS_DROPPING;
	}
	return AS_WALKING;
}

void ASDropping::enter()
{
	if (animationComponent != nullptr)
	{
		animationComponent->changeAnimation(FALLING);
	}
}

void ASDropping::leave()
{
}
