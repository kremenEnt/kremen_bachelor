#pragma once
#include "PhysicsComponent.h"
#include "Timer.h"
#include "Engine.h"
#include <Box2D\Box2D.h>
#include "Actor.h"

class PhysicsTimedBlock : public PhysicsComponent
{
public:
	
	PhysicsTimedBlock(ActorData::ComponentData* componentData)
		: PhysicsComponent(componentData)
	{
		fallTimer.setDuration(0.3f);
		respawnTimer.setDuration(5.f);
	}
	
	~PhysicsTimedBlock()
	{

	}
	
	void onCollision(int fixtureUserData, Actor* collidedActor) override
	{
		switch (fixtureUserData)
		{
		case CATEGORY_CLONE:
			fallTimer.start();

			break;
		default:
			break;
		}
	}
	
	void endCollision(int fixtureUserData, Actor* collidedActor) override
	{
		switch (fixtureUserData)
		{
		case CATEGORY_CLONE:
			fallTimer.stop();
			break;
		default:
			break;
		}
	}
	
	void update(float deltaTime) override
	{
		PhysicsComponent::update(deltaTime);
		if (!init)
		{
			init = true;
			originalPostition = dynamic_cast<PhysicsComponent*>(actor->getComponent("PhysicsComponent"))->getPosition();
		}
		if (fallTimer.hasEnded())
		{
			//body->SetGravityScale(1);
			//body->SetLinearVelocity(b2Vec2(0, -4));
			fallTimer.stop();
			respawnTimer.start();
			actor->setPosition(glm::vec3(-20000, -20000, -20000));
		}
		if (respawnTimer.hasEnded())
		{
			//	body->SetGravityScale(0);
			//body->SetLinearVelocity(b2Vec2(0, 0));
			actor->setPosition(originalPostition);
			respawnTimer.stop();
		}
	}
private:
	bool init = false;
	Timer fallTimer;
	Timer respawnTimer;

	glm::vec3 originalPostition;
};