#pragma once
#include "PhysicsComponent.h"
#include "Engine.h"
#include "Actor.h"

class PhysicsSpring : public PhysicsComponent
{
public:
	
	PhysicsSpring(ActorData::ComponentData* componentData)
		: PhysicsComponent(componentData)
	{

	}
	
	~PhysicsSpring()
	{

	}
	
	void onCollision(int fixtureUserData, Actor* collidedActor) override
	{
		switch (fixtureUserData)
		{
		case CATEGORY_CLONE:
			dynamic_cast<PhysicsComponent*>(collidedActor->getComponent("PhysicsComponent"))->applySpeed(glm::vec2(0, 10));
			break;
		default:
			break;
		}
	}
private:
};