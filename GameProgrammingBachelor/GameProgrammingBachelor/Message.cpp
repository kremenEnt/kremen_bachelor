#include "Message.h"

Message::Message(Message && msg) : senderType(msg.senderType)
{
	//Move values to our new object
	this->sender	= msg.sender;
	this->variables = std::move(msg.variables);
}

Message::~Message()
{
	this->sender		= NULL;

	if (this->variables.size() > 0)
	{
		//Clear content
		this->variables.erase(this->variables.begin(), this->variables.end());
	}
}

void* Message::getSender() const
{
	return this->sender;
}

const std::type_info& Message::getSenderType() const
{
	return this->senderType;
}

std::size_t Message::getVariableCount() const
{
	return this->variables.size();
}

const Variable& Message::getVariable(std::size_t index) const
{
	assert(index >= 0 && index < this->variables.size());

	return this->variables.at(index);
}