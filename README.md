# README #


### What is this repository for? ###

* Kremengine is a game engine written from scratch in C++14. It supports both 3D and 2D. This particular repo is for the implementation of a custom AI level generator.

### How do I get set up? ###

Dependencies:

* SDL2

* SDL_ttf

* SDL_mixer

* LUA

* GLEW

* GLM

* BOX2D

* BOOST

* libnoise

* Raknet

* mysqlcppconn

Setup:

The project files are set up for Visual Studio 2015.

To setup the project you need to have all of the above dependencies. These are included through the project by use of environment variables. Example: SDL_HOME : (PATH_TO_SDL). Which is included in the project as: $(SDL_HOME)

![Includes.png](https://bitbucket.org/repo/6y64q4/images/2596474930-Includes.png)
![Libs.png](https://bitbucket.org/repo/6y64q4/images/720680324-Libs.png)
![Dependencies.png](https://bitbucket.org/repo/6y64q4/images/23893047-Dependencies.png)

Some of the libraries are compiled specifically for Visual studio 2015. BOX2D, MYSQLCPPCONN, lua, BOOST, Raknet

All of these libraries are conveniently located [here](https://drive.google.com/folderview?id=0BxRwBWbJ587OX3RqTUJjQndReUU&usp=sharing).

### Who do I talk to? ###

Christer P. Somby E-Mail: christerpsomby@gmail.com Phone: +47 9385 1001 Skype: quadakachrister